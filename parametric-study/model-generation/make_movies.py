import numpy as np
import os
from PIL import Image
from fpdf import FPDF


directories = ["/home/cerecam/Benjamin_Alheit/simulations/PhD/suture-scale/dynamic/parametric-study/model-generation/models/shear-wave/h-0.8/t-0.2"]

variable_label = 'S'
value = 'Tresca'

# x_left = 0.4
# x_right = 0.7
# y_bottom = 0.18
# y_top = 0.95
x_left = 0.38
x_right = .7
y_bottom = 0.11
y_top = .87

for dir in directories:
    os.system("abaqus cae noGUI=export_frame_images.py -- " + ' '.join([dir, variable_label, value]))
    images_dir = dir + '/' + value + '-images'

    pdf = None
    paths = list(os.listdir(images_dir))
    paths.sort()

    for path in paths:
        print(path)
        full_path = os.path.join(images_dir, path)
        im = Image.open(full_path)
        im_dims = im.size
        crop_dims = (im_dims[0]*x_left, im_dims[1]*(1-y_top), im_dims[0]*x_right, im_dims[1]*(1-y_bottom))
        print(crop_dims)
        im2=im.crop(crop_dims)
        os.system('rm ' + full_path)
        im2.save(full_path)
        width, height = im2.size
        if pdf is None:
            # pdf = FPDF('P', 'mm', np.array(width,height))
            pdf = FPDF(unit="pt", format=(width, height))

        pdf.add_page()
        pdf.image(full_path, 0, 0, width, height)
    pdf.output(images_dir + "/movie.pdf", "F")
