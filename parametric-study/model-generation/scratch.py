import os
import signal
import psutil
import matplotlib.pyplot as plt
import numpy as np

x = np.array([1, 2, 2.5, 3])
x_p_f = np.array([1, 2, 3])
ex_x = np.vstack([x for i in range(x_p_f.size-1)]).T
i = 0
x_p = np.delete(x_p_f, i)
dif = ex_x - x_p
denom = x_p_f[i] - x_p

print(dif)
print(denom)
print(np.prod(dif/denom, axis=1))