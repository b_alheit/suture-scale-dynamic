import os
import shutil
import numpy as np
import socket
from multiprocessing import Process
import standard_job_management_server as jms
import explicit_job_management_server as ejms
import time

root_dir = os.getcwd() + '/models'
if not os.path.exists(root_dir):
    os.system("mkdir " + root_dir)

breathe = False
bite = False
direct_wave = False
shear_wave = True

breathe_submit = True
bite_submit = True
direct_wave_submit = True
shear_wave_submit = True

# os.system("python job_management_server.py &")

standard_job_server_process = Process(target=jms.start)
standard_job_server_process.start()

explicit_job_server_process = Process(target=ejms.start)
explicit_job_server_process.start()

s = socket.socket()             # Create a socket object
host = socket.gethostname()     # Get local machine name
port = 60000                    # Reserve a port for your service.

h_min = 0.0
# h_min = 0.9
h_max = 0.9
n_h = 5

t_min = 0.15
t_max = 0.3
# t_max = 0.15
n_t = 5

# h_vals = np.linspace(h_min, h_max, n_h)[3:-1]
# t_vals = np.linspace(t_min, t_max, n_t)[2:-2]
# h_vals = np.linspace(h_min, h_max, n_h)[0:1]
# t_vals = np.linspace(t_min, t_max, n_t)[-1:]


h_vals = np.linspace(h_min, h_max, n_h)
t_vals = np.linspace(t_min, t_max, n_t)

i_model = 1
n_models = n_h*n_t

time.sleep(5)
if breathe:
    models_dir = root_dir+"/breathe"
    port = 60000                    # Reserve a port for your service.


    if os.path.exists(models_dir):
        shutil.rmtree(models_dir)
    os.system("mkdir " + models_dir)
    for h in h_vals:
        for t in t_vals:
            print(i_model, " / ", n_models)
            model_dir_h = models_dir + "/h-" + str(h)
            if not os.path.exists(model_dir_h):
                os.system("mkdir " + model_dir_h)

            model_dir = model_dir_h + "/t-" + str(t)
            os.system("mkdir " + model_dir)

            command = "abaqus cae noGUI=create_model_breathe.py -- " + ' '.join([str(t), str(h), model_dir])
            # command = "abaqus cae noGUI=create_model_tooth.py -- " + ' '.join([str(t), str(h), model_dir])
            os.system(command)
            i_model += 1
            os.system("cp /home/cerecam/Benjamin_Alheit/simulations/PhD/suture-scale/dynamic/parametric-study/model-generation/umats.f " + model_dir + "/umats.f")
            if not os.path.exists(model_dir + "/Job-1.log") and breathe_submit:
            # if False:
                s = socket.socket()
                s.connect((host, port))
                s.send(bytes(model_dir, 'utf-8'))
                s.close()


if direct_wave:
    models_dir = root_dir+"/direct-wave"
    port = 60001                    # Reserve a port for your service.

    if os.path.exists(models_dir):
        shutil.rmtree(models_dir)
    os.system("mkdir " + models_dir)
    for h in h_vals:
        for t in t_vals:
            print(i_model, " / ", n_models)
            model_dir_h = models_dir + "/h-" + str(h)
            if not os.path.exists(model_dir_h):
                os.system("mkdir " + model_dir_h)

            model_dir = model_dir_h + "/t-" + str(t)
            os.system("mkdir " + model_dir)

            # command = "abaqus cae noGUI=create_model_breathe.py -- " + ' '.join([str(t), str(h), model_dir])
            # command = "abaqus cae noGUI=create_model_tooth.py -- " + ' '.join([str(t), str(h), model_dir])
            command = "abaqus cae noGUI=create_model_direct_wave.py -- " + ' '.join([str(t), str(h), model_dir])
            os.system(command)
            i_model += 1
            os.system("cp /home/cerecam/Benjamin_Alheit/simulations/PhD/suture-scale/dynamic/parametric-study/model-generation/vumats.f " + model_dir + "/vumats.f")
            if not os.path.exists(model_dir + "/Job-1.log") and direct_wave_submit:
                # if False:
                s = socket.socket()
                s.connect((host, port))
                s.send(bytes(model_dir, 'utf-8'))
                s.close()

if shear_wave:
    models_dir = root_dir+"/shear-wave"
    port = 60001                    # Reserve a port for your service.

    # if os.path.exists(models_dir):
    #     shutil.rmtree(models_dir)
    # os.system("mkdir " + models_dir)
    for h in h_vals:
        for t in t_vals:
            print(i_model, " / ", n_models)
            i_model += 1

            model_dir_h = models_dir + "/h-" + str(h)


            if not os.path.exists(model_dir_h):
                os.system("mkdir " + model_dir_h)

            model_dir = model_dir_h + "/t-" + str(t)


            if not os.path.exists(model_dir):
                os.system("mkdir " + model_dir)

            if not os.path.exists(model_dir + "/Job-1.odb"):

                # command = "abaqus cae noGUI=create_model_shear_wave.py -- " + ' '.join([str(t), str(h), model_dir])
                # os.system(command)
                # os.system("cp /home/cerecam/Benjamin_Alheit/simulations/PhD/suture-scale/dynamic/parametric-study/model-generation/vumats.f " + model_dir + "/vumats.f")

                if shear_wave_submit:
                    s = socket.socket()
                    s.connect((host, port))
                    s.send(bytes(model_dir, 'utf-8'))
                    s.close()


if bite:
    models_dir = root_dir+"/bite"
    port = 60001                    # Reserve a port for your service.

    if os.path.exists(models_dir):
        shutil.rmtree(models_dir)
    os.system("mkdir " + models_dir)
    for h in h_vals:
        for t in t_vals:
            print(i_model, " / ", n_models)
            model_dir_h = models_dir + "/h-" + str(h)
            if not os.path.exists(model_dir_h):
                os.system("mkdir " + model_dir_h)

            model_dir = model_dir_h + "/t-" + str(t)
            if not os.path.exists(model_dir):
                os.system("mkdir " + model_dir)

            command = "abaqus cae noGUI=create_model_tooth.py -- " + ' '.join([str(t), str(h), model_dir])
            os.system(command)
            i_model += 1
            # os.system("rm " + model_dir + "/vumats.f")
            os.system("cp /home/cerecam/Benjamin_Alheit/simulations/PhD/suture-scale/dynamic/parametric-study/model-generation/vumats.f " + model_dir + "/vumats.f")

            if not os.path.exists(model_dir + "/Job-1.log") and not os.path.exists(model_dir + "/Job-1.sta") and bite_submit:
            # if False:
                s = socket.socket()
                s.connect((host, port))
                s.send(bytes(model_dir, 'utf-8'))
                s.close()

standard_job_server_process.join()
explicit_job_server_process.join()
