from abaqus import *
from abaqusConstants import *
import __main__
from helpful_functions import *
import numpy as np
# import read_odb_utils
import os
import subprocess
import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import optimization
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior
# from model_creating_macros import *
# from post_processing_macros import *
# import scipy
import sys
# import matplotlib.pyplot as plt
import time
import os

# t_h = 1.4 / 2.
# c_thick = 0.35

dir = sys.argv[-1]
t_h = float(sys.argv[-2])
c_thick = float(sys.argv[-3])

Mdb()

mdb.saveAs(
    pathName=dir+'/model')
import os
os.chdir(dir)
    # r"/home/cerecam/Benjamin_Alheit/simulations/PhD/suture-scale/dynamic/parametric-study/model-generation/h-1.0/t-0.2")

t_h_old = 1.5 / 2.
c_thick_old = 0.2

t_w = 1.
# t_w_old = 1.
c_trans = 0.02

flat_width = t_w/2.
depth = t_w/2.

flat_length = 1.2*t_h_old + c_thick_old*1.2
# flat_length_old = 1.2*t_h + c_thick*1.2

# Part Names
bone_name = "bone"

a = 2
n_points = 150

# t_h = t_h / 2.
x_path = np.linspace(0, depth*1.05, n_points)
x_path -= (x_path[-1] - depth)/2.
y_path = t_h/2. * np.sin(2.*np.pi * x_path / t_w - np.pi/2 )
# y_path = t_h/2. * np.sin(2.*np.pi * x_path / t_w + np.pi/2 )

x_cut = np.linspace(0, flat_width*1.05, n_points)
x_cut -= (x_cut[-1] - flat_width)/2.
y_cut = t_h/2. * np.sin(2.*np.pi * x_cut / t_w - np.pi/2)

y_cut_func = lambda x: t_h/2. * np.sin(2.*np.pi * x / t_w - np.pi/2)

theta = lambda x: np.pi/2. - np.pi/4. * np.cos(2.*np.pi * x / t_w - np.pi/2)

y_cut_top_func = lambda x: y_cut_func(x) + c_thick/2 * np.sin(theta(x)) + c_trans/2. -  t_h/2.
y_cut_bottom_func = lambda x: y_cut_func(x) - c_thick/2 * np.sin(theta(x)) - c_trans/2. -  t_h/2.

y_cut_top = y_cut + c_thick/2 * np.sin(theta(x_cut)) + c_trans/2.
x_cut_top = x_cut - c_thick/2 * np.sign(np.cos(theta(x_cut))) * np.abs(np.cos(theta(x_cut))) ** a

y_cut_bottom = y_cut - c_thick/2 * np.sin(theta(x_cut)) - c_trans/2.
x_cut_bottom = x_cut + c_thick/2 * np.sign(np.cos(theta(x_cut))) * np.abs(np.cos(theta(x_cut))) ** a

y_cut_top -= t_h/2.
y_cut_bottom -= t_h/2.

curvature_angle = 30.

def AAA_automate_suture_interface_generation():
    AA_make_rve()
    complete_model_edit()
    # complete_model()
    make_shell_layers()
    AA_loading()
    # make_offset_spring_bcs()
    # make_tooth()
    # make_dynamic()
    apply_visc()
    make_suture_material()
    history_output()
    change_output_and_make_input_file()
    print >> sys.__stdout__, 'Saving model...'
    mdb.save()



def history_output():
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(step='load')
    regionDef=mdb.models['Model-1'].rootAssembly.allInstances['suture-interface-1'].sets['right-bone']
    mdb.models['Model-1'].HistoryOutputRequest(name='right-bone',
                                               createStepName='load', variables=('SENER', 'PENER', 'VENER', 'DMENER',
                                                                                 'ELKE', 'ELSE', 'ELPD', 'ELVD', 'EKEDEN', 'ECDDEN', 'EVDDEN', 'ALLCD',
                                                                                 'ALLKE', 'ALLPD', 'ALLSE', 'ALLVD', 'ETOTAL'), region=regionDef,
                                               sectionPoints=DEFAULT, rebar=EXCLUDE)
    mdb.models['Model-1'].historyOutputRequests['right-bone'].setValues(variables=(
        'ALLAE', 'ALLCD', 'ALLDC', 'ALLFD', 'ALLIE', 'ALLKE',
        'ALLSE', 'ALLVD', 'ALLWK', 'ALLCW', 'ALLMW', 'ALLPW',
        'ETOTAL'))
    mdb.models['Model-1'].HistoryOutputRequest(name='left-bone',
                                               objectToCopy=mdb.models['Model-1'].historyOutputRequests['right-bone'],
                                               toStepName='load')
    mdb.models['Model-1'].HistoryOutputRequest(name='suture',
                                               objectToCopy=mdb.models['Model-1'].historyOutputRequests['left-bone'],
                                               toStepName='load')
    regionDef=mdb.models['Model-1'].rootAssembly.allInstances['suture-interface-1'].sets['left-bone']
    mdb.models['Model-1'].historyOutputRequests['left-bone'].setValues(
        region=regionDef)
    regionDef=mdb.models['Model-1'].rootAssembly.allInstances['suture-interface-1'].sets['suture']
    mdb.models['Model-1'].historyOutputRequests['suture'].setValues(
        region=regionDef)



def make_suture_material():
    mdb.models['Model-1'].Material(name='VISCO_HGO')
    mdb.models['Model-1'].materials['VISCO_HGO'].Density(table=((1.25e-9, ), ))
    mdb.models['Model-1'].materials['VISCO_HGO'].Depvar(n=18)
    mdb.models['Model-1'].materials['VISCO_HGO'].UserMaterial(mechanicalConstants=(
        89.66, 1e-06, 10.94, 74.24, 0.33333333326, 0.8003, 0.7997, 0.5383,
        0.5386))
    # mdb.models['Model-1'].sections['skin'].setValues(material='VISCO_HGO',
    #                                                  thicknessType=UNIFORM, thickness=0.05, thicknessField='',
    #                                                  poissonDefinition=DEFAULT)
    mdb.models['Model-1'].sections['suture'].setValues(material='VISCO_HGO',
                                                       thickness=None)
def AA_make_rve():
    # Geometric values

    path = tuple(map(tuple, np.array([x_path, y_path]).T))
    cut_top = tuple(map(tuple, np.array([x_cut_top, y_cut_top]).T))
    cut_bottom = tuple(map(tuple, np.array([x_cut_bottom, y_cut_bottom]).T))

    apprx_size = 2 * flat_length
    make_block(bone_name, flat_length, flat_width, depth, apprx_size, b_points=False)
    make_block("coll-block-temp", flat_length, flat_width, depth, apprx_size, b_points=False)

    cut_bone(bone_name, path, cut_top, cut_bottom, flat_width, depth, False)
    cut_collagen("coll-int", "bone-2", bone_name, "coll-block-temp")
    merge_col_bone("merged-shell", "bone-2", "coll-int")
    clean_up_temps()
    remove_redundant(flat_width, depth)

def apply_visc():
    mdb.models['Model-1'].materials['bone'].Viscoelastic(domain=TIME, time=PRONY,
                                                         table=((0.0774, 0.0, 0.04434), (0.7288, 0.0, 1.036e-05)))
    mdb.models['Model-1'].materials['keratin'].Viscoelastic(domain=TIME,
                                                            time=PRONY, table=((0.3872, 0.0, 0.5974), (0.1586, 0.0, 189.1)))


def complete_model_edit():
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=ON,
                                                           engineeringFeatures=ON)
    session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
        referenceRepresentation=OFF)
    p = mdb.models['Model-1'].parts['merged-shell']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    mdb.models['Model-1'].Material(name='suture')
    mdb.models['Model-1'].materials['suture'].Elastic(table=((200.0, 0.49), ))
    mdb.models['Model-1'].Material(name='bone')
    mdb.models['Model-1'].materials['bone'].Elastic(table=((12000.0, 0.25), ))
    mdb.models['Model-1'].HomogeneousSolidSection(name='bone', material='bone',
                                                  thickness=None)
    mdb.models['Model-1'].HomogeneousSolidSection(name='suture', material='suture',
                                                  thickness=None)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((-flat_length*0.99, t_w/4., t_w/4.), ))
    region = p.Set(cells=cells, name='left-bone')
    p = mdb.models['Model-1'].parts['merged-shell']
    p.SectionAssignment(region=region, sectionName='bone', offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((flat_length*0.99, t_w/4., t_w/4.), ))
    region = p.Set(cells=cells, name='right-bone')
    p = mdb.models['Model-1'].parts['merged-shell']
    p.SectionAssignment(region=region, sectionName='bone', offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    # cells = c.findAt(((-0.713709, 0.0, 0.004837), ))
    cells = c.findAt(((0.0, 0.99*t_w/2., 0.000001), ))
    region = p.Set(cells=cells, name='suture')
    p = mdb.models['Model-1'].parts['merged-shell']
    p.SectionAssignment(region=region, sectionName='suture', offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=OFF,
                                                           engineeringFeatures=OFF, mesh=ON)
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshTechnique=ON)
    p = mdb.models['Model-1'].parts['merged-shell']
    # p.seedPart(size=25.0, deviationFactor=0.1, minSizeFactor=0.1)
    # p = mdb.models['Model-1'].parts['merged-shell']
    # p.seedPart(size=0.25, deviationFactor=0.1, minSizeFactor=0.1)
    p.seedPart(size=c_thick/2.5, deviationFactor=0.1, minSizeFactor=0.1)
    # p.seedPart(size=c_thick/2.5, deviationFactor=0.1, minSizeFactor=0.1)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    # pickedRegions = c.findAt(((-flat_length*0.99, t_w/4., t_w/4.), ), ((-0.953709, 0.0,
    #                                                          0.004837), ))
    pickedRegions = c
    p.setMeshControls(regions=pickedRegions, elemShape=TET, technique=FREE)
    elemType1 = mesh.ElemType(elemCode=C3D20R)
    elemType2 = mesh.ElemType(elemCode=C3D15)
    elemType3 = mesh.ElemType(elemCode=C3D10)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((0.10951, 0.5, 0.009666), ), ((-0.953709, 0.0, 0.004837), ))
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
                                                       elemType3))
    # p = mdb.models['Model-1'].parts['merged-shell']
    # c = p.cells
    # pickedRegions = c.findAt(((-0.713709, 0.0, 0.004837), ))
    # p.setMeshControls(regions=pickedRegions, elemShape=TET, technique=FREE)
    # elemType1 = mesh.ElemType(elemCode=C3D20R)
    # elemType2 = mesh.ElemType(elemCode=C3D15)
    # elemType3 = mesh.ElemType(elemCode=C3D10)
    # p = mdb.models['Model-1'].parts['merged-shell']
    # c = p.cells
    # cells = c.findAt(((-0.713709, 0.0, 0.004837), ))
    # pickedRegions =(cells, )
    # p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
    #                                                    elemType3))
    elemType1 = mesh.ElemType(elemCode=C3D20R, elemLibrary=STANDARD)
    elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
    elemType3 = mesh.ElemType(elemCode=C3D10, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((-flat_length*0.99, t_w/4., t_w/4.), ), ((flat_length*0.99, t_w/4., t_w/4.), ))
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
                                                       elemType3))
    elemType1 = mesh.ElemType(elemCode=C3D20R, elemLibrary=STANDARD)
    elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
    elemType3 = mesh.ElemType(elemCode=C3D10H, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((0.0, 0.99*t_w/2., 0.000001), ))
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
                                                       elemType3))
    p = mdb.models['Model-1'].parts['merged-shell']
    p.generateMesh(boundaryPreview=ON)
    p = mdb.models['Model-1'].parts['merged-shell']
    p.generateMesh()
    change_els()
    mdb.meshEditOptions.setValues(enableUndo=True, maxUndoCacheElements=0.5)
    p = mdb.models['Model-1'].parts['merged-shell']
    p.PartFromMesh(name='quater-protrusion-mesh', copySets=True)
    p1 = mdb.models['Model-1'].parts['quater-protrusion-mesh']
    session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    session.viewports['Viewport: 1'].partDisplay.setValues(mesh=OFF)
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshTechnique=OFF)
    session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
        referenceRepresentation=ON)
    p1 = mdb.models['Model-1'].parts['quater-protrusion-mesh']
    session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    p = mdb.models['Model-1'].Part(name='quater-protrusion-mesh-tr',
                                   objectToCopy=mdb.models['Model-1'].parts['quater-protrusion-mesh'],
                                   compressFeatureList=ON, mirrorPlane=XZPLANE)
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.regenerate()
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    a = mdb.models['Model-1'].rootAssembly
    del a.features['merged-shell-1']
    a1 = mdb.models['Model-1'].rootAssembly
    a1.DatumCsysByDefault(CARTESIAN)
    p = mdb.models['Model-1'].parts['quater-protrusion-mesh']
    a1.Instance(name='quater-protrusion-mesh-1', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['quater-protrusion-mesh-tr']
    a1.Instance(name='quater-protrusion-mesh-tr-1', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('quater-protrusion-mesh-tr-1', ), vector=(0.0, t_w,
                                                                         0.0))
    session.viewports['Viewport: 1'].setColor(globalTranslucency=True)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(viewCut=ON)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        activeCutName='Z-Plane', viewCut=ON)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        activeCutName='Y-Plane', viewCut=ON)
    session.viewports['Viewport: 1'].view.setValues(cameraPosition=(0.0947556,
                                                                    5.4808, 0.219183), cameraUpVector=(0, 0, 1))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=4.25056,
                                                    farPlane=5.71104, width=2.18397, height=1.07173, viewOffsetX=0.0126281,
                                                    viewOffsetY=0.0248999)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanMerge(name='protrusion-r', instances=(
        a1.instances['quater-protrusion-mesh-1'],
        a1.instances['quater-protrusion-mesh-tr-1'], ),
                                mergeNodes=BOUNDARY_ONLY, nodeMergingTolerance=1e-06, domain=MESH,
                                originalInstances=SUPPRESS)
    p = mdb.models['Model-1'].parts['quater-protrusion-mesh-tr']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    p1 = mdb.models['Model-1'].parts['protrusion-r']
    session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    p1 = mdb.models['Model-1'].parts['protrusion-r']
    session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    p = mdb.models['Model-1'].Part(name='protrusion-l',
                                   objectToCopy=mdb.models['Model-1'].parts['protrusion-r'],
                                   compressFeatureList=ON, mirrorPlane=XYPLANE)
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['protrusion-l']
    a1.Instance(name='protrusion-l-1', part=p, dependent=ON)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(viewCut=OFF)
    session.viewports['Viewport: 1'].setColor(globalTranslucency=False)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=4.32932,
                                                    farPlane=7.4652, width=3.44058, height=1.68838, cameraPosition=(
            5.87678, 0.996425, -0.0579049), cameraUpVector=(-0.425524, 0.884847,
                                                            -0.189674), cameraTarget=(0.0909824, 0.415645, -0.00662802))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=4.3027,
                                                    farPlane=7.40395, width=3.41942, height=1.678, cameraPosition=(3.02983,
                                                                                                                   3.24013, 4.19285), cameraUpVector=(-0.484804, 0.665029, -0.568068),
                                                    cameraTarget=(0.0513206, 0.446903, 0.0525907))
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('protrusion-l-1', ), vector=(0.0, 0.0, t_w))
    # a1.translate(instanceList=('protrusion-l-1', ), vector=(0.0, 0.0, t_w/2.))
    # a1.translate(instanceList=('protrusion-r-1', ), vector=(0.0, 0.0, t_w/2.))
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanMerge(name='protrusion', instances=(
        a1.instances['protrusion-l-1'], a1.instances['protrusion-r-1'], ),
                                mergeNodes=BOUNDARY_ONLY, nodeMergingTolerance=1e-06, domain=MESH,
                                originalInstances=SUPPRESS)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['protrusion']
    a1.Instance(name='protrusion-2', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['protrusion']
    a1.Instance(name='protrusion-3', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('protrusion-3', ), vector=(0.0, 0.0, t_w))
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('protrusion-2', ), vector=(0.0, 0.0, 2*t_w))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=2.84898,
                                                    farPlane=7.33513, width=3.32574, height=1.63203, viewOffsetX=-0.159776,
                                                    viewOffsetY=-0.014277)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanMerge(name='row', instances=(
        a1.instances['protrusion-2'], a1.instances['protrusion-3'],
        a1.instances['protrusion-1'], ), mergeNodes=BOUNDARY_ONLY,
                                nodeMergingTolerance=1e-06, domain=MESH, originalInstances=SUPPRESS)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['row']
    a1.Instance(name='row-2', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['row']
    a1.Instance(name='row-3', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('row-3', ), vector=(0.0, t_w, 0.0))
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('row-2', ), vector=(0.0, 2.*t_w, 0.0))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=4.88152,
                                                    farPlane=10.858, width=5.83451, height=2.86316, viewOffsetX=-0.0886086,
                                                    viewOffsetY=0.408133)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanMerge(name='suture-interface', instances=(
        a1.instances['row-1'], a1.instances['row-3'], a1.instances['row-2'], ),
                                mergeNodes=BOUNDARY_ONLY, nodeMergingTolerance=1e-06, domain=MESH,
                                originalInstances=SUPPRESS)
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=ON,
                                                           engineeringFeatures=ON)
    session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
        referenceRepresentation=OFF)
    p = mdb.models['Model-1'].parts['protrusion-l']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    p = mdb.models['Model-1'].parts['suture-interface']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    session.viewports['Viewport: 1'].enableMultipleColors()
    session.viewports['Viewport: 1'].setColor(initialColor='#BDBDBD')
    cmap=session.viewports['Viewport: 1'].colorMappings['Material']
    session.viewports['Viewport: 1'].setColor(colorMapping=cmap)
    session.viewports['Viewport: 1'].disableMultipleColors()


def complete_model():
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=ON,
                                                           engineeringFeatures=ON)
    session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
        referenceRepresentation=OFF)
    p = mdb.models['Model-1'].parts['merged-shell']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    mdb.models['Model-1'].Material(name='suture')
    mdb.models['Model-1'].materials['suture'].Elastic(table=((200.0, 0.49), ))
    mdb.models['Model-1'].Material(name='bone')
    mdb.models['Model-1'].materials['bone'].Elastic(table=((12000.0, 0.25), ))
    mdb.models['Model-1'].HomogeneousSolidSection(name='bone', material='bone',
                                                  thickness=None)
    mdb.models['Model-1'].HomogeneousSolidSection(name='suture', material='suture',
                                                  thickness=None)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((-flat_length*0.99, t_w/4., t_w/4.), ))
    region = p.Set(cells=cells, name='left-bone')
    p = mdb.models['Model-1'].parts['merged-shell']
    p.SectionAssignment(region=region, sectionName='bone', offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((flat_length*0.99, t_w/4., t_w/4.), ))
    region = p.Set(cells=cells, name='right-bone')
    p = mdb.models['Model-1'].parts['merged-shell']
    p.SectionAssignment(region=region, sectionName='bone', offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    # cells = c.findAt(((-0.713709, 0.0, 0.004837), ))
    cells = c.findAt(((0.0, 0.99*t_w/2., 0.000001), ))
    region = p.Set(cells=cells, name='suture')
    p = mdb.models['Model-1'].parts['merged-shell']
    p.SectionAssignment(region=region, sectionName='suture', offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=OFF,
                                                           engineeringFeatures=OFF, mesh=ON)
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshTechnique=ON)
    p = mdb.models['Model-1'].parts['merged-shell']
    # p.seedPart(size=25.0, deviationFactor=0.1, minSizeFactor=0.1)
    # p = mdb.models['Model-1'].parts['merged-shell']
    # p.seedPart(size=0.25, deviationFactor=0.1, minSizeFactor=0.1)
    p.seedPart(size=c_thick/2., deviationFactor=0.1, minSizeFactor=0.1)
    # p.seedPart(size=c_thick/2.5, deviationFactor=0.1, minSizeFactor=0.1)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    # pickedRegions = c.findAt(((-flat_length*0.99, t_w/4., t_w/4.), ), ((-0.953709, 0.0,
    #                                                          0.004837), ))
    pickedRegions = c
    p.setMeshControls(regions=pickedRegions, elemShape=TET, technique=FREE)
    elemType1 = mesh.ElemType(elemCode=C3D20R)
    elemType2 = mesh.ElemType(elemCode=C3D15)
    elemType3 = mesh.ElemType(elemCode=C3D10)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((0.10951, 0.5, 0.009666), ), ((-0.953709, 0.0, 0.004837), ))
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
                                                       elemType3))
    # p = mdb.models['Model-1'].parts['merged-shell']
    # c = p.cells
    # pickedRegions = c.findAt(((-0.713709, 0.0, 0.004837), ))
    # p.setMeshControls(regions=pickedRegions, elemShape=TET, technique=FREE)
    # elemType1 = mesh.ElemType(elemCode=C3D20R)
    # elemType2 = mesh.ElemType(elemCode=C3D15)
    # elemType3 = mesh.ElemType(elemCode=C3D10)
    # p = mdb.models['Model-1'].parts['merged-shell']
    # c = p.cells
    # cells = c.findAt(((-0.713709, 0.0, 0.004837), ))
    # pickedRegions =(cells, )
    # p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
    #                                                    elemType3))
    elemType1 = mesh.ElemType(elemCode=C3D20R, elemLibrary=STANDARD)
    elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
    elemType3 = mesh.ElemType(elemCode=C3D10, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((-flat_length*0.99, t_w/4., t_w/4.), ), ((flat_length*0.99, t_w/4., t_w/4.), ))
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
                                                       elemType3))
    elemType1 = mesh.ElemType(elemCode=C3D20R, elemLibrary=STANDARD)
    elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
    elemType3 = mesh.ElemType(elemCode=C3D10H, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((0.0, 0.99*t_w/2., 0.000001), ))
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
                                                       elemType3))
    p = mdb.models['Model-1'].parts['merged-shell']
    p.generateMesh(boundaryPreview=ON)
    p = mdb.models['Model-1'].parts['merged-shell']
    p.generateMesh()
    mdb.meshEditOptions.setValues(enableUndo=True, maxUndoCacheElements=0.5)
    p = mdb.models['Model-1'].parts['merged-shell']
    p.PartFromMesh(name='quater-protrusion-mesh', copySets=True)
    p1 = mdb.models['Model-1'].parts['quater-protrusion-mesh']
    session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    session.viewports['Viewport: 1'].partDisplay.setValues(mesh=OFF)
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshTechnique=OFF)
    session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
        referenceRepresentation=ON)
    p1 = mdb.models['Model-1'].parts['quater-protrusion-mesh']
    session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    p = mdb.models['Model-1'].Part(name='quater-protrusion-mesh-tr',
                                   objectToCopy=mdb.models['Model-1'].parts['quater-protrusion-mesh'],
                                   compressFeatureList=ON, mirrorPlane=XZPLANE)
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.regenerate()
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    a = mdb.models['Model-1'].rootAssembly
    del a.features['merged-shell-1']
    a1 = mdb.models['Model-1'].rootAssembly
    a1.DatumCsysByDefault(CARTESIAN)
    p = mdb.models['Model-1'].parts['quater-protrusion-mesh']
    a1.Instance(name='quater-protrusion-mesh-1', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['quater-protrusion-mesh-tr']
    a1.Instance(name='quater-protrusion-mesh-tr-1', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('quater-protrusion-mesh-tr-1', ), vector=(0.0, t_w,
                                                                         0.0))
    session.viewports['Viewport: 1'].setColor(globalTranslucency=True)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(viewCut=ON)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        activeCutName='Z-Plane', viewCut=ON)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        activeCutName='Y-Plane', viewCut=ON)
    session.viewports['Viewport: 1'].view.setValues(cameraPosition=(0.0947556,
                                                                    5.4808, 0.219183), cameraUpVector=(0, 0, 1))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=4.25056,
                                                    farPlane=5.71104, width=2.18397, height=1.07173, viewOffsetX=0.0126281,
                                                    viewOffsetY=0.0248999)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanMerge(name='protrusion-r', instances=(
        a1.instances['quater-protrusion-mesh-1'],
        a1.instances['quater-protrusion-mesh-tr-1'], ),
                                mergeNodes=BOUNDARY_ONLY, nodeMergingTolerance=1e-06, domain=MESH,
                                originalInstances=SUPPRESS)
    p = mdb.models['Model-1'].parts['quater-protrusion-mesh-tr']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    p1 = mdb.models['Model-1'].parts['protrusion-r']
    session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    p1 = mdb.models['Model-1'].parts['protrusion-r']
    session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    p = mdb.models['Model-1'].Part(name='protrusion-l',
                                   objectToCopy=mdb.models['Model-1'].parts['protrusion-r'],
                                   compressFeatureList=ON, mirrorPlane=XYPLANE)
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['protrusion-l']
    a1.Instance(name='protrusion-l-1', part=p, dependent=ON)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(viewCut=OFF)
    session.viewports['Viewport: 1'].setColor(globalTranslucency=False)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=4.32932,
                                                    farPlane=7.4652, width=3.44058, height=1.68838, cameraPosition=(
            5.87678, 0.996425, -0.0579049), cameraUpVector=(-0.425524, 0.884847,
                                                            -0.189674), cameraTarget=(0.0909824, 0.415645, -0.00662802))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=4.3027,
                                                    farPlane=7.40395, width=3.41942, height=1.678, cameraPosition=(3.02983,
                                                                                                                   3.24013, 4.19285), cameraUpVector=(-0.484804, 0.665029, -0.568068),
                                                    cameraTarget=(0.0513206, 0.446903, 0.0525907))
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('protrusion-l-1', ), vector=(0.0, 0.0, t_w))
    # a1.translate(instanceList=('protrusion-l-1', ), vector=(0.0, 0.0, t_w/2.))
    # a1.translate(instanceList=('protrusion-r-1', ), vector=(0.0, 0.0, t_w/2.))
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanMerge(name='protrusion', instances=(
        a1.instances['protrusion-l-1'], a1.instances['protrusion-r-1'], ),
                                mergeNodes=BOUNDARY_ONLY, nodeMergingTolerance=1e-06, domain=MESH,
                                originalInstances=SUPPRESS)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['protrusion']
    a1.Instance(name='protrusion-2', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['protrusion']
    a1.Instance(name='protrusion-3', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('protrusion-3', ), vector=(0.0, 0.0, t_w))
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('protrusion-2', ), vector=(0.0, 0.0, 2*t_w))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=2.84898,
                                                    farPlane=7.33513, width=3.32574, height=1.63203, viewOffsetX=-0.159776,
                                                    viewOffsetY=-0.014277)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanMerge(name='row', instances=(
        a1.instances['protrusion-2'], a1.instances['protrusion-3'],
        a1.instances['protrusion-1'], ), mergeNodes=BOUNDARY_ONLY,
                                nodeMergingTolerance=1e-06, domain=MESH, originalInstances=SUPPRESS)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['row']
    a1.Instance(name='row-2', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['row']
    a1.Instance(name='row-3', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('row-3', ), vector=(0.0, t_w, 0.0))
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('row-2', ), vector=(0.0, 2.*t_w, 0.0))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=4.88152,
                                                    farPlane=10.858, width=5.83451, height=2.86316, viewOffsetX=-0.0886086,
                                                    viewOffsetY=0.408133)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanMerge(name='suture-interface', instances=(
        a1.instances['row-1'], a1.instances['row-3'], a1.instances['row-2'], ),
                                mergeNodes=BOUNDARY_ONLY, nodeMergingTolerance=1e-06, domain=MESH,
                                originalInstances=SUPPRESS)
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=ON,
                                                           engineeringFeatures=ON)
    session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
        referenceRepresentation=OFF)
    p = mdb.models['Model-1'].parts['protrusion-l']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    p = mdb.models['Model-1'].parts['suture-interface']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    session.viewports['Viewport: 1'].enableMultipleColors()
    session.viewports['Viewport: 1'].setColor(initialColor='#BDBDBD')
    cmap=session.viewports['Viewport: 1'].colorMappings['Material']
    session.viewports['Viewport: 1'].setColor(colorMapping=cmap)
    session.viewports['Viewport: 1'].disableMultipleColors()


def make_shell_layers():
    session.viewports['Viewport: 1'].view.setValues(nearPlane=7.1644,
                                                    farPlane=13.1716, width=6.4661, height=3.17309, viewOffsetX=-0.316294,
                                                    viewOffsetY=0.167875)
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=OFF,
                                                           engineeringFeatures=OFF, mesh=ON)
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshTechnique=ON)
    p = mdb.models['Model-1'].parts['suture-interface']
    f = p.elements
    els = p.elements
    n_els = np.alen(els)

    face1Elements = els[0:0]
    face2Elements = els[0:0]
    face3Elements = els[0:0]
    face4Elements = els[0:0]

    face1ElementsBottom = els[0:0]
    face2ElementsBottom = els[0:0]
    face3ElementsBottom = els[0:0]
    face4ElementsBottom = els[0:0]

    face1ElementsLeft = els[0:0]
    face2ElementsLeft = els[0:0]
    face3ElementsLeft = els[0:0]
    face4ElementsLeft = els[0:0]

    nodes_per_element = np.alen(els[0].getNodes())

    pct = 0
    print >> sys.__stdout__,  "find part top elements"
    if nodes_per_element == 4:
        face1 = np.array([True, True, True, False])
        face2 = np.array([True, True, False, True])
        face3 = np.array([False, True, True, False])
        face4 = np.array([True, False, True, False])

        for el_index in range(np.alen(els)):
            el = els[el_index]
            nodes_on_surface = np.array([None] * 4)
            nodes = el.getNodes()
            for node_index in range(np.alen(nodes)):
                nodes_on_surface[node_index] = find_top(nodes[node_index].coordinates)
                # nodes_on_surface[node_index] = find_bottom(nodes[node_index].coordinates)

            if array_all_equal(nodes_on_surface, np.array([False]*4)):
                pass
            elif array_all_equal(nodes_on_surface, face1):
                # face1Elements.append(el)
                # face1Elements = face1Elements + [el]
                face1Elements = face1Elements + els[el_index:el_index+1]

            elif array_all_equal(nodes_on_surface, face2):
                # face2Elements.append(el)
                # face2Elements = face2Elements + [el]
                face2Elements = face2Elements + els[el_index:el_index+1]

            elif array_all_equal(nodes_on_surface, face3):
                # face3Elements.append(el)
                # face3Elements = face3Elements + [el]
                face3Elements = face3Elements + els[el_index:el_index+1]

            elif array_all_equal(nodes_on_surface, face4):
                # face4Elements.append(el)
                # face4Elements = face4Elements + [el]
                face4Elements = face4Elements + els[el_index:el_index+1]


    elif nodes_per_element == 10:
        face1 = np.array([True, True, True, False, True, True, True, False, False, False])
        face2 = np.array([True, True, False, True, True, False, False, True, True, False])
        face3 = np.array([False, True, True, True, False, True, False, False, True, True])
        face4 = np.array([True, False, True, True, False, False, True, True, False, True])


        print >> sys.__stdout__, ('Quadratic tets')
        for el_index in range(np.alen(els)):
            el = els[el_index]
            nodes_on_surface = np.array([None] * 10)
            nodes_on_bottom = np.array([None] * 10)
            nodes_on_left = np.array([None] * 10)
            nodes = el.getNodes()
            for node_index in range(np.alen(nodes)):
                nodes_on_surface[node_index] = find_top(nodes[node_index].coordinates)
                nodes_on_bottom[node_index] = find_bottom(nodes[node_index].coordinates)
                nodes_on_left[node_index] = find_left(nodes[node_index].coordinates)


            if (100 * el_index/n_els) - pct > 10:
                pct = (100 * el_index/n_els)
                print >> sys.__stdout__,  str(pct) + " %"

            # if nodes_on_surface[0]:
            #         print >> sys.__stdout__, (nodes_on_surface)

            # if nodes_on_surface[0]:
            #         print >> sys.__stdout__,  nodes_on_surface

            if array_all_equal(nodes_on_surface, np.array([False]*10)) and array_all_equal(nodes_on_bottom, np.array([False]*10)) and array_all_equal(nodes_on_left, np.array([False]*10)):
                pass
            elif array_all_equal(nodes_on_surface, face1):
                # face1Elements.append(el)
                face1Elements = face1Elements + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_surface, face2):
                # face2Elements.append(el)
                face2Elements = face2Elements + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_surface, face3):
                # face3Elements.append(el)
                face3Elements = face3Elements + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_surface, face4):
                # face4Elements.append(el)
                face4Elements = face4Elements + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_bottom, face1):
                # face1Elements.append(el)
                face1ElementsBottom = face1ElementsBottom + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_bottom, face2):
                # face2Elements.append(el)
                face2ElementsBottom = face2ElementsBottom + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_bottom, face3):
                # face3Elements.append(el)
                face3ElementsBottom = face3ElementsBottom + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_bottom, face4):
                # face4Elements.append(el)
                face4ElementsBottom = face4ElementsBottom + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_left, face1):
                # face1Elements.append(el)
                face1ElementsLeft = face1ElementsLeft + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_left, face2):
                # face2Elements.append(el)
                face2ElementsLeft = face2ElementsLeft + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_left, face3):
                # face3Elements.append(el)
                face3ElementsLeft = face3ElementsLeft + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_left, face4):
                # face4Elements.append(el)
                face4ElementsLeft = face4ElementsLeft + els[el_index:el_index+1]
    else:
        raise Exception("This is no bueno")

    # a = mdb.models['Model-1'].rootAssembly
    #
    # a.Surface(face1Elements=face1ElementsBottom, face2Elements=face2ElementsBottom,
    #           face3Elements=face3ElementsBottom, face4Elements=face4ElementsBottom,
    #           name='bottom')
    p.Surface(face1Elements=face1ElementsBottom, face2Elements=face2ElementsBottom,
              face3Elements=face3ElementsBottom, face4Elements=face4ElementsBottom,
              name='bottom')

    p.Surface(face1Elements=face1ElementsLeft, face2Elements=face2ElementsLeft,
              face3Elements=face3ElementsLeft, face4Elements=face4ElementsLeft,
              name='left-face')

    # region = a.Surface(face1Elements=face1Elements1, face2Elements=face2Elements1,
    #                    face3Elements=face3Elements1, face4Elements=face4Elements1,
    #                    name='left-face')

    p.generateMeshByOffset(region=regionToolset.Region(face1Elements=face1Elements,
                                                       face2Elements=face2Elements, face3Elements=face3Elements,
                                                       face4Elements=face4Elements), meshType=SHELL,
                           distanceBetweenLayers=0.0, numLayers=2, initialOffset=0.0,
                           shareNodes=True)
    p = mdb.models['Model-1'].parts['suture-interface']
    e = p.elements
    n_new_els = int(np.alen(p.elements) - n_els)
    # elements = e[74556:75354]
    elements = e[-int(n_new_els/2):]
    p.Set(elements=elements, name='OffsetElements-1-Layer-1')
    p = mdb.models['Model-1'].parts['suture-interface']
    e = p.elements
    # elements = e[75354:76152]
    elements = e[-n_new_els:-int(n_new_els/2)]
    p.Set(elements=elements, name='OffsetElements-1-Layer-2')
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=ON,
                                                           engineeringFeatures=ON, mesh=OFF)
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshTechnique=OFF)
    mdb.models['Model-1'].parts['suture-interface'].sets.changeKey(
        fromName='OffsetElements-1-Layer-1', toName='skin')
    mdb.models['Model-1'].parts['suture-interface'].sets.changeKey(
        fromName='OffsetElements-1-Layer-2', toName='keratin')
    mdb.models['Model-1'].Material(name='keratin')
    mdb.models['Model-1'].materials['keratin'].Elastic(table=((1000.0, 0.4), ))
    mdb.models['Model-1'].MembraneSection(name='skin', material='suture',
                                          thicknessType=UNIFORM, thickness=0.05, thicknessField='',
                                          poissonDefinition=DEFAULT)
    mdb.models['Model-1'].HomogeneousShellSection(name='keratin', preIntegrate=OFF,
                                                  material='keratin', thicknessType=UNIFORM, thickness=0.05,
                                                  thicknessField='', idealization=NO_IDEALIZATION,
                                                  poissonDefinition=DEFAULT, thicknessModulus=None, temperature=GRADIENT,
                                                  useDensity=OFF, integrationRule=SIMPSON, numIntPts=5)
    p = mdb.models['Model-1'].parts['suture-interface']
    region = p.sets['keratin']
    p = mdb.models['Model-1'].parts['suture-interface']
    p.SectionAssignment(region=region, sectionName='keratin', offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)
    p = mdb.models['Model-1'].parts['suture-interface']
    region = p.sets['skin']
    p = mdb.models['Model-1'].parts['suture-interface']
    p.SectionAssignment(region=region, sectionName='skin', offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=OFF,
                                                           engineeringFeatures=OFF, mesh=ON)
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshTechnique=ON)
    # elemType1 = mesh.ElemType(elemCode=M3D6, elemLibrary=STANDARD)
    elemType1 = mesh.ElemType(elemCode=M3D3, elemLibrary=EXPLICIT)
    p = mdb.models['Model-1'].parts['suture-interface']
    z1 = p.elements
    # elems1 = z1[74556:75354]
    elems1 = z1[-int(n_new_els/2):]
    pickedRegions =(elems1, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, ))
    # elemType1 = mesh.ElemType(elemCode=STRI65, elemLibrary=STANDARD)
    elemType1 = mesh.ElemType(elemCode=S3R, elemLibrary=EXPLICIT)
    p = mdb.models['Model-1'].parts['suture-interface']
    z1 = p.elements
    # elems1 = z1[75354:76152]
    elems1 = z1[-n_new_els:-int(n_new_els/2)]
    pickedRegions =(elems1, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, ))


def find_top(point):
    return float_equals(point[1], t_w*3.)

def find_bottom(point):
    return float_equals(point[1], 0.)

def find_left(point):
    return float_equals(point[0], -flat_length)


def AA_loading():
    a1 = mdb.models['Model-1'].rootAssembly
    a1.regenerate()
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        adaptiveMeshConstraints=ON)
    # mdb.models['Model-1'].StaticStep(name='load', previous='Initial',
    #                                  maxNumInc=1000, initialInc=0.2, maxInc=0.2, nlgeom=ON)
    mdb.models['Model-1'].ExplicitDynamicsStep(name='load', previous='Initial',
                                               timePeriod=(5.e-6)/4.)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(step='load')
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=ON, bcs=ON,
                                                               predefinedFields=ON, connectors=ON, adaptiveMeshConstraints=OFF)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(step='Initial')
    session.viewports['Viewport: 1'].view.setValues(nearPlane=1.59196,
                                                    farPlane=7.01805, width=3.22546, height=1.58282, viewOffsetX=-0.450735,
                                                    viewOffsetY=0.304575)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=1.94904,
                                                    farPlane=5.97774, width=3.94892, height=1.93784, cameraPosition=(
            4.0092, 1.01789, 2.27302), cameraUpVector=(-0.306446, 0.94131,
                                                       -0.141517), cameraTarget=(-1.27423, 0.991905, -0.155981),
                                                    viewOffsetX=-0.551834, viewOffsetY=0.37289)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=1.73642,
                                                    farPlane=6.19035, width=5.27196, height=2.58709, viewOffsetX=-0.407189,
                                                    viewOffsetY=0.342822)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=1.74928,
                                                    farPlane=6.29437, width=5.31101, height=2.60626, cameraPosition=(
            3.55939, 1.35106, -0.649232), cameraUpVector=(-0.431221, 0.899714,
                                                          0.0675479), cameraTarget=(-1.91966, 0.740556, 1.2008),
                                                    viewOffsetX=-0.410206, viewOffsetY=0.345362)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=1.74228,
                                                    farPlane=6.30138, width=5.28974, height=2.59582, cameraPosition=(
            3.52874, 1.42948, -0.714141), cameraUpVector=(-0.405515, 0.902575,
                                                          0.144623), cameraTarget=(-1.95031, 0.818973, 1.13589),
                                                    viewOffsetX=-0.408563, viewOffsetY=0.343979)
    a = mdb.models['Model-1'].rootAssembly
    xp_dir = (1.0, -np.sin(np.deg2rad(curvature_angle)), 0.0)
    xn_dir = (-1.0, -np.sin(np.deg2rad(curvature_angle)), 0.0)
    zp_dir = (0.0, -np.sin(np.deg2rad(curvature_angle)), 1.0)
    zn_dir = (0.0, -np.sin(np.deg2rad(curvature_angle)), -1.0)
    xpzn_dir = (1.0, -2**0.5*np.sin(np.deg2rad(curvature_angle)), -1.0)
    xpzp_dir = (1.0, -2**0.5*np.sin(np.deg2rad(curvature_angle)), 1.0)
    xnzp_dir = (-1.0, -2**0.5*np.sin(np.deg2rad(curvature_angle)), 1.0)
    xnzn_dir = (-1.0, -2**0.5*np.sin(np.deg2rad(curvature_angle)), -1.0)


    a.DatumCsysByThreePoints(name='xp', coordSysType=CARTESIAN, origin=(0.0, 0.0,0.0),
                             point1=xp_dir, point2=(0.0, 1.0, 0.0))
    a.DatumCsysByThreePoints(name='xn', coordSysType=CARTESIAN, origin=(0.0, 0.0,0.0),
                             point1=xn_dir, point2=(0.0, 1.0, 0.0))
    a.DatumCsysByThreePoints(name='zn', coordSysType=CARTESIAN, origin=(0.0, 0.0,0.0),
                             point1=zn_dir, point2=(0.0, 1.0, 0.0))
    a.DatumCsysByThreePoints(name='zp', coordSysType=CARTESIAN, origin=(0.0, 0.0,0.0),
                             point1=zp_dir, point2=(0.0, 1.0, 0.0))
    a.DatumCsysByThreePoints(name='xpzp', coordSysType=CARTESIAN, origin=(0.0, 0.0,0.0),
                             point1=xp_dir, point2=zp_dir)
    a.DatumCsysByThreePoints(name='xpzn', coordSysType=CARTESIAN, origin=(0.0, 0.0,0.0),
                             point1=xp_dir, point2=zn_dir)
    a.DatumCsysByThreePoints(name='xnzn', coordSysType=CARTESIAN, origin=(0.0, 0.0,0.0),
                             point1=xn_dir, point2=zn_dir)
    a.DatumCsysByThreePoints(name='xnzp', coordSysType=CARTESIAN, origin=(0.0, 0.0,0.0),
                             point1=xn_dir, point2=zp_dir)

    a = mdb.models['Model-1'].rootAssembly
    n1 = a.instances['suture-interface-1'].nodes
    #     print >> sys.__stdout__,  n1[0]
    bottom = n1[0:0]
    xp = n1[0:0]
    xn = n1[0:0]
    zp = n1[0:0]
    zn = n1[0:0]
    xpzp = n1[0:0]
    xpzn = n1[0:0]
    xnzn = n1[0:0]
    xnzp = n1[0:0]
    n_nodes = np.alen(n1)
    pct = 0
    print >> sys.__stdout__,  "Assining node sets"
    for node_index in range(n_nodes):
        if (100 * node_index/n_nodes) - pct > 10:
            pct = (100 * node_index/n_nodes)
            print >> sys.__stdout__,  str(pct) + " %"

        coords = n1[node_index].coordinates
        if float_equals(coords[0], flat_length):
            # if float_equals(coords[2], 0):
            #     xpzn = xpzn + n1[node_index:node_index+1]
            # elif float_equals(coords[2], 3.*t_w):
            #     xpzp = xpzp + n1[node_index:node_index+1]
            # else:
            xp = xp + n1[node_index:node_index+1]
        if float_equals(coords[0], -flat_length):
            # if float_equals(coords[2], 0):
            #     xnzn = xnzn + n1[node_index:node_index+1]
            # elif float_equals(coords[2], 3.*t_w):
            #     xnzp = xnzp + n1[node_index:node_index+1]
            # else:
            xn = xn+ n1[node_index:node_index+1]
        if float_equals(coords[2], 0):
            zn = zn + n1[node_index:node_index+1]
        if float_equals(coords[2], 3.*t_w):
            zp = zp + n1[node_index:node_index+1]
        if float_equals(coords[1], 0):
            bottom = bottom + n1[node_index:node_index+1]

    # a.Set(nodes=zp, name='zp')
    # a.Set(nodes=xn, name='xn')
    # a.Set(nodes=zn, name='zn')
    # a.Set(nodes=xpzn, name='xpzn')
    # a.Set(nodes=xpzp, name='xpzp')
    # a.Set(nodes=xnzp, name='xnzp')
    # a.Set(nodes=xnzn, name='xnzn')

    print >> sys.__stdout__,  "Node sets assigned"


    datums = mdb.models['Model-1'].rootAssembly.datums
    region = a.Set(nodes=xp, name='xp')
    # mdb.models['Model-1'].VelocityBC(name='xp', createStepName='Initial',
    #                                      region=region, v1=SET, v2=UNSET, v3=UNSET, vr1=UNSET, vr2=UNSET,
    #                                      vr3=UNSET, amplitude=UNSET, distributionType=UNIFORM, fieldName='',
    #                                      # localCsys=datums[get_datum_key_from_axis_1_and_2(datums, xp_dir)])
    #                                      # localCsys=datums[get_datum_key_from_axis(datums, direction1=xp_dir, direction3=[0., 0., 1.])])
    #                                      localCsys=datums[get_datum_key_from_axis_and_xy_plane_normal(datums, xp_dir, [0., 0., 1.])])

    region = a.Set(nodes=xn, name='xn')
    # mdb.models['Model-1'].VelocityBC(name='xn', createStepName='Initial',
    #                                      region=region, v1=SET, v2=UNSET, v3=UNSET, vr1=UNSET, vr2=UNSET,
    #                                      vr3=UNSET, amplitude=UNSET, distributionType=UNIFORM, fieldName='',
    #                                      # localCsys=datums[get_datum_key_from_axis_1_and_2(datums, xn_dir)])
    #                                      # localCsys=datums[get_datum_key_from_axis(datums, direction1=xn_dir, direction3=[0., 0., 1.])])
    #                                      localCsys=datums[get_datum_key_from_axis_and_xy_plane_normal(datums, xn_dir, [0., 0., 1.])])



    region = a.Set(nodes=zn, name='zn')
    # mdb.models['Model-1'].VelocityBC(name='zn', createStepName='load',
    #                                      region=region, v1=SET, v2=UNSET, v3=UNSET, vr1=UNSET, vr2=UNSET,
    #                                      vr3=UNSET, amplitude=UNSET, distributionType=UNIFORM, fieldName='',
    #                                      # localCsys=datums[get_datum_key_from_axis_1_and_2(datums, zn_dir)])
    #                                      # localCsys=datums[get_datum_key_from_axis(datums, direction1=zn_dir, direction3=[1., 0., 0.])])
    #                                      localCsys=datums[get_datum_key_from_axis_and_xy_plane_normal(datums, zn_dir, [1., 0., 0.])])
    mdb.models['Model-1'].DisplacementBC(name='zn', createStepName='load',
                                         region=region, u1=UNSET, u2=UNSET, u3=0.0, ur1=UNSET, ur2=UNSET,
                                         ur3=UNSET, amplitude=UNSET, fixed=OFF, distributionType=UNIFORM,
                                         fieldName='', localCsys=None)

    region = a.Set(nodes=zp, name='zp')
    # mdb.models['Model-1'].VelocityBC(name='zp', createStepName='Initial',
    #                                      region=region, v1=SET, v2=UNSET, v3=UNSET, vr1=UNSET, vr2=UNSET,
    #                                      vr3=UNSET, amplitude=UNSET, distributionType=UNIFORM, fieldName='',
    #                                      # localCsys=datums[get_datum_key_from_axis_1_and_2(datums, zp_dir)])
    #                                      # localCsys=datums[get_datum_key_from_axis(datums, direction1=zp_dir, direction3=[1., 0., 0.])])
    #                                      localCsys=datums[get_datum_key_from_axis_and_xy_plane_normal(datums, zp_dir, [1., 0., 0.])])
    mdb.models['Model-1'].DisplacementBC(name='zp', createStepName='load',
                                         region=region, u1=UNSET, u2=UNSET, u3=0.0, ur1=UNSET, ur2=UNSET,
                                         ur3=UNSET, amplitude=UNSET, fixed=OFF, distributionType=UNIFORM,
                                         fieldName='', localCsys=None)

    region = a.Set(nodes=xpzp, name='xpzp')
    # mdb.models['Model-1'].VelocityBC(name='xpzp', createStepName='Initial',
    #                                      region=region, v1=SET, v2=SET, v3=UNSET, vr1=UNSET, vr2=UNSET,
    #                                      vr3=UNSET, amplitude=UNSET, distributionType=UNIFORM, fieldName='',
    #                                      # localCsys=datums[get_datum_key_from_axis_1_and_2(datums, xpzp_dir)])
    #                                      localCsys=datums[get_datum_key_from_xy_plane_normal(datums, np.cross(xp_dir, zp_dir))])

    region = a.Set(nodes=xnzp, name='xnzp')
    # mdb.models['Model-1'].VelocityBC(name='xnzp', createStepName='Initial',
    #                                      region=region, v1=SET, v2=SET, v3=UNSET, vr1=UNSET, vr2=UNSET,
    #                                      vr3=UNSET, amplitude=UNSET, distributionType=UNIFORM, fieldName='',
    #                                      # localCsys=datums[get_datum_key_from_axis_1_and_2(datums, xnzp_dir)])
    #                                      localCsys=datums[get_datum_key_from_xy_plane_normal(datums, np.cross(xn_dir, zp_dir))])


    region = a.Set(nodes=xnzn, name='xnzn')
    # mdb.models['Model-1'].VelocityBC(name='xnzn', createStepName='Initial',
    #                                      region=region, v1=SET, v2=SET, v3=UNSET, vr1=UNSET, vr2=UNSET,
    #                                      vr3=UNSET, amplitude=UNSET, distributionType=UNIFORM, fieldName='',
    #                                      # localCsys=datums[get_datum_key_from_axis_1_and_2(datums, xnzn_dir)])
    #                                      localCsys=datums[get_datum_key_from_xy_plane_normal(datums, np.cross(xn_dir, zn_dir))])


    region = a.Set(nodes=xpzn, name='xpzn')
    # mdb.models['Model-1'].VelocityBC(name='xpzn', createStepName='Initial',
    #                                      region=region, v1=SET, v2=SET, v3=UNSET, vr1=UNSET, vr2=UNSET,
    #                                      vr3=UNSET, amplitude=UNSET, distributionType=UNIFORM, fieldName='',
    #                                      # localCsys=datums[get_datum_key_from_axis_1_and_2(datums, xpzn_dir)])
    #                                      localCsys=datums[get_datum_key_from_xy_plane_normal(datums, np.cross(xp_dir, zn_dir))])



    a = mdb.models['Model-1'].rootAssembly
    # region = a.instances['suture-interface-1'].surfaces['bottom']
    # mdb.models['Model-1'].Pressure(name='breath',
    #                                createStepName='load', region=region, distributionType=UNIFORM,
    #                                field='', magnitude=0.02, amplitude=UNSET)

    a.Set(nodes=bottom, name='bottom')
    mdb.models['Model-1'].materials['bone'].Density(table=((1.95e-09, ), ))
    mdb.models['Model-1'].materials['keratin'].Density(table=((9.4e-10, ), ))
    mdb.models['Model-1'].materials['suture'].Density(table=((1.25e-9, ), ))

    mdb.models['Model-1'].TabularAmplitude(name='Amp-1', timeSpan=STEP,
                                           smooth=SOLVER_DEFAULT, data=((0.0, 1.0), (1.25e-07, 1.0), (1.2525e-07,
                                                                                                      0.0), (1.25e-06, 0.0)))

    region = a.instances['suture-interface-1'].surfaces['left-face']

    # mdb.models['Model-1'].Pressure(name='stress-wave', createStepName='load',
    #                                region=region, distributionType=UNIFORM, field='', magnitude=100.0,
    #                                amplitude='Amp-1')
    mdb.models['Model-1'].SurfaceTraction(amplitude='Amp-1', createStepName='load',
                                          directionVector=((0.0, 0.0, 0.0), (0.0, 10.0, 0.0)), distributionType=
                                          UNIFORM, field='', localCsys=None, magnitude=100.0, name='shear-wave',
                                          region=region, resultant=ON)


def change_output_and_make_input_file():
    # mdb.models['Model-1'].fieldOutputRequests['F-Output-1'].setValues(variables=(
    #     'S', 'E', 'LE', 'UT', 'P'))
    # session.viewports['Viewport: 1'].assemblyDisplay.setValues(
    #     adaptiveMeshConstraints=OFF)
    mdb.models['Model-1'].fieldOutputRequests['F-Output-1'].setValues(variables=
                                                                      tuple("S,E,LE,U,V,A,RF,ELEDEN".split(",")))
    mdb.Job(name='Job-1', model='Model-1', description='', type=ANALYSIS,
            atTime=None, waitMinutes=0, waitHours=0, queue=None, memory=90,
            memoryUnits=PERCENTAGE, getMemoryFromAnalysis=True,
            explicitPrecision=SINGLE, nodalOutputPrecision=SINGLE, echoPrint=OFF,
            modelPrint=OFF, contactPrint=OFF, historyPrint=OFF, userSubroutine='',
            scratch='', resultsFormat=ODB, multiprocessingMode=DEFAULT, numCpus=4,
            numDomains=4, numGPUs=0)
    mdb.jobs['Job-1'].writeInput(consistencyChecking=OFF)




def make_block(part_name, flat_length, flat_width, depth, apprx_size, b_points=False):

    s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=apprx_size)
    g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=STANDALONE)
    s.rectangle(point1=(0.0, 0.0), point2=(-flat_length, flat_width))
    s.FixedConstraint(entity=g[5])
    s.FixedConstraint(entity=g[4])
    s.FixedConstraint(entity=g[3])
    s.FixedConstraint(entity=g[2])

    # s.CircleByCenterPerimeter(center=(0.0, -apprx_radius), point1=(-flat_length, 0.0))
    # s.CircleByCenterPerimeter(center=(0.0, -apprx_radius), point1=(-flat_length, flat_width))
    # s.ConstructionLine(point1=(0.0, -apprx_radius), point2=(-np.sin(theta), -apprx_radius+np.cos(theta)))
    # # s.VerticalConstraint(entity=g[8], addUndoState=False)
    # s.FixedConstraint(entity=g[8])
    # s.Line(point1=(-7.75, 3.375), point2=(-6.625, 2.0))
    # break_point(b_points, "1.1")
    # s.CoincidentConstraint(entity1=v[6], entity2=g[6])
    # break_point(b_points, "1.2")
    # s.CoincidentConstraint(entity1=v[5], entity2=g[7])
    # break_point(b_points, "1.3")
    # s.CoincidentConstraint(entity1=v[5], entity2=g[8])
    # break_point(b_points, "1")
    # s.CoincidentConstraint(entity1=v[6], entity2=g[8])
    # # s.PerpendicularConstraint(entity1=g[9], entity2=g[6])
    # # session.viewports['Viewport: 1'].view.setValues(nearPlane=24.1058,
    # #     farPlane=32.4627, width=30.1321, height=15.2269, cameraPosition=(
    # #     1.25103, -3.15898, 28.2843), cameraTarget=(1.25103, -3.15898, 0))
    # # s.PerpendicularConstraint(entity1=g[9], entity2=g[7])
    # break_point(b_points, "1")
    # s.autoTrimCurve(curve1=g[7], point1=(-(total_length+5), flat_width))
    # break_point(b_points, "2")
    # s.autoTrimCurve(curve1=g[6], point1=(-(total_length+5), -4.65462303161621))
    # break_point(b_points, "3")
    # # s.autoTrimCurve(curve1=g[12], point1=(-(total_length*0.99), -flat_width))
    # s.autoTrimCurve(curve1=g[12], point1=(-(total_length*0.999), np.sqrt(apprx_radius**2-(total_length*0.99)**2)-apprx_radius))
    # break_point(b_points, "4")
    # s.autoTrimCurve(curve1=g[13], point1=(-0.0168264961242676, 0.0302357912063599))
    # break_point(b_points, "5")
    # s.autoTrimCurve(curve1=g[15], point1=(0.90483856201172, 0.302357912063599))
    # break_point(b_points, "6")
    # s.autoTrimCurve(curve1=g[11], point1=(0.01, flat_width*1.05))
    # break_point(b_points, "7")
    # s.autoTrimCurve(curve1=g[4], point1=(-flat_length, flat_width/2.))
    # break_point(b_points, "8")


    p = mdb.models['Model-1'].Part(name=part_name, dimensionality=THREE_D,
                                   type=DEFORMABLE_BODY)
    p = mdb.models['Model-1'].parts[part_name]
    p.BaseSolidExtrude(sketch=s, depth=depth)
    s.unsetPrimaryObject()
    p = mdb.models['Model-1'].parts[part_name]
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    del mdb.models['Model-1'].sketches['__profile__']
    p = mdb.models['Model-1'].parts[part_name]
    f = p.faces
    # p.Mirror(mirrorPlane=f[4], keepOriginal=ON)
    p.Mirror(mirrorPlane=f.findAt(coordinates=(0.0, flat_width/2., depth/2.)), keepOriginal=ON)


def cut_bone(part_name, path, cut_top, cut_bottom, flat_width, depth, b_points):
    p = mdb.models['Model-1'].parts[part_name]
    f, e = p.faces, p.edges
    # t = p.MakeSketchTransform(sketchPlane=f[6], sketchUpEdge=e[21],
    t = p.MakeSketchTransform(sketchPlane=f.findAt(coordinates=(0.0, flat_width, depth/2.)), sketchUpEdge=e.findAt(coordinates=(0.0, flat_width, depth)),
                              sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, flat_width, 0.0))
    s = mdb.models['Model-1'].ConstrainedSketch(name='__sweep__', sheetSize=depth*3.,
                                                gridSpacing=depth/10., transform=t)
    g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=9.34539,
    #     farPlane=14.0878, width=4.72059, height=2.3855, cameraPosition=(
    #     -0.283552, 12, 2.13615), cameraTarget=(-0.283552, 2, 2.13615))
    s.Spline(points=path)
    break_point(b_points, "1")
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=9.21486,
    #     farPlane=14.2183, width=5.89536, height=2.97916, cameraPosition=(
    #     -0.318505, 12, 1.86906), cameraTarget=(-0.318505, 2, 1.86906))
    s.unsetPrimaryObject()
    s.unsetPrimaryObject()
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=33.3508,
    #     farPlane=54.7613, width=21.1243, height=10.1162, cameraPosition=(
    #     -25.6409, 29.4637, -19.2963), cameraUpVector=(0.914255, -0.00632095,
    #     -0.40509), cameraTarget=(-0.664081, -0.017676, 1.44713))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=33.8794,
    #     farPlane=54.1226, width=21.4591, height=10.2766, cameraPosition=(
    #     -24.3944, 34.1074, -12.5509), cameraUpVector=(0.869373, -0.0265511,
    #     -0.493442), cameraTarget=(-0.658399, 0.00349182, 1.47788))
    p = mdb.models['Model-1'].parts[part_name]
    f1, e1 = p.faces, p.edges
    # t = p.MakeSketchTransform(sketchPlane=f1[9], sketchUpEdge=e1[22],
    t = p.MakeSketchTransform(sketchPlane=f1.findAt(coordinates=(0.0, flat_width/2., 0.0)), sketchUpEdge=e1.findAt(coordinates=(0.0, flat_width, 0.0)),
                              sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, 0.0, 0.0))
    s1 = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
                                                 sheetSize=40.58, gridSpacing=1.01, transform=t)
    g1, v1, d1, c1 = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    s1.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=39.2367,
    #     farPlane=44.9334, width=9.72343, height=4.91363, cameraPosition=(
    #     -0.583853, 0.115076, -40.5851), cameraTarget=(-0.583853, 0.115076, 0))
    top_start = cut_top[0]
    top_end = cut_top[-1]
    bottom_start = cut_bottom[0]
    bottom_end = cut_bottom[-1]
    #
    break_point(b_points, "2")
    s1.Spline(points=cut_top)
    break_point(b_points, "3")
    s1.Spline(points=cut_bottom)
    break_point(b_points, "4")
    s1.Line(point1=top_start, point2=bottom_start)
    break_point(b_points, "5")
    s1.Line(point1=top_end, point2=bottom_end)
    break_point(b_points, "6")
    # s1.VerticalConstraint(entity=g1[15], addUndoState=False)
    break_point(b_points, "6.1")
    s1.unsetPrimaryObject()
    break_point(b_points, "6.2")
    p = mdb.models['Model-1'].parts[part_name]
    break_point(b_points, "6.3")
    f, e = p.faces, p.edges
    break_point(b_points, "7")

    p.CutSweep(pathPlane=f.findAt(coordinates=(0.0, flat_width, depth/2.)), pathUpEdge=e.findAt(coordinates=(0.0, flat_width, depth)),
               sketchPlane=f.findAt(coordinates=(0.0, flat_width/2., 0.0)),
               sketchUpEdge=e.findAt(coordinates=(0.0, flat_width, 0.0)),
               pathOrientation=RIGHT, path=s,
               sketchOrientation=RIGHT, profile=s1, profileNormal=ON)
    break_point(b_points, "8")
    del mdb.models['Model-1'].sketches['__sweep__']
    break_point(b_points, "9")
    del mdb.models['Model-1'].sketches['__profile__']
    break_point(b_points, "10")


def cut_collagen(part_name, bone_instance_name, bone_name, coll_block_name):
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts[bone_name]
    a1.Instance(name=bone_instance_name, part=p, dependent=ON)
    p = mdb.models['Model-1'].parts[coll_block_name]
    a1.Instance(name='coll-block-1', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanCut(name='coll-int',
                              instanceToBeCut=mdb.models['Model-1'].rootAssembly.instances['coll-block-1'],
                              cuttingInstances=(a1.instances[bone_instance_name], ),
                              originalInstances=SUPPRESS)



def merge_col_bone(merged_part_name, bone_instance, coll_name):
    coll_instance = coll_name +'-1'
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    a = mdb.models['Model-1'].rootAssembly
    a.features[bone_instance].resume()
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanMerge(name=merged_part_name, instances=(
        a1.instances[bone_instance], a1.instances[coll_instance], ),
                                keepIntersections=ON, originalInstances=SUPPRESS, domain=GEOMETRY)



def clean_up_temps():
    a = mdb.models['Model-1'].rootAssembly
    a.deleteFeatures(('bone-1', 'ker-temp-1', 'coll-temp-1', 'bone-2',
                      'coll-block-1', 'coll-int-1', ))

    del mdb.models['Model-1'].parts['bone']
    del mdb.models['Model-1'].parts['coll-block-temp']
    del mdb.models['Model-1'].parts['coll-int']
    # del mdb.models['Model-1'].parts['coll-temp']
    # del mdb.models['Model-1'].parts['ker-temp']


def remove_redundant(flat_width, depth):

    session.viewports['Viewport: 1'].view.setValues(nearPlane=26.3811,
                                                    farPlane=33.191, width=13.1709, height=6.56253, cameraPosition=(
            2.06433, -16.1075, 25.0359), cameraUpVector=(-0.0529636, 0.956564,
                                                         0.286671), cameraTarget=(0.126557, 1.32854, 1.05097))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=26.039,
                                                    farPlane=33.5417, width=13.0001, height=6.47744, cameraPosition=(
            2.97123, -25.9106, 12.6085), cameraUpVector=(-0.0176719, 0.675817,
                                                         0.736858), cameraTarget=(0.128686, 1.30552, 1.02179))
    p = mdb.models['Model-1'].parts['merged-shell']
    v1 = p.vertices
    p.RemoveRedundantEntities(vertexList=(v1.findAt(coordinates=(0.0, 0.0, 0.0)),
                                          v1.findAt(coordinates=(0.0, flat_width, 0.0)), v1.findAt(coordinates=(0.0, flat_width, depth)), v1.findAt(coordinates=(0.0, 0.0, depth))))
    mdb.models['Model-1'].parts['merged-shell'].checkGeometry()


def float_equals(a, b):
    a = np.asarray(a)
    b = np.asarray(b)
    if a.size != b.size:
        return False
    return np.linalg.norm(a-b) < 1.0e-5


def array_all_equal(a, b):
    if a.size != b.size:
        return False
    else:
        a = a.astype(np.int)
        b = b.astype(np.int)
        return np.sum(np.abs(a-b)) == 0


def totuple(a):
    try:
        return tuple(totuple(i) for i in a)
    except TypeError:
        return a

def get_datum_key_from_axis(datums, direction1=None, direction2=None, direction3=None):
    if direction1 is not None:
        direction1 = np.asarray(direction1)
        direction1 /= np.linalg.norm(direction1)
    if direction2 is not None:
        direction2 = np.asarray(direction2)
        direction2 /= np.linalg.norm(direction2)
    if direction3 is not None:
        direction3 = np.asarray(direction3)
        direction3 /= np.linalg.norm(direction3)
    keys = datums.keys()
    for key in keys:
        try:
            if (direction1 is None or float_equals(np.asarray(datums[key].axis1.direction), direction1) or float_equals(np.asarray(datums[key].axis1.direction), -direction1)) and \
                    (direction2 is None or float_equals(np.asarray(datums[key].axis2.direction), direction2) or float_equals(np.asarray(datums[key].axis2.direction), -direction2)) and \
                    (direction3 is None or float_equals(np.asarray(datums[key].axis3.direction), direction3) or float_equals(np.asarray(datums[key].axis3.direction), -direction3)):
                return key
        except:
            pass

def get_datum_key_from_axis_and_xy_plane_normal(datums, direction, normal):
    normal = np.asarray(normal)
    normal /= np.linalg.norm(normal)
    keys = datums.keys()
    direction = np.asarray(direction)
    direction /= np.linalg.norm(direction)
    for key in keys:
        try:
            datum_normal = np.cross(np.asarray(datums[key].axis1.direction), np.asarray(datums[key].axis2.direction))
            datum_normal /= np.linalg.norm(datum_normal)
            if (float_equals(datum_normal, normal) or float_equals(datum_normal, -normal)) and \
                    float_equals(np.asarray(datums[key].axis1.direction), direction):
                return key
        except:
            pass


def get_datum_key_from_xy_plane_normal(datums, normal):
    normal = np.asarray(normal)
    normal /= np.linalg.norm(normal)
    keys = datums.keys()
    for key in keys:
        try:
            datum_normal = np.cross(np.asarray(datums[key].axis1.direction), np.asarray(datums[key].axis2.direction))
            datum_normal /= np.linalg.norm(datum_normal)
            if float_equals(datum_normal, normal) or float_equals(datum_normal, -normal):
                return key
        except:
            pass

def make_dynamic():
    p = mdb.models['Model-1'].parts['suture-interface']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    mdb.models['Model-1'].materials['bone'].Density(table=((1.95e-09, ), ))
    mdb.models['Model-1'].materials['keratin'].Density(table=((9.4e-10, ), ))
    mdb.models['Model-1'].materials['suture'].Density(table=((1.25e-9, ), ))
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    mdb.models['Model-1'].ImplicitDynamicsStep(name='load', previous='Initial',
                                               maintainAttributes=True, timePeriod=2.0, maxNumInc=1000,
                                               initialInc=0.2, minInc=2e-05, nohaf=ON, nlgeom=ON)
    mdb.models['Model-1'].TimePoint(name='TimePoints-1', points=((0.4, ), (0.8, ),
                                                                 (1.2, ), (1.6, ), (2.0, )))
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(step='load')
    mdb.models['Model-1'].fieldOutputRequests['F-Output-1'].setValues(
        timePoint='TimePoints-1')
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=ON, bcs=ON,
                                                               predefinedFields=ON, connectors=ON, adaptiveMeshConstraints=OFF)
    mdb.models['Model-1'].SmoothStepAmplitude(name='Amp-1', timeSpan=STEP, data=((
                                                                                     0.0, 0.0), (2.0, 1.0)))
    mdb.models['Model-1'].loads['breath'].setValues(amplitude='Amp-1')

    session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=OFF, bcs=OFF,
                                                               predefinedFields=OFF, connectors=OFF)
    # mdb.jobs['Job-1'].writeInput(consistencyChecking=OFF)


def change_els():
    elemType1 = mesh.ElemType(elemCode=UNKNOWN_HEX, elemLibrary=EXPLICIT)
    elemType2 = mesh.ElemType(elemCode=UNKNOWN_WEDGE, elemLibrary=EXPLICIT)
    elemType3 = mesh.ElemType(elemCode=C3D10M, elemLibrary=EXPLICIT)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    # cells = c.getSequenceFromMask(mask=('[#7 ]', ), )
    pickedRegions =(c, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
                                                       elemType3))

def make_offset_spring_bcs():
    print >> sys.__stdout__, '***********'
    print >> sys.__stdout__, 'xp: 1/4'
    make_xn_offset('xp', np.array([1, 0, 0]))
    print >> sys.__stdout__, '***********'
    print >> sys.__stdout__, 'xn: 2/4'
    make_xn_offset('xn', np.array([-1, 0, 0]))
    print >> sys.__stdout__, '***********'
    print >> sys.__stdout__, 'zp: 3/4'
    make_xn_offset('zp', np.array([0, 0, 1]))
    print >> sys.__stdout__, '***********'
    print >> sys.__stdout__, 'zn: 4/4'
    make_xn_offset('zn', np.array([0, 0, -1]))
    print >> sys.__stdout__, '***********'
    print >> sys.__stdout__, 'zn: 5/8'
    make_xn_offset('xpzp', np.array([1./2**0.5, 0, 1./2**0.5]))
    print >> sys.__stdout__, '***********'
    print >> sys.__stdout__, 'zn: 6/8'
    make_xn_offset('xpzn', np.array([1./2**0.5, 0, -1./2**0.5]))
    print >> sys.__stdout__, '***********'
    print >> sys.__stdout__, 'zn: 7/8'
    make_xn_offset('xnzn', np.array([-1./2**0.5, 0, -1./2**0.5]))
    print >> sys.__stdout__, '***********'
    print >> sys.__stdout__, 'zn: 8/8'
    make_xn_offset('xnzp', np.array([-1./2**0.5, 0, 1./2**0.5]))


def make_xn_offset(set_name, offset):
    # set_name = 'xn'
    a = mdb.models['Model-1'].rootAssembly
    # set = a.sets['xn'].nodes
    set = a.sets[set_name].nodes
    # offset = np.array([-1, 0, 0])
    pair_list = []
    p = mdb.models['Model-1'].parts['suture-interface']
    n = p.nodes
    n_nodes = np.alen(n)
    n_new_nodes = np.alen(set)
    print >> sys.__stdout__, 'Making nodes... '

    make_offset_nodes(set, offset, pair_list)
    nodes = n[n_nodes:n_nodes+n_new_nodes]
    # p.Set(nodes=nodes, name='xn-offset')
    p.Set(nodes=nodes, name=set_name+'-offset')
    a = mdb.models['Model-1'].rootAssembly

    a.regenerate()
    a = mdb.models['Model-1'].rootAssembly
    node_set = a.instances['suture-interface-1'].sets[set_name+'-offset'].nodes
    set = a.sets[set_name].nodes

    pairs = []
    # a = mdb.models['Model-1'].rootAssembly

    # n1 = a.instances['suture-interface-1'].nodes
    p = mdb.models['Model-1'].parts['suture-interface']
    n = p.nodes
    n1 = n
    print >> sys.__stdout__, 'Assigning springs... '

    for i_node in range(n_new_nodes):
        # print >> sys.__stdout__, i_node + 1, ' / ', n_new_nodes

        # nodes1 = n1[75982:75983]
        pair = pair_list[i_node]
        # print >> sys.__stdout__, pair
        # node_label = set[i_node].label
        # offset_label = n_new_nodes + i_node
        node_label = pair[0]
        offset_label = pair[1]

        # nodes1 = n1[75982:75983]
        # nodes1 = n1[node_label-1:node_label]
        # nodes1 = set[i_node:i_node+1]
        nodes1 = find_node_by_label(set, node_label)
        # print >> sys.__stdout__, nodes1[0]
        rgn1pair0=regionToolset.Region(nodes=nodes1)
        # nodes1 = n1[offset_label-1:offset_label]
        nodes1 = find_node_by_label(node_set, offset_label)
        # print >> sys.__stdout__, nodes1[0]
        rgn2pair0=regionToolset.Region(nodes=nodes1)

        pairs.append((rgn1pair0, rgn2pair0))

    # region=((rgn1pair0, rgn2pair0), (rgn1pair1, rgn2pair1), )
    region=tuple(pairs)
    mdb.models['Model-1'].rootAssembly.engineeringFeatures.TwoPointSpringDashpot(
        name=set_name, regionPairs=region, axis=NODAL_LINE,
        springBehavior=ON, springStiffness=100.0, dashpotBehavior=OFF,
        dashpotCoefficient=0.0)
    region = a.instances['suture-interface-1'].sets[set_name+'-offset']
    #
    mdb.models['Model-1'].VelocityBC(name=set_name+'-o', createStepName='Initial',
                                     region=region, v1=0.0, v2=0.0, v3=0.0, vr1=0.0, vr2=0.0, vr3=0.0,
                                     amplitude=UNSET, localCsys=None, distributionType=UNIFORM,
                                     fieldName='')

    # print >> sys.__stdout__, pair_list

def find_node_by_label(set, label):
    for i_node in range(np.alen(set)):
        if set[i_node].label == label:
            return set[i_node: i_node+1]


def make_offset_nodes(node_set, offset, pair_list):
    p = mdb.models['Model-1'].parts['suture-interface']
    n_new_nodes = np.alen(node_set)
    a = mdb.models['Model-1'].rootAssembly
    for i_node in range(n_new_nodes):
        # print >> sys.__stdout__, i_node+1, ' / ', n_new_nodes
        node = node_set[i_node]
        coords = np.array(node.coordinates)
        new_node =  p.Node(coordinates=tuple((coords + offset).tolist()))
        pair_list.append([node.label, new_node.label])
        # print >> sys.__stdout__, new_node
        # print >> sys.__stdout__, p.nodes[new_node.label-1]
        # print >> sys.__stdout__, a.instances['suture-interface-1'].nodes[new_node.label-1]



def make_tooth():
    s1 = mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=5.0)
    g, v, d, c = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    s1.setPrimaryObject(option=STANDALONE)
    s1.ConstructionLine(point1=(0.0, -2.5), point2=(0.0, 2.5))
    s1.FixedConstraint(entity=g[2])
    s1.CircleByCenterPerimeter(center=(0.0, 0.0), point1=(1.0, 0.0))
    s1.CoincidentConstraint(entity1=v[0], entity2=g[2], addUndoState=False)
    s1.autoTrimCurve(curve1=g[3], point1=(-0.994202971458435, 0.0114964246749878))
    p = mdb.models['Model-1'].Part(name='tooth', dimensionality=THREE_D,
                                   type=DISCRETE_RIGID_SURFACE)
    p = mdb.models['Model-1'].parts['tooth']
    p.BaseShellRevolve(sketch=s1, angle=360.0, flipRevolveDirection=OFF)
    s1.unsetPrimaryObject()
    p = mdb.models['Model-1'].parts['tooth']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    del mdb.models['Model-1'].sketches['__profile__']
    p = mdb.models['Model-1'].parts['tooth']
    p.ReferencePoint(point=(0.0, 0.0, 0.0))
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=ON,
                                                           engineeringFeatures=ON)
    session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
        referenceRepresentation=OFF)
    p = mdb.models['Model-1'].parts['tooth']
    r = p.referencePoints
    refPoints=(r[2], )
    region=p.Set(referencePoints=refPoints, name='tooth-point')
    mdb.models['Model-1'].parts['tooth'].engineeringFeatures.PointMassInertia(
        name='head-mass', region=region, mass=0.005, i11=1.0, i22=1.0, i33=1.0,
        alpha=0.0, composite=0.0)
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=OFF, bcs=OFF,
                                                               predefinedFields=OFF, connectors=OFF)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['tooth']
    a1.Instance(name='tooth-1', part=p, dependent=ON)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=10.0854,
                                                    farPlane=15.7848, width=9.00128, height=4.88771, cameraPosition=(
            12.5686, 3.56902, 2.67034), cameraUpVector=(-0.516336, 0.850786,
                                                        -0.0977767), cameraTarget=(-0.0361874, 0.831703, 1.12945))
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('tooth-1', ), vector=(0.0, 4.05, 1.5))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=9.40714,
                                                    farPlane=15.7017, width=10.4853, height=5.69355, viewOffsetX=0.630615,
                                                    viewOffsetY=0.398041)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=7.92148,
                                                    farPlane=15.6627, width=8.82939, height=4.79437, cameraPosition=(
            9.33131, 4.32131, 8.6889), cameraUpVector=(-0.587835, 0.782942,
                                                       -0.203597), cameraTarget=(-0.222199, 0.604232, 0.710074),
                                                    viewOffsetX=0.531023, viewOffsetY=0.335179)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(interactions=ON,
                                                               constraints=ON, connectors=ON, engineeringFeatures=ON)
    mdb.models['Model-1'].ContactProperty('general')
    mdb.models['Model-1'].interactionProperties['general'].TangentialBehavior(
        formulation=PENALTY, directionality=ISOTROPIC, slipRateDependency=OFF,
        pressureDependency=OFF, temperatureDependency=OFF, dependencies=0,
        table=((0.2, ), ), shearStressLimit=None, maximumElasticSlip=FRACTION,
        fraction=0.005, elasticSlipStiffness=None)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(step='Initial')
    mdb.models['Model-1'].ContactExp(name='general', createStepName='Initial')
    mdb.models['Model-1'].interactions['general'].includedPairs.setValuesInStep(
        stepName='Initial', useAllstar=ON)
    mdb.models['Model-1'].interactions['general'].contactPropertyAssignments.appendInStep(
        stepName='Initial', assignments=((GLOBAL, SELF, 'general'), ))
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=ON, bcs=ON,
                                                               predefinedFields=ON, interactions=OFF, constraints=OFF,
                                                               engineeringFeatures=OFF)
    a = mdb.models['Model-1'].rootAssembly
    region = a.instances['tooth-1'].sets['tooth-point']
    mdb.models['Model-1'].VelocityBC(name='tooth-control',
                                     createStepName='Initial', region=region, v1=0.0, v2=UNSET, v3=0.0,
                                     vr1=0.0, vr2=0.0, vr3=0.0, amplitude=UNSET, localCsys=None,
                                     distributionType=UNIFORM, fieldName='')
    a = mdb.models['Model-1'].rootAssembly
    region = a.instances['tooth-1'].sets['tooth-point']
    mdb.models['Model-1'].Velocity(name='tooth_init_velocity', region=region,
                                   field='', distributionType=MAGNITUDE, velocity1=0.0, velocity2=-1000.0,
                                   velocity3=0.0, omega=0.0)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(step='load')
    a = mdb.models['Model-1'].rootAssembly
    region = a.instances['tooth-1'].sets['tooth-point']
    # mdb.models['Model-1'].BodyForce(name='bite-force', createStepName='load',
    #                                 region=region, comp2=-1000.0)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(mesh=ON, loads=OFF,
                                                               bcs=OFF, predefinedFields=OFF, connectors=OFF)
    session.viewports['Viewport: 1'].assemblyDisplay.meshOptions.setValues(
        meshTechnique=ON)
    p = mdb.models['Model-1'].parts['tooth']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=OFF,
                                                           engineeringFeatures=OFF, mesh=ON)
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshTechnique=ON)
    p = mdb.models['Model-1'].parts['tooth']
    f = p.faces
    pickedRegions = f.getSequenceFromMask(mask=('[#1 ]', ), )
    p.setMeshControls(regions=pickedRegions, elemShape=TRI)
    p = mdb.models['Model-1'].parts['tooth']
    p.seedPart(size=0.22, deviationFactor=0.1, minSizeFactor=0.1)
    p = mdb.models['Model-1'].parts['tooth']
    p.seedPart(size=0.1, deviationFactor=0.1, minSizeFactor=0.1)
    p = mdb.models['Model-1'].parts['tooth']
    p.generateMesh()
    elemType1 = mesh.ElemType(elemCode=R3D4, elemLibrary=EXPLICIT)
    elemType2 = mesh.ElemType(elemCode=R3D3, elemLibrary=EXPLICIT)
    p = mdb.models['Model-1'].parts['tooth']
    f = p.faces
    faces = f.getSequenceFromMask(mask=('[#1 ]', ), )
    pickedRegions =(faces, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2))



AAA_automate_suture_interface_generation()