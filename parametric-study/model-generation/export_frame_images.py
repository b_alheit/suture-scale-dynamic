from abaqus import *
from abaqusConstants import *
import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import optimization
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior
import sys
import os
import numpy as np
import shutil

dir = sys.argv[-3]
variable_label = sys.argv[-2]
value =sys.argv[-1]

variable_label = 'S'
value = 'Tresca'

images_dir = value + '-images'

i_dir = dir + '/' + images_dir
if os.path.exists(i_dir):
    shutil.rmtree(i_dir)
os.system("mkdir " + i_dir)

def make_movie():

    # session.mdbData.summary()
    o1 = session.openOdb(name=dir + '/Job-1.odb')
    n_frames = np.alen(o1.steps['load'].frames)
    session.viewports['Viewport: 1'].setValues(displayedObject=o1)
    session.viewports['Viewport: 1'].odbDisplay.setValues(viewCut=ON)
    session.viewports['Viewport: 1'].odbDisplay.setValues(viewCutNames=('Z-Plane',
                                                                        ), viewCut=ON)
    session.viewports['Viewport: 1'].odbDisplay.commonOptions.setValues(
        visibleEdges=FEATURE)
    leaf = dgo.LeafFromElementSets(elementSets=('SUTURE-INTERFACE-1.SKIN', ))
    session.viewports['Viewport: 1'].odbDisplay.displayGroup.remove(leaf=leaf)
    leaf = dgo.LeafFromElementSets(elementSets=('SUTURE-INTERFACE-1.KERATIN', ))
    session.viewports['Viewport: 1'].odbDisplay.displayGroup.remove(leaf=leaf)
    session.viewports['Viewport: 1'].odbDisplay.display.setValues(plotState=(CONTOURS_ON_DEF, ))
    session.viewports['Viewport: 1'].viewportAnnotationOptions.setValues(triad=OFF,
                                                                         legend=OFF, title=OFF, state=OFF, annotations=OFF, compass=OFF)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=7.0712,
                                                    farPlane=13.1372, width=5.30647, height=2.88142, viewOffsetX=0.124811,
                                                    viewOffsetY=0.229474)
    session.viewports['Viewport: 1'].odbDisplay.contourOptions.setValues(
        maxAutoCompute=OFF, maxValue=200, minAutoCompute=OFF, minValue=0)
    session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(
        variableLabel=variable_label, outputPosition=INTEGRATION_POINT, refinement=(
            INVARIANT, value), )
    session.pngOptions.setValues(imageSize=(2000*2, 1085*2))
    session.printOptions.setValues(reduceColors=False)
    for i in range(n_frames):
        print >> sys.__stdout__, i+1, ' / ', n_frames
        session.viewports['Viewport: 1'].odbDisplay.setFrame(step=0, frame=i)
        session.printToFile(
            fileName=dir + '/'+images_dir+'/f'+str(i).zfill(2),
            format=PNG, canvasObjects=(session.viewports['Viewport: 1'], ))
    #
    # session.viewports['Viewport: 1'].odbDisplay.setFrame(step=0, frame=0 )
    # session.printToFile(
    #     fileName=dir + '/tresca_images/f0',
    #     format=PNG, canvasObjects=(session.viewports['Viewport: 1'], ))
    #
    # session.viewports['Viewport: 1'].odbDisplay.setFrame(step=0, frame=1 )
    # session.printToFile(
    #     fileName=dir + '/tresca_images/f1',
    #     format=PNG, canvasObjects=(session.viewports['Viewport: 1'], ))
    # session.viewports['Viewport: 1'].odbDisplay.setFrame(step=0, frame=2 )
    # session.printToFile(
    #     fileName=dir + '/tresca_images/f2',
    #     format=PNG, canvasObjects=(session.viewports['Viewport: 1'], ))
    # session.viewports['Viewport: 1'].odbDisplay.setFrame(step=0, frame=3 )
    # session.printToFile(
    #     fileName=dir + '/tresca_images/f3',
    #     format=PNG, canvasObjects=(session.viewports['Viewport: 1'], ))


make_movie()