# import matplotlib.pyplot as plt
# import numpy as np
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
from scipy import interpolate
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib

SMALL_SIZE = 8
MEDIUM_SIZE = 11
# BIGGER_SIZE = 12
BIGGER_SIZE = 13

plt.rc('font', size=BIGGER_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}',
    r'\usepackage{amssymb}']

# h_min = 0.
# h_max = 1.
# n_h = 5
#
# t_min = 0.2
# t_max = 0.3
# n_t = 5
#
# h_vals = np.linspace(h_min, h_max, n_h)
# t_vals = np.linspace(t_min, t_max, n_t)
#
# X, Y = np.meshgrid(h_vals, t_vals)

n = 30

d = np.load('d_models.npy')
h_gird = np.load("h_grid.npy")
t_gird = np.load("t_grid.npy")

# f = interpolate.interp2d(h_gird, t_gird, d[:, :, -1], kind='quintic')
f = interpolate.interp2d(h_gird, t_gird, d[:, :, -1], kind='cubic')

h_fine = np.linspace(np.min(h_gird), np.max(h_gird), n)
t_fine = np.linspace(np.min(t_gird), np.max(t_gird), n)

H, T = np.meshgrid(h_fine, t_fine)
D = np.empty((n, n))
# for i in range(n):
#     D[:, i] = f(H[:, i], T[:, i])
D = f(h_fine, t_fine)
D *= 1e3
fig = plt.figure()
ax = fig.gca(projection='3d')

# X = np.arange(-5, 5, 0.25)
# Y = np.arange(-5, 5, 0.25)
# X, Y = np.meshgrid(X, Y)
# R = np.sqrt(X**2 + Y**2)
# Z = np.sin(R)

# Plot the surface.
# surf = ax.plot_surface(X, Y, d[:, :, -1], cmap=cm.coolwarm,
# surf = ax.plot_surface(h_gird, t_gird, d[:, :, -1], cmap=cm.coolwarm,
# surf = ax.plot_surface(H, T, D, cmap=cm.coolwarm,
surf1 = ax.plot_surface(T, H, D, cmap=cm.viridis,
                       linewidth=1, antialiased=False)
surf = ax.plot_wireframe(T, H, D, color="black")

sd = d[:, :, -1].copy()
sd = sd.flatten() * 1e3
sh = h_gird.flatten()
st = t_gird.flatten()

ax.scatter(st, sh, sd, color='black', edgecolors='white', alpha=1)
# divider = make_axes_locatable(ax)
# cax = divider.append_axes("right", size="5%", pad=0.05)
plt.xlabel("$t$ (mm)")
plt.ylabel("$h$ (mm)")
# plt.zlabel("d")
ax.set_zlabel('$d$ ($\mu$m)')
# cax = fig.add_axes([ax.get_position().x1+0.05, ax.get_position().y0, 0.02, ax.get_position().height])
# plt.colorbar(im, cax=cax)
plt.colorbar(surf1)



plt.tight_layout()
plt.show()