import numpy as np


class Interpolater:
    def __init__(self, x_grid, d):
        self.xp = x_grid
        self.m = self.xp.size
        self.d = d

    def int_an_x(self, x):
        N = np.empty(self.m)
        for i in range(self.m):
            xp = np.delete(self.xp, i)
            N[i] = np.prod((x-xp)/(self.xp[i]-xp))

        return np.tensordot(N, self.d)

    def int_x(self, x):
        N = np.empty([x.size, self.m])
        ex_x = np.vstack([x for i in range(self.xp.size - 1)]).T
        for i in range(self.m):
            xp = np.delete(self.xp, i)
            N[:, i] = np.prod((ex_x-xp)/(self.xp[i]-xp), axis=1)

        return np.matmul(N, self.d)

    def int_xy_arrays(self, x, y):
        m, n = x.size, y.size
        res = np.empty([m, n])
        for i in range(m):
            for j in range(n):
                res[i, j] = self.int_an_xy(x[i], y[j])

        return res

    def int_xy_grid(self, x_grid, y_grid):
        res = np.empty(x_grid.shape)
        for i in range(x_grid.shape[0]):
            for j in range(x_grid.shape[1]):
                res[i, j] = self.int_an_xy(x_grid[i, j], y_grid[i, j])

        return res
