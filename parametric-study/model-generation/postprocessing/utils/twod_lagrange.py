import numpy as np


class Interpolater:
    def __init__(self, x_grid, y_grid, d):
        self.xp = x_grid[:, 0]
        self.m = self.xp.size
        self.yq = y_grid[0, :]
        self.n = self.yq.size
        self.x_grid = x_grid
        self.y_grid = y_grid
        self.d = d

    def int_an_xy(self, x, y):
        N = np.empty([self.m, self.n])
        for i in range(self.m):
            for j in range(self.n):
                xp = np.delete(self.xp, i)
                yq = np.delete(self.yq, j)
                N[i, j] = np.prod((x-xp)/(self.xp[i]-xp)) * np.prod((y-yq)/(self.yq[j]-yq))

        return np.tensordot(N, self.d)

    def int_xy_arrays(self, x, y):
        m, n = x.size, y.size
        res = np.empty([m, n])
        for i in range(m):
            for j in range(n):
                res[i, j] = self.int_an_xy(x[i], y[j])

        return res

    def int_xy_grid(self, x_grid, y_grid):
        res = np.empty(x_grid.shape)
        for i in range(x_grid.shape[0]):
            for j in range(x_grid.shape[1]):
                res[i, j] = self.int_an_xy(x_grid[i, j], y_grid[i, j])

        return res
