import os
import numpy as np

root_dir = os.getcwd() + '/../../../models'


h_min = 0.0
# h_min = 0.9
h_max = 0.9
n_h = 5

t_min = 0.15
t_max = 0.3
# t_max = 0.15
n_t = 5

h_vals = np.linspace(h_min, h_max, n_h)
t_vals = np.linspace(t_min, t_max, n_t)

h_gird = np.empty([n_h, n_t])
t_gird = np.empty([n_h, n_t])

i_model = 1
n_models = n_h*n_t

n_inc_h = 200
n_frame_h = n_inc_h + 1

# max_en = np.empty([n_frame, n_h, n_t])
diss = np.empty([n_frame_h, n_h, n_t])
times = np.empty([n_frame_h, n_h, n_t])

models_dir = root_dir+"/shear-wave"


for ih in range(n_h):
    h = h_vals[ih]
    for it in range(n_t):
        t = t_vals[it]
        print(i_model, " / ", n_models)

        model_dir_h = models_dir + "/h-" + str(h)

        model_dir = model_dir_h + "/t-" + str(t)

        command = "abaqus cae noGUI=dissipation.py -- " + model_dir
        os.system(command)
        try:
            diss[:, ih, it] = np.load(model_dir + "/disp.npy")
            times[:, ih, it] = np.load(model_dir + "/t.npy")
        except:
            pass

        h_gird[ih, it] = h
        t_gird[ih, it] = t

        i_model += 1


np.save("h_gird", h_gird)
np.save("t_gird", t_gird)
np.save("diss", diss)
np.save("times", times)
