import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import optimization
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior
import numpy as np
import sys
import os

odb_dir = sys.argv[-1]
os.chdir(odb_dir)


def save_s_ens():
    o3 = session.openOdb(name=odb_dir + '/Job-1.odb', readOnly=False)
    t = np.array(o3.steps['load'].historyRegions['ElementSet SUTURE-INTERFACE-1.LEFT-BONE'].historyOutputs['ALLSE'].data)[:, 0]
    en_l = np.array(o3.steps['load'].historyRegions['ElementSet SUTURE-INTERFACE-1.LEFT-BONE'].historyOutputs['ALLSE'].data)[:, 1]
    en_r = np.array(o3.steps['load'].historyRegions['ElementSet SUTURE-INTERFACE-1.RIGHT-BONE'].historyOutputs['ALLSE'].data)[:, 1]
    print >> sys.__stdout__, en_l.size
    np.save('t', t)
    np.save('en-l', en_l)
    np.save('en-r', en_r)

    o3.close()


save_s_ens()
# o3 = session.openOdb(name=odb_dir + '/Job-1.odb', readOnly=False)
# print >> sys.__stdout__, o3
# print >> sys.__stdout__, np.array(o3.steps['load'].historyRegions['ElementSet SUTURE-INTERFACE-1.LEFT-BONE'].historyOutputs['ALLSE'].data)
# # print >> sys.__stdout__, o3.steps['load'].frames[-1].fieldOutputs['ESEDEN'].bulkDataBlocks.__sizeof__()
# # print >> sys.__stdout__, dir(o3.steps['load'].frames[-1].fieldOutputs['ESEDEN'].bulkDataBlocks.__sizeof__)
# # print >> sys.__stdout__, ""
# print >> sys.__stdout__, o3.steps['load'].frames[-1].fieldOutputs['ESEDEN'].bulkDataBlocks[1].data.max()
# print >> sys.__stdout__, ""
# print >> sys.__stdout__, dir(o3.steps['load'].frames[-1].fieldOutputs['ESEDEN'].bulkDataBlocks[2])
# print >> sys.__stdout__, ""
# print >> sys.__stdout__, dir(o3.steps['load'].frames[-1].fieldOutputs['UT'].bulkDataBlocks[0].data)
# print >> sys.__stdout__, o3.steps['load'].frames[-1].fieldOutputs['UT'].bulkDataBlocks[0].data.transpose()[1].mean()
# print >> sys.__stdout__, ""