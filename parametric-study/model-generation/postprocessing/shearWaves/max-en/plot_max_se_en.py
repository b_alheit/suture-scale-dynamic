from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
import postprocessing.utils.twod_lagrange as twoDL
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import matplotlib
from matplotlib.ticker import FormatStrFormatter

SMALL_SIZE = 8
MEDIUM_SIZE = 11
BIGGER_SIZE = 13
FONT = 16

plt.rc('font', size=FONT)          # controls default text sizes
plt.rc('axes', titlesize=FONT)     # fontsize of the axes title
plt.rc('axes', labelsize=FONT)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}',
    r'\usepackage{amssymb}']

m = 20
n = 20

h_gird = np.load("h_gird.npy")
t_gird = np.load("t_gird.npy")

d = np.load("max-se-den.npy").max(axis=0)

inter = twoDL.Interpolater(h_gird, t_gird, d)

h_fine = np.linspace(np.min(h_gird), np.max(h_gird), m)
t_fine = np.linspace(np.min(t_gird), np.max(t_gird), n)

H, T = np.meshgrid(h_fine, t_fine)
D = inter.int_xy_grid(H, T)
fig = plt.figure()
ax = fig.gca(projection='3d')

surf1 = ax.plot_surface(H, T, D, cmap=cm.viridis, linewidth=1, antialiased=False)
surf = ax.plot_wireframe(H, T, D, color="black")

sd = d[:, :].copy()
sd = sd.flatten()
sh = h_gird.flatten()
st = t_gird.flatten()

plt.title("Shear stress wave")
ax.scatter(sh, st, sd, color='black', edgecolors='white', alpha=1)
plt.xlabel("$h$ (mm)")
plt.ylabel("$t$ (mm)")
ax.set_zlabel("$\max_{\\boldsymbol{x}, t} \Psi^\infty$")
plt.colorbar(surf1)
ax.view_init(40, -110)
# plt.title("$a$ = "+a[ia])
plt.tight_layout()
ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
# plt.savefig("max-en-a"+a[ia]+".pdf")
plt.savefig("shear-max-se.pdf")


plt.show()