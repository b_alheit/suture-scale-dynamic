import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import optimization
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior
import numpy as np
import sys
import os

odb_dir = sys.argv[-1]
os.chdir(odb_dir)


def save_max_se_den():
    o3 = session.openOdb(name=odb_dir + '/Job-1.odb', readOnly=False)
    n_frames = len(o3.steps['load'].frames)
    max_en_den = np.zeros(n_frames)
    times = np.zeros(n_frames)
    for i_frame in range(n_frames):

        desc = o3.steps['load'].frames[i_frame].description
        times[i_frame] = float(desc[desc.find("=")+1:])

        max_en_den[i_frame] = np.array([block.data.max() for block in
                                        o3.steps['load'].frames[i_frame].fieldOutputs['ESEDEN'].bulkDataBlocks]).max()

    np.save('max-en-den', max_en_den)
    np.save('times', times)

    o3.close()


save_max_se_den()
# o3 = session.openOdb(name=odb_dir + '/Job-1.odb', readOnly=False)
# desc = o3.steps['load'].frames[4].description
# print >> sys.__stdout__, desc
# print >> sys.__stdout__, float(desc[desc.find("=")+1:])
# print >> sys.__stdout__, len(o3.steps['load'].frames)
# print >> sys.__stdout__, o3.steps['load'].frames[1].fieldOutputs['ESEDEN'].bulkDataBlocks[0].data.max()
# print >> sys.__stdout__,  np.array([block.data.max() for block in o3.steps['load'].frames[1].fieldOutputs['ESEDEN'].bulkDataBlocks]).max()
# print >> sys.__stdout__, o3.steps['load'].frames[i_frame].fieldOutputs['UT'].bulkDataBlocks[0].data.transpose()[1].mean()
# print >> sys.__stdout__, o3.steps['load'].historyRegions['Assembly ASSEMBLY'].historyOutputs['ALLVD']
# # print >> sys.__stdout__, o3.steps['load'].frames[-1].fieldOutputs['ESEDEN'].bulkDataBlocks.__sizeof__()
# # print >> sys.__stdout__, dir(o3.steps['load'].frames[-1].fieldOutputs['ESEDEN'].bulkDataBlocks.__sizeof__)
# # print >> sys.__stdout__, ""
# print >> sys.__stdout__, o3.steps['load'].frames[-1].fieldOutputs['ESEDEN'].bulkDataBlocks[1].data.max()
# print >> sys.__stdout__, ""
# print >> sys.__stdout__, dir(o3.steps['load'].frames[-1].fieldOutputs['ESEDEN'].bulkDataBlocks[2])
# print >> sys.__stdout__, ""
# print >> sys.__stdout__, dir(o3.steps['load'].frames[-1].fieldOutputs['UT'].bulkDataBlocks[0].data)
# print >> sys.__stdout__, o3.steps['load'].frames[-1].fieldOutputs['UT'].bulkDataBlocks[0].data.transpose()[1].mean()
# print >> sys.__stdout__, ""