import os
import numpy as np

root_dir = os.getcwd() + '/../../models'


# h_min = 0.0
h_min = 0.0
h_max = 1.5
n_h = 7
# n_h = 1

t_min = 0.15
t_max = 0.3
# t_max = 0.15
n_t = 5
# n_t = 1

a_min = 1
a_max = 3
n_a = 3
# n_a = 1

h_vals = np.linspace(h_min, h_max, n_h)
t_vals = np.linspace(t_min, t_max, n_t)
a_vals = np.linspace(a_min, a_max, n_a)

h_gird = np.empty([n_a, n_h, n_t])
a_gird = np.empty([n_a, n_h, n_t])
t_gird = np.empty([n_a, n_h, n_t])

i_model = 1
n_models = n_h*n_t*n_a

n_inc = 5
n_frame = n_inc+1

u2_dif = np.empty([n_frame, n_a, n_h, n_t])

models_dir = root_dir+"/quasi-bite"


for ia in range(n_a):
    a = a_vals[ia]
    for ih in range(n_h):
        h = h_vals[ih]
        for it in range(n_t):
            t = t_vals[it]
            print(i_model, " / ", n_models)
            model_dir_a = models_dir + "/a-" + str(a)

            model_dir_h = model_dir_a + "/h-" + str(h)

            model_dir = model_dir_h + "/t-" + str(t)

            command = "abaqus cae noGUI=get_u2_dif.py -- " + model_dir
            os.system(command)

            u2_dif[:, ia, ih, it] = np.load(model_dir + "/u2_dif.npy")

            h_gird[ia, ih, it] = h
            t_gird[ia, ih, it] = t
            a_gird[ia, ih, it] = a

            i_model += 1


np.save("h_gird", h_gird)
np.save("t_gird", t_gird)
np.save("a_gird", a_gird)
np.save("u2_dif", u2_dif)
