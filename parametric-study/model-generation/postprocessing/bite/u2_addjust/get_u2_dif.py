import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import optimization
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior
import numpy as np
import sys
import os

odb_dir = sys.argv[-1]
os.chdir(odb_dir)

def save_u2_dif():
    o3 = session.openOdb(name=odb_dir + '/Job-1.odb', readOnly=False)
    n_frames = np.alen(o3.steps['load'].frames)
    u2_dif = np.zeros(n_frames, dtype=float)
    left_bone = o3.rootAssembly.instances['SUTURE-INTERFACE-1'].nodeSets['LEFT-BONE']
    right_bone = o3.rootAssembly.instances['SUTURE-INTERFACE-1'].nodeSets['RIGHT-BONE']


    for i_frame in range(n_frames):
        # print >> sys.__stdout__, "frame ", i_frame, "/", n_frames
        ut_lbone = o3.steps['load'].frames[i_frame].fieldOutputs['UT'].getSubset(region=left_bone)
        # print >> sys.__stdout__, "got l max"
        ut_lbone = ut_lbone.bulkDataBlocks[0].data.transpose()[1].max()

        ut_rbone = o3.steps['load'].frames[i_frame].fieldOutputs['UT'].getSubset(region=right_bone)
        ut_rbone = ut_rbone.bulkDataBlocks[0].data.transpose()[1].max()
        # ut_rbone = o3.steps['load'].frames[-1].fieldOutputs['UT'].getSubset(region=right_bone).bulkDataBlocks[0].data.transpose()[1].max()
        # print >> sys.__stdout__, "got r max"
        u_bone = np.max([ut_lbone, ut_rbone])

        u2_mean = np.array([o3.steps['load'].frames[i_frame].fieldOutputs['UT'].bulkDataBlocks[0].data.transpose()[1].mean()])
        # print >> sys.__stdout__, "got avg"
        u2_dif[i_frame] = u2_mean - u_bone


    np.save('u2_dif', u2_dif)

    o3.close()

save_u2_dif()
# o3 = session.openOdb(name=odb_dir + '/Job-1.odb', readOnly=False)
# # print >> sys.__stdout__, type(o3.rootAssembly.instances['SUTURE-INTERFACE-1'].elementSets['LEFT-BONE'])
# left_bone = o3.rootAssembly.instances['SUTURE-INTERFACE-1'].nodeSets['LEFT-BONE']
# # print >> sys.__stdout__, ""
# print >> sys.__stdout__, left_bone
# # print >> sys.__stdout__, ""
# ut_lbone = o3.steps['load'].frames[-1].fieldOutputs['UT'].getSubset(region=left_bone)
# # print >> sys.__stdout__, dir(ut_lbone.bulkDataBlocks[0])
# print >> sys.__stdout__, ut_lbone.bulkDataBlocks[0].data.transpose()[1].max()
# # print >> sys.__stdout__, o3.steps['load'].frames[-1].fieldOutputs['UT'].getSubset(region=left_bone).bulkDataBlocks[0].data.transpose()[1].max()
# # print >> sys.__stdout__, ""
# # print >> sys.__stdout__, dir(o3.steps['load'].frames[-1].fieldOutputs['UT'].bulkDataBlocks[0].data)
# # print >> sys.__stdout__, o3.steps['load'].frames[-1].fieldOutputs['UT'].bulkDataBlocks[0].data.transpose()[1].mean()
# # print >> sys.__stdout__, ""