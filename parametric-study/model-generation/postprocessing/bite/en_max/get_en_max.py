import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import optimization
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior
import numpy as np
import sys
import os

odb_dir = sys.argv[-1]
os.chdir(odb_dir)

def save_u2_mean():
    o3 = session.openOdb(name=odb_dir + '/Job-1.odb', readOnly=False)
    n_frames = np.alen(o3.steps['load'].frames)
    max_en = np.empty(n_frames, dtype=float)

    for i_frame in range(n_frames):
        data_blocks = o3.steps['load'].frames[i_frame].fieldOutputs['ESEDEN'].bulkDataBlocks
        max_en[i_frame] = np.max([block.data.max() for block in data_blocks])

    np.save('max_en', max_en)

    o3.close()

save_u2_mean()
# o3 = session.openOdb(name=odb_dir + '/Job-1.odb', readOnly=False)
# # print >> sys.__stdout__, o3
# # print >> sys.__stdout__, ""
# # print >> sys.__stdout__, o3.steps['load'].frames[-1].fieldOutputs['ESEDEN'].bulkDataBlocks.__sizeof__()
# # print >> sys.__stdout__, dir(o3.steps['load'].frames[-1].fieldOutputs['ESEDEN'].bulkDataBlocks.__sizeof__)
# # print >> sys.__stdout__, ""
# print >> sys.__stdout__, o3.steps['load'].frames[-1].fieldOutputs['ESEDEN'].bulkDataBlocks[1].data.max()
# print >> sys.__stdout__, ""
# print >> sys.__stdout__, dir(o3.steps['load'].frames[-1].fieldOutputs['ESEDEN'].bulkDataBlocks[2])
# print >> sys.__stdout__, ""
# print >> sys.__stdout__, dir(o3.steps['load'].frames[-1].fieldOutputs['UT'].bulkDataBlocks[0].data)
# print >> sys.__stdout__, o3.steps['load'].frames[-1].fieldOutputs['UT'].bulkDataBlocks[0].data.transpose()[1].mean()
# print >> sys.__stdout__, ""