import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import optimization
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior
import numpy as np
import sys
import os

odb_dir = sys.argv[-1]
os.chdir(odb_dir)

def save_en():
    o3 = session.openOdb(name=odb_dir + '/Job-1.odb', readOnly=False)
    t = np.array(o3.steps['load'].historyRegions['ElementSet SUTURE-INTERFACE-1.LEFT-BONE'].historyOutputs['ALLSE'].data)[:, 0]
    lbone_en = np.array(o3.steps['load'].historyRegions['ElementSet SUTURE-INTERFACE-1.LEFT-BONE'].historyOutputs['ALLSE'].data)[:, 1]
    rbone_en = np.array(o3.steps['load'].historyRegions['ElementSet SUTURE-INTERFACE-1.RIGHT-BONE'].historyOutputs['ALLSE'].data)[:, 1]
    suture_en = np.array(o3.steps['load'].historyRegions['ElementSet SUTURE-INTERFACE-1.SUTURE'].historyOutputs['ALLSE'].data)[:, 1]

    np.save('t', t)
    np.save('en-l', lbone_en)
    np.save('en-r', rbone_en)
    np.save('en-s', suture_en)

    o3.close()

save_en()
# o3 = session.openOdb(name=odb_dir + '/Job-1.odb', readOnly=False)
# print >> sys.__stdout__, o3.steps['load'].historyRegions['ElementSet SUTURE-INTERFACE-1.LEFT-BONE'].historyOutputs
# print >> sys.__stdout__, ""
# print >> sys.__stdout__, np.array(o3.steps['load'].historyRegions['ElementSet SUTURE-INTERFACE-1.LEFT-BONE'].historyOutputs['ALLSE'].data)
# print >> sys.__stdout__, ""
# print >> sys.__stdout__, dir(o3.steps['load'].frames[-1].fieldOutputs['UT'].bulkDataBlocks[0].data)
# print >> sys.__stdout__, o3.steps['load'].frames[-1].fieldOutputs['UT'].bulkDataBlocks[0].data.transpose()[1].mean()
# print >> sys.__stdout__, ""