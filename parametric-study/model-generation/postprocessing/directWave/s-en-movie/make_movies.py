import numpy as np
import os
from PIL import Image
from fpdf import FPDF
import shutil


root_dir = os.getcwd()
models_dir = root_dir+"/../../../models/direct-wave"
# models_dir = root_dir+"/models/breathe"
# models_dir = "../../../models/breathe"


h_min = 0.0
# h_min = 0.9
h_max = 0.9
n_h = 2

t_min = 0.15
t_max = 0.3
# t_max = 0.15
n_t = 2

# h_vals = np.linspace(h_min, h_max, n_h)[3:-1]
# t_vals = np.linspace(t_min, t_max, n_t)[2:-2]
# h_vals = np.linspace(h_min, h_max, n_h)[0:1]
# t_vals = np.linspace(t_min, t_max, n_t)[-1:]


h_vals = np.linspace(h_min, h_max, n_h)
t_vals = np.linspace(t_min, t_max, n_t)

i_model = 1
n_models = n_h*n_t
# directories = ["/home/cerecam/Benjamin_Alheit/simulations/PhD/suture-scale/dynamic/parametric-study/model-generation/models/shear-wave/h-0.8/t-0.2"]
directories = []

variable_label = 'ESDEN'
value = 'a'

# x_left = 0.4
# x_right = 0.7
# y_bottom = 0.18
# y_top = 0.95
x_left = 0.38
x_right = .7
y_bottom = 0.11
y_top = .87

for i_h in range(h_vals.size):
    for i_t in range(t_vals.size):
        h = h_vals[i_h]
        t = t_vals[i_t]
        print(i_model, " / ", n_models)
        model_dir_h = models_dir + "/h-" + str(h)

        model_dir = model_dir_h + "/t-" + str(t)
        dir = model_dir
        # directories.append(model_dir)


# for dir in directories:
        os.system("abaqus cae noGUI=export_frame_images.py -- " + ' '.join([dir, variable_label, value]))
        images_dir = dir + '/' + '-images'

        # if os.path.exists(images_dir):
        #     shutil.rmtree(images_dir)
        # os.system("mkdir " + images_dir)

        pdf = None
        paths = list(os.listdir(images_dir))
        paths.sort()

        for path in paths:
            print(path)
            full_path = os.path.join(images_dir, path)
            im = Image.open(full_path)
            im_dims = im.size
            crop_dims = (im_dims[0]*x_left, im_dims[1]*(1-y_top), im_dims[0]*x_right, im_dims[1]*(1-y_bottom))
            print(crop_dims)
            im2=im.crop(crop_dims)
            os.system('rm ' + full_path)
            im2.save(full_path)
            width, height = im2.size
            if pdf is None:
                # pdf = FPDF('P', 'mm', np.array(width,height))
                pdf = FPDF(unit="pt", format=(width, height))

            pdf.add_page()
            pdf.image(full_path, 0, 0, width, height)
        pdf.output(images_dir + "/movie.pdf", "F")
        os.system("cp " +images_dir+ "/movie.pdf " + root_dir + "/movie-" + "h-" + str(h)+ "t-" + str(t)+".pdf")
