import numpy as np
import matplotlib.pyplot as plt
import postprocessing.utils.oned_lagrange as l1d

i_h = 0
i_t = 0

en = np.load("max-se-den.npy")[:, i_h, i_t]
val_time = np.load("times.npy")[:, i_h, i_t]

n = 500

interp = l1d.Interpolater(val_time, en)
t_smooth = np.linspace(val_time[0], val_time[-1], n)
en_smooth = interp.int_x(t_smooth)
# en_no_pre = en[en > np.max(en)/1e6]
# t_no_pre = val_time[en > np.max(en)/1e6]
# en_grad = np.gradient(en_no_pre, t_no_pre)
# t_max = t_no_pre[en_grad < 0][0]
# t_ref = t_no_pre[t_no_pre > t_max][np.argmin(en_no_pre[t_no_pre > t_max])]
# en_no_ref = en[val_time < t_ref]
# t_no_ref = val_time[val_time < t_ref]

# i_t_ref = val_time[en>np.max(en)/1e-6]
# befor_ref = val_time < 7.e-7
# en = en[befor_ref]
# val_time = val_time[befor_ref]
#
# plt.figure()
# plt.plot(t_no_pre, en_grad)
# plt.grid()
# plt.title("Grad"
#           )

plt.figure()
plt.plot(val_time, en)
# plt.plot(val_time, en, linewidth=0, marker='s')
# plt.plot(t_smooth, en_smooth)
plt.grid()
plt.title("Reflection ih="+str(i_h)+" it="+str(i_t))

# plt.figure()
# plt.plot(t_no_ref, en_no_ref)
# plt.grid()
# plt.title("No reflection ih="+str(i_h)+" it="+str(i_t))
plt.show()