from abaqus import *
from abaqusConstants import *
import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import optimization
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior
import sys
import os
import numpy as np
import shutil

dir = sys.argv[-3]
variable_label = sys.argv[-2]
value =sys.argv[-1]

variable_label = 'ESDEN'
value = ''
os.chdir(dir)

# images_dir = value + '-images'
#
# i_dir = dir + '/' + images_dir
# if os.path.exists(i_dir):
#     shutil.rmtree(i_dir)
# os.system("mkdir " + i_dir)

def get_data():

    # session.mdbData.summary()
    o1 = session.openOdb(name=dir + '/Job-1.odb')
    # print >> sys.__stdout__, o1
    # print >> sys.__stdout__, ""
    # print >> sys.__stdout__, o1.rootAssembly
    # print >> sys.__stdout__, ""
    # print >> sys.__stdout__, o1.sections
    # print >> sys.__stdout__, ""
    # print >> sys.__stdout__, o1.steps['load'].historyRegions['ElementSet SUTURE-INTERFACE-1.RIGHT-BONE'].historyOutputs['ALLSE']
    # print >> sys.__stdout__, ""
    right_bone_strain_energy = np.array(o1.steps['load'].historyRegions['ElementSet SUTURE-INTERFACE-1.RIGHT-BONE'].historyOutputs['ALLSE'].data)

    np.save('right-bone-strain-energy', right_bone_strain_energy)


get_data()