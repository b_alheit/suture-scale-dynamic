import numpy as np
import matplotlib.pyplot as plt

i_h = 0
i_t = 0

en = np.load("right-bone-energy.npy")[i_h, i_t, :]
val_time = np.load("val-times.npy")[i_h, i_t, :]
befor_ref = val_time < 7.e-7
en = en[befor_ref]
val_time = val_time[befor_ref]

plt.plot(val_time, en)
plt.grid()
plt.title("ih="+str(i_h)+" it="+str(i_t))
plt.show()