import numpy as np
import os



root_dir = os.getcwd()
models_dir = root_dir+"/../../../models/direct-wave"
# models_dir = root_dir+"/models/breathe"
# models_dir = "../../../models/breathe"


h_min = 0.0
# h_min = 0.9
h_max = 0.9
n_h = 5

t_min = 0.15
t_max = 0.3
# t_max = 0.15
n_t = 5

# h_vals = np.linspace(h_min, h_max, n_h)[3:-1]
# t_vals = np.linspace(t_min, t_max, n_t)[2:-2]
# h_vals = np.linspace(h_min, h_max, n_h)[0:1]
# t_vals = np.linspace(t_min, t_max, n_t)[-1:]


h_vals = np.linspace(h_min, h_max, n_h)
t_vals = np.linspace(t_min, t_max, n_t)

i_model = 1
n_models = n_h*n_t

variable_label = 'ESDEN'
value = 'a'

n_inc = 200
en = np.empty((n_h, n_t, n_inc+1))
val_times = np.empty((n_h, n_t, n_inc+1))
h_grid = np.empty((n_h, n_t))
t_grid = np.empty((n_h, n_t))

for i_h in range(h_vals.size):
    for i_t in range(t_vals.size):
        try:
            h = h_vals[i_h]
            t = t_vals[i_t]
            print(i_model, " / ", n_models)
            model_dir_h = models_dir + "/h-" + str(h)

            model_dir = model_dir_h + "/t-" + str(t)
            dir = model_dir

            os.system("abaqus cae noGUI=get_energy_history.py -- " + ' '.join([dir, variable_label, value]))
            i_model += 1
            val_times[i_h, i_t, :] = np.load(model_dir+'/right-bone-strain-energy.npy')[:, 0]
            en[i_h, i_t, :] = np.load(model_dir+'/right-bone-strain-energy.npy')[:, 1]
            h_grid[i_h, i_t] = h
            t_grid[i_h, i_t] = t
        except:
            pass


print('en')
print(en[:, :, -1])
np.save("right-bone-energy", en)
print('val times')
print(val_times[:, :, -1])
np.save("val-times", val_times)
print(h_grid)
print(t_grid)
np.save("h_grid", h_grid)
np.save("t_grid", t_grid)