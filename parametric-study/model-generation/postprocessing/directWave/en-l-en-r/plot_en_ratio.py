from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
import postprocessing.utils.twod_lagrange as twoDL
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import matplotlib
from matplotlib.ticker import FormatStrFormatter

SMALL_SIZE = 8
MEDIUM_SIZE = 11
BIGGER_SIZE = 13
FONT = 16

plt.rc('font', size=FONT)          # controls default text sizes
plt.rc('axes', titlesize=FONT)     # fontsize of the axes title
plt.rc('axes', labelsize=FONT)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}',
    r'\usepackage{amssymb}']

m = 20
n = 20

def get_r_max_filter(en , val_time):
    # en = np.load("en-l.npy")[:, i_h, i_t]
    # val_time = np.load("times.npy")[:, i_h, i_t]
    en_no_pre = en[en > np.max(en)/1e6]
    t_no_pre = val_time[en > np.max(en)/1e6]
    en_grad = np.gradient(en_no_pre, t_no_pre)
    t_max = t_no_pre[en_grad < 0][0]
    t_ref = t_no_pre[t_no_pre > t_max][np.argmin(en_no_pre[t_no_pre > t_max])]
    en_no_ref = en[val_time < t_ref]
    # t_no_ref = val_time[val_time < t_ref]
    return np.max(en_no_ref)

h_gird = np.load("h_gird.npy")
t_gird = np.load("t_gird.npy")
times = np.load("times.npy")
en_r = np.load('en-r.npy')
en_l = np.load('en-l.npy')
models_shape = h_gird.shape
en_max_l = en_l.max(axis=0)
# en_max_l_00 = e
en_max_r = np.empty(models_shape)
for i in range(models_shape[0]):
    for j in range(models_shape[1]):
        en_max_r[i, j] = get_r_max_filter(en_r[:, i, j], times[:, i, j])

d = en_max_l/en_max_r

inter = twoDL.Interpolater(h_gird, t_gird, d)

h_fine = np.linspace(np.min(h_gird), np.max(h_gird), m)
t_fine = np.linspace(np.min(t_gird), np.max(t_gird), n)

H, T = np.meshgrid(h_fine, t_fine)
D = inter.int_xy_grid(H, T)
D -= np.abs(D.min())/5e2
fig = plt.figure()
ax = fig.gca(projection='3d')

surf1 = ax.plot_surface(H, T, D, cmap=cm.viridis, linewidth=1, antialiased=False)
surf = ax.plot_wireframe(H, T, D, color="black")

sd = d[:, :].copy()
sd = sd.flatten()
sh = h_gird.flatten()
st = t_gird.flatten()

ax.scatter(sh, st, sd, color='black', edgecolors='white', alpha=1)
plt.xlabel("$h$ (mm)")
plt.ylabel("$t$ (mm)")
ax.set_zlabel("$r_d$")
plt.title("Direct stress wave")
plt.colorbar(surf1)
ax.view_init(32, 235)
ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
# plt.title("$a$ = "+a[ia])
plt.tight_layout()
plt.savefig("direct-stress-wave-rd.pdf")

plt.show()