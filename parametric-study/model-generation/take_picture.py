from abaqus import *
from abaqusConstants import *
import __main__
from helpful_functions import *
import numpy as np
# import read_odb_utils
import os
import subprocess
import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import optimization
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior
# from model_creating_macros import *
# from post_processing_macros import *
# import scipy
import sys
# import matplotlib.pyplot as plt
import time
import os

# t_h = 1.4 / 2.
# c_thick = 0.35

dir = sys.argv[-1]
out_path = sys.argv[-2]



def take_a_piccie_there_man():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    openMdb(
        pathName=dir + '/model.cae')
    session.viewports['Viewport: 1'].setValues(displayedObject=None)
    p = mdb.models['Model-1'].parts['merged-shell']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    p = mdb.models['Model-1'].parts['suture-interface']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    session.viewports['Viewport: 1'].enableMultipleColors()
    session.viewports['Viewport: 1'].setColor(initialColor='#BDBDBD')
    cmap=session.viewports['Viewport: 1'].colorMappings['Material']
    session.viewports['Viewport: 1'].setColor(colorMapping=cmap)
    session.viewports['Viewport: 1'].disableMultipleColors()
    session.viewports['Viewport: 1'].enableMultipleColors()
    session.viewports['Viewport: 1'].setColor(initialColor='#BDBDBD')
    cmap = session.viewports['Viewport: 1'].colorMappings['Material']
    cmap.updateOverrides(overrides={'keratin':(True, '#C80000', 'Default',
                                               '#C80000'), 'suture':(True, '#C80000', 'Default', '#C80000')})
    session.viewports['Viewport: 1'].setColor(colorMapping=cmap)
    session.viewports['Viewport: 1'].disableMultipleColors()
    session.viewports['Viewport: 1'].enableMultipleColors()
    session.viewports['Viewport: 1'].setColor(initialColor='#BDBDBD')
    cmap = session.viewports['Viewport: 1'].colorMappings['Material']
    cmap.updateOverrides(overrides={'bone':(True, '#FFD700', 'Default',
                                            '#FFD700')})
    session.viewports['Viewport: 1'].setColor(colorMapping=cmap)
    session.viewports['Viewport: 1'].disableMultipleColors()
    session.viewports['Viewport: 1'].enableMultipleColors()
    session.viewports['Viewport: 1'].setColor(initialColor='#BDBDBD')
    cmap = session.viewports['Viewport: 1'].colorMappings['Material']
    cmap.updateOverrides(overrides={'VISCO_HGO':(True, '#C80000', 'Default',
                                                 '#C80000'), 'keratin':(True, '#996337', 'Default', '#996337'),
                                    'suture': (True, '#C86D2B', 'Default', '#C86D2B')})
    session.viewports['Viewport: 1'].setColor(colorMapping=cmap)
    session.viewports['Viewport: 1'].disableMultipleColors()
    session.viewports['Viewport: 1'].enableMultipleColors()
    session.viewports['Viewport: 1'].setColor(initialColor='#BDBDBD')
    cmap = session.viewports['Viewport: 1'].colorMappings['Material']
    session.viewports['Viewport: 1'].setColor(colorMapping=cmap)
    session.viewports['Viewport: 1'].disableMultipleColors()
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshVisibleEdges=FREE)
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshVisibleEdges=FEATURE)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=7.0845,
                                                    farPlane=13.2076, width=6.5272, height=3.39323, cameraPosition=(
            4.99792, 4.51456, 9.79995), cameraUpVector=(-0.39854, 0.78935,
                                                        -0.467003))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=7.12553,
                                                    farPlane=13.1666, width=6.565, height=3.41288, cameraPosition=(4.99792,
                                                                                                                   4.51456, 9.79995), cameraUpVector=(-0.322743, 0.794293, -0.514719),
                                                    cameraTarget=(0.0226488, 1.40297, 1.57439))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=7.34034,
                                                    farPlane=12.9774, width=6.76291, height=3.51577, cameraPosition=(
            2.44516, 3.75943, 11.0986), cameraUpVector=(-0.135451, 0.8389,
                                                        -0.527162), cameraTarget=(0.0121191, 1.39986, 1.57975))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=7.24107,
                                                    farPlane=13.0632, width=6.67145, height=3.46822, cameraPosition=(
            2.5752, 4.31913, 10.9075), cameraUpVector=(-0.176103, 0.806273,
                                                       -0.564722), cameraTarget=(0.0128183, 1.40287, 1.57872))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=7.25198,
                                                    farPlane=13.0524, width=6.6815, height=3.47344, cameraPosition=(2.5752,
                                                                                                                    4.31913, 10.9075), cameraUpVector=(-0.150075, 0.806458, -0.571929),
                                                    cameraTarget=(0.0128183, 1.40287, 1.57872))
    session.viewports['Viewport: 1'].enableMultipleColors()
    session.viewports['Viewport: 1'].setColor(initialColor='#BDBDBD')
    cmap = session.viewports['Viewport: 1'].colorMappings['Material']
    cmap.updateOverrides(overrides={'bone':(True, '#E5EC00', 'Default',
                                            '#E5EC00')})
    session.viewports['Viewport: 1'].setColor(colorMapping=cmap)
    session.viewports['Viewport: 1'].disableMultipleColors()
    session.viewports['Viewport: 1'].enableMultipleColors()
    session.viewports['Viewport: 1'].setColor(initialColor='#BDBDBD')
    cmap = session.viewports['Viewport: 1'].colorMappings['Material']
    cmap.updateOverrides(overrides={'bone':(True, '#EEEE00', 'Default',
                                            '#EEEE00')})
    session.viewports['Viewport: 1'].setColor(colorMapping=cmap)
    session.viewports['Viewport: 1'].disableMultipleColors()
    session.viewports['Viewport: 1'].enableMultipleColors()
    session.viewports['Viewport: 1'].setColor(initialColor='#BDBDBD')
    cmap = session.viewports['Viewport: 1'].colorMappings['Material']
    session.viewports['Viewport: 1'].setColor(colorMapping=cmap)
    session.viewports['Viewport: 1'].disableMultipleColors()
    session.viewports['Viewport: 1'].view.setValues(nearPlane=7.30816,
                                                    farPlane=12.9962, width=6.08634, height=3.16404, viewOffsetX=-0.04243,
                                                    viewOffsetY=0.00164998)
    session.viewports['Viewport: 1'].viewportAnnotationOptions.setValues(triad=OFF,
                                                                         legend=OFF, title=OFF, state=OFF, annotations=OFF, compass=OFF)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=7.32023,
                                                    farPlane=12.9841, width=6.09639, height=3.16927, cameraPosition=(
            2.57517, 4.31981, 10.9073), cameraUpVector=(-0.16104, 0.806471,
                                                        -0.568921), cameraTarget=(0.0127914, 1.40355, 1.57851),
                                                    viewOffsetX=-0.0425001, viewOffsetY=0.0016527)
    session.viewports['Viewport: 1'].lightOptions.lights[0].setValues(latitude=46)
    session.viewports['Viewport: 1'].lightOptions.lights[0].setValues(latitude=35)
    session.viewports['Viewport: 1'].lightOptions.lights[0].setValues(latitude=10)
    session.viewports['Viewport: 1'].lightOptions.lights[0].setValues(latitude=10)
    imporve = 10
    session.pngOptions.setValues(imageSize=(2000, 1039))
    # session.pngOptions.setValues(imageSize=(4096, 4096))
    # session.pngOptions.setValues(imageSize=(4096, 4096))
    session.printOptions.setValues(reduceColors=False)
    print >> sys.stdout, out_path
    session.printToFile(
        fileName=out_path,
        format=PNG, canvasObjects=(session.viewports['Viewport: 1'], ))
    # quit(int(100))

take_a_piccie_there_man()