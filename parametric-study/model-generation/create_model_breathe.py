from abaqus import *
from abaqusConstants import *
import __main__
from helpful_functions import *
import numpy as np
# import read_odb_utils
import os
import subprocess
import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import optimization
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior
# from model_creating_macros import *
# from post_processing_macros import *
# import scipy
import sys
# import matplotlib.pyplot as plt
import time
import os

# t_h = 1.4 / 2.
# c_thick = 0.35

dir = sys.argv[-1]
t_h = float(sys.argv[-2])
c_thick = float(sys.argv[-3])

Mdb()

mdb.saveAs(
    pathName=dir+'/model')
import os
os.chdir(dir)
    # r"/home/cerecam/Benjamin_Alheit/simulations/PhD/suture-scale/dynamic/parametric-study/model-generation/h-1.0/t-0.2")

t_h_old = 1.5 / 2.
c_thick_old = 0.2

t_w = 1.
# t_w_old = 1.
c_trans = 0.02

flat_width = t_w/2.
depth = t_w/2.

flat_length = 1.2*t_h_old + c_thick_old*1.2
# flat_length_old = 1.2*t_h + c_thick*1.2

# Part Names
bone_name = "bone"

a = 2
n_points = 150

# t_h = t_h / 2.
x_path = np.linspace(0, depth*1.05, n_points)
x_path -= (x_path[-1] - depth)/2.
y_path = t_h/2. * np.sin(2.*np.pi * x_path / t_w - np.pi/2 )
# y_path = t_h/2. * np.sin(2.*np.pi * x_path / t_w + np.pi/2 )

x_cut = np.linspace(0, flat_width*1.05, n_points)
x_cut -= (x_cut[-1] - flat_width)/2.
y_cut = t_h/2. * np.sin(2.*np.pi * x_cut / t_w - np.pi/2)

y_cut_func = lambda x: t_h/2. * np.sin(2.*np.pi * x / t_w - np.pi/2)

theta = lambda x: np.pi/2. - np.pi/4. * np.cos(2.*np.pi * x / t_w - np.pi/2)

y_cut_top_func = lambda x: y_cut_func(x) + c_thick/2 * np.sin(theta(x)) + c_trans/2. -  t_h/2.
y_cut_bottom_func = lambda x: y_cut_func(x) - c_thick/2 * np.sin(theta(x)) - c_trans/2. -  t_h/2.

y_cut_top = y_cut + c_thick/2 * np.sin(theta(x_cut)) + c_trans/2.
x_cut_top = x_cut - c_thick/2 * np.sign(np.cos(theta(x_cut))) * np.abs(np.cos(theta(x_cut))) ** a

y_cut_bottom = y_cut - c_thick/2 * np.sin(theta(x_cut)) - c_trans/2.
x_cut_bottom = x_cut + c_thick/2 * np.sign(np.cos(theta(x_cut))) * np.abs(np.cos(theta(x_cut))) ** a

y_cut_top -= t_h/2.
y_cut_bottom -= t_h/2.

curvature_angle = 0.816
# curvature_angle = 0.5
# curvature_angle = 0.1

def AAA_automate_suture_interface_generation():
    AA_make_rve()
    # complete_model_edit()
    complete_model()
    make_shell_layers()
    AA_loading()
    make_dynamic()
    apply_visc()
    make_suture_material()
    # history_output()
    standard_history()
    change_output_and_make_input_file()
    mdb.save()




def standard_history():
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        adaptiveMeshConstraints=ON, optimizationTasks=OFF,
        geometricRestrictions=OFF, stopConditions=OFF)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(step='load')
    regionDef=mdb.models['Model-1'].rootAssembly.allInstances['suture-interface-1'].sets['left-bone']
    mdb.models['Model-1'].HistoryOutputRequest(name='left-bone',
                                               createStepName='load', variables=PRESELECT, region=regionDef,
                                               sectionPoints=DEFAULT, rebar=EXCLUDE)
    mdb.models['Model-1'].HistoryOutputRequest(name='right-bone',
                                               objectToCopy=mdb.models['Model-1'].historyOutputRequests['left-bone'],
                                               toStepName='load')
    mdb.models['Model-1'].HistoryOutputRequest(name='suture',
                                               objectToCopy=mdb.models['Model-1'].historyOutputRequests['right-bone'],
                                               toStepName='load')
    regionDef=mdb.models['Model-1'].rootAssembly.allInstances['suture-interface-1'].sets['right-bone']
    mdb.models['Model-1'].historyOutputRequests['right-bone'].setValues(
        region=regionDef)
    regionDef=mdb.models['Model-1'].rootAssembly.allInstances['suture-interface-1'].sets['suture']
    mdb.models['Model-1'].historyOutputRequests['suture'].setValues(
        region=regionDef)




def make_suture_material():
    mdb.models['Model-1'].Material(name='VISCO_HGO')
    mdb.models['Model-1'].materials['VISCO_HGO'].Density(table=((1.25e-9, ), ))
    # mdb.models['Model-1'].materials['VISCO_HGO'].Depvar(n=18)
    mdb.models['Model-1'].materials['VISCO_HGO'].Depvar(n=19)
    mdb.models['Model-1'].materials['VISCO_HGO'].UserMaterial(mechanicalConstants=(
        89.66, 1e-06, 10.94, 74.24, 0.33333333326, 0.8003, 0.7997, 0.5383,
        0.5386))
    # mdb.models['Model-1'].sections['skin'].setValues(material='VISCO_HGO',
    #                                                  thicknessType=UNIFORM, thickness=0.05, thicknessField='',
    #                                                  poissonDefinition=DEFAULT)
    mdb.models['Model-1'].sections['suture'].setValues(material='VISCO_HGO',
                                                       thickness=None)

def apply_visc():
    mdb.models['Model-1'].materials['bone'].Viscoelastic(domain=TIME, time=PRONY,
                                                         table=((0.0774, 0.0, 0.04434), (0.7288, 0.0, 1.036e-05)))
    mdb.models['Model-1'].materials['keratin'].Viscoelastic(domain=TIME,
                                                            time=PRONY, table=((0.3872, 0.0, 0.5974), (0.1586, 0.0, 189.1)))


def AA_make_rve():
    # Geometric values

    path = tuple(map(tuple, np.array([x_path, y_path]).T))
    cut_top = tuple(map(tuple, np.array([x_cut_top, y_cut_top]).T))
    cut_bottom = tuple(map(tuple, np.array([x_cut_bottom, y_cut_bottom]).T))

    apprx_size = 2 * flat_length
    make_block(bone_name, flat_length, flat_width, depth, apprx_size, b_points=False)
    make_block("coll-block-temp", flat_length, flat_width, depth, apprx_size, b_points=False)

    cut_bone(bone_name, path, cut_top, cut_bottom, flat_width, depth, False)
    cut_collagen("coll-int", "bone-2", bone_name, "coll-block-temp")
    merge_col_bone("merged-shell", "bone-2", "coll-int")
    clean_up_temps()
    remove_redundant(flat_width, depth)


def complete_model_edit():
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=ON,
                                                           engineeringFeatures=ON)
    session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
        referenceRepresentation=OFF)
    p = mdb.models['Model-1'].parts['merged-shell']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    mdb.models['Model-1'].Material(name='suture')
    mdb.models['Model-1'].materials['suture'].Elastic(table=((200.0, 0.49), ))
    mdb.models['Model-1'].Material(name='bone')
    mdb.models['Model-1'].materials['bone'].Elastic(table=((12000.0, 0.25), ))
    mdb.models['Model-1'].HomogeneousSolidSection(name='bone', material='bone',
                                                  thickness=None)
    mdb.models['Model-1'].HomogeneousSolidSection(name='suture', material='suture',
                                                  thickness=None)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((-flat_length*0.99, t_w/4., t_w/4.), ))
    region = p.Set(cells=cells, name='left-bone')
    p = mdb.models['Model-1'].parts['merged-shell']
    p.SectionAssignment(region=region, sectionName='bone', offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((flat_length*0.99, t_w/4., t_w/4.), ))
    region = p.Set(cells=cells, name='right-bone')
    p = mdb.models['Model-1'].parts['merged-shell']
    p.SectionAssignment(region=region, sectionName='bone', offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    # cells = c.findAt(((-0.713709, 0.0, 0.004837), ))
    cells = c.findAt(((0.0, 0.99*t_w/2., 0.000001), ))
    region = p.Set(cells=cells, name='suture')
    p = mdb.models['Model-1'].parts['merged-shell']
    p.SectionAssignment(region=region, sectionName='suture', offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=OFF,
                                                           engineeringFeatures=OFF, mesh=ON)
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshTechnique=ON)
    p = mdb.models['Model-1'].parts['merged-shell']
    # p.seedPart(size=25.0, deviationFactor=0.1, minSizeFactor=0.1)
    # p = mdb.models['Model-1'].parts['merged-shell']
    # p.seedPart(size=0.25, deviationFactor=0.1, minSizeFactor=0.1)
    p.seedPart(size=c_thick/2., deviationFactor=0.1, minSizeFactor=0.1)
    # p.seedPart(size=c_thick/2.5, deviationFactor=0.1, minSizeFactor=0.1)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    # pickedRegions = c.findAt(((-flat_length*0.99, t_w/4., t_w/4.), ), ((-0.953709, 0.0,
    #                                                          0.004837), ))
    pickedRegions = c
    p.setMeshControls(regions=pickedRegions, elemShape=TET, technique=FREE)
    elemType1 = mesh.ElemType(elemCode=C3D20R)
    elemType2 = mesh.ElemType(elemCode=C3D15)
    elemType3 = mesh.ElemType(elemCode=C3D10)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((0.10951, 0.5, 0.009666), ), ((-0.953709, 0.0, 0.004837), ))
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
                                                       elemType3))
    # p = mdb.models['Model-1'].parts['merged-shell']
    # c = p.cells
    # pickedRegions = c.findAt(((-0.713709, 0.0, 0.004837), ))
    # p.setMeshControls(regions=pickedRegions, elemShape=TET, technique=FREE)
    # elemType1 = mesh.ElemType(elemCode=C3D20R)
    # elemType2 = mesh.ElemType(elemCode=C3D15)
    # elemType3 = mesh.ElemType(elemCode=C3D10)
    # p = mdb.models['Model-1'].parts['merged-shell']
    # c = p.cells
    # cells = c.findAt(((-0.713709, 0.0, 0.004837), ))
    # pickedRegions =(cells, )
    # p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
    #                                                    elemType3))
    elemType1 = mesh.ElemType(elemCode=C3D20R, elemLibrary=STANDARD)
    elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
    elemType3 = mesh.ElemType(elemCode=C3D10, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((-flat_length*0.99, t_w/4., t_w/4.), ), ((flat_length*0.99, t_w/4., t_w/4.), ))
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
                                                       elemType3))
    elemType1 = mesh.ElemType(elemCode=C3D20R, elemLibrary=STANDARD)
    elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
    # elemType3 = mesh.ElemType(elemCode=C3D10H, elemLibrary=STANDARD)
    elemType3 = mesh.ElemType(elemCode=C3D10, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((0.0, 0.99*t_w/2., 0.000001), ))
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
                                                       elemType3))
    p = mdb.models['Model-1'].parts['merged-shell']
    p.generateMesh(boundaryPreview=ON)
    p = mdb.models['Model-1'].parts['merged-shell']
    p.generateMesh()
    # mdb.meshEditOptions.setValues(enableUndo=True, maxUndoCacheElements=0.5)
    # p = mdb.models['Model-1'].parts['merged-shell']
    # p.PartFromMesh(name='quater-protrusion-mesh', copySets=True)
    # p1 = mdb.models['Model-1'].parts['quater-protrusion-mesh']
    # session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    # session.viewports['Viewport: 1'].partDisplay.setValues(mesh=OFF)
    # session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
    #     meshTechnique=OFF)
    # session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
    #     referenceRepresentation=ON)
    # p1 = mdb.models['Model-1'].parts['quater-protrusion-mesh']
    # session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    # p = mdb.models['Model-1'].Part(name='quater-protrusion-mesh-tr',
    #                                objectToCopy=mdb.models['Model-1'].parts['quater-protrusion-mesh'],
    #                                compressFeatureList=ON, mirrorPlane=XZPLANE)
    # session.viewports['Viewport: 1'].setValues(displayedObject=p)
    # a1 = mdb.models['Model-1'].rootAssembly
    # a1.regenerate()
    # a = mdb.models['Model-1'].rootAssembly
    # session.viewports['Viewport: 1'].setValues(displayedObject=a)
    # a = mdb.models['Model-1'].rootAssembly
    # del a.features['merged-shell-1']
    # a1 = mdb.models['Model-1'].rootAssembly
    # a1.DatumCsysByDefault(CARTESIAN)
    # p = mdb.models['Model-1'].parts['quater-protrusion-mesh']
    # a1.Instance(name='quater-protrusion-mesh-1', part=p, dependent=ON)
    # a1 = mdb.models['Model-1'].rootAssembly
    # p = mdb.models['Model-1'].parts['quater-protrusion-mesh-tr']
    # a1.Instance(name='quater-protrusion-mesh-tr-1', part=p, dependent=ON)
    # a1 = mdb.models['Model-1'].rootAssembly
    # a1.translate(instanceList=('quater-protrusion-mesh-tr-1', ), vector=(0.0, t_w,
    #                                                                      0.0))
    # session.viewports['Viewport: 1'].setColor(globalTranslucency=True)
    # session.viewports['Viewport: 1'].assemblyDisplay.setValues(viewCut=ON)
    # session.viewports['Viewport: 1'].assemblyDisplay.setValues(
    #     activeCutName='Z-Plane', viewCut=ON)
    # session.viewports['Viewport: 1'].assemblyDisplay.setValues(
    #     activeCutName='Y-Plane', viewCut=ON)
    # session.viewports['Viewport: 1'].view.setValues(cameraPosition=(0.0947556,
    #                                                                 5.4808, 0.219183), cameraUpVector=(0, 0, 1))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.25056,
    #                                                 farPlane=5.71104, width=2.18397, height=1.07173, viewOffsetX=0.0126281,
    #                                                 viewOffsetY=0.0248999)
    # a1 = mdb.models['Model-1'].rootAssembly
    # a1.InstanceFromBooleanMerge(name='protrusion-r', instances=(
    #     a1.instances['quater-protrusion-mesh-1'],
    #     a1.instances['quater-protrusion-mesh-tr-1'], ),
    #                             mergeNodes=BOUNDARY_ONLY, nodeMergingTolerance=1e-06, domain=MESH,
    #                             originalInstances=SUPPRESS)
    # p = mdb.models['Model-1'].parts['quater-protrusion-mesh-tr']
    # session.viewports['Viewport: 1'].setValues(displayedObject=p)
    # p1 = mdb.models['Model-1'].parts['protrusion-r']
    # session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    # p1 = mdb.models['Model-1'].parts['protrusion-r']
    # session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    # p = mdb.models['Model-1'].Part(name='protrusion-l',
    #                                objectToCopy=mdb.models['Model-1'].parts['protrusion-r'],
    #                                compressFeatureList=ON, mirrorPlane=XYPLANE)
    # session.viewports['Viewport: 1'].setValues(displayedObject=p)
    # a = mdb.models['Model-1'].rootAssembly
    # session.viewports['Viewport: 1'].setValues(displayedObject=a)
    # a1 = mdb.models['Model-1'].rootAssembly
    # p = mdb.models['Model-1'].parts['protrusion-l']
    # a1.Instance(name='protrusion-l-1', part=p, dependent=ON)
    # session.viewports['Viewport: 1'].assemblyDisplay.setValues(viewCut=OFF)
    # session.viewports['Viewport: 1'].setColor(globalTranslucency=False)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.32932,
    #                                                 farPlane=7.4652, width=3.44058, height=1.68838, cameraPosition=(
    #         5.87678, 0.996425, -0.0579049), cameraUpVector=(-0.425524, 0.884847,
    #                                                         -0.189674), cameraTarget=(0.0909824, 0.415645, -0.00662802))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.3027,
    #                                                 farPlane=7.40395, width=3.41942, height=1.678, cameraPosition=(3.02983,
    #                                                                                                                3.24013, 4.19285), cameraUpVector=(-0.484804, 0.665029, -0.568068),
    #                                                 cameraTarget=(0.0513206, 0.446903, 0.0525907))
    # a1 = mdb.models['Model-1'].rootAssembly
    # a1.translate(instanceList=('protrusion-l-1', ), vector=(0.0, 0.0, t_w))
    # # a1.translate(instanceList=('protrusion-l-1', ), vector=(0.0, 0.0, t_w/2.))
    # # a1.translate(instanceList=('protrusion-r-1', ), vector=(0.0, 0.0, t_w/2.))
    # a1 = mdb.models['Model-1'].rootAssembly
    # a1.InstanceFromBooleanMerge(name='protrusion', instances=(
    #     a1.instances['protrusion-l-1'], a1.instances['protrusion-r-1'], ),
    #                             mergeNodes=BOUNDARY_ONLY, nodeMergingTolerance=1e-06, domain=MESH,
    #                             originalInstances=SUPPRESS)
    # a1 = mdb.models['Model-1'].rootAssembly
    # p = mdb.models['Model-1'].parts['protrusion']
    # a1.Instance(name='protrusion-2', part=p, dependent=ON)
    # a1 = mdb.models['Model-1'].rootAssembly
    # p = mdb.models['Model-1'].parts['protrusion']
    # a1.Instance(name='protrusion-3', part=p, dependent=ON)
    # a1 = mdb.models['Model-1'].rootAssembly
    # a1.translate(instanceList=('protrusion-3', ), vector=(0.0, 0.0, t_w))
    # a1 = mdb.models['Model-1'].rootAssembly
    # a1.translate(instanceList=('protrusion-2', ), vector=(0.0, 0.0, 2*t_w))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=2.84898,
    #                                                 farPlane=7.33513, width=3.32574, height=1.63203, viewOffsetX=-0.159776,
    #                                                 viewOffsetY=-0.014277)
    # a1 = mdb.models['Model-1'].rootAssembly
    # a1.InstanceFromBooleanMerge(name='row', instances=(
    #     a1.instances['protrusion-2'], a1.instances['protrusion-3'],
    #     a1.instances['protrusion-1'], ), mergeNodes=BOUNDARY_ONLY,
    #                             nodeMergingTolerance=1e-06, domain=MESH, originalInstances=SUPPRESS)
    # a1 = mdb.models['Model-1'].rootAssembly
    # p = mdb.models['Model-1'].parts['row']
    # a1.Instance(name='row-2', part=p, dependent=ON)
    # a1 = mdb.models['Model-1'].rootAssembly
    # p = mdb.models['Model-1'].parts['row']
    # a1.Instance(name='row-3', part=p, dependent=ON)
    # a1 = mdb.models['Model-1'].rootAssembly
    # a1.translate(instanceList=('row-3', ), vector=(0.0, t_w, 0.0))
    # a1 = mdb.models['Model-1'].rootAssembly
    # a1.translate(instanceList=('row-2', ), vector=(0.0, 2.*t_w, 0.0))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.88152,
    #                                                 farPlane=10.858, width=5.83451, height=2.86316, viewOffsetX=-0.0886086,
    #                                                 viewOffsetY=0.408133)
    # a1 = mdb.models['Model-1'].rootAssembly
    # a1.InstanceFromBooleanMerge(name='suture-interface', instances=(
    #     a1.instances['row-1'], a1.instances['row-3'], a1.instances['row-2'], ),
    #                             mergeNodes=BOUNDARY_ONLY, nodeMergingTolerance=1e-06, domain=MESH,
    #                             originalInstances=SUPPRESS)
    # session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=ON,
    #                                                        engineeringFeatures=ON)
    # session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
    #     referenceRepresentation=OFF)
    # p = mdb.models['Model-1'].parts['protrusion-l']
    # session.viewports['Viewport: 1'].setValues(displayedObject=p)
    # p = mdb.models['Model-1'].parts['suture-interface']
    # session.viewports['Viewport: 1'].setValues(displayedObject=p)
    # session.viewports['Viewport: 1'].enableMultipleColors()
    # session.viewports['Viewport: 1'].setColor(initialColor='#BDBDBD')
    # cmap=session.viewports['Viewport: 1'].colorMappings['Material']
    # session.viewports['Viewport: 1'].setColor(colorMapping=cmap)
    # session.viewports['Viewport: 1'].disableMultipleColors()


def complete_model():
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=ON,
                                                           engineeringFeatures=ON)
    session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
        referenceRepresentation=OFF)
    p = mdb.models['Model-1'].parts['merged-shell']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    mdb.models['Model-1'].Material(name='suture')
    mdb.models['Model-1'].materials['suture'].Elastic(table=((200.0, 0.49), ))
    mdb.models['Model-1'].Material(name='bone')
    mdb.models['Model-1'].materials['bone'].Elastic(table=((12000.0, 0.25), ))
    mdb.models['Model-1'].HomogeneousSolidSection(name='bone', material='bone',
                                                  thickness=None)
    mdb.models['Model-1'].HomogeneousSolidSection(name='suture', material='suture',
                                                  thickness=None)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((-flat_length*0.99, t_w/4., t_w/4.), ))
    region = p.Set(cells=cells, name='left-bone')
    p = mdb.models['Model-1'].parts['merged-shell']
    p.SectionAssignment(region=region, sectionName='bone', offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((flat_length*0.99, t_w/4., t_w/4.), ))
    region = p.Set(cells=cells, name='right-bone')
    p = mdb.models['Model-1'].parts['merged-shell']
    p.SectionAssignment(region=region, sectionName='bone', offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    # cells = c.findAt(((-0.713709, 0.0, 0.004837), ))
    cells = c.findAt(((0.0, 0.99*t_w/2., 0.000001), ))
    region = p.Set(cells=cells, name='suture')
    p = mdb.models['Model-1'].parts['merged-shell']
    p.SectionAssignment(region=region, sectionName='suture', offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=OFF,
                                                           engineeringFeatures=OFF, mesh=ON)
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshTechnique=ON)
    p = mdb.models['Model-1'].parts['merged-shell']
    # p.seedPart(size=25.0, deviationFactor=0.1, minSizeFactor=0.1)
    # p = mdb.models['Model-1'].parts['merged-shell']
    # p.seedPart(size=0.25, deviationFactor=0.1, minSizeFactor=0.1)
    p.seedPart(size=c_thick/2., deviationFactor=0.1, minSizeFactor=0.1)
    # p.seedPart(size=c_thick/2.5, deviationFactor=0.1, minSizeFactor=0.1)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    # pickedRegions = c.findAt(((-flat_length*0.99, t_w/4., t_w/4.), ), ((-0.953709, 0.0,
    #                                                          0.004837), ))
    pickedRegions = c
    p.setMeshControls(regions=pickedRegions, elemShape=TET, technique=FREE)
    elemType1 = mesh.ElemType(elemCode=C3D20R)
    elemType2 = mesh.ElemType(elemCode=C3D15)
    elemType3 = mesh.ElemType(elemCode=C3D10)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((0.10951, 0.5, 0.009666), ), ((-0.953709, 0.0, 0.004837), ))
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
                                                       elemType3))
    # p = mdb.models['Model-1'].parts['merged-shell']
    # c = p.cells
    # pickedRegions = c.findAt(((-0.713709, 0.0, 0.004837), ))
    # p.setMeshControls(regions=pickedRegions, elemShape=TET, technique=FREE)
    # elemType1 = mesh.ElemType(elemCode=C3D20R)
    # elemType2 = mesh.ElemType(elemCode=C3D15)
    # elemType3 = mesh.ElemType(elemCode=C3D10)
    # p = mdb.models['Model-1'].parts['merged-shell']
    # c = p.cells
    # cells = c.findAt(((-0.713709, 0.0, 0.004837), ))
    # pickedRegions =(cells, )
    # p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
    #                                                    elemType3))
    elemType1 = mesh.ElemType(elemCode=C3D20R, elemLibrary=STANDARD)
    elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
    elemType3 = mesh.ElemType(elemCode=C3D10, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((-flat_length*0.99, t_w/4., t_w/4.), ), ((flat_length*0.99, t_w/4., t_w/4.), ))
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
                                                       elemType3))
    elemType1 = mesh.ElemType(elemCode=C3D20R, elemLibrary=STANDARD)
    elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
    # elemType3 = mesh.ElemType(elemCode=C3D10H, elemLibrary=STANDARD)
    elemType3 = mesh.ElemType(elemCode=C3D10, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((0.0, 0.99*t_w/2., 0.000001), ))
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
                                                       elemType3))
    p = mdb.models['Model-1'].parts['merged-shell']
    p.generateMesh(boundaryPreview=ON)
    p = mdb.models['Model-1'].parts['merged-shell']
    p.generateMesh()
    mdb.meshEditOptions.setValues(enableUndo=True, maxUndoCacheElements=0.5)
    p = mdb.models['Model-1'].parts['merged-shell']
    p.PartFromMesh(name='quater-protrusion-mesh', copySets=True)
    p1 = mdb.models['Model-1'].parts['quater-protrusion-mesh']
    session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    session.viewports['Viewport: 1'].partDisplay.setValues(mesh=OFF)
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshTechnique=OFF)
    session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
        referenceRepresentation=ON)
    p1 = mdb.models['Model-1'].parts['quater-protrusion-mesh']
    session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    p = mdb.models['Model-1'].Part(name='quater-protrusion-mesh-tr',
                                   objectToCopy=mdb.models['Model-1'].parts['quater-protrusion-mesh'],
                                   compressFeatureList=ON, mirrorPlane=XZPLANE)
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.regenerate()
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    a = mdb.models['Model-1'].rootAssembly
    del a.features['merged-shell-1']
    a1 = mdb.models['Model-1'].rootAssembly
    a1.DatumCsysByDefault(CARTESIAN)
    p = mdb.models['Model-1'].parts['quater-protrusion-mesh']
    a1.Instance(name='quater-protrusion-mesh-1', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['quater-protrusion-mesh-tr']
    a1.Instance(name='quater-protrusion-mesh-tr-1', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('quater-protrusion-mesh-tr-1', ), vector=(0.0, t_w,
                                                                         0.0))
    session.viewports['Viewport: 1'].setColor(globalTranslucency=True)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(viewCut=ON)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        activeCutName='Z-Plane', viewCut=ON)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        activeCutName='Y-Plane', viewCut=ON)
    session.viewports['Viewport: 1'].view.setValues(cameraPosition=(0.0947556,
                                                                    5.4808, 0.219183), cameraUpVector=(0, 0, 1))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=4.25056,
                                                    farPlane=5.71104, width=2.18397, height=1.07173, viewOffsetX=0.0126281,
                                                    viewOffsetY=0.0248999)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanMerge(name='protrusion-r', instances=(
        a1.instances['quater-protrusion-mesh-1'],
        a1.instances['quater-protrusion-mesh-tr-1'], ),
                                mergeNodes=BOUNDARY_ONLY, nodeMergingTolerance=1e-06, domain=MESH,
                                originalInstances=SUPPRESS)
    p = mdb.models['Model-1'].parts['quater-protrusion-mesh-tr']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    p1 = mdb.models['Model-1'].parts['protrusion-r']
    session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    p1 = mdb.models['Model-1'].parts['protrusion-r']
    session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    p = mdb.models['Model-1'].Part(name='protrusion-l',
                                   objectToCopy=mdb.models['Model-1'].parts['protrusion-r'],
                                   compressFeatureList=ON, mirrorPlane=XYPLANE)
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['protrusion-l']
    a1.Instance(name='protrusion-l-1', part=p, dependent=ON)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(viewCut=OFF)
    session.viewports['Viewport: 1'].setColor(globalTranslucency=False)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=4.32932,
                                                    farPlane=7.4652, width=3.44058, height=1.68838, cameraPosition=(
            5.87678, 0.996425, -0.0579049), cameraUpVector=(-0.425524, 0.884847,
                                                            -0.189674), cameraTarget=(0.0909824, 0.415645, -0.00662802))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=4.3027,
                                                    farPlane=7.40395, width=3.41942, height=1.678, cameraPosition=(3.02983,
                                                                                                                   3.24013, 4.19285), cameraUpVector=(-0.484804, 0.665029, -0.568068),
                                                    cameraTarget=(0.0513206, 0.446903, 0.0525907))
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('protrusion-l-1', ), vector=(0.0, 0.0, t_w))
    # a1.translate(instanceList=('protrusion-l-1', ), vector=(0.0, 0.0, t_w/2.))
    # a1.translate(instanceList=('protrusion-r-1', ), vector=(0.0, 0.0, t_w/2.))
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanMerge(name='protrusion', instances=(
        a1.instances['protrusion-l-1'], a1.instances['protrusion-r-1'], ),
                                mergeNodes=BOUNDARY_ONLY, nodeMergingTolerance=1e-06, domain=MESH,
                                originalInstances=SUPPRESS)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['protrusion']
    a1.Instance(name='protrusion-2', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['protrusion']
    a1.Instance(name='protrusion-3', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('protrusion-3', ), vector=(0.0, 0.0, t_w))
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('protrusion-2', ), vector=(0.0, 0.0, 2*t_w))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=2.84898,
                                                    farPlane=7.33513, width=3.32574, height=1.63203, viewOffsetX=-0.159776,
                                                    viewOffsetY=-0.014277)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanMerge(name='row', instances=(
        a1.instances['protrusion-2'], a1.instances['protrusion-3'],
        a1.instances['protrusion-1'], ), mergeNodes=BOUNDARY_ONLY,
                                nodeMergingTolerance=1e-06, domain=MESH, originalInstances=SUPPRESS)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['row']
    a1.Instance(name='row-2', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts['row']
    a1.Instance(name='row-3', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('row-3', ), vector=(0.0, t_w, 0.0))
    a1 = mdb.models['Model-1'].rootAssembly
    a1.translate(instanceList=('row-2', ), vector=(0.0, 2.*t_w, 0.0))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=4.88152,
                                                    farPlane=10.858, width=5.83451, height=2.86316, viewOffsetX=-0.0886086,
                                                    viewOffsetY=0.408133)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanMerge(name='suture-interface', instances=(
        a1.instances['row-1'], a1.instances['row-3'], a1.instances['row-2'], ),
                                mergeNodes=BOUNDARY_ONLY, nodeMergingTolerance=1e-06, domain=MESH,
                                originalInstances=SUPPRESS)
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=ON,
                                                           engineeringFeatures=ON)
    session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
        referenceRepresentation=OFF)
    p = mdb.models['Model-1'].parts['protrusion-l']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    p = mdb.models['Model-1'].parts['suture-interface']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    session.viewports['Viewport: 1'].enableMultipleColors()
    session.viewports['Viewport: 1'].setColor(initialColor='#BDBDBD')
    cmap=session.viewports['Viewport: 1'].colorMappings['Material']
    session.viewports['Viewport: 1'].setColor(colorMapping=cmap)
    session.viewports['Viewport: 1'].disableMultipleColors()


def make_shell_layers():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    session.viewports['Viewport: 1'].view.setValues(nearPlane=7.1644,
                                                    farPlane=13.1716, width=6.4661, height=3.17309, viewOffsetX=-0.316294,
                                                    viewOffsetY=0.167875)
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=OFF,
                                                           engineeringFeatures=OFF, mesh=ON)
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshTechnique=ON)
    p = mdb.models['Model-1'].parts['suture-interface']
    f = p.elements
    els = p.elements
    n_els = np.alen(els)


    # face1Elements = f[52244:52245]+f[52299:52300]+f[52373:52374]+f[52377:52378]+\
    #     f[52452:52453]+f[52458:52459]+f[56386:56387]+f[56441:56442]+\
    #     f[56515:56516]+f[56519:56520]+f[56594:56595]+f[56600:56601]+\
    #     f[60528:60529]+f[60583:60584]+f[60657:60658]+f[60661:60662]+\
    #     f[60736:60737]+f[60742:60743]+f[64670:64671]+f[64725:64726]+\
    #     f[64799:64800]+f[64803:64804]+f[64878:64879]+f[64884:64885]+\
    #     f[68812:68813]+f[68867:68868]+f[68941:68942]+f[68945:68946]+\
    #     f[69020:69021]+f[69026:69027]+f[72954:72955]+f[73009:73010]+\
    #     f[73083:73084]+f[73087:73088]+f[73162:73163]+f[73168:73169]
    # face2Elements = f[52045:52046]+f[52218:52219]+f[52226:52227]+f[52228:52229]+\
    #     f[52246:52247]+f[52248:52250]+f[52342:52343]+f[52360:52361]+\
    #     f[52367:52368]+f[52369:52370]+f[52461:52462]+f[52475:52476]+\
    #     f[52485:52486]+f[52489:52490]+f[52508:52509]+f[52971:52972]+\
    #     f[53279:53280]+f[53293:53294]+f[53827:53828]+f[56066:56067]+\
    #     f[56099:56100]+f[56103:56104]+f[56122:56123]+f[56129:56130]+\
    #     f[56143:56144]+f[56160:56161]+f[56190:56191]+f[56212:56213]+\
    #     f[56222:56223]+f[56259:56260]+f[56317:56318]+f[56332:56333]+\
    #     f[56334:56336]+f[56338:56339]+f[56340:56341]+f[56344:56345]+\
    #     f[56350:56352]+f[56369:56370]+f[56372:56373]+f[56376:56378]+\
    #     f[56379:56381]+f[56399:56400]+f[56407:56408]+f[56409:56410]+\
    #     f[56411:56412]+f[56435:56436]+f[56457:56458]+f[56460:56461]+\
    #     f[56475:56476]+f[56535:56536]+f[56544:56545]+f[56590:56591]+\
    #     f[56606:56607]+f[56641:56642]+f[56643:56644]+f[56744:56745]+\
    #     f[56825:56826]+f[56902:56903]+f[56963:56964]+f[56978:56979]+\
    #     f[57029:57030]+f[57048:57049]+f[57074:57075]+f[57096:57097]+\
    #     f[57104:57105]+f[57114:57115]+f[57127:57128]+f[57135:57136]+\
    #     f[57139:57140]+f[57166:57167]+f[57176:57177]+f[57178:57179]+\
    #     f[57196:57197]+f[57200:57202]+f[57238:57239]+f[57277:57278]+\
    #     f[57280:57281]+f[57286:57287]+f[57346:57347]+f[57372:57373]+\
    #     f[57397:57398]+f[57403:57405]+f[57416:57417]+f[57428:57429]+\
    #     f[57434:57435]+f[57440:57441]+f[57443:57444]+f[57485:57486]+\
    #     f[57489:57490]+f[57540:57541]+f[57656:57657]+f[57664:57665]+\
    #     f[57776:57777]+f[57828:57829]+f[57933:57934]+f[57944:57945]+\
    #     f[57956:57957]+f[57960:57961]+f[57962:57965]+f[57967:57968]+\
    #     f[57976:57977]+f[57982:57985]+f[60329:60330]+f[60502:60503]+\
    #     f[60510:60511]+f[60512:60513]+f[60530:60531]+f[60532:60534]+\
    #     f[60626:60627]+f[60644:60645]+f[60651:60652]+f[60653:60654]+\
    #     f[60745:60746]+f[60759:60760]+f[60769:60770]+f[60773:60774]+\
    #     f[60792:60793]+f[61255:61256]+f[61563:61564]+f[61577:61578]+\
    #     f[62111:62112]+f[64350:64351]+f[64383:64384]+f[64387:64388]+\
    #     f[64406:64407]+f[64413:64414]+f[64427:64428]+f[64444:64445]+\
    #     f[64474:64475]+f[64496:64497]+f[64506:64507]+f[64543:64544]+\
    #     f[64601:64602]+f[64616:64617]+f[64618:64620]+f[64622:64623]+\
    #     f[64624:64625]+f[64628:64629]+f[64634:64636]+f[64653:64654]+\
    #     f[64656:64657]+f[64660:64662]+f[64663:64665]+f[64683:64684]+\
    #     f[64691:64692]+f[64693:64694]+f[64695:64696]+f[64719:64720]+\
    #     f[64741:64742]+f[64744:64745]+f[64759:64760]+f[64819:64820]+\
    #     f[64828:64829]+f[64874:64875]+f[64890:64891]+f[64925:64926]+\
    #     f[64927:64928]+f[65028:65029]+f[65109:65110]+f[65186:65187]+\
    #     f[65247:65248]+f[65262:65263]+f[65313:65314]+f[65332:65333]+\
    #     f[65358:65359]+f[65380:65381]+f[65388:65389]+f[65398:65399]+\
    #     f[65411:65412]+f[65419:65420]+f[65423:65424]+f[65450:65451]+\
    #     f[65460:65461]+f[65462:65463]+f[65480:65481]+f[65484:65486]+\
    #     f[65522:65523]+f[65561:65562]+f[65564:65565]+f[65570:65571]+\
    #     f[65630:65631]+f[65656:65657]+f[65681:65682]+f[65687:65689]+\
    #     f[65700:65701]+f[65712:65713]+f[65718:65719]+f[65724:65725]+\
    #     f[65727:65728]+f[65769:65770]+f[65773:65774]+f[65824:65825]+\
    #     f[65940:65941]+f[65948:65949]+f[66060:66061]+f[66112:66113]+\
    #     f[66217:66218]+f[66228:66229]+f[66240:66241]+f[66244:66245]+\
    #     f[66246:66249]+f[66251:66252]+f[66260:66261]+f[66266:66269]+\
    #     f[68613:68614]+f[68786:68787]+f[68794:68795]+f[68796:68797]+\
    #     f[68814:68815]+f[68816:68818]+f[68910:68911]+f[68928:68929]+\
    #     f[68935:68936]+f[68937:68938]+f[69029:69030]+f[69043:69044]+\
    #     f[69053:69054]+f[69057:69058]+f[69076:69077]+f[69539:69540]+\
    #     f[69847:69848]+f[69861:69862]+f[70395:70396]+f[72634:72635]+\
    #     f[72667:72668]+f[72671:72672]+f[72690:72691]+f[72697:72698]+\
    #     f[72711:72712]+f[72728:72729]+f[72758:72759]+f[72780:72781]+\
    #     f[72790:72791]+f[72827:72828]+f[72885:72886]+f[72900:72901]+\
    #     f[72902:72904]+f[72906:72907]+f[72908:72909]+f[72912:72913]+\
    #     f[72918:72920]+f[72937:72938]+f[72940:72941]+f[72944:72946]+\
    #     f[72947:72949]+f[72967:72968]+f[72975:72976]+f[72977:72978]+\
    #     f[72979:72980]+f[73003:73004]+f[73025:73026]+f[73028:73029]+\
    #     f[73043:73044]+f[73103:73104]+f[73112:73113]+f[73158:73159]+\
    #     f[73174:73175]+f[73209:73210]+f[73211:73212]+f[73312:73313]+\
    #     f[73393:73394]+f[73470:73471]+f[73531:73532]+f[73546:73547]+\
    #     f[73597:73598]+f[73616:73617]+f[73642:73643]+f[73664:73665]+\
    #     f[73672:73673]+f[73682:73683]+f[73695:73696]+f[73703:73704]+\
    #     f[73707:73708]+f[73734:73735]+f[73744:73745]+f[73746:73747]+\
    #     f[73764:73765]+f[73768:73770]+f[73806:73807]+f[73845:73846]+\
    #     f[73848:73849]+f[73854:73855]+f[73914:73915]+f[73940:73941]+\
    #     f[73965:73966]+f[73971:73973]+f[73984:73985]+f[73996:73997]+\
    #     f[74002:74003]+f[74008:74009]+f[74011:74012]+f[74053:74054]+\
    #     f[74057:74058]+f[74108:74109]+f[74224:74225]+f[74232:74233]+\
    #     f[74344:74345]+f[74396:74397]+f[74501:74502]+f[74512:74513]+\
    #     f[74524:74525]+f[74528:74529]+f[74530:74533]+f[74535:74536]+\
    #     f[74544:74545]+f[74550:74553]
    # face3Elements = f[51924:51925]+f[51957:51958]+f[51961:51962]+f[51980:51981]+\
    #     f[51987:51988]+f[52001:52002]+f[52018:52019]+f[52048:52049]+\
    #     f[52070:52071]+f[52080:52081]+f[52117:52118]+f[52175:52176]+\
    #     f[52190:52191]+f[52192:52194]+f[52196:52197]+f[52198:52199]+\
    #     f[52202:52203]+f[52208:52210]+f[52227:52228]+f[52230:52231]+\
    #     f[52234:52236]+f[52237:52239]+f[52257:52258]+f[52265:52266]+\
    #     f[52267:52268]+f[52269:52270]+f[52293:52294]+f[52315:52316]+\
    #     f[52318:52319]+f[52333:52334]+f[52393:52394]+f[52402:52403]+\
    #     f[52448:52449]+f[52464:52465]+f[52499:52500]+f[52501:52502]+\
    #     f[52602:52603]+f[52683:52684]+f[52760:52761]+f[52821:52822]+\
    #     f[52836:52837]+f[52887:52888]+f[52906:52907]+f[52932:52933]+\
    #     f[52954:52955]+f[52962:52963]+f[52972:52973]+f[52985:52986]+\
    #     f[52993:52994]+f[52997:52998]+f[53024:53025]+f[53034:53035]+\
    #     f[53036:53037]+f[53054:53055]+f[53058:53060]+f[53096:53097]+\
    #     f[53135:53136]+f[53138:53139]+f[53144:53145]+f[53204:53205]+\
    #     f[53230:53231]+f[53255:53256]+f[53261:53263]+f[53274:53275]+\
    #     f[53286:53287]+f[53292:53293]+f[53298:53299]+f[53301:53302]+\
    #     f[53343:53344]+f[53347:53348]+f[53398:53399]+f[53514:53515]+\
    #     f[53522:53523]+f[53634:53635]+f[53686:53687]+f[53791:53792]+\
    #     f[53802:53803]+f[53814:53815]+f[53818:53819]+f[53820:53823]+\
    #     f[53825:53826]+f[53834:53835]+f[53840:53843]+f[56187:56188]+\
    #     f[56360:56361]+f[56368:56369]+f[56370:56371]+f[56388:56389]+\
    #     f[56390:56392]+f[56484:56485]+f[56502:56503]+f[56509:56510]+\
    #     f[56511:56512]+f[56603:56604]+f[56617:56618]+f[56627:56628]+\
    #     f[56631:56632]+f[56650:56651]+f[57113:57114]+f[57421:57422]+\
    #     f[57435:57436]+f[57969:57970]+f[60208:60209]+f[60241:60242]+\
    #     f[60245:60246]+f[60264:60265]+f[60271:60272]+f[60285:60286]+\
    #     f[60302:60303]+f[60332:60333]+f[60354:60355]+f[60364:60365]+\
    #     f[60401:60402]+f[60459:60460]+f[60474:60475]+f[60476:60478]+\
    #     f[60480:60481]+f[60482:60483]+f[60486:60487]+f[60492:60494]+\
    #     f[60511:60512]+f[60514:60515]+f[60518:60520]+f[60521:60523]+\
    #     f[60541:60542]+f[60549:60550]+f[60551:60552]+f[60553:60554]+\
    #     f[60577:60578]+f[60599:60600]+f[60602:60603]+f[60617:60618]+\
    #     f[60677:60678]+f[60686:60687]+f[60732:60733]+f[60748:60749]+\
    #     f[60783:60784]+f[60785:60786]+f[60886:60887]+f[60967:60968]+\
    #     f[61044:61045]+f[61105:61106]+f[61120:61121]+f[61171:61172]+\
    #     f[61190:61191]+f[61216:61217]+f[61238:61239]+f[61246:61247]+\
    #     f[61256:61257]+f[61269:61270]+f[61277:61278]+f[61281:61282]+\
    #     f[61308:61309]+f[61318:61319]+f[61320:61321]+f[61338:61339]+\
    #     f[61342:61344]+f[61380:61381]+f[61419:61420]+f[61422:61423]+\
    #     f[61428:61429]+f[61488:61489]+f[61514:61515]+f[61539:61540]+\
    #     f[61545:61547]+f[61558:61559]+f[61570:61571]+f[61576:61577]+\
    #     f[61582:61583]+f[61585:61586]+f[61627:61628]+f[61631:61632]+\
    #     f[61682:61683]+f[61798:61799]+f[61806:61807]+f[61918:61919]+\
    #     f[61970:61971]+f[62075:62076]+f[62086:62087]+f[62098:62099]+\
    #     f[62102:62103]+f[62104:62107]+f[62109:62110]+f[62118:62119]+\
    #     f[62124:62127]+f[64471:64472]+f[64644:64645]+f[64652:64653]+\
    #     f[64654:64655]+f[64672:64673]+f[64674:64676]+f[64768:64769]+\
    #     f[64786:64787]+f[64793:64794]+f[64795:64796]+f[64887:64888]+\
    #     f[64901:64902]+f[64911:64912]+f[64915:64916]+f[64934:64935]+\
    #     f[65397:65398]+f[65705:65706]+f[65719:65720]+f[66253:66254]+\
    #     f[68492:68493]+f[68525:68526]+f[68529:68530]+f[68548:68549]+\
    #     f[68555:68556]+f[68569:68570]+f[68586:68587]+f[68616:68617]+\
    #     f[68638:68639]+f[68648:68649]+f[68685:68686]+f[68743:68744]+\
    #     f[68758:68759]+f[68760:68762]+f[68764:68765]+f[68766:68767]+\
    #     f[68770:68771]+f[68776:68778]+f[68795:68796]+f[68798:68799]+\
    #     f[68802:68804]+f[68805:68807]+f[68825:68826]+f[68833:68834]+\
    #     f[68835:68836]+f[68837:68838]+f[68861:68862]+f[68883:68884]+\
    #     f[68886:68887]+f[68901:68902]+f[68961:68962]+f[68970:68971]+\
    #     f[69016:69017]+f[69032:69033]+f[69067:69068]+f[69069:69070]+\
    #     f[69170:69171]+f[69251:69252]+f[69328:69329]+f[69389:69390]+\
    #     f[69404:69405]+f[69455:69456]+f[69474:69475]+f[69500:69501]+\
    #     f[69522:69523]+f[69530:69531]+f[69540:69541]+f[69553:69554]+\
    #     f[69561:69562]+f[69565:69566]+f[69592:69593]+f[69602:69603]+\
    #     f[69604:69605]+f[69622:69623]+f[69626:69628]+f[69664:69665]+\
    #     f[69703:69704]+f[69706:69707]+f[69712:69713]+f[69772:69773]+\
    #     f[69798:69799]+f[69823:69824]+f[69829:69831]+f[69842:69843]+\
    #     f[69854:69855]+f[69860:69861]+f[69866:69867]+f[69869:69870]+\
    #     f[69911:69912]+f[69915:69916]+f[69966:69967]+f[70082:70083]+\
    #     f[70090:70091]+f[70202:70203]+f[70254:70255]+f[70359:70360]+\
    #     f[70370:70371]+f[70382:70383]+f[70386:70387]+f[70388:70391]+\
    #     f[70393:70394]+f[70402:70403]+f[70408:70411]+f[72755:72756]+\
    #     f[72928:72929]+f[72936:72937]+f[72938:72939]+f[72956:72957]+\
    #     f[72958:72960]+f[73052:73053]+f[73070:73071]+f[73077:73078]+\
    #     f[73079:73080]+f[73171:73172]+f[73185:73186]+f[73195:73196]+\
    #     f[73199:73200]+f[73218:73219]+f[73681:73682]+f[73989:73990]+\
    #     f[74003:74004]+f[74537:74538]
    # face4Elements = f[51984:51985]+f[52123:52124]+f[52247:52248]+f[52258:52259]+\
    #     f[52276:52277]+f[52281:52282]+f[52283:52284]+f[52300:52301]+\
    #     f[52437:52438]+f[52493:52494]+f[52860:52861]+f[52935:52936]+\
    #     f[53817:53818]+f[53844:53845]+f[56126:56127]+f[56265:56266]+\
    #     f[56389:56390]+f[56400:56401]+f[56418:56419]+f[56423:56424]+\
    #     f[56425:56426]+f[56442:56443]+f[56579:56580]+f[56635:56636]+\
    #     f[57002:57003]+f[57077:57078]+f[57959:57960]+f[57986:57987]+\
    #     f[60268:60269]+f[60407:60408]+f[60531:60532]+f[60542:60543]+\
    #     f[60560:60561]+f[60565:60566]+f[60567:60568]+f[60584:60585]+\
    #     f[60721:60722]+f[60777:60778]+f[61144:61145]+f[61219:61220]+\
    #     f[62101:62102]+f[62128:62129]+f[64410:64411]+f[64549:64550]+\
    #     f[64673:64674]+f[64684:64685]+f[64702:64703]+f[64707:64708]+\
    #     f[64709:64710]+f[64726:64727]+f[64863:64864]+f[64919:64920]+\
    #     f[65286:65287]+f[65361:65362]+f[66243:66244]+f[66270:66271]+\
    #     f[68552:68553]+f[68691:68692]+f[68815:68816]+f[68826:68827]+\
    #     f[68844:68845]+f[68849:68850]+f[68851:68852]+f[68868:68869]+\
    #     f[69005:69006]+f[69061:69062]+f[69428:69429]+f[69503:69504]+\
    #     f[70385:70386]+f[70412:70413]+f[72694:72695]+f[72833:72834]+\
    #     f[72957:72958]+f[72968:72969]+f[72986:72987]+f[72991:72992]+\
    #     f[72993:72994]+f[73010:73011]+f[73147:73148]+f[73203:73204]+\
    #     f[73570:73571]+f[73645:73646]+f[74527:74528]+f[74554:74555]

    face1Elements = els[0:0]
    face2Elements = els[0:0]
    face3Elements = els[0:0]
    face4Elements = els[0:0]

    face1ElementsBottom = els[0:0]
    face2ElementsBottom = els[0:0]
    face3ElementsBottom = els[0:0]
    face4ElementsBottom = els[0:0]

    nodes_per_element = np.alen(els[0].getNodes())

    pct = 0
    print >> sys.__stdout__,  "find part top elements"
    if nodes_per_element == 4:
        face1 = np.array([True, True, True, False])
        face2 = np.array([True, True, False, True])
        face3 = np.array([False, True, True, False])
        face4 = np.array([True, False, True, False])

        for el_index in range(np.alen(els)):
            el = els[el_index]
            nodes_on_surface = np.array([None] * 4)
            nodes = el.getNodes()
            for node_index in range(np.alen(nodes)):
                nodes_on_surface[node_index] = find_top(nodes[node_index].coordinates)
                # nodes_on_surface[node_index] = find_bottom(nodes[node_index].coordinates)

            if array_all_equal(nodes_on_surface, np.array([False]*4)):
                pass
            elif array_all_equal(nodes_on_surface, face1):
                # face1Elements.append(el)
                # face1Elements = face1Elements + [el]
                face1Elements = face1Elements + els[el_index:el_index+1]

            elif array_all_equal(nodes_on_surface, face2):
                # face2Elements.append(el)
                # face2Elements = face2Elements + [el]
                face2Elements = face2Elements + els[el_index:el_index+1]

            elif array_all_equal(nodes_on_surface, face3):
                # face3Elements.append(el)
                # face3Elements = face3Elements + [el]
                face3Elements = face3Elements + els[el_index:el_index+1]

            elif array_all_equal(nodes_on_surface, face4):
                # face4Elements.append(el)
                # face4Elements = face4Elements + [el]
                face4Elements = face4Elements + els[el_index:el_index+1]


    elif nodes_per_element == 10:
        face1 = np.array([True, True, True, False, True, True, True, False, False, False])
        face2 = np.array([True, True, False, True, True, False, False, True, True, False])
        face3 = np.array([False, True, True, True, False, True, False, False, True, True])
        face4 = np.array([True, False, True, True, False, False, True, True, False, True])


        print >> sys.__stdout__, ('Quadratic tets')
        for el_index in range(np.alen(els)):
            el = els[el_index]
            nodes_on_surface = np.array([None] * 10)
            nodes_on_bottom = np.array([None] * 10)
            nodes = el.getNodes()
            for node_index in range(np.alen(nodes)):
                nodes_on_surface[node_index] = find_top(nodes[node_index].coordinates)
                nodes_on_bottom[node_index] = find_bottom(nodes[node_index].coordinates)


            if (100 * el_index/n_els) - pct > 10:
                pct = (100 * el_index/n_els)
                print >> sys.__stdout__,  str(pct) + " %"

            # if nodes_on_surface[0]:
            #         print >> sys.__stdout__, (nodes_on_surface)

            # if nodes_on_surface[0]:
            #         print >> sys.__stdout__,  nodes_on_surface

            if array_all_equal(nodes_on_surface, np.array([False]*10)) and array_all_equal(nodes_on_bottom, np.array([False]*10)) :
                pass
            elif array_all_equal(nodes_on_surface, face1):
                # face1Elements.append(el)
                face1Elements = face1Elements + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_surface, face2):
                # face2Elements.append(el)
                face2Elements = face2Elements + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_surface, face3):
                # face3Elements.append(el)
                face3Elements = face3Elements + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_surface, face4):
                # face4Elements.append(el)
                face4Elements = face4Elements + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_bottom, face1):
                # face1Elements.append(el)
                face1ElementsBottom = face1ElementsBottom + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_bottom, face2):
                # face2Elements.append(el)
                face2ElementsBottom = face2ElementsBottom + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_bottom, face3):
                # face3Elements.append(el)
                face3ElementsBottom = face3ElementsBottom + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_bottom, face4):
                # face4Elements.append(el)
                face4ElementsBottom = face4ElementsBottom + els[el_index:el_index+1]
    else:
        raise Exception("This is no bueno")

    # a = mdb.models['Model-1'].rootAssembly
    #
    # a.Surface(face1Elements=face1ElementsBottom, face2Elements=face2ElementsBottom,
    #           face3Elements=face3ElementsBottom, face4Elements=face4ElementsBottom,
    #           name='bottom')
    p.Surface(face1Elements=face1ElementsBottom, face2Elements=face2ElementsBottom,
              face3Elements=face3ElementsBottom, face4Elements=face4ElementsBottom,
              name='bottom')

    p.generateMeshByOffset(region=regionToolset.Region(face1Elements=face1Elements,
                                                       face2Elements=face2Elements, face3Elements=face3Elements,
                                                       face4Elements=face4Elements), meshType=SHELL,
                           distanceBetweenLayers=0.0, numLayers=2, initialOffset=0.0,
                           shareNodes=True)
    p = mdb.models['Model-1'].parts['suture-interface']
    e = p.elements
    n_new_els = int(np.alen(p.elements) - n_els)
    # elements = e[74556:75354]
    elements = e[-int(n_new_els/2):]
    p.Set(elements=elements, name='OffsetElements-1-Layer-1')
    p = mdb.models['Model-1'].parts['suture-interface']
    e = p.elements
    # elements = e[75354:76152]
    elements = e[-n_new_els:-int(n_new_els/2)]
    p.Set(elements=elements, name='OffsetElements-1-Layer-2')
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=ON,
                                                           engineeringFeatures=ON, mesh=OFF)
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshTechnique=OFF)
    mdb.models['Model-1'].parts['suture-interface'].sets.changeKey(
        fromName='OffsetElements-1-Layer-1', toName='skin')
    mdb.models['Model-1'].parts['suture-interface'].sets.changeKey(
        fromName='OffsetElements-1-Layer-2', toName='keratin')
    mdb.models['Model-1'].Material(name='keratin')
    mdb.models['Model-1'].materials['keratin'].Elastic(table=((1000.0, 0.4), ))
    mdb.models['Model-1'].MembraneSection(name='skin', material='suture',
                                          thicknessType=UNIFORM, thickness=0.05, thicknessField='',
                                          poissonDefinition=DEFAULT)
    mdb.models['Model-1'].HomogeneousShellSection(name='keratin', preIntegrate=OFF,
                                                  material='keratin', thicknessType=UNIFORM, thickness=0.05,
                                                  thicknessField='', idealization=NO_IDEALIZATION,
                                                  poissonDefinition=DEFAULT, thicknessModulus=None, temperature=GRADIENT,
                                                  useDensity=OFF, integrationRule=SIMPSON, numIntPts=5)
    p = mdb.models['Model-1'].parts['suture-interface']
    region = p.sets['keratin']
    p = mdb.models['Model-1'].parts['suture-interface']
    p.SectionAssignment(region=region, sectionName='keratin', offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)
    p = mdb.models['Model-1'].parts['suture-interface']
    region = p.sets['skin']
    p = mdb.models['Model-1'].parts['suture-interface']
    p.SectionAssignment(region=region, sectionName='skin', offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=OFF,
                                                           engineeringFeatures=OFF, mesh=ON)
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshTechnique=ON)
    elemType1 = mesh.ElemType(elemCode=M3D6, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts['suture-interface']
    z1 = p.elements
    # elems1 = z1[74556:75354]
    elems1 = z1[-int(n_new_els/2):]
    pickedRegions =(elems1, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, ))
    elemType1 = mesh.ElemType(elemCode=STRI65, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts['suture-interface']
    z1 = p.elements
    # elems1 = z1[75354:76152]
    elems1 = z1[-n_new_els:-int(n_new_els/2)]
    pickedRegions =(elems1, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, ))


def find_top(point):
    return float_equals(point[1], t_w*3.)

def find_bottom(point):
    return float_equals(point[1], 0.)


def AA_loading():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    a1 = mdb.models['Model-1'].rootAssembly
    a1.regenerate()
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        adaptiveMeshConstraints=ON)
    mdb.models['Model-1'].StaticStep(name='load', previous='Initial',
                                     maxNumInc=1000, initialInc=0.2, maxInc=0.2, nlgeom=ON)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(step='load')
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=ON, bcs=ON,
                                                               predefinedFields=ON, connectors=ON, adaptiveMeshConstraints=OFF)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(step='Initial')
    session.viewports['Viewport: 1'].view.setValues(nearPlane=1.59196,
                                                    farPlane=7.01805, width=3.22546, height=1.58282, viewOffsetX=-0.450735,
                                                    viewOffsetY=0.304575)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=1.94904,
                                                    farPlane=5.97774, width=3.94892, height=1.93784, cameraPosition=(
            4.0092, 1.01789, 2.27302), cameraUpVector=(-0.306446, 0.94131,
                                                       -0.141517), cameraTarget=(-1.27423, 0.991905, -0.155981),
                                                    viewOffsetX=-0.551834, viewOffsetY=0.37289)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=1.73642,
                                                    farPlane=6.19035, width=5.27196, height=2.58709, viewOffsetX=-0.407189,
                                                    viewOffsetY=0.342822)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=1.74928,
                                                    farPlane=6.29437, width=5.31101, height=2.60626, cameraPosition=(
            3.55939, 1.35106, -0.649232), cameraUpVector=(-0.431221, 0.899714,
                                                          0.0675479), cameraTarget=(-1.91966, 0.740556, 1.2008),
                                                    viewOffsetX=-0.410206, viewOffsetY=0.345362)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=1.74228,
                                                    farPlane=6.30138, width=5.28974, height=2.59582, cameraPosition=(
            3.52874, 1.42948, -0.714141), cameraUpVector=(-0.405515, 0.902575,
                                                          0.144623), cameraTarget=(-1.95031, 0.818973, 1.13589),
                                                    viewOffsetX=-0.408563, viewOffsetY=0.343979)
    a = mdb.models['Model-1'].rootAssembly
    xp_dir = (1.0, -np.sin(np.deg2rad(curvature_angle)), 0.0)
    xn_dir = (-1.0, -np.sin(np.deg2rad(curvature_angle)), 0.0)
    zp_dir = (0.0, -np.sin(np.deg2rad(curvature_angle)), 1.0)
    zn_dir = (0.0, -np.sin(np.deg2rad(curvature_angle)), -1.0)
    xpzn_dir = (1.0, -2**0.5*np.sin(np.deg2rad(curvature_angle)), -1.0)
    xpzp_dir = (1.0, -2**0.5*np.sin(np.deg2rad(curvature_angle)), 1.0)
    xnzp_dir = (-1.0, -2**0.5*np.sin(np.deg2rad(curvature_angle)), 1.0)
    xnzn_dir = (-1.0, -2**0.5*np.sin(np.deg2rad(curvature_angle)), -1.0)


    a.DatumCsysByThreePoints(name='xp', coordSysType=CARTESIAN, origin=(0.0, 0.0,0.0),
                             point1=xp_dir, point2=(0.0, 1.0, 0.0))
    a.DatumCsysByThreePoints(name='xn', coordSysType=CARTESIAN, origin=(0.0, 0.0,0.0),
                             point1=xn_dir, point2=(0.0, 1.0, 0.0))
    a.DatumCsysByThreePoints(name='zn', coordSysType=CARTESIAN, origin=(0.0, 0.0,0.0),
                             point1=zn_dir, point2=(0.0, 1.0, 0.0))
    a.DatumCsysByThreePoints(name='zp', coordSysType=CARTESIAN, origin=(0.0, 0.0,0.0),
                             point1=zp_dir, point2=(0.0, 1.0, 0.0))
    a.DatumCsysByThreePoints(name='xpzp', coordSysType=CARTESIAN, origin=(0.0, 0.0,0.0),
                             point1=xp_dir, point2=zp_dir)
    a.DatumCsysByThreePoints(name='xpzn', coordSysType=CARTESIAN, origin=(0.0, 0.0,0.0),
                             point1=xp_dir, point2=zn_dir)
    a.DatumCsysByThreePoints(name='xnzn', coordSysType=CARTESIAN, origin=(0.0, 0.0,0.0),
                             point1=xn_dir, point2=zn_dir)
    a.DatumCsysByThreePoints(name='xnzp', coordSysType=CARTESIAN, origin=(0.0, 0.0,0.0),
                             point1=xn_dir, point2=zp_dir)

    a = mdb.models['Model-1'].rootAssembly
    n1 = a.instances['suture-interface-1'].nodes
    #     print >> sys.__stdout__,  n1[0]
    bottom = n1[0:0]
    xp = n1[0:0]
    xn = n1[0:0]
    zp = n1[0:0]
    zn = n1[0:0]
    xpzp = n1[0:0]
    xpzn = n1[0:0]
    xnzn = n1[0:0]
    xnzp = n1[0:0]
    n_nodes = np.alen(n1)
    pct = 0
    print >> sys.__stdout__,  "Assining node sets"
    for node_index in range(n_nodes):
        if (100 * node_index/n_nodes) - pct > 10:
            pct = (100 * node_index/n_nodes)
            print >> sys.__stdout__,  str(pct) + " %"

        coords = n1[node_index].coordinates
        if float_equals(coords[0], flat_length):
            if float_equals(coords[2], 0):
                xpzn = xpzn + n1[node_index:node_index+1]
            elif float_equals(coords[2], 3.*t_w):
                xpzp = xpzp + n1[node_index:node_index+1]
            else:
                xp = xp + n1[node_index:node_index+1]
        elif float_equals(coords[0], -flat_length):
            if float_equals(coords[2], 0):
                xnzn = xnzn + n1[node_index:node_index+1]
            elif float_equals(coords[2], 3.*t_w):
                xnzp = xnzp + n1[node_index:node_index+1]
            else:
                xn = xn+ n1[node_index:node_index+1]
        elif float_equals(coords[2], 0):
            zn = zn + n1[node_index:node_index+1]
        elif float_equals(coords[2], 3.*t_w):
            zp = zp + n1[node_index:node_index+1]
        if float_equals(coords[1], 0):
            bottom = bottom + n1[node_index:node_index+1]

    # a.Set(nodes=zp, name='zp')
    # a.Set(nodes=xn, name='xn')
    # a.Set(nodes=zn, name='zn')
    # a.Set(nodes=xpzn, name='xpzn')
    # a.Set(nodes=xpzp, name='xpzp')
    # a.Set(nodes=xnzp, name='xnzp')
    # a.Set(nodes=xnzn, name='xnzn')

    print >> sys.__stdout__,  "Node sets assigned"


    datums = mdb.models['Model-1'].rootAssembly.datums
    region = a.Set(nodes=xp, name='xp')
    mdb.models['Model-1'].DisplacementBC(name='xp', createStepName='Initial',
                                         region=region, u1=SET, u2=UNSET, u3=UNSET, ur1=UNSET, ur2=UNSET,
                                         ur3=UNSET, amplitude=UNSET, distributionType=UNIFORM, fieldName='',
                                         # localCsys=datums[get_datum_key_from_axis_1_and_2(datums, xp_dir)])
                                         # localCsys=datums[get_datum_key_from_axis(datums, direction1=xp_dir, direction3=[0., 0., 1.])])
                                         localCsys=datums[get_datum_key_from_axis_and_xy_plane_normal(datums, xp_dir, [0., 0., 1.])])

    region = a.Set(nodes=xn, name='xn')
    mdb.models['Model-1'].DisplacementBC(name='xn', createStepName='Initial',
                                         region=region, u1=SET, u2=UNSET, u3=UNSET, ur1=UNSET, ur2=UNSET,
                                         ur3=UNSET, amplitude=UNSET, distributionType=UNIFORM, fieldName='',
                                         # localCsys=datums[get_datum_key_from_axis_1_and_2(datums, xn_dir)])
                                         # localCsys=datums[get_datum_key_from_axis(datums, direction1=xn_dir, direction3=[0., 0., 1.])])
                                         localCsys=datums[get_datum_key_from_axis_and_xy_plane_normal(datums, xn_dir, [0., 0., 1.])])



    region = a.Set(nodes=zn, name='zn')
    mdb.models['Model-1'].DisplacementBC(name='zn', createStepName='Initial',
                                         region=region, u1=SET, u2=UNSET, u3=UNSET, ur1=UNSET, ur2=UNSET,
                                         ur3=UNSET, amplitude=UNSET, distributionType=UNIFORM, fieldName='',
                                         # localCsys=datums[get_datum_key_from_axis_1_and_2(datums, zn_dir)])
                                         # localCsys=datums[get_datum_key_from_axis(datums, direction1=zn_dir, direction3=[1., 0., 0.])])
                                         localCsys=datums[get_datum_key_from_axis_and_xy_plane_normal(datums, zn_dir, [1., 0., 0.])])


    region = a.Set(nodes=zp, name='zp')
    mdb.models['Model-1'].DisplacementBC(name='zp', createStepName='Initial',
                                         region=region, u1=SET, u2=UNSET, u3=UNSET, ur1=UNSET, ur2=UNSET,
                                         ur3=UNSET, amplitude=UNSET, distributionType=UNIFORM, fieldName='',
                                         # localCsys=datums[get_datum_key_from_axis_1_and_2(datums, zp_dir)])
                                         # localCsys=datums[get_datum_key_from_axis(datums, direction1=zp_dir, direction3=[1., 0., 0.])])
                                         localCsys=datums[get_datum_key_from_axis_and_xy_plane_normal(datums, zp_dir, [1., 0., 0.])])


    region = a.Set(nodes=xpzp, name='xpzp')
    mdb.models['Model-1'].DisplacementBC(name='xpzp', createStepName='Initial',
                                         region=region, u1=SET, u2=SET, u3=UNSET, ur1=UNSET, ur2=UNSET,
                                         ur3=UNSET, amplitude=UNSET, distributionType=UNIFORM, fieldName='',
                                         # localCsys=datums[get_datum_key_from_axis_1_and_2(datums, xpzp_dir)])
                                         localCsys=datums[get_datum_key_from_xy_plane_normal(datums, np.cross(xp_dir, zp_dir))])

    region = a.Set(nodes=xnzp, name='xnzp')
    mdb.models['Model-1'].DisplacementBC(name='xnzp', createStepName='Initial',
                                         region=region, u1=SET, u2=SET, u3=UNSET, ur1=UNSET, ur2=UNSET,
                                         ur3=UNSET, amplitude=UNSET, distributionType=UNIFORM, fieldName='',
                                         # localCsys=datums[get_datum_key_from_axis_1_and_2(datums, xnzp_dir)])
                                         localCsys=datums[get_datum_key_from_xy_plane_normal(datums, np.cross(xn_dir, zp_dir))])


    region = a.Set(nodes=xnzn, name='xnzn')
    mdb.models['Model-1'].DisplacementBC(name='xnzn', createStepName='Initial',
                                         region=region, u1=SET, u2=SET, u3=UNSET, ur1=UNSET, ur2=UNSET,
                                         ur3=UNSET, amplitude=UNSET, distributionType=UNIFORM, fieldName='',
                                         # localCsys=datums[get_datum_key_from_axis_1_and_2(datums, xnzn_dir)])
                                         localCsys=datums[get_datum_key_from_xy_plane_normal(datums, np.cross(xn_dir, zn_dir))])


    region = a.Set(nodes=xpzn, name='xpzn')
    mdb.models['Model-1'].DisplacementBC(name='xpzn', createStepName='Initial',
                                         region=region, u1=SET, u2=SET, u3=UNSET, ur1=UNSET, ur2=UNSET,
                                         ur3=UNSET, amplitude=UNSET, distributionType=UNIFORM, fieldName='',
                                         # localCsys=datums[get_datum_key_from_axis_1_and_2(datums, xpzn_dir)])
                                         localCsys=datums[get_datum_key_from_xy_plane_normal(datums, np.cross(xp_dir, zn_dir))])



    a = mdb.models['Model-1'].rootAssembly
    region = a.instances['suture-interface-1'].surfaces['bottom']
    mdb.models['Model-1'].Pressure(name='breath',
                                   createStepName='load', region=region, distributionType=UNIFORM,
                                   # field='', magnitude=0.02, amplitude=UNSET)
                                   # field='', magnitude=2, amplitude=UNSET)
                                   field='', magnitude=0.005, amplitude=UNSET)

    a.Set(nodes=bottom, name='bottom')


def change_output_and_make_input_file():
    mdb.models['Model-1'].fieldOutputRequests['F-Output-1'].setValues(variables=(
        'S', 'E', 'LE', 'UT', 'P', 'ELEDEN'))

    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        adaptiveMeshConstraints=OFF)
    mdb.Job(name='Job-1', model='Model-1', description='', type=ANALYSIS,
            atTime=None, waitMinutes=0, waitHours=0, queue=None, memory=90,
            memoryUnits=PERCENTAGE, getMemoryFromAnalysis=True,
            explicitPrecision=SINGLE, nodalOutputPrecision=SINGLE, echoPrint=OFF,
            modelPrint=OFF, contactPrint=OFF, historyPrint=OFF, userSubroutine='',
            scratch='', resultsFormat=ODB, multiprocessingMode=DEFAULT, numCpus=4,
            numDomains=4, numGPUs=0)
    mdb.jobs['Job-1'].writeInput(consistencyChecking=OFF)




def make_block(part_name, flat_length, flat_width, depth, apprx_size, b_points=False):

    s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=apprx_size)
    g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=STANDALONE)
    s.rectangle(point1=(0.0, 0.0), point2=(-flat_length, flat_width))
    s.FixedConstraint(entity=g[5])
    s.FixedConstraint(entity=g[4])
    s.FixedConstraint(entity=g[3])
    s.FixedConstraint(entity=g[2])

    # s.CircleByCenterPerimeter(center=(0.0, -apprx_radius), point1=(-flat_length, 0.0))
    # s.CircleByCenterPerimeter(center=(0.0, -apprx_radius), point1=(-flat_length, flat_width))
    # s.ConstructionLine(point1=(0.0, -apprx_radius), point2=(-np.sin(theta), -apprx_radius+np.cos(theta)))
    # # s.VerticalConstraint(entity=g[8], addUndoState=False)
    # s.FixedConstraint(entity=g[8])
    # s.Line(point1=(-7.75, 3.375), point2=(-6.625, 2.0))
    # break_point(b_points, "1.1")
    # s.CoincidentConstraint(entity1=v[6], entity2=g[6])
    # break_point(b_points, "1.2")
    # s.CoincidentConstraint(entity1=v[5], entity2=g[7])
    # break_point(b_points, "1.3")
    # s.CoincidentConstraint(entity1=v[5], entity2=g[8])
    # break_point(b_points, "1")
    # s.CoincidentConstraint(entity1=v[6], entity2=g[8])
    # # s.PerpendicularConstraint(entity1=g[9], entity2=g[6])
    # # session.viewports['Viewport: 1'].view.setValues(nearPlane=24.1058,
    # #     farPlane=32.4627, width=30.1321, height=15.2269, cameraPosition=(
    # #     1.25103, -3.15898, 28.2843), cameraTarget=(1.25103, -3.15898, 0))
    # # s.PerpendicularConstraint(entity1=g[9], entity2=g[7])
    # break_point(b_points, "1")
    # s.autoTrimCurve(curve1=g[7], point1=(-(total_length+5), flat_width))
    # break_point(b_points, "2")
    # s.autoTrimCurve(curve1=g[6], point1=(-(total_length+5), -4.65462303161621))
    # break_point(b_points, "3")
    # # s.autoTrimCurve(curve1=g[12], point1=(-(total_length*0.99), -flat_width))
    # s.autoTrimCurve(curve1=g[12], point1=(-(total_length*0.999), np.sqrt(apprx_radius**2-(total_length*0.99)**2)-apprx_radius))
    # break_point(b_points, "4")
    # s.autoTrimCurve(curve1=g[13], point1=(-0.0168264961242676, 0.0302357912063599))
    # break_point(b_points, "5")
    # s.autoTrimCurve(curve1=g[15], point1=(0.90483856201172, 0.302357912063599))
    # break_point(b_points, "6")
    # s.autoTrimCurve(curve1=g[11], point1=(0.01, flat_width*1.05))
    # break_point(b_points, "7")
    # s.autoTrimCurve(curve1=g[4], point1=(-flat_length, flat_width/2.))
    # break_point(b_points, "8")


    p = mdb.models['Model-1'].Part(name=part_name, dimensionality=THREE_D,
                                   type=DEFORMABLE_BODY)
    p = mdb.models['Model-1'].parts[part_name]
    p.BaseSolidExtrude(sketch=s, depth=depth)
    s.unsetPrimaryObject()
    p = mdb.models['Model-1'].parts[part_name]
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    del mdb.models['Model-1'].sketches['__profile__']
    p = mdb.models['Model-1'].parts[part_name]
    f = p.faces
    # p.Mirror(mirrorPlane=f[4], keepOriginal=ON)
    p.Mirror(mirrorPlane=f.findAt(coordinates=(0.0, flat_width/2., depth/2.)), keepOriginal=ON)


def cut_bone(part_name, path, cut_top, cut_bottom, flat_width, depth, b_points):
    p = mdb.models['Model-1'].parts[part_name]
    f, e = p.faces, p.edges
    # t = p.MakeSketchTransform(sketchPlane=f[6], sketchUpEdge=e[21],
    t = p.MakeSketchTransform(sketchPlane=f.findAt(coordinates=(0.0, flat_width, depth/2.)), sketchUpEdge=e.findAt(coordinates=(0.0, flat_width, depth)),
                              sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, flat_width, 0.0))
    s = mdb.models['Model-1'].ConstrainedSketch(name='__sweep__', sheetSize=depth*3.,
                                                gridSpacing=depth/10., transform=t)
    g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=9.34539,
    #     farPlane=14.0878, width=4.72059, height=2.3855, cameraPosition=(
    #     -0.283552, 12, 2.13615), cameraTarget=(-0.283552, 2, 2.13615))
    s.Spline(points=path)
    break_point(b_points, "1")
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=9.21486,
    #     farPlane=14.2183, width=5.89536, height=2.97916, cameraPosition=(
    #     -0.318505, 12, 1.86906), cameraTarget=(-0.318505, 2, 1.86906))
    s.unsetPrimaryObject()
    s.unsetPrimaryObject()
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=33.3508,
    #     farPlane=54.7613, width=21.1243, height=10.1162, cameraPosition=(
    #     -25.6409, 29.4637, -19.2963), cameraUpVector=(0.914255, -0.00632095,
    #     -0.40509), cameraTarget=(-0.664081, -0.017676, 1.44713))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=33.8794,
    #     farPlane=54.1226, width=21.4591, height=10.2766, cameraPosition=(
    #     -24.3944, 34.1074, -12.5509), cameraUpVector=(0.869373, -0.0265511,
    #     -0.493442), cameraTarget=(-0.658399, 0.00349182, 1.47788))
    p = mdb.models['Model-1'].parts[part_name]
    f1, e1 = p.faces, p.edges
    # t = p.MakeSketchTransform(sketchPlane=f1[9], sketchUpEdge=e1[22],
    t = p.MakeSketchTransform(sketchPlane=f1.findAt(coordinates=(0.0, flat_width/2., 0.0)), sketchUpEdge=e1.findAt(coordinates=(0.0, flat_width, 0.0)),
                              sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, 0.0, 0.0))
    s1 = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
                                                 sheetSize=40.58, gridSpacing=1.01, transform=t)
    g1, v1, d1, c1 = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    s1.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=39.2367,
    #     farPlane=44.9334, width=9.72343, height=4.91363, cameraPosition=(
    #     -0.583853, 0.115076, -40.5851), cameraTarget=(-0.583853, 0.115076, 0))
    top_start = cut_top[0]
    top_end = cut_top[-1]
    bottom_start = cut_bottom[0]
    bottom_end = cut_bottom[-1]
    #
    break_point(b_points, "2")
    s1.Spline(points=cut_top)
    break_point(b_points, "3")
    s1.Spline(points=cut_bottom)
    break_point(b_points, "4")
    s1.Line(point1=top_start, point2=bottom_start)
    break_point(b_points, "5")
    s1.Line(point1=top_end, point2=bottom_end)
    break_point(b_points, "6")
    # s1.VerticalConstraint(entity=g1[15], addUndoState=False)
    break_point(b_points, "6.1")
    s1.unsetPrimaryObject()
    break_point(b_points, "6.2")
    p = mdb.models['Model-1'].parts[part_name]
    break_point(b_points, "6.3")
    f, e = p.faces, p.edges
    break_point(b_points, "7")

    p.CutSweep(pathPlane=f.findAt(coordinates=(0.0, flat_width, depth/2.)), pathUpEdge=e.findAt(coordinates=(0.0, flat_width, depth)),
               sketchPlane=f.findAt(coordinates=(0.0, flat_width/2., 0.0)),
               sketchUpEdge=e.findAt(coordinates=(0.0, flat_width, 0.0)),
               pathOrientation=RIGHT, path=s,
               sketchOrientation=RIGHT, profile=s1, profileNormal=ON)
    break_point(b_points, "8")
    del mdb.models['Model-1'].sketches['__sweep__']
    break_point(b_points, "9")
    del mdb.models['Model-1'].sketches['__profile__']
    break_point(b_points, "10")


def cut_collagen(part_name, bone_instance_name, bone_name, coll_block_name):
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts[bone_name]
    a1.Instance(name=bone_instance_name, part=p, dependent=ON)
    p = mdb.models['Model-1'].parts[coll_block_name]
    a1.Instance(name='coll-block-1', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanCut(name='coll-int',
                              instanceToBeCut=mdb.models['Model-1'].rootAssembly.instances['coll-block-1'],
                              cuttingInstances=(a1.instances[bone_instance_name], ),
                              originalInstances=SUPPRESS)



def merge_col_bone(merged_part_name, bone_instance, coll_name):
    coll_instance = coll_name +'-1'
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    a = mdb.models['Model-1'].rootAssembly
    a.features[bone_instance].resume()
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanMerge(name=merged_part_name, instances=(
        a1.instances[bone_instance], a1.instances[coll_instance], ),
                                keepIntersections=ON, originalInstances=SUPPRESS, domain=GEOMETRY)


def clean_up_temps():
    a = mdb.models['Model-1'].rootAssembly
    a.deleteFeatures(('bone-1', 'ker-temp-1', 'coll-temp-1', 'bone-2',
                      'coll-block-1', 'coll-int-1', ))

    del mdb.models['Model-1'].parts['bone']
    del mdb.models['Model-1'].parts['coll-block-temp']
    del mdb.models['Model-1'].parts['coll-int']
    # del mdb.models['Model-1'].parts['coll-temp']
    # del mdb.models['Model-1'].parts['ker-temp']


def remove_redundant(flat_width, depth):

    session.viewports['Viewport: 1'].view.setValues(nearPlane=26.3811,
                                                    farPlane=33.191, width=13.1709, height=6.56253, cameraPosition=(
            2.06433, -16.1075, 25.0359), cameraUpVector=(-0.0529636, 0.956564,
                                                         0.286671), cameraTarget=(0.126557, 1.32854, 1.05097))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=26.039,
                                                    farPlane=33.5417, width=13.0001, height=6.47744, cameraPosition=(
            2.97123, -25.9106, 12.6085), cameraUpVector=(-0.0176719, 0.675817,
                                                         0.736858), cameraTarget=(0.128686, 1.30552, 1.02179))
    p = mdb.models['Model-1'].parts['merged-shell']
    v1 = p.vertices
    p.RemoveRedundantEntities(vertexList=(v1.findAt(coordinates=(0.0, 0.0, 0.0)),
                                          v1.findAt(coordinates=(0.0, flat_width, 0.0)), v1.findAt(coordinates=(0.0, flat_width, depth)), v1.findAt(coordinates=(0.0, 0.0, depth))))
    mdb.models['Model-1'].parts['merged-shell'].checkGeometry()


def float_equals(a, b):
    a = np.asarray(a)
    b = np.asarray(b)
    if a.size != b.size:
        return False
    return np.linalg.norm(a-b) < 1.0e-5


def array_all_equal(a, b):
    if a.size != b.size:
        return False
    else:
        a = a.astype(np.int)
        b = b.astype(np.int)
        return np.sum(np.abs(a-b)) == 0


def totuple(a):
    try:
        return tuple(totuple(i) for i in a)
    except TypeError:
        return a

def get_datum_key_from_axis(datums, direction1=None, direction2=None, direction3=None):
    if direction1 is not None:
        direction1 = np.asarray(direction1)
        direction1 /= np.linalg.norm(direction1)
    if direction2 is not None:
        direction2 = np.asarray(direction2)
        direction2 /= np.linalg.norm(direction2)
    if direction3 is not None:
        direction3 = np.asarray(direction3)
        direction3 /= np.linalg.norm(direction3)
    keys = datums.keys()
    for key in keys:
        try:
            if (direction1 is None or float_equals(np.asarray(datums[key].axis1.direction), direction1) or float_equals(np.asarray(datums[key].axis1.direction), -direction1)) and \
                    (direction2 is None or float_equals(np.asarray(datums[key].axis2.direction), direction2) or float_equals(np.asarray(datums[key].axis2.direction), -direction2)) and \
                    (direction3 is None or float_equals(np.asarray(datums[key].axis3.direction), direction3) or float_equals(np.asarray(datums[key].axis3.direction), -direction3)):
                return key
        except:
            pass

def get_datum_key_from_axis_and_xy_plane_normal(datums, direction, normal):
    normal = np.asarray(normal)
    normal /= np.linalg.norm(normal)
    keys = datums.keys()
    direction = np.asarray(direction)
    direction /= np.linalg.norm(direction)
    for key in keys:
        try:
            datum_normal = np.cross(np.asarray(datums[key].axis1.direction), np.asarray(datums[key].axis2.direction))
            datum_normal /= np.linalg.norm(datum_normal)
            if (float_equals(datum_normal, normal) or float_equals(datum_normal, -normal)) and \
                    float_equals(np.asarray(datums[key].axis1.direction), direction):
                return key
        except:
            pass


def get_datum_key_from_xy_plane_normal(datums, normal):
    normal = np.asarray(normal)
    normal /= np.linalg.norm(normal)
    keys = datums.keys()
    for key in keys:
        try:
            datum_normal = np.cross(np.asarray(datums[key].axis1.direction), np.asarray(datums[key].axis2.direction))
            datum_normal /= np.linalg.norm(datum_normal)
            if float_equals(datum_normal, normal) or float_equals(datum_normal, -normal):
                return key
        except:
            pass

def make_dynamic():
    p = mdb.models['Model-1'].parts['suture-interface']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    mdb.models['Model-1'].materials['bone'].Density(table=((1.95e-09, ), ))
    mdb.models['Model-1'].materials['keratin'].Density(table=((9.4e-10, ), ))
    mdb.models['Model-1'].materials['suture'].Density(table=((1.25e-9, ), ))
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    mdb.models['Model-1'].ImplicitDynamicsStep(name='load', previous='Initial',
                                               maintainAttributes=True, timePeriod=2.0, maxNumInc=1000,
                                               # initialInc=0.2, minInc=2e-05, nohaf=ON, nlgeom=ON)
                                               initialInc=2.0, minInc=2e-05, nohaf=ON, nlgeom=ON)
    # mdb.models['Model-1'].TimePoint(name='TimePoints-1', points=((0.4, ), (0.8, ),
    #                                                              (1.2, ), (1.6, ), (2.0, )))
    mdb.models['Model-1'].TimePoint(name='TimePoints-1', points=((2.0, ), ))
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(step='load')
    mdb.models['Model-1'].fieldOutputRequests['F-Output-1'].setValues(
        timePoint='TimePoints-1')
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=ON, bcs=ON,
                                                               predefinedFields=ON, connectors=ON, adaptiveMeshConstraints=OFF)
    mdb.models['Model-1'].SmoothStepAmplitude(name='Amp-1', timeSpan=STEP, data=((
                                                                                     0.0, 0.0), (2.0, 1.0)))
    mdb.models['Model-1'].loads['breath'].setValues(amplitude='Amp-1')

    session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=OFF, bcs=OFF,
                                                               predefinedFields=OFF, connectors=OFF)
    # mdb.jobs['Job-1'].writeInput(consistencyChecking=OFF)


def history_output():
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(step='load')
    regionDef=mdb.models['Model-1'].rootAssembly.allInstances['suture-interface-1'].sets['right-bone']
    mdb.models['Model-1'].HistoryOutputRequest(name='right-bone',
                                               createStepName='load', variables=('SENER', 'PENER', 'VENER', 'DMENER',
                                                                                 'ELKE', 'ELSE', 'ELPD', 'ELVD', 'EKEDEN', 'ECDDEN', 'EVDDEN', 'ALLCD',
                                                                                 'ALLKE', 'ALLPD', 'ALLSE', 'ALLVD', 'ETOTAL'), region=regionDef,
                                               sectionPoints=DEFAULT, rebar=EXCLUDE)
    mdb.models['Model-1'].historyOutputRequests['right-bone'].setValues(variables=(
        'ALLAE', 'ALLCD', 'ALLDC', 'ALLDMD', 'ALLFD', 'ALLIE', 'ALLKE',
        'ALLPD', 'ALLSE', 'ALLVD', 'ALLWK', 'ALLCW', 'ALLMW', 'ALLPW',
        'ETOTAL'))
    mdb.models['Model-1'].HistoryOutputRequest(name='left-bone',
                                               objectToCopy=mdb.models['Model-1'].historyOutputRequests['right-bone'],
                                               toStepName='load')
    mdb.models['Model-1'].HistoryOutputRequest(name='suture',
                                               objectToCopy=mdb.models['Model-1'].historyOutputRequests['left-bone'],
                                               toStepName='load')
    regionDef=mdb.models['Model-1'].rootAssembly.allInstances['suture-interface-1'].sets['left-bone']
    mdb.models['Model-1'].historyOutputRequests['left-bone'].setValues(
        region=regionDef)
    regionDef=mdb.models['Model-1'].rootAssembly.allInstances['suture-interface-1'].sets['suture']
    mdb.models['Model-1'].historyOutputRequests['suture'].setValues(
        region=regionDef)



AAA_automate_suture_interface_generation()