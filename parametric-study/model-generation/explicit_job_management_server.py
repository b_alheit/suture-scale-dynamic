import socket                   # Import socket module
import os


def start():

    port = 60001                    # Reserve a port for your service.
    s = socket.socket()             # Create a socket object
    host = socket.gethostname()     # Get local machine name
    s.bind((host, port))            # Bind to the port
    s.listen(5)                     # Now wait for client connection.

    running_jobs = 0
    job_queue = []

    print('Explicit server listening....')

    while True:
        conn, addr = s.accept()     # Establish connection with client.
        print('Explicit server got connection from', addr)
        data = conn.recv(1024)
        msg = data.decode('utf-8')
        print('Explicit server received:', msg)

        if msg == 'done' and len(job_queue) > 0:
                os.system("python run_abaqus_job_explicit.py " + job_queue.pop(0) + " &")
        elif msg == 'done':
            running_jobs -= 1
        else:
            job_queue.append(msg)

        if running_jobs == 0 and len(job_queue) > 0:
            os.system("python run_abaqus_job_explicit.py " + job_queue.pop(0) + " &")
            running_jobs += 1

        print('Current job_queue: ', job_queue)
        print('Running jobs: ', running_jobs)




        # filename='mytext.txt'
        # f = open(filename,'rb')
        # l = f.read(1024)
        # while (l):
        #     conn.send(l)
        #     print('Sent ',repr(l))
        #     l = f.read(1024)
        # f.close()

        # print('Done sending')
        # conn.send(b'Thank you for connecting')
        conn.close()

# start()