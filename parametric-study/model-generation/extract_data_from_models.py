import os
import shutil
import numpy as np

root_dir = os.getcwd()
models_dir = root_dir+"/models"

h_min = 0.
h_max = 1.
n_h = 5

t_min = 0.2
t_max = 0.3
n_t = 5

h_vals = np.linspace(h_min, h_max, n_h)
t_vals = np.linspace(t_min, t_max, n_t)

i_model = 1
n_models = n_h*n_t
n_inc = 5
p = np.empty((n_h, n_t, n_inc+1))
d = np.empty((n_h, n_t, n_inc+1))
h_grid = np.empty((n_h, n_t))
t_grid = np.empty((n_h, n_t))

for i_h in range(h_vals.size):
    for i_t in range(t_vals.size):
        h = h_vals[i_h]
        t = t_vals[i_t]
        print(i_model, " / ", n_models)
        model_dir_h = models_dir + "/h-" + str(h)

        model_dir = model_dir_h + "/t-" + str(t)

        command = "abaqus cae noGUI=extract_data.py -- " + ' '.join([model_dir])
        os.system(command)
        i_model += 1
        p[i_h, i_t, :] = np.load(model_dir+'/p.npy')
        d[i_h, i_t, :] = np.load(model_dir+'/d.npy')
        h_grid[i_h, i_t] = h
        t_grid[i_h, i_t] = t

print('P')
print(p[:, :, -1])
print('d')
print(d[:, :, -1])
np.save("p_models", p)
np.save("d_models", d)
np.save("h_grid", h_grid)
np.save("t_grid", t_grid)