subroutine vdload ( &
! Read only (unmodifiable)variables -
 nblock, ndim, stepTime, totalTime, &
 amplitude, curCoords, velocity, dirCos, jltyp, sname, &
! Write only (modifiable) variable -
 value )

include 'vaba_param.inc'

dimension curCoords(nblock,ndim), velocity(nblock,ndim), &
  dirCos(nblock,ndim,ndim), value(nblock)
character*80 sname
doubleprecision :: a, d, R, e1, e2, es, p0, v1, v2, pi, stepLength, f, tw, r_pos
stepLength = 0.0005d0
d=0.1d0
R = 1.5d0
v1 = 0.4d0
v2 = 0.4d0
e1 = 12000d0
e2 = 12000d0
pi = 3.14159265359d0
tw = 1d0

!d = d * stepTime/stepLength
a = (R*d)**0.5
es = 1d0/((1d0-v1**2)/e1 + (1d0-v2**2)/e2 )
f = 4d0*es*R**0.5d0*d**(3d0/2d0)/3d0
p0 = 3d0*f/(2d0*pi*a**2d0)


do km = 1, nblock
    r_pos = (curCoords(km, 1) ** 2d0 + (curCoords(km, 3) - 3d0*tw/2d0)**2d0)**0.5d0
    if (r_pos .leq. a) then
        value = p0*(1d0-(r_pos/a)**2d0)**0.5d0
    else
        value = 0d0
    end if
enddo

return
end