# -*- coding: mbcs -*-
# Do not delete the following import lines
from abaqus import *
from abaqusConstants import *
import __main__

def make_dynamic():
    p = mdb.models['Model-1'].parts['suture-interface']
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    mdb.models['Model-1'].materials['bone'].Density(table=((1.64e-09, ), ))
    mdb.models['Model-1'].materials['keratin'].Density(table=((9.4e-10, ), ))
    mdb.models['Model-1'].materials['suture'].Density(table=((9.6e-10, ), ))
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    mdb.models['Model-1'].ImplicitDynamicsStep(name='load', previous='Initial', 
        maintainAttributes=True, timePeriod=2.0, maxNumInc=1000, 
        initialInc=0.2, minInc=2e-05, nohaf=ON, nlgeom=ON)
    mdb.models['Model-1'].TimePoint(name='TimePoints-1', points=((0.4, ), (0.8, ), 
        (1.2, ), (1.6, ), (2.0, )))
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(step='load')
    mdb.models['Model-1'].fieldOutputRequests['F-Output-1'].setValues(
        timePoint='TimePoints-1')
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=ON, bcs=ON, 
        predefinedFields=ON, connectors=ON, adaptiveMeshConstraints=OFF)
    mdb.models['Model-1'].SmoothStepAmplitude(name='Amp-1', timeSpan=STEP, data=((
        0.0, 0.0), (2.0, 1.0)))
    mdb.models['Model-1'].loads['breath'].setValues(amplitude='Amp-1')

    session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=OFF, bcs=OFF,
        predefinedFields=OFF, connectors=OFF)
    mdb.jobs['Job-1'].writeInput(consistencyChecking=OFF)


def p_amp():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=ON, bcs=ON, 
        predefinedFields=ON, connectors=ON)
    mdb.models['Model-1'].loads['breath'].setValues(amplitude=UNSET)
    mdb.models['Model-1'].loads['breath'].setValues(amplitude='Amp-1')


