import os
import shutil
import numpy as np
import socket
from multiprocessing import Process
import standard_job_management_server as jms
import explicit_job_management_server as ejms
import time
from PIL import Image
from lxml import etree as et
from bs4 import BeautifulSoup as bs
soup = bs('<svg></svg>', 'xml')

root_dir = os.getcwd() + '/models'
pic_dir = os.getcwd() + '/pics'
if not os.path.exists(root_dir):
    os.system("mkdir " + root_dir)

x_left = 0.3
x_right = .68
y_bottom = 0.11
y_top = .8

# x_left = 0.45
# x_right = .55
# y_bottom = 0.22
# y_top = .6


h_min = 0.0
# h_min = 0.9
h_max = 0.9
n_h = 5

t_min = 0.15
t_max = 0.3
# t_max = 0.15
n_t = 5

h_vals = np.linspace(h_min, h_max, n_h)
t_vals = np.linspace(t_min, t_max, n_t)

i_model = 1
n_models = n_h*n_t

# time.sleep(5)
models_dir = root_dir+"/breathe"
port = 60000                    # Reserve a port for your service.


xml = et.parse(pic_dir+'/base.svg')

svg = xml.getroot()
soup = bs(open(pic_dir+'/base.svg').read(), 'xml')

print(soup)

# if os.path.exists(models_dir):
#     shutil.rmtree(models_dir)
# os.system("mkdir " + models_dir)
for ih in range(n_h):
    h = h_vals[ih]
    for it in range(n_t):
        t = t_vals[it]
        print(i_model, " / ", n_models)
        model_dir_h = models_dir + "/h-" + str(h) + "/t-" + str(t)

        out_path = pic_dir + "/h-" + str(h) + "t-" + str(t)
        # os.system("abaqus cae noGUI=take_picture.py -- " + ' '.join([out_path, model_dir_h]))
        # # os.system("abaqus cae script=take_picture.py -- " + ' '.join([out_path, model_dir_h]))
        # # os.system("abaqus cae guiTester=take_picture.py -- " + ' '.join([out_path, model_dir_h]))
        full_path = out_path
        # im = Image.open(full_path)
        # im_dims = im.size
        # crop_dims = (im_dims[0]*x_left, im_dims[1]*(1-y_top), im_dims[0]*x_right, im_dims[1]*(1-y_bottom))
        # print(crop_dims)
        # im2=im.crop(crop_dims)
        # os.system('rm ' + full_path)
        # im2.save(png_path)
        # os.system("convert " + ' '.join([png_path, svg_path]))
        # os.system('rm ' + png_path)
        # pic_xml = et.parse(svg_path)

        png_path = full_path+'.png'
        svg_path = full_path+'.svg'
        # pic_svg = xml.getroot()
        pic_svg = bs(open(svg_path).read(), 'xml')
        im = pic_svg.find('image')

        # print(pic_svg)
        # print(im)
        if i_model == 1:
            width = int(im['width'])
            height = int(im['height'])
        # im['width'] = 100
        # print(im)
        new_x = ih * width
        new_y = it * height
        print(new_x)
        print(new_y)
        im['x'] = ih * width
        im['y'] = (n_t-1-it) * height
        print(im)
        soup.find('svg').append(im)
        i_model += 1

s = soup.find('svg')
tot_width = n_h * width
tot_height = n_t * height
s['width'] = n_h * width
s['height'] = n_t * height
s['viewBox'] = "0 0 " + ' '.join([str(tot_width), str(tot_height)])
s['enable-background'] = "new 0 0 " + ' '.join([str(tot_width), str(tot_height)])
print(soup)
open(pic_dir+'/res.svg', 'w+').write(str(soup))
os.system("convert " + ' '.join([pic_dir+'/res.svg', pic_dir+'/res.png']))
