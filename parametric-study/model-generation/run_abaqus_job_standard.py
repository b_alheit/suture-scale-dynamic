import os
from multiprocessing import Process
import time
import subprocess
import signal
import numpy as np
import psutil
import sys
import socket

wrk_dir = sys.argv[-1]
os.chdir(wrk_dir)
pid = None

proc = subprocess.Popen(['abaqus', 'job=Job-1', 'user=umats.f', 'cpus=15'])

standard_started = False

while not standard_started:
    time.sleep(10)
    try:
        print("Checking if standard started")
        standard_started = 'Run standard' in open("Job-1.log", 'r').read()[:5000]
        print("Not started")

    except:
        print("Couldn't check file")
        pass

print("Standard started")
log_file = open("Job-1.log", 'r').read()
# start_time = log_file[log_file.find("Begin Abaqus/Standard Analysis\n") + len("Begin Abaqus/Standard Analysis\n"): log_file.find("\nRun standard")].split(" ")[4].split(":")
start_time = log_file[log_file.find("Begin Abaqus/Standard Analysis\n") + len("Begin Abaqus/Standard Analysis\n"): log_file.find("\nRun standard")]
start_time = start_time[start_time.find(":")-2:start_time.find(":")+6].split(":")
print("Start time: " + ':'.join(start_time))
print(start_time)
start_time = np.array([float(t) for t in start_time])
time.sleep(2)

time_diff = 1000

for proc in psutil.process_iter():
    if proc.name() == "standard":
        pro_str = str(proc)
        print(pro_str)
        # pro_str = pro_str[pro_str.find("started='")+len("started='2020-04-06 "):pro_str.find("')")].split(":")
        pro_str = pro_str[pro_str.find(":")-2:pro_str.find("')")].split(":")

        print(pro_str)
        pro_str = np.array([float(t) for t in pro_str])
        if np.linalg.norm(pro_str - start_time) < time_diff or pid is None:
            pid = proc.pid
            time_diff = np.linalg.norm(pro_str - start_time)


finished = False
# p = Process(target=start_simulation)
# p.start()
print("pid: ", pid)
while not finished:
    time.sleep(10)
    try:
        # print("Checking if finished")
        # print("pid: ", pid)

        finished = 'ANALYSIS SUMMARY' in open("Job-1.msg", 'r').read()[-5000:]
        # print("Not finished")

    except:
        print("Couldn't check file")
        pass


os.kill(pid, signal.SIGKILL)

s = socket.socket()             # Create a socket object
host = socket.gethostname()     # Get local machine name
port = 60000                    # Reserve a port for your service.

s.connect((host, port))
s.send(bytes("done", 'utf-8'))
print("Simulation finished")
s.close()
# p.terminate()
# p.join()