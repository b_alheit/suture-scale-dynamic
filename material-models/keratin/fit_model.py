import numpy as np
import tran_iso_model as ts
import matplotlib.lines as mlines
import matplotlib
import matplotlib.pyplot as plt
import scipy.optimize as op

Et = 0.7*1e9
El = 1.3*1e9
v = 0.4

data = np.genfromtxt("keratin.csv", delimiter=',')[2:-3, :]
data[:, 1::2] *= 1e6
labels = ["$10^{-5}$", '$10^{-3}$', '$10^{-1}$']
Ed = data[-1, 1]/data[-1, 0]
# print("Ed before: ", Ed)
n_plots = 3
Ediff = Et - Ed
data[:, 1::2] += data[:, 0::2] * Ediff

rates = [10**-5, 10**-3, 10**-1]

def model_stress(Et, El, v, beta, tau):
    e11s = data[:, 0::2]
    stress = np.empty(shape=e11s.shape)
    for i in range(n_plots):
        edot = rates[i]
        for j in range(stress.shape[0]):
            stress[j, i] = ts.uniaxial(beta, tau, El, Et, v, e11s[j, i], edot)

    return stress

def res(params):
    n_params = np.alen(params)
    beta = params[:int(-n_params/2)]
    tau = params[int(-n_params/2):]
    stress_model = model_stress(Et, El, v, beta, tau)
    stress_data = data[:, 1::2]
    return np.linalg.norm(stress_model-stress_data)**2
    # return np.linalg.norm((stress_model-stress_data)/stress_data)**2


# n = 8
# tau_start = 0.0000001
# tau_end = 1
# beta_start = 0.00001
# beta_end = 10.
# mult = 2
# # taus = tau_start * mult ** np.arange(0, n)
# # betas = beta_start * mult ** np.arange(0, n)
# taus = tau_start * np.geomspace(1, tau_end/tau_start, n)
# betas = beta_start * np.geomspace(1, beta_end/beta_start, n)
# res_min = -1
# tg, bg = np.meshgrid(taus, betas)
#
# n_samples = n**4
# num = 1
# for i in range(n):
#     for j in range(n):
#         for k in range(n):
#             for l in range(n):
#                 print(num, ' / ', n_samples, '  ', str(100*num/n_samples)[:5],"%")
#                 num += 1
#                 beta1, tau1, beta2, tau2 = bg[i, j], tg[i, j], bg[k, l], tg[k, l]
#                 params = np.concatenate(( np.array([beta1, beta2]), np.array([tau1, tau2])))
#                 res_current = res(params)
#                 if res_min < 0 or res_current < res_min:
#                     beta1_best = beta1
#                     tau1_best = tau1
#                     beta2_best = beta2
#                     tau2_best = tau2
#                     res_min = res_current


n = 30
tau_start = 0.0001
tau_end = 1000
beta_start = 0.00001
beta_end = 1000.
mult = 2
# taus = tau_start * mult ** np.arange(0, n)
# betas = beta_start * mult ** np.arange(0, n)
taus = tau_start * np.geomspace(1, tau_end/tau_start, n)
betas = beta_start * np.geomspace(1, beta_end/beta_start, n)
res_min = -1
tg, bg = np.meshgrid(taus, betas)

n_samples = n**2
num = 1
for i in range(n):
    for j in range(n):
        print(num, ' / ', n_samples, '  ', str(100*num/n_samples)[:5],"%")
        num += 1
        beta1, tau1 = bg[i, j], tg[i, j]
        params = np.concatenate((np.array([beta1]), np.array([tau1])))
        res_current = res(params)
        if res_min < 0 or res_current < res_min:
            beta1_best = beta1
            tau1_best = tau1
            # beta2_best = beta2
            # tau2_best = tau2
            res_min = res_current

# # beta[0] = 0.1
#
beta = np.array([beta1_best])
tau = np.array([tau1_best])
# tau = np.ones(2)
print('beta =', beta)
print('tau =', tau)

# beta = np.array([0.19306977, 0.19306977*10])
# tau = np.array([1.e1, 1.e-07])

# beta = np.array([1.e-05, 1.38949549e+00])
# tau = np.array([1., 1.])
# beta = np.array([1.389 * 7])
# tau = np.array([1. * 7])

params = np.concatenate((beta, tau))
n_params = np.alen(params)

result = op.minimize(res, params, bounds=tuple([(1.e-16, np.inf)]*n_params))
"""
      fun: 62274512377845.836
 hess_inv: <2x2 LbfgsInvHessProduct with dtype=float64>
      jac: array([ 8593750., 11718750.])
  message: b'CONVERGENCE: REL_REDUCTION_OF_F_<=_FACTR*EPSMCH'
     nfev: 39
      nit: 12
   status: 0
  success: True
        x: array([12.81158086, 11.27085686])
[12.81158086 11.27085686]

"""
print(result)
print(result.x)
params = result.x
beta = params[:int(-n_params/2)]
tau = params[int(-n_params/2):]
print("*******************")
stress = model_stress(Et, El, v, beta, tau)
strain = data[:, 0::2]

cmap = matplotlib.cm.get_cmap('cividis')
pad = 0.15
# colors = ["#" + str(hex(int((n_plots-i) * (255-pad)/n_plots)))[-2:]*3 for i in range(n_plots)]

legend_items = [
    mlines.Line2D([], [], color='black', linewidth=0, marker='x', mfc='none', label='Abaqus \nimplementation'),
    mlines.Line2D([], [], color='black', linewidth=0, marker='o', mfc='none', label='Test data'),
    mlines.Line2D([], [], color='black', label='Analytical \nsolution')
]

colors = [cmap(pad + (1-2*pad)*i/(n_plots-1.)) for i in range(n_plots)]
for i in range(n_plots):
    i = n_plots-i-1
    plt.plot(data[:, i*2], data[:, i*2 +1]*1e-6, mfc='none', linewidth=0, marker='o', color=colors[i])
    plt.plot(strain[:, i], stress[:, i]*1e-6, color=colors[i])
    legend_items.append(mlines.Line2D([], [], color=colors[i], linewidth=0, marker='s', markersize=19, label=str(labels[i]) + '/s'))

plt.legend(handles=legend_items)
plt.ylabel("Stress (MPa)")
plt.xlabel("Strain")
plt.grid()
plt.tight_layout()
plt.show()
