import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib

Et = 0.7*1e3
El = 1.3*1e3
v = 0.4

data = np.genfromtxt("keratin.csv", delimiter=',')[2:-3, :]
labels = ["$10^{-5}$", '$10^{-3}$', '$10^{-1}$']
Ed = data[-1, 1]/data[-1, 0]
print("Ed before: ", Ed)
Ediff = Et - Ed
data[:, 1::2] += data[:, 0::2] * Ediff
Ed = data[-1, 1]/data[-1, 0]
print("Ed after: ", Ed)
cmap = matplotlib.cm.get_cmap('cividis')
n_plots = np.alen(labels)
pad = 0.15
# colors = ["#" + str(hex(int((n_plots-i) * (255-pad)/n_plots)))[-2:]*3 for i in range(n_plots)]

legend_items = [
    mlines.Line2D([], [], color='black', linewidth=0, marker='x', mfc='none', label='Abaqus \nimplementation'),
    mlines.Line2D([], [], color='black', linewidth=0, marker='o', mfc='none', label='Test data'),
    mlines.Line2D([], [], color='black', label='Analytical \nsolution')
]

colors = [cmap(pad + (1-2*pad)*i/(n_plots-1.)) for i in range(n_plots)]
for i in range(n_plots):
    i = n_plots-i-1
    plt.plot(data[:, i*2], data[:, i*2 +1], mfc='none', linewidth=0, marker='o', color=colors[i])
    legend_items.append(mlines.Line2D([], [], color=colors[i], linewidth=0, marker='s', markersize=19, label=str(labels[i]) + '/s'))

plt.legend(handles=legend_items)
plt.ylabel("Stress (MPa)")
plt.xlabel("Strain")
plt.grid()
plt.show()