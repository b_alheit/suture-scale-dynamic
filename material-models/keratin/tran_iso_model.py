import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as op


def tran_iso(strain, t, El, Et, v, beta, tau):
    # mu = Et/(2.*(1+v))
    # S = 1./np.array([
    #     [Et, -Et/v, -El/v, 0, 0, 0],
    #     [-Et/v, Et, -El/v, 0, 0, 0],
    #     [-El/v, -El/v, El, 0, 0, 0],
    #     [0, 0, 0, mu, 0, 0],
    #     [0, 0, 0, 0, mu, 0],
    #     [0, 0, 0, 0, 0, mu],
    # ])
    #
    # C = np.linalg.inv(S)
    # eps = np.array([strain[0, 0], strain[1, 1], strain[2, 2], 2*strain[0, 1], 2*strain[0, 2], 2*strain[1, 2]])

    El = Et

    S = 1./np.array([
        [Et, -Et/v, -El/v],
        [-Et/v, Et, -El/v],
        [-El/v, -El/v, El]])

    C = np.linalg.inv(S)
    eps = np.array([strain[0, 0], strain[1, 1], strain[2, 2]])

    # sigma_inf = np.diag(np.matmul(C, eps))
    # sigma_tilde = np.eye(3, 3) * np.trace(sigma_inf)/3.
    # sigma_bar = sigma_inf - sigma_tilde
    # sigma = sigma_tilde + (1. + np.dot(beta, 1-np.exp(-t/tau))/t)*sigma_bar

    sigma_inf = np.diag(np.matmul(C, eps))
    sigma = (1. + np.dot(beta, 1-np.exp(-t/tau))/t)*sigma_inf
    return sigma


def uniaxial(beta, tau, El, Et, v, e11, e11dot):

    t = e11/e11dot

    def sig_2233(e2233):
        strain = np.diag([e11, e2233[0], e2233[1]])
        stress = tran_iso(strain, t, El, Et, v, beta, tau)
        return np.array([stress[1, 1], stress[2, 2]])

    e2233_guess = -v*e11*np.ones(2)
    e2233 = op.root(sig_2233, e2233_guess).x
    print("e22: ", e2233[0], "e33: ", e2233[1])
    strain = np.diag([e11, e2233[0], e2233[1]])
    stress = tran_iso(strain, t, El, Et, v, beta, tau)
    return stress[0, 0]
#
# El = 1.3e9
# Et = 0.7e9
# v = 0.4

