import numpy as np
import matplotlib.pyplot as plt

rates = [0.00057, 0.057]

for rate in rates:
    d11 = np.load("test_data/"+str(rate)+'-11-scaled.npy')
    d22 = np.load("test_data/"+str(rate)+'-22-scaled.npy')
    d11 = d11[d11[:,0] < 1.54, :]
    d22 = d22[d22[:,0] < 1.54, :]
    plt.plot(d11[:, 0], d11[:, 1])
    plt.plot(d22[:, 0], d22[:, 1])

plt.show()