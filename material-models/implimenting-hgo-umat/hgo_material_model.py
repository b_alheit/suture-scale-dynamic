import numpy as np


def s_dot_11(G, k1, k2, kap, lam, lam_dot, I_1, I_1_dot, I_4, I_4_dot, E, E_dot):

    return (2./3.) * G * (2*I_1*lam**(-3)*lam_dot - I_1_dot * lam**(-2)) + \
             + 2./3. * k1 * E * np.e ** (k2 * E **2) * (kap * (2 * I_1*lam**(-3)*lam_dot - I_1_dot * lam ** (-2)) + (1 - 3 * kap)*(2*I_4*lam**(-3)*lam_dot-I_4_dot*lam**(-2))) + \
             +2 * k1 * E_dot * np.e ** (k2 * E **2) * (1 + 2*k2*E**2)*(kap*(1 - I_1*lam**(-2)/3.) + (1-3*kap)*(1-I_4*lam**(-2)/3.))


def s_dot_22(G, k1, k2, kap, lam, lam_dot, I_1, I_1_dot, I_4, I_4_dot, E, E_dot):

    return -(2./3.) * G * I_1_dot + 2./3. * k1 * E * np.e ** (k2 * E **2) * (-kap * I_1_dot - (1 - 3 * kap)*I_4_dot) + \
           +2 * k1 * E_dot * np.e ** (k2 * E **2) * (1 + 2*k2*E**2)*(kap*(1 - I_1/3.) + (1-3*kap)*(-I_4/3.))


def s_dot_33(G, k1, k2, kap, lam, lam_dot, I_1, I_1_dot, I_4, I_4_dot, E, E_dot):

    return (2./3.) * G * (-2*I_1*lam*lam_dot - I_1_dot * lam**(2)) + \
           + 2./3. * k1 * E * np.e ** (k2 * E ** 2) * (kap * (-2 * I_1*lam*lam_dot - I_1_dot * lam ** 2) + (1 - 3 * kap)*(-2*I_4*lam*lam_dot-I_4_dot*lam**2)) + \
           +2 * k1 * E_dot * np.e ** (k2 * E **2) * (1 + 2*k2*E**2)*(kap*(1 - I_1*lam**2/3.) + (1-3*kap)*(-I_4*lam**2./3.))



def s_dot_s(G, k1, k2, kap, lam, lam_dot):
    I_1 = lam**2 + 1 + lam**(-2)
    I_1_dot = 2*lam*lam_dot - 2*lam**(-3)*lam_dot

    I_4 = lam**2
    I_4_dot = -2*lam*lam_dot

    E = kap * (I_1 - 3) + (1-3*kap)*(I_4 - 1)
    E_dot = kap * I_1_dot + (1-3*kap) * I_4_dot

    return s_dot_11(G, k1, k2, kap, lam, lam_dot, I_1, I_1_dot, I_4, I_4_dot, E, E_dot), \
           s_dot_22(G, k1, k2, kap, lam, lam_dot, I_1, I_1_dot, I_4, I_4_dot, E, E_dot), \
           s_dot_33(G, k1, k2, kap, lam, lam_dot, I_1, I_1_dot, I_4, I_4_dot, E, E_dot)



def N_HGO_viso(G, k1, k2, kap, lam, lam_dot, beta, tau):

    if np.abs(lam_dot) < 1.e-12:
        t = 1
        lam_t = lambda T: 1 + (lam-1)*T
    else:
        lam_t = lambda T: 1 + lam_dot*T
        t = (lam - 1) /lam_dot

    I_1_t = lambda T: lam_t(T)**2 + 1 + lam_t(T)**(-2)
    I_1_dot_t = lambda T: 2*lam_t(T)*lam_dot - 2*lam_t(T)**(-3)*lam_dot

    I_4_t = lambda T: lam_t(T)**2
    I_4_dot_t = lambda T: 2*lam_t(T)*lam_dot

    E_t = lambda T: kap * (I_1_t(T) - 3) + (1-3*kap)*(I_4_t(T) - 1)
    E_dot_t = lambda T: kap * I_1_dot_t(T) + (1-3*kap) * I_4_dot_t(T)

    n_m = np.alen(beta)
    E = E_t(t)

    s11 = 2*G*(1-lam**-4) + 2 * k1 * E * np.e ** (k2 * E ** 2) * (kap * (1 - lam ** -4) + (1 - 3*kap))
    s22 = 2*G*(1-lam**-2) + 2 * k1 * E * np.e ** (k2 * E ** 2) * kap * (1 - lam ** -2)

    if np.abs(t) > 1.e-13:
        for i in range(n_m):
            func11 = lambda T: np.exp((T-t)/tau[i])*s_dot_11(G, k1, k2, kap, lam_t(T), lam_dot, I_1_t(T), I_1_dot_t(T), I_4_t(T), I_4_dot_t(T), E_t(T), E_dot_t(T))
            func33 = lambda T: np.exp((T-t)/tau[i])*s_dot_33(G, k1, k2, kap, lam_t(T), lam_dot, I_1_t(T), I_1_dot_t(T), I_4_t(T), I_4_dot_t(T), E_t(T), E_dot_t(T))
            # i1 = integrate(func11, 0, t, 3, t/100)
            # i2 = -lam**-4*integrate(func33, 0, t, 3, t/100)
            s11 += beta[i] * (integrate(func11, 0, t, 5, t) - lam**-4*integrate(func33, 0, t, 5, t))

        for i in range(n_m):
            func22 = lambda T: np.exp((T-t)/tau[i])*s_dot_22(G, k1, k2, kap, lam_t(T), lam_dot, I_1_t(T), I_1_dot_t(T), I_4_t(T), I_4_dot_t(T), E_t(T), E_dot_t(T))
            func33 = lambda T: np.exp((T-t)/tau[i])*s_dot_33(G, k1, k2, kap, lam_t(T), lam_dot, I_1_t(T), I_1_dot_t(T), I_4_t(T), I_4_dot_t(T), E_t(T), E_dot_t(T))
            # i1 = integrate(func22, 0, t, 5, t/100)
            # i2 = - lam**-2*integrate(func33, 0, t, 5, t/100)
            s22 += beta[i] * (integrate(func22, 0, t, 5, t) - lam**-2*integrate(func33, 0, t, 5, t))

    return s11*lam, s22


def N_HGO_viso_vec_11(G, k1, k2, kap, lam_vec, lam_dot, beta, tau):

    N_vals = np.alen(lam_vec)
    s11 = np.zeros(N_vals)
    for val in range(N_vals):
        lam = lam_vec[val]
        if np.abs(lam_dot) < 1.e-12:
            t = 1
            lam_t = lambda T: 1 + (lam-1)*T
        else:
            lam_t = lambda T: 1 + lam_dot*T
            t = (lam - 1) /lam_dot

        I_1_t = lambda T: lam_t(T)**2 + 1 + lam_t(T)**(-2)
        I_1_dot_t = lambda T: 2*lam_t(T)*lam_dot - 2*lam_t(T)**(-3)*lam_dot

        I_4_t = lambda T: lam_t(T)**2
        I_4_dot_t = lambda T: 2*lam_t(T)*lam_dot

        E_t = lambda T: kap * (I_1_t(T) - 3) + (1-3*kap)*(I_4_t(T) - 1)
        E_dot_t = lambda T: kap * I_1_dot_t(T) + (1-3*kap) * I_4_dot_t(T)

        n_m = np.alen(beta)
        E = E_t(t)

        s11[val] = 2*G*(1-lam**-4) + 2 * k1 * E * np.e ** (k2 * E ** 2) * (kap * (1 - lam ** -4) + (1 - 3*kap))

        if np.abs(t) > 1.e-13:
            for i in range(n_m):
                func11 = lambda T: np.exp((T-t)/tau[i])*s_dot_11(G, k1, k2, kap, lam_t(T), lam_dot, I_1_t(T), I_1_dot_t(T), I_4_t(T), I_4_dot_t(T), E_t(T), E_dot_t(T))
                func33 = lambda T: np.exp((T-t)/tau[i])*s_dot_33(G, k1, k2, kap, lam_t(T), lam_dot, I_1_t(T), I_1_dot_t(T), I_4_t(T), I_4_dot_t(T), E_t(T), E_dot_t(T))
                s11[val] += beta[i] * (integrate(func11, 0, t, 3, t/50) - lam**-4*integrate(func33, 0, t, 3, t/50))

    return s11*lam_vec

def N_HGO_viso_vec_22(G, k1, k2, kap, lam_vec, lam_dot, beta, tau):

    N_vals = np.alen(lam_vec)
    s22 = np.zeros(N_vals)
    for val in range(N_vals):
        lam = lam_vec[val]
        if np.abs(lam_dot) < 1.e-12:
            t = 1
            lam_t = lambda T: 1 + (lam-1)*T
        else:
            lam_t = lambda T: 1 + lam_dot*T
            t = (lam - 1) /lam_dot

        I_1_t = lambda T: lam_t(T)**2 + 1 + lam_t(T)**(-2)
        I_1_dot_t = lambda T: 2*lam_t(T)*lam_dot - 2*lam_t(T)**(-3)*lam_dot

        I_4_t = lambda T: lam_t(T)**2
        I_4_dot_t = lambda T: 2*lam_t(T)*lam_dot

        E_t = lambda T: kap * (I_1_t(T) - 3) + (1-3*kap)*(I_4_t(T) - 1)
        E_dot_t = lambda T: kap * I_1_dot_t(T) + (1-3*kap) * I_4_dot_t(T)

        n_m = np.alen(beta)
        E = E_t(t)

        s22[val] = 2*G*(1-lam**-2) + 2 * k1 * E * np.e ** (k2 * E ** 2) * kap * (1 - lam ** -2)

        if np.abs(t) > 1.e-13:
            for i in range(n_m):
                func22 = lambda T: np.exp((T-t)/tau[i])*s_dot_22(G, k1, k2, kap, lam_t(T), lam_dot, I_1_t(T), I_1_dot_t(T), I_4_t(T), I_4_dot_t(T), E_t(T), E_dot_t(T))
                func33 = lambda T: np.exp((T-t)/tau[i])*s_dot_33(G, k1, k2, kap, lam_t(T), lam_dot, I_1_t(T), I_1_dot_t(T), I_4_t(T), I_4_dot_t(T), E_t(T), E_dot_t(T))
                s22[val] += beta[i] * (integrate(func22, 0, t, 3, t/50) - lam**-2*integrate(func33, 0, t, 3, t/50))

    return s22

def integrate(func, t0, t1, n_gp, dt):
    # TODO
    n_incs = int((t1-t0)/dt+1)
    incs = np.linspace(t0, t1, n_incs)
    res = func(0)*0
    for i in range(n_incs-1):
        res += gauss_quad(func, incs[i], incs[i+1], n_gp)

    return res


def gauss_quad(func, t0, t1, n_gp):
    if n_gp == 1:
        xi = np.array([0])
        weights = np.array([2])
    elif n_gp == 2:
        xi = np.array([-1/(3**0.5), 1/(3**0.5)])
        weights = np.array([1, 1])
    elif n_gp == 3:
        xi = np.array([-(3/5)**0.5, 0, (3/5)**0.5])
        weights = np.array([5/9, 8/9, 5/9])
    elif n_gp == 5:
        xi = np.array([- 0.538469,
                       0.538469,
                       -0.906180,
                       0.906180,
                       0])

        weights = np.array([(322 + 13*(70**0.5))/900,
                            (322 + 13*(70**0.5))/900,
                            (322 - 13*(70**0.5))/900,
                            (322 - 13*(70**0.5))/900,
                            128/225])

    else:
        print("Invalid input for linear quadrature. Gauss points set to 5 for accuracy.")
        n_gp = 5
        xi = np.array([- 0.538469,
                       0.538469,
                       -0.906180,
                       0.906180,
                       0])

        weights = np.array([(322 + 13*(70**0.5))/900,
                            (322 + 13*(70**0.5))/900,
                            (322 - 13*(70**0.5))/900,
                            (322 - 13*(70**0.5))/900,
                            128/225])

    def x(xi):
        return (t0 + t1) / 2 + xi * (t1 - t0) / 2

    val = 0 * func(0)
    J = (t1 - t0) / 2

    for i in range(n_gp):
        # j = J
        # ints = integrand(x(xi[i]))
        # w = weights[i]
        val += J * (func(x(xi[i])) * weights[i])

    return val