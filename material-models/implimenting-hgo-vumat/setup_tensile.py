# -*- coding: mbcs -*-
# Do not delete the following import lines
from abaqus import *
from abaqusConstants import *
import __main__
import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import optimization
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior
import sys
import os
import socket

os.chdir('/home/cerecam/Benjamin_Alheit/simulations/PhD/suture-scale/dynamic/material-models/implimenting-hgo-umat')

rate = float(sys.argv[-1])
strain = float(sys.argv[-2])

n_print = int(sys.argv[-3])
n_steps = int(sys.argv[-4])

t_end = strain/rate

def set_up_job():
    openMdb(
    pathName='/home/cerecam/Benjamin_Alheit/simulations/PhD/suture-scale/dynamic/material-models/implimenting-hgo-umat/test-block.cae')
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        adaptiveMeshConstraints=ON)
    mdb.models['Model-1'].steps['load'].setValues(timePeriod=t_end,
        initialInc=t_end/float(n_steps), minInc=t_end/100000000., maxInc=t_end/float(n_steps))
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=ON, bcs=ON, 
        predefinedFields=ON, connectors=ON, adaptiveMeshConstraints=OFF)
    mdb.models['Model-1'].boundaryConditions['pull'].setValues(u1=strain)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=OFF, bcs=OFF, 
        predefinedFields=OFF, connectors=OFF)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        adaptiveMeshConstraints=ON)
    mdb.models['Model-1'].fieldOutputRequests['F-Output-1'].setValues(
        numIntervals=n_print)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        adaptiveMeshConstraints=OFF)
    mdb.jobs['Job-1'].writeInput(consistencyChecking=OFF)
    mdb.save()



set_up_job()
