SUBROUTINE ORIENT(T,NOEL,NPT,LAYER,KSPT,COORDS,BASIS, &
        ORNAME,NNODES,CNODES,JNNUM)

    INCLUDE 'ABA_PARAM.INC'

    CHARACTER*80 ORNAME

    DIMENSION T(3,3),COORDS(3),BASIS(3,3),CNODES(3,NNODES)
    DIMENSION JNNUM(NNODES)

    T = 0

    !    T(1, 1) = 1
    !    T(2, 2) = 1
    !    T(3, 3) = 1

    T(1, 1) = 1
    !    T(2, 1) = 1
    T(2, 2) = 1
    T(3, 3) = 1

    !    T(:, 1) = ...
    !    T(:, 2) = ...
    !    T(:, 3) = ...


    RETURN
END

SUBROUTINE UVARM(UVAR,DIRECT,T,TIME,DTIME,CMNAME,ORNAME, &
        NUVARM,NOEL,NPT,LAYER,KSPT,KSTEP,KINC,NDI,NSHR,COORD, &
        JMAC,JMATYP,MATLAYO,LACCFLA)
    INCLUDE 'ABA_PARAM.INC'

    CHARACTER*80 CMNAME,ORNAME
    CHARACTER*3 FLGRAY(15)
    DIMENSION UVAR(NUVARM),DIRECT(3,3),T(3,3),TIME(2)
    DIMENSION ARRAY(15),JARRAY(15),JMAC(*),JMATYP(*),COORD(*)

    doubleprecision :: F(3, 3), a(3), a0(3), fibre_stretch, jacobian

    !     The dimensions of the variables FLGRAY, ARRAY and JARRAY
    !     must be set equal to or greater than 15.
    CALL GETVRM('DG',ARRAY,JARRAY,FLGRAY,JRCD,JMAC,JMATYP, &
            MATLAYO,LACCFLA)
    F(1, 1) = ARRAY(1)
    F(2, 2) = ARRAY(2)
    F(3, 3) = ARRAY(3)
    F(1, 2) = ARRAY(4)
    F(1, 3) = ARRAY(5)
    F(2, 3) = ARRAY(6)
    F(2, 1) = ARRAY(7)
    F(3, 1) = ARRAY(8)
    F(3, 2) = ARRAY(9)

    print*, 'KINC ', KINC, '\t NPT', NPT
    call print_matrix(F, 3, 3)
    print*,
    print*,

    !    a0(1) = DIRECT(1, 1)
    !    a0(2) = DIRECT(2, 1)
    !    a0(3) = DIRECT(3, 1)
    !
    !    a = F(:, 1)
    !    fibre_stretch = dot_product(a, a) ** 0.5
    !    a = a0 * fibre_stretch
    !    jacobian = F(1, 1) * (F(2, 2) * F(3, 3) - F(2, 3) * F(3, 2)) &
    !            - F(1, 2) * (F(2, 1) * F(3, 3) - F(2, 3) * F(3, 1)) &
    !            + F(1, 3) * (F(2, 1) * F(3, 2) - F(2, 2) * F(3, 1))
    !
    !    UVAR(1:3) = a
    !    UVAR(4) = fibre_stretch
    !    UVAR(5) = jacobian

    RETURN
END

subroutine vumat( &
!C Read only (unmodifiable)variables -
  nblock, ndir, nshr, nstatev, nfieldv, nprops, lanneal, &
  stepTime, totalTime, dt, cmname, coordMp, charLength, &
  props, density, strainInc, relSpinInc, &
  tempOld, stretchOld, defgradOld, fieldOld, &
  stressOld, stateOld, enerInternOld, enerInelasOld, &
  tempNew, stretchNew, defgradNew, fieldNew, &
!C Write only (modifiable) variables -
  stressNew, stateNew, enerInternNew, enerInelasNew )

include 'vaba_param.inc'

dimension props(nprops), density(nblock), coordMp(nblock,*), &
  charLength(nblock), strainInc(nblock,ndir+nshr),&
  relSpinInc(nblock,nshr), tempOld(nblock),&
  stretchOld(nblock,ndir+nshr),&
  defgradOld(nblock,ndir+nshr+nshr),&
  fieldOld(nblock,nfieldv), stressOld(nblock,ndir+nshr),&
  stateOld(nblock,nstatev), enerInternOld(nblock),&
  enerInelasOld(nblock), tempNew(nblock),&
  stretchNew(nblock,ndir+nshr),&
  defgradNew(nblock,ndir+nshr+nshr),&
  fieldNew(nblock,nfieldv),&
  stressNew(nblock,ndir+nshr), stateNew(nblock,nstatev),&
  enerInternNew(nblock), enerInelasNew(nblock)

character*80 cmname
doubleprecision :: stress_dummy(6), ddsdde_dummy(6, 6), DFGRD0(3, 3), DFGRD1(3, 3), state_dummy(nstatev)
integer :: n


!defgradNew(nblock,ndir+nshr+nshr) Deformation gradient tensor at each material point at the beginning of the increment.
!Stored in 3D as (11,22 ,33 ,12 ,23 ,31 ,21 ,32 , 13) and in 2D as (, , , , )

!defgradOld(nblock,ndir+nshr+nshr) Deformation gradient tensor at each material point at the beginning of the increment.
!Stored in 3D as (11,22 ,33 ,12 ,23 ,31 ,21 ,32 , 13) and in 2D as (, , , , )
IF (cmname(1:9) .EQ. 'VISCO_HGO') THEN
    n = (nprops - 5)/2
    do 100 km = 1,nblock
!        TODO subroutine ve_to_vs
!        TODO subroutine vs_to_ve
!        TODO subroutine ve_to_mat
!        TODO subroutine mat_to_ve

        call ve_to_matns(defgradOld(km, :), DFGRD0)
        call ve_to_matns(defgradNew(km, :), DFGRD1)
        state_dummy = stateOld(km, :)
        call umat_hgo_visco(PROPS(1), PROPS(2), PROPS(3), PROPS(4), PROPS(5), DFGRD0, DFGRD1, n, &
                PROPS(6:6+n-1), PROPS(6+n:6+2*n-1), state_dummy, dt, stress_dummy, ddsdde_dummy)
        call vs_to_ve(stress_dummy, stressNew(km, :))

        stateNew(km, :) = state_dummy
    100 continue
END IF

return
end

SUBROUTINE UMAT(STRESS,STATEV,DDSDDE,SSE,SPD,SCD, &
        RPL,DDSDDT,DRPLDE,DRPLDT,&
        STRAN,DSTRAN,TIME,DTIME,TEMP,DTEMP,PREDEF,DPRED,CMNAME, &
        NDI,NSHR,NTENS,NSTATV,PROPS,NPROPS,COORDS,DROT,PNEWDT,&
        CELENT,DFGRD0,DFGRD1,NOEL,NPT,LAYER,KSPT,KSTEP,KINC)

    INCLUDE 'ABA_PARAM.INC'

    CHARACTER*80 CMNAME
    DIMENSION STRESS(NTENS),STATEV(NSTATV), &
            DDSDDE(NTENS,NTENS),DDSDDT(NTENS),DRPLDE(NTENS), &
            STRAN(NTENS),DSTRAN(NTENS),TIME(2),PREDEF(1),DPRED(1), &
            PROPS(NPROPS),COORDS(3),DROT(3,3),DFGRD0(3,3),DFGRD1(3,3)
    integer :: n
    doubleprecision :: dt
    !    CHARACTER(len=255) :: cwd
    !    CALL getcwd(cwd)
    !    print*, TRIM(cwd)
    print*, "*********************"
    print*, 'STEP=', KSTEP, ' NPT=', NPT, ' KINC=', KINC

    IF (CMNAME(1:5) .EQ. 'VISCO') THEN

        IF (CMNAME(1:9) .EQ. 'VISCO_HGO') THEN
            n = (NPROPS - 5)/2
            dt = TIME(2) - TIME(1)
            !            print*, "DTIME"
            !            print*, DTIME
            !            print*, "dt top"
            !            print*, DTIME
            call umat_hgo_visco(PROPS(1), PROPS(2), PROPS(3), PROPS(4), PROPS(5), DFGRD0, DFGRD1, n, &
                    PROPS(6:6+n-1), PROPS(6+n:6+2*n-1), STATEV, DTIME, STRESS, DDSDDE)
        END IF

    ELSE
        STRESS = 0
        DDSDDE = 0

        IF (CMNAME(1:2) .EQ. 'NH') THEN
            call umat_nh_general(PROPS(1), PROPS(2), DFGRD1, STRESS, DDSDDE)
            call umat_nh_general(PROPS(1), PROPS(2), DFGRD1, STRESS, DDSDDE)
        ELSE IF(CMNAME(1:2) .EQ. 'MR') THEN
            call umat_mr_general(PROPS(1), PROPS(2), PROPS(3), DFGRD1, STRESS, DDSDDE)
            !        call umat_mooney_rivlin_specific(PROPS(1), PROPS(2), PROPS(3), DFGRD1, STRESS, DDSDDE)
        ELSE IF(CMNAME(1:2) .EQ. 'PL') THEN
            !        STRESS = 0
            !        DDSDDE = 0
            call poly_general(PROPS(1), PROPS(2), PROPS(3), PROPS(4), PROPS(5), PROPS(6), PROPS(7), DFGRD1, STRESS, DDSDDE)
            !        call poly_general_tran_iso(PROPS(1), PROPS(2), PROPS(3), PROPS(4), PROPS(5), PROPS(6), PROPS(7), DFGRD1, STRESS, DDSDDE)
        ELSE IF(CMNAME(1:3) .EQ. 'HGO') THEN
            !        STRESS = 0
            !        DDSDDE = 0
            call umat_hgo(PROPS(1), PROPS(2), PROPS(3), PROPS(4), PROPS(5), DFGRD1, STRESS, DDSDDE)
        END IF
    END IF

    RETURN
END


subroutine umat_nh_general(G, D, F, s, c)
    doubleprecision, intent(in) :: F(3, 3), G, D

    doubleprecision :: dphidi(2), d2phidi2(3), dphidj, d2phidj2

    doubleprecision :: jac, det3

    doubleprecision, intent(out) :: c(6, 6), s(6)

    jac = det3(F)

    dphidi = 0
    dphidi(1) = G

    d2phidi2 = 0

    dphidj = (2./D)*(jac-1.)
    d2phidj2 = 2./D

    call near_incompressible_isotropic(dphidi, d2phidi2, dphidj, d2phidj2, F, s, c)

endsubroutine umat_nh_general

subroutine umat_mr_general(G, H, D, F, s, c)
    doubleprecision, intent(in) :: F(3, 3), G, D, H

    doubleprecision :: dphidi(2), d2phidi2(3), dphidj, d2phidj2

    doubleprecision :: jac, det3

    doubleprecision, intent(out) :: c(6, 6), s(6)

    jac = det3(F)

    dphidi(1) = G
    dphidi(2) = H

    d2phidi2 = 0

    dphidj = (2./D)*(jac-1.)
    d2phidj2 = 2./D

    call near_incompressible_isotropic(dphidi, d2phidi2, dphidj, d2phidj2, F, s, c)

endsubroutine umat_mr_general

subroutine poly_general_tran_iso(c10, c01, c11, c20, c02, d1, d2, F, s, c)
    doubleprecision, intent(in) :: F(3, 3), c10, c01, c11, c20, c02, d1, d2

    doubleprecision :: dphidi(4), d2phidi2(10), dphidj, d2phidj2, i1, i2, trB, B(3, 3)

    doubleprecision :: j, det3, tr3

    doubleprecision, intent(out) :: c(6, 6), s(6)

    J = det3(F)
    B = matmul(F, transpose(F)) * J **(-2./3.)
    trB = tr3(B)

    i1 = trB
    i2 = 0.5*(trB**2 - tr3(matmul(B, B)))

    dphidi = 0
    dphidi(1) = c10 + c11*(i2-3.) + 2*c20*(i1-3.)
    dphidi(2) = c01 + c11*(i1-3.) + 2*c02*(i2-3.)

    d2phidi2 = 0
    d2phidi2(1) = 2*c20
    d2phidi2(2) = 2*c02
    d2phidi2(5) = c11

    dphidj = (2./d1)*(J-1.) + (4./d2)*(J-1.)**3
    d2phidj2 = (2./d1) + (12./d2)*(J-1.)**2

    call near_incompressible_transversely_isotropic(dphidi, d2phidi2, dphidj, d2phidj2, F, s, c)

endsubroutine poly_general_tran_iso

subroutine poly_general(c10, c01, c11, c20, c02, d1, d2, F, s, c)
    doubleprecision, intent(in) :: F(3, 3), c10, c01, c11, c20, c02, d1, d2

    doubleprecision :: dphidi(2), d2phidi2(3), dphidj, d2phidj2, i1, i2, trB, B(3, 3)

    doubleprecision :: j, det3, tr3

    doubleprecision, intent(out) :: c(6, 6), s(6)


    J = det3(F)
    B = matmul(F, transpose(F)) * J **(-2./3.)
    trB = tr3(B)

    i1 = trB
    i2 = 0.5*(trB**2 - tr3(matmul(B, B)))

    dphidi(1) = c10 + c11*(i2-3.) + 2*c20*(i1-3.)
    dphidi(2) = c01 + c11*(i1-3.) + 2*c02*(i2-3.)

    d2phidi2(1) = 2*c20
    d2phidi2(2) = 2*c02
    d2phidi2(3) = c11

    dphidj = (2./d1)*(J-1.) + (4./d2)*(J-1.)**3
    d2phidj2 = (2./d1) + (12./d2)*(J-1.)**2

    call near_incompressible_isotropic(dphidi, d2phidi2, dphidj, d2phidj2, F, s, c)

endsubroutine poly_general

subroutine umat_hgo(c1, d, k1, k2, kap, F, s, c)
    doubleprecision, intent(in) :: c1, d, k1, k2, kap, F(3, 3)

    doubleprecision :: J, det3, I1, tr3, I4, E, ht4a
    doubleprecision :: dphidi(4), d2phidi2(10), dphidj, d2phidj2, trB, B(3, 3)

    doubleprecision :: siso(6), svol(6), ciso(6, 6), cvol(6, 6)

    doubleprecision, intent(out) :: s(6), c(6, 6)

    J = det3(F)
    B = matmul(F, transpose(F)) * J **(-2.d0/3.d0)
    I1 = tr3(B)
    I4 = dot_product(F(:, 1), F(:, 1))* J **(-2.d0/3.d0)
    E = kap*(I1-3.d0) + (1.d0-3.d0*kap)*(I4-1.d0)

    ht4a = 5.d-1 + sign(5.d-1,E + 2.d-16)
    E = max(E, 0.d0)

    dphidi = 0
    dphidi(1) = c1 + k1*kap*E*exp(k2*E**2)
    dphidi(3) = k1*(1.d0-3.d0*kap)*E*exp(k2*E**2)

    d2phidi2 = 0
    d2phidi2(1) = k1*kap**2*exp(k2*E**2)*(ht4a + 2.d0 *k2 * E **2)
    d2phidi2(3) = k1*(1.d0-3.d0*kap)**2*exp(k2*E**2)*(ht4a + 2.d0 *k2 * E **2)
    d2phidi2(6) = k1*(1.d0-3.d0*kap)*kap*exp(k2*E**2)*(ht4a + 2 *k2 * E **2)

    dphidj = (J-1.d0/J)/d
    d2phidj2 = (1.d0+1.d0/J/J)/d

    siso = 0
    svol = 0
    ciso = 0
    cvol = 0
    !        call near_incompressible_transversely_isotropic(dphidi, d2phidi2, dphidj, d2phidj2, F, s, c)
    call near_incompressible_transversely_isotropic_seperate(dphidi, d2phidi2, dphidj, d2phidj2, F, s, c, siso, ciso, svol, cvol)

    print*, "F"
    call print_matrix(F, 3, 3)
    print*, "siso"
    print*, siso
    print*, "svol"
    print*, svol
    print*, "s"
    print*, s
    print*,
    print*,


endsubroutine umat_hgo

subroutine umat_hgo_visco(c1, d, k1, k2, kap, F0, F, n, beta, tau, state, dt, s, c)
    doubleprecision, intent(in) :: c1, d, k1, k2, kap, F0(3, 3), F(3, 3), dt

    doubleprecision :: J, det3, I1, tr3, I4, E, ht4a
    doubleprecision :: dphidi(4), d2phidi2(10), dphidj, d2phidj2, trB, B(3, 3)

    integer, intent(in) :: n
    doubleprecision, intent(in) :: beta(n), tau(n)

    doubleprecision, intent(inout) :: state((n+1)*6)

    doubleprecision, intent(out) :: s(6), c(6, 6)

    J = det3(F)
    B = matmul(F, transpose(F)) * J **(-2.d0/3.d0)
    I1 = tr3(B)
    I4 = dot_product(F(:, 1), F(:, 1))* J **(-2.d0/3.d0)
    E = kap*(I1-3.d0) + (1.d0-3.d0*kap)*(I4-1.d0)

    ht4a = 5.d-1 + sign(5.d-1,E + 2.d-16)
    E = max(E, 0.d0)

    dphidi = 0
    dphidi(1) = c1 + k1*kap*E*exp(k2*E**2)
    dphidi(3) = k1*(1.d0-3.d0*kap)*E*exp(k2*E**2)

    d2phidi2 = 0
    d2phidi2(1) = k1*kap**2*exp(k2*E**2)*(ht4a + 2.d0 *k2 * E **2)
    d2phidi2(3) = k1*(1.d0-3.d0*kap)**2*exp(k2*E**2)*(ht4a + 2.d0 *k2 * E **2)
    d2phidi2(6) = k1*(1.d0-3.d0*kap)*kap*exp(k2*E**2)*(ht4a + 2.d0 *k2 * E **2)

    dphidj = (J-1.d0/J)/d
    d2phidj2 = (1.d0+1.d0/J/J)/d

    !    call near_incompressible_transversely_isotropic(dphidi, d2phidi2, dphidj, d2phidj2, F, s, c)
    s = 0
    c = 0
    call implement_visco_elasticity(dphidi, d2phidi2, dphidj, d2phidj2, n, beta, tau, state, F0, F, dt, s, c)

endsubroutine umat_hgo_visco


subroutine implement_visco_elasticity(dphidi, d2phidi2, dphidj, d2phidj2, n, beta, tau, state, F0, F1, dt, sf, cf)
    doubleprecision, intent(in) :: dphidi(4), d2phidi2(10), dphidj, d2phidj2, F0(3, 3), F1(3, 3), dt
    integer, intent(in) :: n
    doubleprecision, intent(in) :: beta(n), tau(n)

    doubleprecision, intent(inout) :: state((n+1)*6)

    doubleprecision :: s(6), svalpa(6), sv(6), siso(6), svol(6), c(6, 6), ciso(6, 6), cvol(6, 6), cv(6, 6), xi(n)
    doubleprecision :: xialpha, xi_bar, G(3, 3), F0inv(3, 3), detGinv, det3, h(3, 3), h_alpha(3, 3), s_v_alpha(3, 3)
    doubleprecision :: sig_inf_n(3, 3), sig_inf_n1(3, 3), h_other(6), jac
    integer :: alpha

    doubleprecision, intent(out) :: sf(6), cf(6, 6)

    s = 0
    siso = 0
    svol = 0
    c = 0
    ciso = 0
    cvol = 0
    !    print*, "dt"
    !    print*, dt
    !    print*, "dphidi"
    !    print*, dphidi
    !    print*, "d2phidi2"
    !    print*, d2phidi2
    !    print*, "dphidj"
    !    print*, dphidj
    !    print*, "d2phidj2"
    !    print*, d2phidj2
    !    print*, "F1"
    !    call print_matrix(F1, 3, 3)

    call near_incompressible_transversely_isotropic_seperate(dphidi, d2phidi2, dphidj, d2phidj2, F1, s, c, siso, ciso, svol, cvol)

    F0inv = 0
    call matinv3(F0, F0inv)

    G = matmul(F1, F0inv)
    detGinv = 1.d0/det3(G)
    jac = det3(F1)

    xi = exp(-dt/(2.d0*tau))
    xi_bar = dot_product(beta, xi)
    sv = 0
    s_v_alpha = 0
    call voigt_to_mat(siso, sig_inf_n1)
    call voigt_to_mat(state(1:6), sig_inf_n)
    h = 0
    print*, "F0"
    call print_matrix(F0, 3, 3)
    print*, "F1"
    call print_matrix(F1, 3, 3)
    print*, "G"
    call print_matrix(G, 3, 3)
    print*, "det G inv: ", detGinv
    !    call print_matrix(F0, 3, 3)
    do alpha=1,n
        xialpha = exp(-dt/(2.d0*tau(alpha)))
        s_v_alpha = 0
        print*, 's v ', alpha, ' n'
        print*, state(alpha*6+1:(alpha+2)*6)
        call voigt_to_mat(state(alpha*6+1:(alpha+2)*6), s_v_alpha)

        h_alpha = 0
        h_alpha = detGinv*matmul(G, matmul((xialpha**2*s_v_alpha-beta(alpha)*xialpha*sig_inf_n), transpose(G)))
        h = h + h_alpha
        s_v_alpha = beta(alpha)*xialpha*sig_inf_n1 + h_alpha
        call mat_to_voigt(s_v_alpha, state(alpha*6+1:(alpha+2)*6))
        h_other = beta(alpha)*xialpha*(- state(1:6)) + xialpha**2*state(alpha*6+1:(alpha+2)*6)
        !        state(alpha*6+1:(alpha+2)*6) = beta(alpha)*xialpha*(siso - state(1:6)) + xialpha**2*state(alpha*6+1:(alpha+2)*6)

        print*, 's v ', alpha, ' n + 1'
        print*, state(alpha*6+1:(alpha+2)*6)
        print*,
        call print_matrix(s_v_alpha, 3, 3)
        print*, 'h alpha'
        call print_matrix(h_alpha, 3, 3)
        print*, 'h'
        call print_matrix(h, 3, 3)
        print*, 'h_other'
        print*, h_other


        sv = sv + state(alpha*6+1:(alpha+2)*6)
    end do
    print*, 's inf n'
    print*, state(1:6)
    state(1:6) = siso
    print*, 's inf n 1'
    print*, state(1:6)
    sf = siso + sv + svol
    cf = (1.d0+dot_product(beta, xi))*ciso + cvol
    call add_odot_I_sym(jac*h, cf)
    print*, 'c'
    call print_matrix(cf, 6, 6)

    !    print*, "beta"
    !    print*, beta
    !    print*, "tau"
    !    print*, tau
    !    print*, "xi"
    !    print*, xi
    !
    !    print*, "siso"
    !    print*, siso
    !    print*, "sv"
    !    print*, sv
    !    print*, "svol"
    !    print*, svol
    !    print*, "sf"
    !    print*, sf
    !
    !    print*, "ciso"
    !    print*, ciso
    !    print*, "cvol"
    !    print*, cvol
    !    print*, "cf"
    !    print*, cf
    !
    !    print*,
    !    print*,

endsubroutine implement_visco_elasticity

subroutine implement_visco_elasticity_old(dphidi, d2phidi2, dphidj, d2phidj2, n, beta, tau, state, F1, dt, sf, cf)
    doubleprecision, intent(in) :: dphidi(4), d2phidi2(10), dphidj, d2phidj2, F1(3, 3), dt
    integer, intent(in) :: n
    doubleprecision, intent(in) :: beta(n), tau(n)

    doubleprecision, intent(inout) :: state((n+1)*6)

    doubleprecision :: s(6), svalpa(6), sv(6), siso(6), svol(6), c(6, 6), ciso(6, 6), cvol(6, 6), cv(6, 6), xi(n)
    doubleprecision :: xialpha
    integer :: alpha

    doubleprecision, intent(out) :: sf(6), cf(6, 6)

    s = 0
    siso = 0
    svol = 0
    c = 0
    ciso = 0
    cvol = 0
    !    print*, "dt"
    !    print*, dt
    print*, "dphidi"
    print*, dphidi
    print*, "d2phidi2"
    print*, d2phidi2
    print*, "dphidj"
    print*, dphidj
    print*, "d2phidj2"
    print*, d2phidj2
    print*, "F1"
    call print_matrix(F1, 3, 3)

    call near_incompressible_transversely_isotropic_seperate(dphidi, d2phidi2, dphidj, d2phidj2, F1, s, c, siso, ciso, svol, cvol)

    xi = exp(-dt/(2.d0*tau))
    sv = 0
    svalpha = 0
    do alpha=1,n
        !        svalpha = beta(alpa)*xi(alpha)*siso + xi(alpha)**2*state(alpha*6+1:(alpha+2)*6) - beta(alpha)*xi(alpha)*state(1:6)
        !        state(alpha*6+1:(alpha+2)*6) = svalpa
        xialpha = exp(-dt/(2.d0*tau(alpha)))
        !        print*, "xi ", alpha
        !        print*, xialpha
        !        print*, "siso(n) "
        !        print*, state(1:6)
        !        print*, "svn ", alpha
        !        print*, state(alpha*6+1:(alpha+2)*6)
        !        state(alpha*6+1:(alpha+2)*6) = beta(alpha)*xi(alpha)*siso + xi(alpha)**2*state(alpha*6+1:(alpha+2)*6) - beta(alpha)*xi(alpha)*state(1:6)
        state(alpha*6+1:(alpha+2)*6) = beta(alpha)*xialpha*(siso - state(1:6)) + xialpha**2*state(alpha*6+1:(alpha+2)*6)
        !        print*, "svn+1 ", alpha
        !        print*, state(alpha*6+1:(alpha+2)*6)
        sv = sv + state(alpha*6+1:(alpha+2)*6)
    end do
    state(1:6) = siso
    sf = siso + sv + svol
    cf = (1.d0+dot_product(beta, xi))*ciso + cvol

    !    print*, "beta"
    !    print*, beta
    !    print*, "tau"
    !    print*, tau
    !    print*, "xi"
    !    print*, xi
    !
    print*, "siso"
    print*, siso
    print*, "sv"
    print*, sv
    print*, "svol"
    print*, svol
    print*, "sf"
    print*, sf

    print*, "ciso"
    print*, ciso
    print*, "cvol"
    print*, cvol
    print*, "cf"
    print*, cf

    print*,
    print*,

endsubroutine implement_visco_elasticity_old

subroutine near_incompressible_transversely_isotropic_seperate(dphidi, d2phidi2, dphidj, d2phidj2, F, s, c, siso, ciso, svol, cvol)
    doubleprecision, intent(in) :: dphidi(4), d2phidi2(10), dphidj, d2phidj2, F(3, 3)

    doubleprecision :: invar(4), di(4, 3, 3), di2(4, 6, 6), B(3, 3), B2(3, 3), trB, J, Jm, IOI_coeff, q(4), aoa(3, 3)
    doubleprecision :: divoigt(4, 6), si_coeff, coeff, t1(6, 6), t2(6, 6), a(3)
    doubleprecision :: det3, dum, tr3

    integer :: m, n, map(4, 4)

    doubleprecision, intent(inout) :: s(6), c(6, 6)
    doubleprecision, intent(inout) :: siso(6), ciso(6, 6), svol(6), cvol(6, 6)

    q = [-1.d0/3.d0, -2.d0/3.d0, -1.d0/3.d0, -2.d0/3.d0]
    J = det3(F)
    B = matmul(F, transpose(F))
    a = F(:, 1)
    trB = B(1, 1) + B(2, 2) + B(3, 3)


    invar(1) = trB
    invar(2) = 5.d-1*(trB**2 - tr3(matmul(B, B))) ! <- TODO
    invar(3) = dot_product(a, a)
    invar(4) = dot_product(a, matmul(B,a))

    di(1, :, :) = B
    di(2, :, :) = B*trB - matmul(B, B)
    call outer(a, a, di(3, :, :))
    call outer(a, matmul(B, a), di(4, :, :))
    di(4, :, :) = di(4, :, :) + transpose(di(4, :, :))

    call mat_to_voigt(di(1, :, :), divoigt(1, :))
    call mat_to_voigt(di(2, :, :), divoigt(2, :))
    call mat_to_voigt(di(3, :, :), divoigt(3, :))
    call mat_to_voigt(di(4, :, :), divoigt(4, :))

    di2 = 0
    call add_sym_otimes(B, B, di2(2, :, :))
    call odot(-B, B, di2(2, :, :))
    call odot(aoa, B, di2(4, :, :))
    call odot(B, aoa, di2(4, :, :))

    IOI_coeff = 0
    si_coeff = 0
    Jm = J**(-7.d0/3.d0)
    do n=1,4
        coeff = 4.d0*J**(4.d0*q(n)-1.d0)*d2phidi2(n)

        IOI_coeff = IOI_coeff + coeff*(q(n)*invar(n))**2
        call add_sym_otimes(coeff*di(n, :, :), di(n, :, :), c)
        call add_otimes_I_sym(coeff*q(n)*invar(n)*di(n, :, :), c)
    end do

    map(1, :) = [0, 5, 6, 7]
    map(2, :) = [0, 0, 8, 9]
    map(3, :) = [0, 0, 0, 10]

    do n=1,4
        do m=n+1,4

            coeff = 4.d0*J**(2.d0*(q(m) + q(n))-1.d0)*d2phidi2(map(n, m))

            IOI_coeff = IOI_coeff +  2.d0*q(m)*q(n)*invar(m)*invar(n)*coeff
            call add_sym_otimes(2.d0*coeff*di(m, :, :), di(n, :, :), c)
            call add_otimes_I_sym(coeff*(q(m)*invar(m)*di(n, :, :) + q(n)*invar(n)*di(m, :, :)), c)
        end do
    end do

    do m=1,4

        coeff = 2.d0 * J **(2.d0*q(m)-1.d0)*dphidi(m)
        s = s + coeff*divoigt(m, :)
        si_coeff = si_coeff + coeff * q(m) * invar(m)

        coeff = coeff * 2.d0
        IOI_coeff = IOI_coeff +  q(m) ** 2.d0 * invar(m) * coeff

        c = c + coeff*di2(m, :, :)
        call add_odot_I_sym(5.d-1*coeff*di(m, :, :), c)
        call add_otimes_I_sym(q(m)*coeff*di(m, :, :), c)
    end do

    ciso = c
    ciso(1:3, 1:3) = ciso(1:3, 1:3) + IOI_coeff
    cvol(1:3, 1:3) = cvol(1:3, 1:3) + (dphidj + J * d2phidj2)
    c = ciso + cvol
    !    IOI_coeff = IOI_coeff + (dphidj + J * d2phidj2)
    !    c(1:3, 1:3) = c(1:3, 1:3) + IOI_coeff

    siso = s
    siso(1:3) = siso(1:3) + si_coeff
    svol(1:3) = svol(1:3) + dphidj
    s = siso + svol
    !    si_coeff = si_coeff+dphidj
    !    s(1:3) = s(1:3) + si_coeff

end subroutine near_incompressible_transversely_isotropic_seperate

subroutine near_incompressible_transversely_isotropic(dphidi, d2phidi2, dphidj, d2phidj2, F, s, c)
    doubleprecision, intent(in) :: dphidi(4), d2phidi2(10), dphidj, d2phidj2, F(3, 3)

    doubleprecision :: invar(4), di(4, 3, 3), di2(4, 6, 6), B(3, 3), B2(3, 3), trB, J, Jm, IOI_coeff, q(4), aoa(3, 3)
    doubleprecision :: divoigt(4, 6), si_coeff, coeff, t1(6, 6), t2(6, 6), a(3)
    doubleprecision :: det3, dum, tr3

    integer :: m, n, map(4, 4)

    doubleprecision, intent(inout) :: s(6), c(6, 6)

    q = [-1.d0/3.d0, -2.d0/3.d0, -1.d0/3.d0, -2.d0/3.d0]
    J = det3(F)
    B = matmul(F, transpose(F))
    a = F(:, 1)
    trB = B(1, 1) + B(2, 2) + B(3, 3)


    invar(1) = trB
    invar(2) = 5.d-1*(trB**2 - tr3(matmul(B, B))) ! <- TODO
    invar(3) = dot_product(a, a)
    invar(4) = dot_product(a, matmul(B,a))

    di(1, :, :) = B
    di(2, :, :) = B*trB - matmul(B, B)
    call outer(a, a, di(3, :, :))
    aoa = di(3, :, :)
    call outer(a, matmul(B, a), di(4, :, :))
    di(4, :, :) = di(4, :, :) + transpose(di(4, :, :))

    call mat_to_voigt(di(1, :, :), divoigt(1, :))
    call mat_to_voigt(di(2, :, :), divoigt(2, :))
    call mat_to_voigt(di(3, :, :), divoigt(3, :))
    call mat_to_voigt(di(4, :, :), divoigt(4, :))

    di2 = 0
    call add_sym_otimes(B, B, di2(2, :, :))
    call odot(-B, B, di2(2, :, :))
    call odot(aoa, B, di2(4, :, :))
    call odot(B, aoa, di2(4, :, :))

    IOI_coeff = 0
    si_coeff = 0
    Jm = J**(-7.d0/3.d0)
    do n=1,4
        coeff = 4.d0*J**(4.d0*q(n)-1.d0)*d2phidi2(n)

        IOI_coeff = IOI_coeff + coeff*(q(n)*invar(n))**2
        call add_sym_otimes(coeff*di(n, :, :), di(n, :, :), c)
        call add_otimes_I_sym(coeff*q(n)*invar(n)*di(n, :, :), c)
    end do

    map(1, :) = [0, 5, 6, 7]
    map(2, :) = [0, 0, 8, 9]
    map(3, :) = [0, 0, 0, 10]

    do n=1,4
        do m=n+1,4

            coeff = 4.d0*J**(2.d0*(q(m) + q(n))-1.d0)*d2phidi2(map(n, m))

            IOI_coeff = IOI_coeff +  2.d0*q(m)*q(n)*invar(m)*invar(n)*coeff
            call add_sym_otimes(2.d0*coeff*di(m, :, :), di(n, :, :), c)
            call add_otimes_I_sym(coeff*(q(m)*invar(m)*di(n, :, :) + q(n)*invar(n)*di(m, :, :)), c)
        end do
    end do

    do m=1,4

        coeff = 2.d0 * J **(2.d0*q(m)-1.d0)*dphidi(m)
        s = s + coeff*divoigt(m, :)
        si_coeff = si_coeff + coeff * q(m) * invar(m)

        coeff = coeff * 2.d0
        IOI_coeff = IOI_coeff +  q(m) ** 2 * invar(m) * coeff

        c = c + coeff*di2(m, :, :)
        call add_odot_I_sym(5.d-1*coeff*di(m, :, :), c)
        call add_otimes_I_sym(q(m)*coeff*di(m, :, :), c)
    end do

    IOI_coeff = IOI_coeff + (dphidj + J * d2phidj2)
    c(1:3, 1:3) = c(1:3, 1:3) + IOI_coeff

    si_coeff = si_coeff+dphidj
    s(1:3) = s(1:3) + si_coeff

end subroutine near_incompressible_transversely_isotropic

subroutine near_incompressible_isotropic(dphidi, d2phidi2, dphidj, d2phidj2, F, s, c)
    doubleprecision, intent(in) :: dphidi(2), d2phidi2(3), dphidj, d2phidj2, F(3, 3)

    doubleprecision :: invar(2), di(2, 3, 3), di2(2, 6, 6), B(3, 3), B2(3, 3), trB, J, Jm, IOI_coeff, q(2)
    doubleprecision :: divoigt(2, 6), si_coeff, coeff, t1(6, 6), t2(6, 6)
    doubleprecision :: det3, dum, tr3

    integer :: m, n

    doubleprecision, intent(inout) :: s(6), c(6, 6)

    q = [-1./3., -2./3.]
    J = det3(F)
    B = matmul(F, transpose(F))
    trB = B(1, 1) + B(2, 2) + B(3, 3)
    di(1, :, :) = B
    di(2, :, :) = B*trB - matmul(B, B)

    call mat_to_voigt(di(1, :, :), divoigt(1, :))
    call mat_to_voigt(di(2, :, :), divoigt(2, :))


    invar(1) = trB
    invar(2) = 0.5*(trB**2 - tr3(matmul(B, B))) ! <- TODO


    !    TODO
    di2 = 0
    t1=0
    t2=0
    di2(1, :, :) = 0
    call add_sym_otimes(B, B, di2(2, :, :))
    call odot(-B, B, di2(2, :, :))
    call add_sym_otimes(B, B, t1)
    call odot(B, B, t2)

    IOI_coeff = 0
    si_coeff = 0
    Jm = J**(-7./3.)
    do n=1,2
        coeff = 4.*J**(4*q(n)-1)*d2phidi2(n)

        IOI_coeff = IOI_coeff + coeff*(q(n)*invar(n))**2
        call add_sym_otimes(coeff*di(n, :, :), di(n, :, :), c)
        call add_otimes_I_sym(coeff*q(n)*invar(n)*di(n, :, :), c)
    end do

    coeff = 4.*J**(2*(q(1) + q(2))-1)*d2phidi2(3)

    IOI_coeff = IOI_coeff +  2*q(1)*q(2)*invar(1)*invar(2)*coeff
    call add_sym_otimes(2*coeff*di(1, :, :), di(2, :, :), c)
    call add_otimes_I_sym(coeff*(q(2)*invar(2)*di(1, :, :) + q(1)*invar(1)*di(2, :, :)), c)

    do m=1,2

        coeff = 2 * J **(2*q(m)-1)*dphidi(m)
        s = s + coeff*divoigt(m, :)
        si_coeff = si_coeff + coeff * q(m) * invar(m)

        coeff = coeff * 2
        IOI_coeff = IOI_coeff +  q(m) ** 2 * invar(m) * coeff

        c = c + coeff*di2(m, :, :)
        call add_odot_I_sym(0.5*coeff*di(m, :, :), c)
        call add_otimes_I_sym(q(m)*coeff*di(m, :, :), c)
    end do

    IOI_coeff = IOI_coeff + (dphidj + J * d2phidj2)
    c(1:3, 1:3) = c(1:3, 1:3) + IOI_coeff

    si_coeff = si_coeff+dphidj
    s(1:3) = s(1:3) + si_coeff

end subroutine near_incompressible_isotropic

subroutine odot(A, B, c)
    doubleprecision, intent(in) :: A(3, 3), B(3, 3)
    doubleprecision, intent(inout) :: c(6, 6)

    integer :: i, j, iv(6), jv(6), is, js, ks, ls

    iv = [1, 2, 3, 1, 1, 2]
    jv = [1, 2, 3, 2, 3, 3]


    do i=1,6
        do j=1,6
            is = iv(i)
            js = jv(i)
            ks = iv(j)
            ls = jv(j)
            c(i, j) = c(i, j) + 5.d-1*(A(is, ks) * B(js, ls) + A(is, ls) * B(js, ks))
        end do
    end do


end subroutine odot

subroutine add_odot_I_sym(A, c)
    doubleprecision, intent(in) :: A(3, 3)
    doubleprecision, intent(inout) :: c(6, 6)

    c(1, 1) = c(1, 1) + 2.d0 * A(1, 1)
    c(2, 2) = c(2, 2) + 2.d0 * A(2, 2)
    c(3, 3) = c(3, 3) + 2.d0 * A(3, 3)

    c(1, 4) = c(1, 4) + A(1, 2)
    c(1, 5) = c(1, 5) + A(1, 3)
    c(2, 4) = c(2, 4) + A(2, 1)
    c(2, 6) = c(2, 6) + A(3, 2)
    c(3, 5) = c(3, 5) + A(3, 1)
    c(3, 6) = c(3, 6) + A(3, 2)

    c(4, 1) = c(4, 1) + A(2, 1)
    c(5, 1) = c(5, 1) + A(3, 1)
    c(4, 2) = c(4, 2) + A(1, 2)
    c(6, 2) = c(6, 2) + A(2, 3)
    c(5, 3) = c(5, 3) + A(1, 3)
    c(6, 3) = c(6, 3) + A(2, 3)

    c(4, 5) = c(4, 5) + 5.d-1 * A(2, 3)
    c(5, 4) = c(5, 4) + 5.d-1 * A(3, 2)

    c(4, 6) = c(4, 6) + 5.d-1 * A(1, 3)
    c(6, 4) = c(6, 4) + 5.d-1 * A(3, 1)

    c(5, 6) = c(5, 6) + 5.d-1 * A(1, 2)
    c(6, 5) = c(6, 5) + 5.d-1 * A(2, 1)

    c(4, 4) = c(4, 4) + 5.d-1*(A(1, 1) + A(2, 2))
    c(5, 5) = c(5, 5) + 5.d-1*(A(1, 1) + A(3, 3))
    c(6, 6) = c(6, 6) + 5.d-1*(A(2, 2) + A(3, 3))

end subroutine add_odot_I_sym

subroutine add_otimes_I_sym(A, c)
    doubleprecision, intent(in) :: A(3, 3)
    doubleprecision, intent(inout) :: c(6, 6)

    integer :: i, j

    do i=1,3
        do j=1,3
            c(i, j) = c(i, j) + A(i, i) + A(j, j)
        end do
    end do

    c(4, 1:3) = c(4, 1:3) + A(1, 2)
    c(1:3, 4) = c(1:3, 4) + A(1, 2)

    c(5, 1:3) = c(5, 1:3) + A(1, 3)
    c(1:3, 5) = c(1:3, 5) + A(1, 3)

    c(6, 1:3) = c(6, 1:3) + A(2, 3)
    c(1:3, 6) = c(1:3, 6) + A(2, 3)


end subroutine add_otimes_I_sym

subroutine add_sym_otimes(A, B, c)
    doubleprecision, intent(in) :: A(3, 3), B(3, 3)
    doubleprecision, intent(inout) :: c(6, 6)

    integer :: i, j, iv(6), jv(6)

    iv = [1, 2, 3, 1, 1, 2]
    jv = [1, 2, 3, 2, 3, 3]


    do i=1,6
        do j=1,6
            c(i, j) = c(i, j) + 5.d-1*(A(iv(i), jv(i)) * B(iv(j), jv(j)) + A(iv(j), jv(j)) * B(iv(i), jv(i)))
        end do
    end do


end subroutine add_sym_otimes

subroutine outer(a, b, out)
    doubleprecision, intent(in):: a(3), b(3)

    doubleprecision, intent(out) :: out(3, 3)

    out(1, :) = [a(1)*b(1), a(1)*b(2), a(1)*b(3)]
    out(2, :) = [a(2)*b(1), a(2)*b(2), a(2)*b(3)]
    out(3, :) = [a(3)*b(1), a(3)*b(2), a(3)*b(3)]

endsubroutine outer

subroutine matinv3(A, B)
    !! Performs a direct calculation of the inverse of a 3×3 matrix.
    doubleprecision, intent(in) :: A(3,3)   !! Matrix
    doubleprecision, intent(out) :: B(3,3)   !! Inverse matrix
    doubleprecision :: detinv, det3

    ! Calculate the inverse determinant of the matrix
    !    detinv = 1/(A(1,1)*A(2,2)*A(3,3) - A(1,1)*A(2,3)*A(3,2)&
    !            - A(1,2)*A(2,1)*A(3,3) + A(1,2)*A(2,3)*A(3,1)&
    !            + A(1,3)*A(2,1)*A(3,2) - A(1,3)*A(2,2)*A(3,1))

    detinv = 1/det3(A)

    ! Calculate the inverse of the matrix
    B(1,1) = +detinv * (A(2,2)*A(3,3) - A(2,3)*A(3,2))
    B(2,1) = -detinv * (A(2,1)*A(3,3) - A(2,3)*A(3,1))
    B(3,1) = +detinv * (A(2,1)*A(3,2) - A(2,2)*A(3,1))
    B(1,2) = -detinv * (A(1,2)*A(3,3) - A(1,3)*A(3,2))
    B(2,2) = +detinv * (A(1,1)*A(3,3) - A(1,3)*A(3,1))
    B(3,2) = -detinv * (A(1,1)*A(3,2) - A(1,2)*A(3,1))
    B(1,3) = +detinv * (A(1,2)*A(2,3) - A(1,3)*A(2,2))
    B(2,3) = -detinv * (A(1,1)*A(2,3) - A(1,3)*A(2,1))
    B(3,3) = +detinv * (A(1,1)*A(2,2) - A(1,2)*A(2,1))
end subroutine

function det3(A) result(det)
    doubleprecision, intent(in) :: A(3, 3)
    doubleprecision :: det

    det = A(1,1)*A(2,2)*A(3,3) - A(1,1)*A(2,3)*A(3,2)&
            - A(1,2)*A(2,1)*A(3,3) + A(1,2)*A(2,3)*A(3,1)&
            + A(1,3)*A(2,1)*A(3,2) - A(1,3)*A(2,2)*A(3,1)

end function det3

function tr3(A) result(tr)
    doubleprecision, intent(in) :: A(3, 3)
    doubleprecision :: tr

    tr = A(1,1)+A(2,2)+A(3,3)

end function tr3

subroutine print_matrix(mat, rows, collumns)
    integer :: rows, collumns
    double precision :: mat(rows, collumns)
    integer :: i, j

    do i =1,rows
        print*,mat(i,:)
    enddo
end

function delta(i, j) result(d)
    integer, intent(in) :: i! input
    integer :: d
    ! integer             :: j ! output
    if (i==j)then
        d = 1
    else
        d = 0
    endif
end function delta

subroutine mat_to_voigt(in, out)
    doubleprecision, intent(in) :: in(3, 3)

    doubleprecision, intent(out) :: out(6)
    out = [in(1, 1), in(2, 2), in(3, 3), in(1, 2), in(1, 3), in(2, 3)]
end subroutine mat_to_voigt

subroutine ve_to_matns(in, out)
    doubleprecision, intent(in) :: in(9)
    doubleprecision, intent(out) :: out(3, 3)

    out(1, 1) = in(1)
    out(2, 2) = in(2)
    out(3, 3) = in(3)
    out(1, 2) = in(4)
    out(2, 3) = in(5)
    out(3, 1) = in(6)
    out(2, 1) = in(7)
    out(3, 2) = in(8)
    out(1, 3) = in(9)

endsubroutine

subroutine vs_to_ve(in, out)
    doubleprecision, intent(in) :: in(6)
    doubleprecision, intent(out) :: out(6)

    out(1:4) = in(1:4)
    out(5) = in(6)
    out(6) = in(5)

endsubroutine

subroutine voigt_to_mat(in, out)
    doubleprecision, intent(in) :: in(6)

    doubleprecision, intent(out) :: out(3, 3)
    out(1, 1) = in(1)
    out(2, 2) = in(2)
    out(3, 3) = in(3)
    out(1, 2) = in(4)
    out(1, 3) = in(5)
    out(2, 3) = in(6)
    out(2, 1) = in(4)
    out(3, 1) = in(5)
    out(3, 2) = in(6)
end

subroutine uanisohyper_inv (ainv, ua, zeta, nfibers, ninv, &
        ui1, ui2, ui3, temp, noel, cmname, incmpflag, ihybflag, &
        numstatev, statev, numfieldv, fieldv, fieldvinc, &
        numprops, props)

    include 'aba_param.inc'

    character*80 cmname
    dimension ua(2), ainv(ninv), ui1(ninv), &
            ui2(ninv*(ninv+1)/2), ui3(ninv*(ninv+1)/2), &
            statev(numstatev), fieldv(numfieldv),&
            fieldvinc(numfieldv), props(numprops)


    if (cmname(1:10) .eq. 'UANISO_HGO') then
        call UANISOHYPER_INVHGO(ainv, ua, zeta, nfibers, ninv, &
                ui1, ui2, ui3, temp, noel, cmname, incmpflag, ihybflag,&
                numstatev, statev, numfieldv, fieldv, fieldvinc,&
                numprops, props)
    else if(cmname(1:13) .eq. 'UANISO_INVISO') then
        call UANISOHYPER_INVISO(ainv, ua, zeta, nfibers, ninv,&
                ui1, ui2, ui3, temp, noel, cmname, incmpflag, ihybflag,&
                numstatev, statev, numfieldv, fieldv, fieldvinc,&
                numprops, props)
    else if(cmname(1:16) .eq. 'UANISO_FUNGINV44') then
        call UANISOHYPER_FUNGINV44(ainv, ua, zeta, nfibers, ninv,&
                ui1, ui2, ui3, temp, noel, cmname, incmpflag, ihybflag,&
                numstatev, statev, numfieldv, fieldv, fieldvinc,&
                numprops, props)
    else if(cmname(1:16) .eq. 'UANISO_FUNGINV45') then
        call UANISOHYPER_FUNGINV45(ainv, ua, zeta, nfibers, ninv,&
                ui1, ui2, ui3, temp, noel, cmname, incmpflag, ihybflag,&
                numstatev, statev, numfieldv, fieldv, fieldvinc,&
                numprops, props)
    else
        write(6,*)'ERROR: User subroutine UANISOHYPER_INV missing!'
        call xit
    end if



    return
end


subroutine uanisohyper_invhgo (ainv, ua, zeta, nfibers, ninv, &
        ui1, ui2, ui3, temp, noel, cmname, incmpflag, ihybflag, &
        numstatev, statev, numfieldv, fieldv, fieldvinc,&
        numprops, props)

    include 'aba_param.inc'

    character*80 cmname
    dimension ua(2), ainv(ninv), ui1(ninv), &
            ui2(ninv*(ninv+1)/2), ui3(ninv*(ninv+1)/2),&
            statev(numstatev), fieldv(numfieldv), &
            fieldvinc(numfieldv), props(numprops)

    !     ainv: invariants
    !     ua  : energies ua(1): utot, ua(2); udev
    !     ui1 : dUdI
    !     ui2 : d2U/dIdJ
    !     ui3 : d3U/dIdJdJ, not used for regular elements

    parameter ( half = 0.5d0, &
            zero = 0.d0, &
            one  = 1.d0, &
            two  = 2.d0, &
            three= 3.d0, &
            four = 4.d0, &
            five = 5.d0, &
            six  = 6.d0, &
            !
            index_I1 = 1, &
            index_J  = 3, &
            asmall   = 2.d-16  )

    !     HGO model

    C10 = props(1)
    rk1 = props(3)
    rk2 = props(4)
    rkp = props(5)

    ua(2) = zero
    om3kp = one - three * rkp
    do k1 = 1, nfibers
        !             index_i4 = 4 + k1*(k1-1) + 2*(k1-1)
        index_i4 = indxInv4(k1,k1)
        E_alpha1 = rkp  * (ainv(index_i1) - three) &
                + om3kp * (ainv(index_i4) - one  )
        E_alpha = max(E_alpha1, zero)
        ht4a    = half + sign(half,E_alpha1 + asmall)
        aux     = exp(rk2*E_alpha*E_alpha)
        !     energy
        ua(2) = ua(2) +  aux - one
        !     ui1
        ui1(index_i1) = ui1(index_i1) + aux * E_alpha
        ui1(index_i4) = rk1 * om3kp * aux * E_alpha
        !     ui2
        aux2 = ht4a + two * rk2 * E_alpha * E_alpha
        ui2(indx(index_I1,index_I1)) = ui2(indx(index_I1,index_I1)) &
                + aux * aux2
        ui2(indx(index_I1,index_i4)) = rk1*rkp*om3kp * aux * aux2
        ui2(indx(index_i4,index_i4)) = rk1*om3kp*om3kp*aux * aux2
    end do

    !     deviatoric energy

    ua(2) = ua(2) * rk1 / (two * rk2)
    ua(2) = ua(2) + C10 * (ainv(index_i1) - three)

    !     compute derivatives

    ui1(index_i1) = rk1 * rkp * ui1(index_i1) + C10
    ui2(indx(index_I1,index_I1))= ui2(indx(index_I1,index_I1)) &
            * rk1 * rkp * rkp

    !     compressible case
    if(props(2).gt.zero) then
        Dinv = one / props(2)
        det = ainv(index_J)
        ua(1) = ua(2) + Dinv *((det*det - one)/two - log(det))
        ui1(index_J) = Dinv * (det - one/det)
        ui2(indx(index_J,index_J))= Dinv * (one + one / det / det)
        if (hybflag.eq.1) then
            ui3(indx(index_J,index_J))= - Dinv * two / (det*det*det)
        end if
    end if
    print*, "ainv"
    print*, ainv
    print*, "ui1"
    print*, ui1
    print*, "ui2"
    print*, ui2
    print*,
    print*,
    return
end

integer function indx( i, j )
    include 'aba_param.inc'
    ii = min(i,j)
    jj = max(i,j)
    indx = ii + jj*(jj-1)/2
    return
end

integer function indxInv4( i, j )
    include 'aba_param.inc'
    ii = min(i,j)
    jj = max(i,j)
    indxInv4 = 4 + jj*(jj-1) + 2*(ii-1)
    return
end

integer function indxInv5( i, j )
    include 'aba_param.inc'
    ii = min(i,j)
    jj = max(i,j)
    indxInv5 = 5 + jj*(jj-1) + 2*(ii-1)
    return
end