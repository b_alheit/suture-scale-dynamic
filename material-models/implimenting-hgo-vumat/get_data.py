from abaqus import *
from abaqusConstants import *
import __main__
import numpy as np
# import read_odb_utils
import os
import subprocess
import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import optimization
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior
# from model_creating_macros import *
# from post_processing_macros import *
# import scipy
import sys
# import matplotlib.pyplot as plt
import time
import os

# t_h = 1.4 / 2.
# c_thick = 0.35

dir = sys.argv[-2]
rate = sys.argv[-1]
os.chdir(dir)


def extract_pressure_displacement_data():
    o3 = session.openOdb(
        name='/home/cerecam/Benjamin_Alheit/simulations/PhD/suture-scale/dynamic/material-models/implimenting-hgo-umat/Job-1.odb',
        readOnly=False)

    # print >> sys.__stdout__, o3.steps

    n_frames = np.alen(o3.steps['load'].frames)
    print >> sys.__stdout__, o3.steps['load'].frames[0].fieldOutputs
    E = np.empty(n_frames)
    S1 = np.empty(n_frames)
    S2 = np.empty(n_frames)
    for i_frame in range(n_frames):
        e_field =  o3.steps['load'].frames[i_frame].fieldOutputs['LE'].values
        s_field =  o3.steps['load'].frames[i_frame].fieldOutputs['S'].values

        E[i_frame] = np.mean([val.maxPrincipal for val in e_field])
        S1[i_frame] = np.mean([val.maxPrincipal for val in s_field])
        S2[i_frame] = np.mean([val.midPrincipal for val in s_field])



    print >> sys.__stdout__, 'E'
    print >> sys.__stdout__, E
    print >> sys.__stdout__, o3.steps['load'].frames[0].fieldOutputs['LE'].values[0].maxPrincipal
    print >> sys.__stdout__, 'S1'
    print >> sys.__stdout__, S1
    print >> sys.__stdout__, 'S2'
    print >> sys.__stdout__, S2
    print >> sys.__stdout__, o3.steps['load'].frames[0].fieldOutputs['S'].values[0].maxPrincipal
    np.save(dir +"/aba_data/" + 'E-'+rate, E)
    np.save(dir +"/aba_data/" + 'S1-'+rate, S1)
    np.save(dir +"/aba_data/" + 'S2-'+rate, S2)
    # p = np.empty(n_frames)
    # d = np.empty(n_frames)
    # set = o3.rootAssembly.nodeSets['BOTTOM']
    #
    # for i_frame in range(n_frames):
    #     print >> sys.__stdout__, i_frame+1, " / ", n_frames
    #     bottom_disp = o3.steps['load'].frames[i_frame].fieldOutputs['UT'].getSubset(region=set)
    #     n_nodes = np.alen(bottom_disp)
    #     d[i_frame] = np.mean(np.array([bottom_disp.values[i_node].data[1] for i_node in range(n_nodes)]))
    #
    #     p_frame =  o3.steps['load'].frames[i_frame].fieldOutputs['P']
    #     n_p_vals = np.alen(p_frame)
    #
    #     p[i_frame] = np.mean(np.array([p_frame.values[i_val].data for i_val in range(n_p_vals)]))
    #
    # print >> sys.__stdout__, "p", p
    # print >> sys.__stdout__, "d", d
    #
    # np.save("p", p)
    # np.save("d", d)

    # print o3.steps['load'].frames[0].fieldOutputs['UT'].values[0]
    # print '\n'
    # print o3.steps['load'].frames[-1].fieldOutputs['P'].values[0]
    # values = o3.steps['load'].frames[-1].fieldOutputs['P'].values
    #
    # for val in values:
    #     if val.data < 0.000001:
    #         print val
    #
    # print '\n'
    # # print o3.rootAssembly.instances['SUTURE-INTERFACE-1'].surfaces['BOTTOM']
    # # print '\n'
    # # print o3.rootAssembly.nodeSets['BOTTOM']
    # surface = o3.rootAssembly.instances['SUTURE-INTERFACE-1'].surfaces['BOTTOM']
    # set = o3.rootAssembly.nodeSets['BOTTOM']
    # bottom_disp = o3.steps['load'].frames[0].fieldOutputs['UT'].getSubset(region=set)
    # print "\n"
    # print bottom_disp
    # # bottom_press = o3.steps['load'].frames[0].fieldOutputs['P'].getSubset(region=surface)
    # # print "\n"
    # # print bottom_press

    """
        node_set =  o3.rootAssembly.instances['MERGED-SHELL-1'].nodeSets['LOAD-AREA']
        bone_set =  o3.rootAssembly.instances['MERGED-SHELL-1'].elementSets['LEFT-BONE']
    
        field=frames[i].fieldOutputs['S'].getSubset(region=bone_set).getScalarField(invariant=MISES)
    """

extract_pressure_displacement_data()