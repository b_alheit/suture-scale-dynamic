module globals
    integer, public :: i_v(6), j_v(6)
    integer, public :: i_vns(9), j_vns(9)
end


subroutine setGlobals()
    use globals
    i_v = [1, 2, 3, 1, 1, 2]
    j_v = [1, 2, 3, 2, 3, 3]
    i_vns = [1, 2, 3, 1, 1, 2, 2, 3, 3]
    j_vns = [1, 2, 3, 2, 3, 3, 1, 1, 2]
end subroutine setGlobals

SUBROUTINE UMAT(STRESS,STATEV,DDSDDE,SSE,SPD,SCD, &
        RPL,DDSDDT,DRPLDE,DRPLDT, &
        STRAN,DSTRAN,TIME,DTIME,TEMP,DTEMP,PREDEF,DPRED,CMNAME, &
        NDI,NSHR,NTENS,NSTATV,PROPS,NPROPS,COORDS,DROT,PNEWDT, &
        CELENT,DFGRD0,DFGRD1,NOEL,NPT,LAYER,KSPT,JSTEP,KINC)

    INCLUDE 'ABA_PARAM.INC'

    CHARACTER*80 CMNAME
!    DIMENSION STRESS(NTENS),STATEV(NSTATV), &
    DIMENSION STRESS(9),STATEV(NSTATV), &
            DDSDDE(NTENS,NTENS),DDSDDT(NTENS),DRPLDE(NTENS), &
            STRAN(NTENS),DSTRAN(NTENS),TIME(2),PREDEF(1),DPRED(1), &
            PROPS(NPROPS),COORDS(3),DROT(3,3),DFGRD0(3,3),DFGRD1(3,3), JSTEP(4)

    integer :: n

    logical :: debug


    IF (CMNAME(1:8) .EQ. 'UMAT_HGO') THEN
!        n = (NPROPS - 2)/2
        if (NPT == 1 .and. NOEL==1 .and. PROPS(6)>0d0) then
            print*, '*********************************'
            print*, 'KINC: ', KINC
            print*, 'STRESS'
            print*, STRESS
            debug = .true.
        else
            debug = .false.
        end if
        n = (NPROPS - 6)/2


        !        call threeDlinearVisoElastic(PROPS(1), PROPS(2), n, PROPS(3:2+n), PROPS(3+n:2+2*n), STATEV, DFGRD0, DFGRD1, DTIME, STRESS, DDSDDE, debug)
!        call umat_hgo(PROPS(1), PROPS(2), PROPS(3), PROPS(4), PROPS(5), DFGRD1, STRESS, DDSDDE, debug)
        call umat_hgo_p_el(PROPS(1), PROPS(2), PROPS(3), PROPS(4), PROPS(5), DFGRD1, STRESS, DDSDDE, debug)
!        call visco_hgo(PROPS(1), PROPS(2), PROPS(3), PROPS(4), PROPS(5), PROPS(7:6+n), PROPS(7+n:6+n*2), n, STATEV, DFGRD0, DFGRD1, DTIME, STRESS, DDSDDE, debug)
!        call visco_hgo(c1, d, k1, k2, kap, beta, tau, n, state, F0, F1, s, c, log)
    END IF
!    IF (CMNAME(1:14) .EQ. 'UMAT_HGO') THEN
!        n = (NPROPS - 2)/2
!
!        call threeDlinearVisoElastic(PROPS(1), PROPS(2), n, PROPS(3:2+n), PROPS(3+n:2+2*n), STATEV, DFGRD0, DFGRD1, DTIME, STRESS, DDSDDE, debug)
!    END IF

    RETURN
END

subroutine visco_hgo(c1, d, k1, k2, kap, beta, tau, n, state, F0, F1, dt, s, c, log)
    integer, intent(in) :: n
    doubleprecision, intent(in) :: c1, d, k1, k2, kap, beta(n), tau(n), F0(3, 3), F1(3, 3), dt
    logical, intent(in) :: log

    doubleprecision, intent(inout) ::  state((n+1)*6)

    doubleprecision ::  siso(6), svol(6), ciso(6, 6), cvol(6, 6), sf(6), cf(6, 6), p_info(3)

    doubleprecision, intent(out) :: s(9), c(6, 6)

    p_info = s(7:9)
    call hgo_p_iso_vol_split(c1, d, k1, k2, kap, F1, p_info, siso, svol, ciso, cvol, log)
    if (n>0) then
        call implement_visco_elasticity(siso, svol, ciso, cvol, n, beta, tau, state, F0, F1, dt, sf, cf)
        c = cf
        s(1:6) = sf
        else
        s(1:6) = siso + svol
        c = ciso + cvol
    end if

    s(8:9) = p_info(2:3)

endsubroutine visco_hgo



!subroutine implement_visco_elasticity(dphidi, d2phidi2, dphidj, d2phidj2, n, beta, tau, state, F0, F1, dt, sf, cf)
subroutine implement_visco_elasticity(siso, svol, ciso, cvol, n, beta, tau, state, F0, F1, dt, sf, cf)
    doubleprecision, intent(in) :: F0(3, 3), F1(3, 3), dt, siso(6), svol(6), ciso(6, 6), cvol(6, 6)
    integer, intent(in) :: n
    doubleprecision, intent(in) :: beta(n), tau(n)

    doubleprecision, intent(inout) :: state((n+1)*6)

    doubleprecision :: s(6), svalpa(6), sv(6), cv(6, 6), xi(n), c(6, 6)
    doubleprecision :: xialpha, xi_bar, G(3, 3), F0inv(3, 3), detGinv, det3, h(3, 3), h_alpha(3, 3), s_v_alpha(3, 3)
    doubleprecision :: sig_inf_n(3, 3), sig_inf_n1(3, 3), h_other(6), jac
    integer :: alpha

    doubleprecision, intent(out) :: sf(6), cf(6, 6)

    F0inv = 0
    call matinv3(F0, F0inv)

    G = matmul(F1, F0inv)
    detGinv = 1.d0/det3(G)
    jac = det3(F1)

    xi = exp(-dt/(2.d0*tau))
    xi_bar = dot_product(beta, xi)
    sv = 0
    s_v_alpha = 0
    call voigt_to_mat(siso, sig_inf_n1)
    call voigt_to_mat(state(1:6), sig_inf_n)
    h = 0

    do alpha=1,n
        xialpha = exp(-dt/(2.d0*tau(alpha)))
        s_v_alpha = 0

        call voigt_to_mat(state(alpha*6+1:(alpha+2)*6), s_v_alpha)

        h_alpha = 0
        h_alpha = detGinv*matmul(G, matmul((xialpha**2*s_v_alpha-beta(alpha)*xialpha*sig_inf_n), transpose(G)))
        h = h + h_alpha
        s_v_alpha = beta(alpha)*xialpha*sig_inf_n1 + h_alpha
        call mat_to_voigt(s_v_alpha, state(alpha*6+1:(alpha+2)*6))

        sv = sv + state(alpha*6+1:(alpha+2)*6)
    end do

    state(1:6) = siso

    sf = siso + sv + svol
    cf = (1.d0+dot_product(beta, xi))*ciso + cvol
    call add_odot_I_sym(jac*h, cf)

endsubroutine implement_visco_elasticity


subroutine hgo_p_iso_vol_split(c1, d, k1, k2, kap, F, p_info, sb, sp, cb, cp, log)
    use globals
    doubleprecision, intent(in) :: c1, d, k1, k2, kap, F(3, 3)
    logical, intent(in) :: log

    doubleprecision :: detF, det3, I1, tr3, I4, E, ht4a
    doubleprecision :: dphidi(4), d2phidi2(10), dphidj, d2phidj2, trB, B(3, 3), stress(3, 3)
    doubleprecision :: ddphiddI1, ddphiddI4, ddphidI1dI4, A1(3, 3), A4(3, 3), delta(3, 3), dphidI1, dphidI4, AJBAR
    integer :: i, j, k, l, iv, jv
    doubleprecision :: siso(6), svol(6), ciso(6, 6), cvol(6, 6)
    doubleprecision :: otimes_s, odot_s

    doubleprecision, intent(inout) :: p_info(3)

    !    doubleprecision, intent(out) :: s(9), c(6, 6)
    doubleprecision, intent(out) :: sb(6), sp(6), cb(6, 6), cp(6, 6)
    call setGlobals


    delta = 0
    delta(1, 1) = 1d0
    delta(2, 2) = 1d0
    delta(3, 3) = 1d0
    detF = det3(F)
    B = matmul(F, transpose(F)) * detF **(-2.d0/3.d0)
    I1 = tr3(B)
    I4 = dot_product(F(:, 1), F(:, 1))* detF **(-2.d0/3.d0)
    E = kap*(I1-3.d0) + (1.d0-3.d0*kap)*(I4-1.d0)

    ht4a = 5.d-1 + sign(5.d-1,E + 2.d-16)
    E = max(E, 0.d0)


    dphidI1 =  c1 + k1*kap*E*exp(k2*E**2)
    dphidI4 =  k1*(1.d0-3.d0*kap)*E*exp(k2*E**2)

    ddphiddI1 = k1*kap**2*exp(k2*E**2)*(ht4a + 2.d0 *k2 * E **2)
    ddphiddI4 = k1*(1.d0-3.d0*kap)**2*exp(k2*E**2)*(ht4a + 2.d0 *k2 * E **2)
    ddphidI1dI4 = k1*(1.d0-3.d0*kap)*kap*exp(k2*E**2)*(ht4a + 2 *k2 * E **2)

    if (d > 0d0) then
        AJBAR = s(7)
        !        dphidj = (detF-1.d0/detF)/d
        dphidj = (AJBAR-1.d0/AJBAR)/d

        d2phidj2 =  detF*(1.d0+1.d0/AJBAR/AJBAR)/d

        !        s(8) = detF*(1.d0+1.d0/AJBAR/AJBAR)/d
        p_info(2) = d2phidj2
        p_info(3) = -2d0*detF/AJBAR/AJBAR/AJBAR/d
    else
        dphidj = 0d0
        d2phidj2 = 0d0
    end if
    A1 = B
    A4 = 0

    do i=1,3
        do j=1,3
            A4(i, j) = F(i, 1) * F(j, 1)* detF **(-2.d0/3.d0)
        enddo
    enddo

    stress = 2d0*(dphidI1*(A1 - I1 * delta /3d0) + dphidI4*(A4 - I4 * delta /3d0)) / detF + dphidj * delta
    call mat_to_voigt(stress, sb)
    stress = dphidj * delta
    call mat_to_voigt(stress, sp)

    do iv=1,6
        do jv = iv,6
            i = i_v(iv)
            j = j_v(iv)
            k = i_v(jv)
            l = j_v(jv)
            cb(iv, jv) = ddphiddI1 * (A1(i, j) * A1(j, k) - I1 * (delta(i,j)*A1(k, l) + A1(i, j) * delta(k, l))/3d0 &
                    + I1 ** 2 * delta(i, j) * delta(k, l)/9d0) & ! m=n=1
                    + ddphiddI4 * (A4(i, j) * A4(j, k) - I4 * (delta(i,j)*A4(k, l) + A4(i, j) * delta(k, l))/3d0 &
                            + I4 ** 2 * delta(i, j) * delta(k, l)/9d0) & ! m=n=4
                    + ddphidI1dI4 * (otimes_s(A1, A4, i, j, k, l) &
                            - (I4 * otimes_s(A1, delta, i, j, k, l) + I1 * otimes_s(delta, A4, i, j, k, l))/3d0 &
                            + 2d0 * I1 * I4 * delta(i, j) * delta(k, l)/9d0) & ! m=1 n=4 & n=1 m=4
                    + dphidI1 * ((otimes_s(A1, delta, i, j, k, l) + I1 * odot_s(delta, delta, i, j, k, l))/3d0 + I1 * delta(i,j)* delta(i,k)/9d0) & ! m=1
                    + dphidI4 * ((otimes_s(A4, delta, i, j, k, l) - I4 * odot_s(delta, delta, i, j, k, l))/3d0 + I4 * delta(i,j)* delta(i,k)/9d0) ! m=4
        enddo
    enddo

    do iv=1,6
        do jv=1,iv-1
            cb(iv, jv) = cb(jv, iv)
        end do
    end do

    cb = cb * 4d0/detF

    do iv=1,6
        do jv=1,6
            i = i_v(iv)
            j = j_v(iv)
            k = i_v(jv)
            l = j_v(jv)
            !            cvol(iv, jv) = detF * (dphidj + detF * d2phidj2) * delta(i, j) * delta(k, l)
            cp(iv, jv) = (dphidj + detF * d2phidj2) * delta(i, j) * delta(k, l)
        enddo
    enddo

    !    c = c / detF
    !    cvol = cvol / detF

    if (log) then
        print*, 'F'
        call print_matrix(F, 3, 3)
        print*, 'Stress iso'
        call print_matrix(sb, 3, 3)
        print*, 'Stress vol'
        call print_matrix(sp, 3, 3)
        print*, 'C bar'
        call print_matrix(cb, 6, 6)
        print*, 'C vol'
        call print_matrix(cp, 6, 6)

    end if

!    call mat_to_voigt(stress, s)
    !    c = c + cvol

    if (log) then
!        print*, 'C'
!        call print_matrix(c, 6, 6)
    end if

endsubroutine hgo_p_iso_vol_split



subroutine umat_hgo(c1, d, k1, k2, kap, F, s, c, log)
    use globals
    doubleprecision, intent(in) :: c1, d, k1, k2, kap, F(3, 3)
    logical, intent(in) :: log

    doubleprecision :: detF, det3, I1, tr3, I4, E, ht4a
    doubleprecision :: dphidi(4), d2phidi2(10), dphidj, d2phidj2, trB, B(3, 3), stress(3, 3)
    doubleprecision :: ddphiddI1, ddphiddI4, ddphidI1dI4, A1(3, 3), A4(3, 3), delta(3, 3), dphidI1, dphidI4
    integer :: i, j, k, l, iv, jv
    doubleprecision :: siso(6), svol(6), ciso(6, 6), cvol(6, 6)
    doubleprecision :: otimes_s, odot_s

    doubleprecision, intent(out) :: s(6), c(6, 6)
    call setGlobals


    delta = 0
    delta(1, 1) = 1d0
    delta(2, 2) = 1d0
    delta(3, 3) = 1d0
    detF = det3(F)
    B = matmul(F, transpose(F)) * detF **(-2.d0/3.d0)
    I1 = tr3(B)
    I4 = dot_product(F(:, 1), F(:, 1))* detF **(-2.d0/3.d0)
    E = kap*(I1-3.d0) + (1.d0-3.d0*kap)*(I4-1.d0)

    ht4a = 5.d-1 + sign(5.d-1,E + 2.d-16)
    E = max(E, 0.d0)


    dphidI1 =  c1 + k1*kap*E*exp(k2*E**2)
    dphidI4 =  k1*(1.d0-3.d0*kap)*E*exp(k2*E**2)

    ddphiddI1 = k1*kap**2*exp(k2*E**2)*(ht4a + 2.d0 *k2 * E **2)
    ddphiddI4 = k1*(1.d0-3.d0*kap)**2*exp(k2*E**2)*(ht4a + 2.d0 *k2 * E **2)
    ddphidI1dI4 = k1*(1.d0-3.d0*kap)*kap*exp(k2*E**2)*(ht4a + 2 *k2 * E **2)

    dphidj = (detF-1.d0/detF)/d
    d2phidj2 = (1.d0+1.d0/detF/detF)/d
    A1 = B
    A4 = 0

    do i=1,3
        do j=1,3
            A4(i, j) = F(i, 1) * F(j, 1)* detF **(-2.d0/3.d0)
        enddo
    enddo

    stress = 2d0*(dphidI1*(A1 - I1 * delta /3d0) + dphidI4*(A4 - I4 * delta /3d0)) / detF + dphidj * delta

    do iv=1,6
        do jv = iv,6
            i = i_v(iv)
            j = j_v(iv)
            k = i_v(jv)
            l = j_v(jv)
            c(iv, jv) = ddphiddI1 * (A1(i, j) * A1(j, k) - I1 * (delta(i,j)*A1(k, l) + A1(i, j) * delta(k, l))/3d0 &
            + I1 ** 2 * delta(i, j) * delta(k, l)/9d0) & ! m=n=1
            + ddphiddI4 * (A4(i, j) * A4(j, k) - I4 * (delta(i,j)*A4(k, l) + A4(i, j) * delta(k, l))/3d0 &
            + I4 ** 2 * delta(i, j) * delta(k, l)/9d0) & ! m=n=4
            + ddphidI1dI4 * (otimes_s(A1, A4, i, j, k, l) &
            - (I4 * otimes_s(A1, delta, i, j, k, l) + I1 * otimes_s(delta, A4, i, j, k, l))/3d0 &
            + 2d0 * I1 * I4 * delta(i, j) * delta(k, l)/9d0) & ! m=1 n=4 & n=1 m=4
            + dphidI1 * ((otimes_s(A1, delta, i, j, k, l) + I1 * odot_s(delta, delta, i, j, k, l))/3d0 + I1 * delta(i,j)* delta(i,k)/9d0) & ! m=1
            + dphidI4 * ((otimes_s(A4, delta, i, j, k, l) - I4 * odot_s(delta, delta, i, j, k, l))/3d0 + I4 * delta(i,j)* delta(i,k)/9d0) ! m=4
        enddo
    enddo

    do iv=1,6
        do jv=1,iv-1
            c(iv, jv) = c(jv, iv)
        end do
    end do

    c = c * 4

    do iv=1,6
        do jv=1,6
            i = i_v(iv)
            j = j_v(iv)
            k = i_v(jv)
            l = j_v(jv)
            cvol(iv, jv) = detF * (dphidj + detF * d2phidj2) * delta(i, j) * delta(k, l)
        enddo
    enddo

    c = c / detF
    cvol = cvol / detF

    if (log) then
        print*, 'F'
        call print_matrix(F, 3, 3)
        print*, 'Stress'
        call print_matrix(stress, 3, 3)
        print*, 'C bar'
        call print_matrix(c, 6, 6)
        print*, 'C vol'
        call print_matrix(cvol, 6, 6)

    end if

    call mat_to_voigt(stress, s)
    c = c + cvol

    if (log) then
        print*, 'C'
        call print_matrix(c, 6, 6)
    end if

endsubroutine umat_hgo


subroutine umat_hgo_p_el(c1, d, k1, k2, kap, F, s, c, log)
    use globals
    doubleprecision, intent(in) :: c1, d, k1, k2, kap, F(3, 3)
    logical, intent(in) :: log

    doubleprecision :: detF, det3, I1, tr3, I4, E, ht4a
    doubleprecision :: dphidi(4), d2phidi2(10), dphidj, d2phidj2, trB, B(3, 3), stress(3, 3)
    doubleprecision :: ddphiddI1, ddphiddI4, ddphidI1dI4, A1(3, 3), A4(3, 3), delta(3, 3), dphidI1, dphidI4, AJBAR
    integer :: i, j, k, l, iv, jv
    doubleprecision :: siso(6), svol(6), ciso(6, 6), cvol(6, 6)
    doubleprecision :: otimes_s, odot_s

    doubleprecision, intent(inout) :: s(9)
    doubleprecision, intent(out) :: c(6, 6)
!    doubleprecision, intent(out) :: s(9), c(6, 6)
    call setGlobals


    delta = 0
    delta(1, 1) = 1d0
    delta(2, 2) = 1d0
    delta(3, 3) = 1d0
    detF = det3(F)
    B = matmul(F, transpose(F)) * detF **(-2.d0/3.d0)
    I1 = tr3(B)
    I4 = dot_product(F(:, 1), F(:, 1))* detF **(-2.d0/3.d0)
    E = kap*(I1-3.d0) + (1.d0-3.d0*kap)*(I4-1.d0)

    ht4a = 5.d-1 + sign(5.d-1,E + 2.d-16)
    E = max(E, 0.d0)


    dphidI1 =  c1 + k1*kap*E*exp(k2*E**2)
    dphidI4 =  k1*(1.d0-3.d0*kap)*E*exp(k2*E**2)

    ddphiddI1 = k1*kap**2*exp(k2*E**2)*(ht4a + 2.d0 *k2 * E **2)
    ddphiddI4 = k1*(1.d0-3.d0*kap)**2*exp(k2*E**2)*(ht4a + 2.d0 *k2 * E **2)
    ddphidI1dI4 = k1*(1.d0-3.d0*kap)*kap*exp(k2*E**2)*(ht4a + 2 *k2 * E **2)

    if (d > 0d0) then
        AJBAR = s(7)
        dphidj = (detF-1.d0/detF)/d
!        dphidj = (AJBAR-1.d0/AJBAR)/d

!        d2phidj2 =  detF*(1.d0+1.d0/AJBAR/AJBAR)/d
        d2phidj2 =  detF*(1.d0+1.d0/detF/detF)/d

!        s(8) = detF*(1.d0+1.d0/AJBAR/AJBAR)/d
        s(8) = d2phidj2
        s(9) = -2d0*detF/AJBAR/AJBAR/AJBAR/d
        else
        dphidj = 0d0
        d2phidj2 = 0d0
    end if
    A1 = B
    A4 = 0

    do i=1,3
        do j=1,3
            A4(i, j) = F(i, 1) * F(j, 1)* detF **(-2.d0/3.d0)
        enddo
    enddo

    stress = 2d0*(dphidI1*(A1 - I1 * delta /3d0) + dphidI4*(A4 - I4 * delta /3d0)) / detF + dphidj * delta

    do iv=1,6
        do jv = iv,6
            i = i_v(iv)
            j = j_v(iv)
            k = i_v(jv)
            l = j_v(jv)
            c(iv, jv) = ddphiddI1 * (A1(i, j) * A1(j, k) - I1 * (delta(i,j)*A1(k, l) + A1(i, j) * delta(k, l))/3d0 &
            + I1 ** 2 * delta(i, j) * delta(k, l)/9d0) & ! m=n=1
            + ddphiddI4 * (A4(i, j) * A4(j, k) - I4 * (delta(i,j)*A4(k, l) + A4(i, j) * delta(k, l))/3d0 &
            + I4 ** 2 * delta(i, j) * delta(k, l)/9d0) & ! m=n=4
            + ddphidI1dI4 * (otimes_s(A1, A4, i, j, k, l) &
            - (I4 * otimes_s(A1, delta, i, j, k, l) + I1 * otimes_s(delta, A4, i, j, k, l))/3d0 &
            + 2d0 * I1 * I4 * delta(i, j) * delta(k, l)/9d0) & ! m=1 n=4 & n=1 m=4
            + dphidI1 * ((otimes_s(A1, delta, i, j, k, l) + I1 * odot_s(delta, delta, i, j, k, l))/3d0 + I1 * delta(i,j)* delta(i,k)/9d0) & ! m=1
            + dphidI4 * ((otimes_s(A4, delta, i, j, k, l) - I4 * odot_s(delta, delta, i, j, k, l))/3d0 + I4 * delta(i,j)* delta(i,k)/9d0) ! m=4
        enddo
    enddo

    do iv=1,6
        do jv=1,iv-1
            c(iv, jv) = c(jv, iv)
        end do
    end do

    c = c * 4

    do iv=1,6
        do jv=1,6
            i = i_v(iv)
            j = j_v(iv)
            k = i_v(jv)
            l = j_v(jv)
!            cvol(iv, jv) = detF * (dphidj + detF * d2phidj2) * delta(i, j) * delta(k, l)
            cvol(iv, jv) = detF * (dphidj + detF * d2phidj2) * delta(i, j) * delta(k, l)
        enddo
    enddo

    c = c / detF
    cvol = cvol / detF

    if (log) then
        print*, 'F'
        call print_matrix(F, 3, 3)
        print*, 'S(7:9)'
        print*, s(7:9)

        print*, 'Stress'
        call print_matrix(stress, 3, 3)
        print*, 'C bar'
        call print_matrix(c, 6, 6)
        print*, 'C vol'
        call print_matrix(cvol, 6, 6)

    end if

    call mat_to_voigt(stress, s)
    c = c + cvol

    if (log) then
        print*, 'C'
        call print_matrix(c, 6, 6)
    end if

endsubroutine umat_hgo_p_el


function odot_s(A, B, i, j, k, l) result(res)
    doubleprecision, intent(in) :: A(3, 3), B(3, 3)
    integer, intent(in) :: i, j, k, l
    doubleprecision :: res

    res = (A(i, k) * B(j, l) + A(i, l) * B(j, k) + B(i, k) * A(j, l) + B(i, l) * A(j, k) ) * 0.5d0

end function odot_s

function otimes_s(A, B, i, j, k, l) result(res)
    doubleprecision, intent(in) :: A(3, 3), B(3, 3)
    integer, intent(in) :: i, j, k, l
    doubleprecision :: res

    res = A(i, j) * B(k, l) + A(k, l) * B(i, j)

end function otimes_s


function det3(A) result(det)
    doubleprecision, intent(in) :: A(3, 3)
    doubleprecision :: det

    det = A(1,1)*A(2,2)*A(3,3) - A(1,1)*A(2,3)*A(3,2)&
            - A(1,2)*A(2,1)*A(3,3) + A(1,2)*A(2,3)*A(3,1)&
            + A(1,3)*A(2,1)*A(3,2) - A(1,3)*A(2,2)*A(3,1)

end function det3

function tr3(A) result(tr)
    doubleprecision, intent(in) :: A(3, 3)
    doubleprecision :: tr

    tr = A(1,1)+A(2,2)+A(3,3)

end function tr3


subroutine print_matrix(mat, rows, collumns)
    integer :: rows, collumns
    double precision :: mat(rows, collumns)
    integer :: i, j

    do i =1,rows
        print*,mat(i,:)
    enddo
end

subroutine voigt_to_mat(voigt, mat)
    use globals
    doubleprecision, intent(in) :: voigt(6)
    integer :: i, j
    doubleprecision, intent(out) :: mat(3, 3)
    call setGlobals

    do i=1,6
        mat(i_v(i), j_v(i)) = voigt(i)
        if (i .GT. 3) then
            mat(j_v(i), i_v(i)) = voigt(i)
        end if
    end do

end subroutine voigt_to_mat

subroutine mat_to_voigt(mat, voigt)
    use globals
    doubleprecision, intent(in) :: mat(3, 3)
    integer :: i
    doubleprecision, intent(out) :: voigt(6)
    call setGlobals

    do i=1,6
        voigt(i) = mat(i_v(i), j_v(i))
    end do

end subroutine mat_to_voigt

subroutine matinv3(A, B)
    !! Performs a direct calculation of the inverse of a 3×3 matrix.
    doubleprecision, intent(in) :: A(3,3)   !! Matrix
    doubleprecision, intent(out) :: B(3,3)   !! Inverse matrix
    doubleprecision :: detinv, det3

    ! Calculate the inverse determinant of the matrix
    !    detinv = 1/(A(1,1)*A(2,2)*A(3,3) - A(1,1)*A(2,3)*A(3,2)&
    !            - A(1,2)*A(2,1)*A(3,3) + A(1,2)*A(2,3)*A(3,1)&
    !            + A(1,3)*A(2,1)*A(3,2) - A(1,3)*A(2,2)*A(3,1))

    detinv = 1/det3(A)

    ! Calculate the inverse of the matrix
    B(1,1) = +detinv * (A(2,2)*A(3,3) - A(2,3)*A(3,2))
    B(2,1) = -detinv * (A(2,1)*A(3,3) - A(2,3)*A(3,1))
    B(3,1) = +detinv * (A(2,1)*A(3,2) - A(2,2)*A(3,1))
    B(1,2) = -detinv * (A(1,2)*A(3,3) - A(1,3)*A(3,2))
    B(2,2) = +detinv * (A(1,1)*A(3,3) - A(1,3)*A(3,1))
    B(3,2) = -detinv * (A(1,1)*A(3,2) - A(1,2)*A(3,1))
    B(1,3) = +detinv * (A(1,2)*A(2,3) - A(1,3)*A(2,2))
    B(2,3) = -detinv * (A(1,1)*A(2,3) - A(1,3)*A(2,1))
    B(3,3) = +detinv * (A(1,1)*A(2,2) - A(1,2)*A(2,1))
end subroutine


subroutine add_odot_I_sym(A, c)
    doubleprecision, intent(in) :: A(3, 3)
    doubleprecision, intent(inout) :: c(6, 6)

    c(1, 1) = c(1, 1) + 2.d0 * A(1, 1)
    c(2, 2) = c(2, 2) + 2.d0 * A(2, 2)
    c(3, 3) = c(3, 3) + 2.d0 * A(3, 3)

    c(1, 4) = c(1, 4) + A(1, 2)
    c(1, 5) = c(1, 5) + A(1, 3)
    c(2, 4) = c(2, 4) + A(2, 1)
    c(2, 6) = c(2, 6) + A(3, 2)
    c(3, 5) = c(3, 5) + A(3, 1)
    c(3, 6) = c(3, 6) + A(3, 2)

    c(4, 1) = c(4, 1) + A(2, 1)
    c(5, 1) = c(5, 1) + A(3, 1)
    c(4, 2) = c(4, 2) + A(1, 2)
    c(6, 2) = c(6, 2) + A(2, 3)
    c(5, 3) = c(5, 3) + A(1, 3)
    c(6, 3) = c(6, 3) + A(2, 3)

    c(4, 5) = c(4, 5) + 5.d-1 * A(2, 3)
    c(5, 4) = c(5, 4) + 5.d-1 * A(3, 2)

    c(4, 6) = c(4, 6) + 5.d-1 * A(1, 3)
    c(6, 4) = c(6, 4) + 5.d-1 * A(3, 1)

    c(5, 6) = c(5, 6) + 5.d-1 * A(1, 2)
    c(6, 5) = c(6, 5) + 5.d-1 * A(2, 1)

    c(4, 4) = c(4, 4) + 5.d-1*(A(1, 1) + A(2, 2))
    c(5, 5) = c(5, 5) + 5.d-1*(A(1, 1) + A(3, 3))
    c(6, 6) = c(6, 6) + 5.d-1*(A(2, 2) + A(3, 3))

end subroutine add_odot_I_sym