import matplotlib.pyplot as plt
import numpy as np
import matplotlib.lines as mlines
import matplotlib
import hgo_material_model as hm


# rates = [0.001, 0.01, 0.1, 1, 300, 1500]
# rates = [0.005, 0.05, 0.5]
rates = [0.00057, 0.057, 0.57]
res = 50
# rates.reverse()
n_plots = len(rates)
pad = 0.1

cmap = matplotlib.cm.get_cmap('cividis')

# colors = ["#" + str(hex(int((n_plots-i) * (255-pad)/n_plots)))[-2:]*3 for i in range(n_plots)]
colors = [cmap(pad + (1-2*pad)*i/(n_plots-1.)) for i in range(n_plots)]
# colors.reverse()
# data = np.genfromtxt("bone_fitting/bone-visco-data.csv", delimiter=",")[:, 2:]
# data[0, :] = 0.
# data[:, 1::2] *= 1.e6
# data[:, 1::2] -= 4.2e9*data[:, 0::2]

# data = np.genfromtxt("bone_fitting/keratin.csv", delimiter=",")[:, :]
data = np.zeros([10, 6])
data[0, :] = 0.
data[:, 1::2] *= 1.e6
# data[:, 1::2] -= 4.2e9*data[:, 0::2]

Et = 0.7e9

Ed = data[np.logical_not(np.isnan(data[:, 1])), [1]][-1]/data[np.logical_not(np.isnan(data[:, 0])), [0]][-1]
# print("Ed before: ", Ed)
n_plots = 3
Ediff = Et - Ed
data[:, 1::2] += data[:, 0::2] * Ediff


n_pts_model = 100

data_m = np.empty((n_pts_model, data.shape[1]))
for i_rate in range(n_plots):
    e_data = data[:, 2*i_rate]

    data_m[:, 2*i_rate] = np.linspace(0, e_data[np.logical_not(np.isnan(e_data))][-1], n_pts_model)
    # data[:, 2*i_rate]



legend_items = [
    mlines.Line2D([], [], color='black', linewidth=0, marker='x', mfc='none', label='$\sigma_{11}$ Abaqus'),
    mlines.Line2D([], [], color='black', linewidth=0, marker='+', mfc='none', label='$\sigma_{22}$ Abaqus'),
    mlines.Line2D([], [], color='black', linewidth=0, marker='o', mfc='none', label='$\sigma_{11}$ data'),
    mlines.Line2D([], [], color='black', linewidth=0, marker='s', mfc='none', label='$\sigma_{22}$ data'),
    mlines.Line2D([], [], color='black', linestyle="-" , label='$\sigma_{11}$ model analytical'),
    mlines.Line2D([], [], color='black', linestyle="--", label='$\sigma_{22}$ model analytical')]

def le_visco_elastic_uniaxial(mu, lam, beta, tau, t, edot):
    N = np.size(beta)
    xi_bar = 0
    for alpha in range(N):
        xi_bar += beta[alpha] * tau[alpha] * (1-np.exp(-t/tau[alpha]))

    # xi_bar = np.array([beta[alpha] * tau[alpha] * [1-np.exp(-t/tau[alpha])] for alpha in range(N)])
    a = (-lam*t+2.*mu*xi_bar/3.)
    b = (2.*t*(mu+lam) + 2.*mu*xi_bar/3.)
    r = a[b != 0]/b[b != 0]
    r = np.concatenate((np.array([1.]), r))
    # e11 = edot*t
    # e22 = r*e11
    se = edot * t * mu*(3*lam+2*mu)/(lam+mu)
    s11 = edot * (t * (2.*mu + lam*(1.+2.*r)) + 4.*mu*(1-r)*xi_bar/3.)
    sv = s11 - se
    return s11

def stress(params):
    mu = params[0]
    lam = params[1]
    n = int((np.size(params) - 2)/2)
    beta = params[2:n+2]
    tau = params[n+2:]
    out = []
    for i in range(int(data.shape[1]/2)):
        # s_data = data[:, 2*i+1]
        e_data = data_m[:, 2*i]
        # s_data = s_data[np.logical_not(np.isnan(s_data))]
        # e_data = e_data[np.logical_not(np.isnan(e_data))]
        e_dot = rates[i]
        t = e_data/e_dot
        s_model = le_visco_elastic_uniaxial(mu, lam, beta, tau, t, e_dot)
        out.append(s_model)
        # out += np.linalg.norm(s_model-s_data) ** 2

    return out

# params = np.array([4.28571429e+09 ,1.71428571e+10 ,4.49432128e-01, 2.44926765e+00,
#                    2.60657389e-02 ,9.53814862e-06])
# params = np.array([4.80000000e+09, 4.80000000e+09, 3.99307703e-01, 3.75958682e+00,
#                    4.43406040e-02, 1.03589601e-05])

params = np.array([2.50000000e+08, 1.00000000e+09, 8.52591649e-01, 3.49184024e-01,
                   5.97377110e-01, 1.89109322e+02])
s_model = stress(params)

beta = [0.80032026, 0.79969059]
tau = [0.53832051, 0.53859957]

G = 89.65666257763586
k1 = 10.935489714569641
k2 = 74.24064952130111
kap = 0.33333333333333326

for i_rate in range(n_plots):
    i_rate = n_plots - i_rate - 1
    rate = rates[i_rate]
    E = np.exp(np.load("aba_data/E-" + str(rate) + ".npy"))
    S1 = np.load("aba_data/S1-" + str(rate) + ".npy")
    plt.plot(E, S1, linewidth=0, marker='x', color=colors[i_rate])
    S2 = np.load("aba_data/S2-" + str(rate) + ".npy")
    plt.plot(E, S2, linewidth=0, marker='+', color=colors[i_rate])

    lam = np.linspace(E[0], E[-1], res)

    n_11 = hm.N_HGO_viso_vec_11(G, k1, k2, kap, lam, rate, beta, tau)*lam
    n_22 = hm.N_HGO_viso_vec_22(G, k1, k2, kap, lam, rate, beta, tau)
    plt.plot(lam, n_11, linestyle='-', color=colors[i_rate])
    plt.plot(lam, n_22, linestyle='--', color=colors[i_rate])

    if i_rate < 2:
        d11 = np.load("test_data/"+str(rate)+'-11-scaled.npy')
        d22 = np.load("test_data/"+str(rate)+'-22-scaled.npy')
        d11 = d11[d11[:,0] < 1.54, :]
        d22 = d22[d22[:,0] < 1.54, :]
        d11[:, 1] *= d11[:, 0]
        plt.plot(d11[:, 0], d11[:, 1], linewidth=0, mfc='none', marker='o', color=colors[i_rate])
        plt.plot(d22[:, 0], d22[:, 1], linewidth=0, mfc='none', marker='s', color=colors[i_rate])

    # plt.plot(data[:, 2*i_rate], data[:, 2*i_rate+1]/1.e6, linewidth=0, mfc='none', marker='o', color=colors[i_rate])
    # plt.plot(data_m[:, 2*i_rate], s_model[i_rate]/1.e6, color=colors[i_rate])

    legend_items.append(mlines.Line2D([], [], color=colors[i_rate], linewidth=0, marker='s', markersize=15, label=str(rate) + '/s'))


# blue_line = mlines.Line2D([], [], color='blue', marker='*',
#                           markersize=15, label='Blue stars')
plt.legend(handles=legend_items)


plt.ylabel("Stress (MPa)")
plt.xlabel("Strain")

plt.grid()
plt.show()