import os
from multiprocessing import Process
import socket
import time
import standard_job_management_server as jms

# standard_job_server_process = Process(target=jms.start)
# standard_job_server_process.start()
extensions = ['com', 'dat', 'log', 'msg', 'odb', 'prt', 'sim', 'sta', 'inp']
# rates = [0.001, 0.01, 0.1, 1, 300, 1500]
rates = [10**-5, 10**-3, 10**-1]
# strains = [0.008, 0.008, 0.009, 0.008, 0.008, 0.007]
strains = [0.055, 0.06, 0.066]
for i_rate in range(len(rates)):
    rate = rates[i_rate]
    print(rate)
    strain = strains[i_rate]
    # command =
    os.system("abaqus cae noGUI=setup_tensile.py -- 100 20 " + " ".join([str(strain), str(rate)]))

    os.system("python run_abaqus_job_standard.py " + "/home/cerecam/Benjamin_Alheit/simulations/PhD/suture-scale/dynamic/material-models/bone-ker")

    os.system("abaqus cae noGUI=get_data.py -- /home/cerecam/Benjamin_Alheit/simulations/PhD/suture-scale/dynamic/material-models/bone-ker " + str(rate))

    for ext in extensions:
        os.system('rm Job-1.'+ext)

#
# time.sleep(2)
# s = socket.socket()             # Create a socket object
# host = socket.gethostname()     # Get local machine name
# port = 60000                    # Reserve a port for your service.
#
# s.connect((host, port))
# s.send(bytes("/home/cerecam/Benjamin_Alheit/simulations/PhD/suture-scale/dynamic/material-models", 'utf-8'))
# s.close()
#
#
# standard_job_server_process.join()