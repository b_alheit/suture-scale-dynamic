import numpy as np
import matplotlib.pyplot as plt
import hgo_material_model as hm

import matplotlib

SMALL_SIZE = 8
MEDIUM_SIZE = 10
BIGGER_SIZE = 12

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}',
    r'\usepackage{amssymb}']

names = ["0.5", "5", "50"]
marker1 = ['s', 'x', '|']
marker2 = ['o', '^', 'd']
colours = ['black', 'dimgrey', 'darkgray']
ls = ['-', '--', '-.']

# plt.figure(figsize=(12,10))
fig = matplotlib.pyplot.gcf()
fig.set_size_inches(7, 5.5)

# for i in range(len(names)):
#     name = "n"+names[i]
#     lam1 = np.load("./data/"+name+"-lam1.npy")
#     s1 = np.load("./data/"+name+"-s1.npy")
#     s2 = np.load("./data/"+name+"-s2.npy")
#
#     # plt.plot(lam1, s1, linewidth=0, marker=marker1[i], mfc="none", label="Abaqus $\sigma_{11}$ - "+name+"%/s")
#     plt.plot(lam1, s1, linewidth=0, marker=marker1[1], mfc="none", label="Abaqus $\sigma_{11}$ - "+name+"%/s")
#     plt.plot(lam1, s2, linewidth=0, marker=marker1[1], mfc="none", label="Abaqus $\sigma_{22}$ - "+name+"%/s")
#     # plt.plot(lam1, s2, linewidth=0, marker=marker2[i], mfc="none", label="Abaqus $\sigma_{22}$ - "+name+"%/s")


# NN
# beta = [2.23379057e-01, 3.24110880e+02]
# tau = [1.06486141e+01, 6.06058655e-04]
#
# G = 65.77479479620887
# k1 = 160.11990547070704
# k2 = 41.422875738301514
# kap = 0.33333333333333326

# N
beta = [0.80032026, 0.79969059]
tau = [0.53832051, 0.53859957]

G = 89.65666257763586
k1 = 10.935489714569641
k2 = 74.24064952130111
kap = 0.33333333333333326

# lam1 = np.load("./data/0.5-lam1.npy")
lam1 = np.linspace(1, 1.5, 20)
lam_dots = [0.005, 0.05, 0.5]
lam_lots = np.linspace(lam1[0], lam1[-1], 50)
for i in range(len(lam_dots)):
    name = names[i]
    l_name = "n"+names[i]
    # lam1 = np.load("./data/"+l_name+"-lam1.npy")
    # s1 = np.load("./data/"+l_name+"-s1.npy")
    # s2 = np.load("./data/"+l_name+"-s2.npy")

    # plt.plot(lam1, s1, linewidth=0, marker=marker1[i], mfc="none", label="Abaqus $\sigma_{11}$ - "+name+"%/s")
    n_11 = hm.N_HGO_viso_vec_11(G, k1, k2, kap, lam1, lam_dots[i], beta, tau)*lam1
    n_22 = hm.N_HGO_viso_vec_22(G, k1, k2, kap, lam1, lam_dots[i], beta, tau)

    n_11_p = hm.N_HGO_viso_vec_11(G, k1, k2, kap, lam_lots, lam_dots[i], beta, tau)*lam_lots
    n_22_p = hm.N_HGO_viso_vec_22(G, k1, k2, kap, lam_lots, lam_dots[i], beta, tau)


    # plt.plot(lam1, n_11, label="Model $\sigma_{11}$ - "+name+"%$s^{-1}$", linestyle='-', color=colours[i])
    plt.plot(lam_lots, n_11_p, label="Model $\sigma_{11}$ - "+name+"\%$s^{-1}$", linestyle='-', color=colours[i])
    # plt.plot(lam1, s1, linewidth=0, marker='x', mfc="none", label="Abaqus $\sigma_{11}$ - "+name+"%$s^{-1}$", color=colours[i])
    plt.plot(lam1, n_11, linewidth=0, marker='x', label="Abaqus $\sigma_{11}$ - "+name+"\%$s^{-1}$", color=colours[i])
    # plt.plot(lam1, n_22, label="Model $\sigma_{22}$ - "+name+"%$s^{-1}$", linestyle='--', color=colours[i])
    plt.plot(lam_lots, n_22_p, label="Model $\sigma_{22}$ - "+name+"\%$s^{-1}$", linestyle='--', color=colours[i])
    # plt.plot(lam1, s2, linewidth=0, marker='+', mfc="none", label="Abaqus $\sigma_{22}$ - "+name+"%$s^{-1}$", color=colours[i])
    plt.plot(lam1, n_22, linewidth=0, marker='+', label="Abaqus $\sigma_{22}$ - "+name+"\%$s^{-1}$", color=colours[i])


plt.grid()
plt.legend()
plt.xlabel("$\lambda_1$")
plt.ylabel("$\sigma$ $(MPa)$")
plt.tight_layout()
# plt.savefig('/home/cerecam/Dropbox/written-work/PhD-proposal/figures/viscoelastic-abaqus.png')
plt.show()