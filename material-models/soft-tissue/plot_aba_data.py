import numpy as np
import matplotlib.pyplot as plt

rates = [0.005, 0.05, 0.5]
off = 1
for rate in rates:
    LE = np.load("aba_data/LE-"+str(rate)+".npy")[:]
    S = np.load("aba_data/S-"+str(rate)+".npy")[:]
    lams = np.exp(LE)
    plt.plot(lams,S, label=str(rate))

plt.grid()
plt.show()