import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as op
import time

labels = [
    "0.001 s$^{-1}$",
    "0.01 s$^{-1}$",
    "0.1 s$^{-1}$",
    "1 s$^{-1}$",
    "300 s$^{-1}$",
    "1500 s$^{-1}$",
          ]

edots = [
    0.001,
    # 0.000000000000000000000001,
    0.01,
    0.1,
    1.,
    300,
    1500,
    # 15000000000000
]

def le_visco_elastic_uniaxial(mu, lam, beta, tau, t, edot):
    N = np.size(beta)
    xi_bar = 0
    for alpha in range(N):
        xi_bar += beta[alpha] * tau[alpha] * (1-np.exp(-t/tau[alpha]))

    # xi_bar = np.array([beta[alpha] * tau[alpha] * [1-np.exp(-t/tau[alpha])] for alpha in range(N)])
    a = (-lam*t+2.*mu*xi_bar/3.)
    b = (2.*t*(mu+lam) + 2.*mu*xi_bar/3.)
    r = a[b != 0]/b[b != 0]
    r = np.concatenate((np.array([1.]), r))
    e11 = edot*t
    e22 = r*e11
    se = edot * t * mu*(3*lam+2*mu)/(lam+mu)
    s11 = edot * (t * (2.*mu + lam*(1.+2.*r)) + 4.*mu*(1-r)*xi_bar/3.)
    sv = s11 - se
    return s11

data = np.genfromtxt("bone-visco-data.csv", delimiter=",")[:, 2:]
data[0, :] = 0.
data[:, 1::2] *= 1.e6
data[:, 1::2] -= 4.2e9*data[:, 0::2]

# for i in range(int(data.shape[1]/2)):
#     data


def res(params):
    mu = params[0]
    lam = params[1]
    n = int((np.size(params) - 2)/2)
    beta = params[2:n+2]
    tau = params[n+2:]
    out = 0
    for i in range(int(data.shape[1]/2)):
        s_data = data[:, 2*i+1]
        e_data = data[:, 2*i]
        s_data = s_data[np.logical_not(np.isnan(s_data))]
        e_data = e_data[np.logical_not(np.isnan(e_data))]
        e_dot = edots[i]
        t = e_data/e_dot
        s_model = le_visco_elastic_uniaxial(mu, lam, beta, tau, t, e_dot)
        # out += np.linalg.norm((s_model[s_data!=0.]-s_data[s_data!=0.])/s_data[s_data!=0.]) ** 2
        out += np.linalg.norm(s_model-s_data) ** 2

    return out ** 0.5

def stress(params):
    mu = params[0]
    lam = params[1]
    n = int((np.size(params) - 2)/2)
    beta = params[2:n+2]
    tau = params[n+2:]
    out = []
    for i in range(int(data.shape[1]/2)):
        s_data = data[:, 2*i+1]
        e_data = data[:, 2*i]
        s_data = s_data[np.logical_not(np.isnan(s_data))]
        e_data = e_data[np.logical_not(np.isnan(e_data))]
        e_dot = edots[i]
        t = e_data/e_dot
        s_model = le_visco_elastic_uniaxial(mu, lam, beta, tau, t, e_dot)
        out.append(s_model)
        # out += np.linalg.norm(s_model-s_data) ** 2

    return out

# E = 16.2e9
E = 12.0e9
v = 0.25

mu_init = E/(2*(1+v))
lam_init = E*v/((1+v)*(1-2*v))
# mu_init =  6.4799999999999995e9
# lam_init = 6.4799999999999995e9
# beta_init = np.array([3960000.0,
#                       2.192723404255319])
beta_init = np.array([])
# beta_init = np.array([10.,
#                       1.])
# tau_init = np.array([33.333333333333336,
#                      103524.22907488987])
tau_init = np.array([])
# tau_init = np.array([0.005,
#                      0.5])
n = 20
tau_start = 0.0000001
tau_end = 10
beta_start = 0.00001
beta_end = 100.
mult = 2
# taus = tau_start * mult ** np.arange(0, n)
# betas = beta_start * mult ** np.arange(0, n)
taus = tau_start * np.geomspace(1, tau_end/tau_start, n)
betas = beta_start * np.geomspace(1, beta_end/beta_start, n)
res_min = -1
tg, bg = np.meshgrid(taus, betas)

n_samples = n**4
num = 1
for i in range(n):
    for j in range(n):
        for k in range(n):
            for l in range(n):
                print(num, ' / ', n_samples, '  ', str(100*num/n_samples)[:5],"%")
                num += 1
                beta1, tau1, beta2, tau2 = bg[i, j], tg[i, j], bg[k, l], tg[k, l]
                params = np.concatenate((np.array([mu_init]), np.array([lam_init]), np.array([beta1, beta2]), np.array([tau1, tau2])))
                res_current = res(params)
                if res_min < 0 or res_current < res_min:
                    beta1_best = beta1
                    tau1_best = tau1
                    beta2_best = beta2
                    tau2_best = tau2
                    res_min = res_current
# #
# #
# # # params = np.concatenate((np.array([mu_init]), np.array([lam_init]), np.array([beta_best]), np.array([tau_best])))
params = np.concatenate((np.array([mu_init]), np.array([lam_init]), np.array([beta1_best, beta2_best]), np.array([tau1_best, tau2_best])))
print("beta1_best ", beta1_best)
print("beta2_best ", beta2_best)
print("tau1_best ", tau1_best)
print("tau2_best ", tau2_best)
# params = np.concatenate((np.array([mu_init]), np.array([lam_init]), beta_init, tau_init))
# beta1_best  0.37275937203149423
# beta2_best  1.9306977288832496
# tau1_best  0.019306977288832496
# tau2_best  7.196856730011519e-06

# beta1_best  0.6158482110660255
# beta2_best  1.438449888287663
# tau1_best  0.004281332398719396
# tau2_best  1.2742749857031334e-05

# beta1_best  0.6158482110660255
# beta2_best  3.3598182862837813
# tau1_best  0.011288378916846883
# tau2_best  4.832930238571752e-06

# beta_init = np.array([0.37275937203149423,
#                       1.9306977288832496])
# tau_init = np.array([0.019306977288832496,
#                      7.196856730011519e-06])

# beta_init = np.array([0.6158482110660255,
#                       1.438449888287663])
# tau_init = np.array([0.004281332398719396,
#                      1.2742749857031334e-05])
# beta_init = np.array([0.6158482110660255,
#                       3.3598182862837813])
# tau_init = np.array([0.011288378916846883,
#                      4.832930238571752e-06])
# beta_init = np.array([4.49432128e-01,
#                       2.44926765e+00])
# tau_init = np.array([2.60657389e-02,
#                      9.53814862e-06])
#
# params = np.concatenate((np.array([mu_init]), np.array([lam_init]), beta_init, tau_init))
# params = [5.78571429e+09,
#           2.31428571e+10,
#           6.25652511e-01,
#           1.46135002e+00,
#           4.34919453e-03,
#           6.28072710e-06]

# result = op.minimize(res, params, method='L-BFGS-B', bounds=((0, np.inf), (0, np.inf), (0, np.inf), (0, np.inf), (0, np.inf), (0, np.inf)))
result = op.minimize(res, params, bounds=((1.e-16, np.inf), (1.e-16, np.inf), (1.e-16, np.inf), (1.e-16, np.inf), (1.e-16, np.inf), (1.e-16, np.inf)))
s_model = stress(np.array(result.x))
print(result.x)
print(result)
#
# params = np.array([4.28571429e+09 ,1.71428571e+10 ,4.49432128e-01, 2.44926765e+00,
#  2.60657389e-02 ,9.53814862e-06])
# params = np.array([mu_init ,lam_init ,4.49432128e-01, 2.44926765e+00,
#  2.60657389e-02 ,9.53814862e-06])
# s_model = stress(params)

# n = 10000
# t1 = time.time()
# for i in range(n):
#     res(params)
# t2 = time.time()
# print("time taken per calc: ", (t2-t1)/n, )

# [6.48000000e+09 6.48000000e+09 2.67107312e+00 2.67094756e-01
#  9.28725843e-06 1.33563095e-01]
"""
[4.28571429e+09 1.71428571e+10 4.49432128e-01 2.44926765e+00
 2.60657389e-02 9.53814862e-06]
"""

"""
beta1_best  0.6158482110660255
beta2_best  3.3598182862837813
tau1_best  0.011288378916846883
tau2_best  1.2742749857031334e-05
[4.80000000e+09 4.80000000e+09 3.99307703e-01 3.75958682e+00
 4.43406040e-02 1.03589601e-05]
      fun: 31458969.074966352
 hess_inv: <6x6 LbfgsInvHessProduct with dtype=float64>
      jac: array([ 0.00000000e+00,  0.00000000e+00, -2.43352413e+07, -1.31812729e+07,
       -4.76136804e+04,  2.78155604e+08])
  message: b'CONVERGENCE: REL_REDUCTION_OF_F_<=_FACTR*EPSMCH'
     nfev: 273
      nit: 27
   status: 0
  success: True
        x: array([4.80000000e+09, 4.80000000e+09, 3.99307703e-01, 3.75958682e+00,
       4.43406040e-02, 1.03589601e-05])
       >>> b1 = 3.99307703e-01

>>> b2 = 3.75958682e+00
>>> b1+b2
4.158894523
>>> bs = b1 +b2
>>> g1 = b1/(1+bs)
>>> g2 = b2/(1+bs)
>>> g1
0.07740179629952865
>>> g2
0.7287582258638088
>>> 

"""

for i in range(int(data.shape[1]/2)):
    plt.plot(data[:, 2*i], data[:, 2*i+1], linewidth=0, mfc='none', marker='o', label=labels[i])
    e_data = data[:, 2*i]
    e_data = e_data[np.logical_not(np.isnan(e_data))]
    plt.plot(e_data, s_model[i], label="model"+labels[i])


# plt.plot(data[:, 0], data[:, 0] * 12e9, label="12 GPa")


plt.legend()
plt.grid()
plt.xlabel("Strain")
plt.ylabel("Stress [MPa]")
# print(data.shape)
plt.show()