module globals
    integer, public :: i_v(6), j_v(6)
    integer, public :: i_vns(9), j_vns(9)
end


subroutine setGlobals()
    use globals
    i_v = [1, 2, 3, 1, 1, 2]
    j_v = [1, 2, 3, 2, 3, 3]
    i_vns = [1, 2, 3, 1, 1, 2, 2, 3, 3]
    j_vns = [1, 2, 3, 2, 3, 3, 1, 1, 2]
end subroutine setGlobals


SUBROUTINE UMAT(STRESS,STATEV,DDSDDE,SSE,SPD,SCD, &
         RPL,DDSDDT,DRPLDE,DRPLDT, &
         STRAN,DSTRAN,TIME,DTIME,TEMP,DTEMP,PREDEF,DPRED,CMNAME, &
         NDI,NSHR,NTENS,NSTATV,PROPS,NPROPS,COORDS,DROT,PNEWDT, &
         CELENT,DFGRD0,DFGRD1,NOEL,NPT,LAYER,KSPT,KSTEP,KINC)

INCLUDE 'ABA_PARAM.INC'

CHARACTER*80 CMNAME
DIMENSION STRESS(NTENS),STATEV(NSTATV), &
 DDSDDE(NTENS,NTENS),DDSDDT(NTENS),DRPLDE(NTENS), &
 STRAN(NTENS),DSTRAN(NTENS),TIME(2),PREDEF(1),DPRED(1), &
 PROPS(NPROPS),COORDS(3),DROT(3,3),DFGRD0(3,3),DFGRD1(3,3)

    integer :: n

    logical :: debug
    debug = .false.

    IF (CMNAME(1:14) .EQ. 'UMAT_BONE_VISC') THEN
        n = (NPROPS - 2)/2
        if (NPT == 1) then
            print*, '*********************************'
            print*, 'KINC: ', KINC
            debug = .true.
        end if

        call threeDlinearVisoElastic(PROPS(1), PROPS(2), n, PROPS(3:2+n), PROPS(3+n:2+2*n), STATEV, DFGRD0, DFGRD1, DTIME, STRESS, DDSDDE, debug)
    END IF

RETURN
END

subroutine threeDlinearVisoElastic(mu, lam, n, beta, tau, state_ev, fn, fn1, dt, stress, c, debug)
    integer, intent(in) :: n
    doubleprecision, intent(in) :: mu, lam, beta(n), tau(n), fn(3, 3), fn1(3, 3), dt
    logical, intent(in) :: debug

    doubleprecision, intent(inout) :: state_ev(6*(1+n))

    doubleprecision :: s(3, 3), sbn1_inf(3, 3), G(3, 3), J_n1, B(3, 3), Fninv(3, 3), xib, xi, I(3, 3)
    doubleprecision :: sbn_inf(3, 3), svn(3, 3), dGinv
    doubleprecision :: bob(6, 6), btb(6, 6), soi(6, 6), ios(6, 6)
    integer :: alpha

    doubleprecision :: det3, tr !functions

    doubleprecision, intent(out):: stress(6), c(6, 6)

    J_n1 = det3(fn1)
    B = matmul(fn1, transpose(fn1))
    I=0
    I(1, 1) = 1.d0
    I(2, 2) = 1.d0
    I(3, 3) = 1.d0

    s = (2.d0*mu*(matmul(B, B) - B) + lam * (tr(B, 3) - 3.d0) * B)/(2d0*J_n1)
    sbn1_inf = s - I*tr(s, 3) / 3.d0
    call voigt_to_mat(state_ev(1:6), sbn_inf)
    call mat_to_voigt(sbn1_inf, state_ev(1:6))

    call matinv3(fn, Fninv)
    G = matmul(fn1, Fninv)
    dGinv = 1.d0/det3(G)
    xib=0

    do alpha=1,n
        xi = exp(-dt/(2.d0*tau(alpha)))
        xib = xib + xi *  beta(alpha)
        call voigt_to_mat(state_ev(alpha*6+1:alpha*6+6), svn)
        svn = beta(alpha) * xi * (sbn1_inf + dGinv * matmul(G, matmul((xi * svn - sbn_inf), transpose(G))))
        call mat_to_voigt(svn, state_ev(alpha*6+1:alpha*6+6))
        s = s + svn
    end do

    call mat_odot(B, B, bob)
    call mat_otimes(B, B, btb)
    call mat_odot(s, I, soi)
    call mat_odot(I, s, ios)

    c = (2.d0*mu*(1.d0+xib)*(bob - btb/3.d0) + (2.d0*mu/3.d0 + lam) * btb)/J_n1 + soi + ios
    call mat_to_voigt(s, stress)
    if (debug) then
        print*, 'stress'
        print*, stress
        print*, "state_ev"
        print*, state_ev
        print*, 'stress v1'
        print*, state_ev(7:12)
        print*, 'stress v2'
        print*, state_ev(13:18)
        print*, 'tau'
        print*, tau
        print*, 'beta'
        print*, beta
        print*, 'c'
        call print_matrix(c, 6, 6)
    end if

end subroutine threeDlinearVisoElastic

subroutine matinv3(A, B)
    !! Performs a direct calculation of the inverse of a 3×3 matrix.
    doubleprecision, intent(in) :: A(3,3)   !! Matrix
    doubleprecision, intent(out) :: B(3,3)   !! Inverse matrix
    doubleprecision :: detinv, det3

    detinv = 1.d0/det3(A)

    ! Calculate the inverse of the matrix
    B(1,1) = +detinv * (A(2,2)*A(3,3) - A(2,3)*A(3,2))
    B(2,1) = -detinv * (A(2,1)*A(3,3) - A(2,3)*A(3,1))
    B(3,1) = +detinv * (A(2,1)*A(3,2) - A(2,2)*A(3,1))
    B(1,2) = -detinv * (A(1,2)*A(3,3) - A(1,3)*A(3,2))
    B(2,2) = +detinv * (A(1,1)*A(3,3) - A(1,3)*A(3,1))
    B(3,2) = -detinv * (A(1,1)*A(3,2) - A(1,2)*A(3,1))
    B(1,3) = +detinv * (A(1,2)*A(2,3) - A(1,3)*A(2,2))
    B(2,3) = -detinv * (A(1,1)*A(2,3) - A(1,3)*A(2,1))
    B(3,3) = +detinv * (A(1,1)*A(2,2) - A(1,2)*A(2,1))
end subroutine


function det3(A) result(det)
    doubleprecision, intent(in) :: A(3, 3)
    doubleprecision :: det

    det = A(1,1)*A(2,2)*A(3,3) - A(1,1)*A(2,3)*A(3,2)&
            - A(1,2)*A(2,1)*A(3,3) + A(1,2)*A(2,3)*A(3,1)&
            + A(1,3)*A(2,1)*A(3,2) - A(1,3)*A(2,2)*A(3,1)

end function det3

function tr(A, n) result(trace)
    integer, intent(in) :: n
    doubleprecision, intent(in) :: A(n, n)
    integer :: i
    doubleprecision :: trace
    trace = 0d0
    do i=1,n
        trace = trace + A(i, i)
    end do

end function tr

subroutine mat_to_voigt(mat, voigt)
    use globals
    doubleprecision, intent(in) :: mat(3, 3)
    integer :: i
    doubleprecision, intent(out) :: voigt(6)
    call setGlobals

    do i=1,6
        voigt(i) = mat(i_v(i), j_v(i))
    end do

end subroutine mat_to_voigt

subroutine voigt_to_mat(voigt, mat)
    use globals
    doubleprecision, intent(in) :: voigt(6)
    integer :: i, j
    doubleprecision, intent(out) :: mat(3, 3)
    call setGlobals

    do i=1,6
        mat(i_v(i), j_v(i)) = voigt(i)
        if (i .GT. 3) then
            mat(j_v(i), i_v(i)) = voigt(i)
        end if
    end do

end subroutine voigt_to_mat


subroutine mat_otimes(A, B, out)
    use globals
    doubleprecision, intent(in) :: A(3, 3), B(3, 3)
    integer :: i, j
    integer :: ic, jc, kc, lc
    doubleprecision, intent(out) :: out(6, 6)

    call setGlobals
    !    out = 0
    do i=1,6
        do j=1,6
            ic = i_v(i)
            jc = j_v(i)
            kc = i_v(j)
            lc = j_v(j)
            out(i, j) = A(ic, jc) * B(kc, lc)
        end do
    end do

end subroutine mat_otimes

subroutine mat_odot(A, B, out)
    use globals
    doubleprecision, intent(in) :: A(3, 3), B(3, 3)
    integer :: i, j
    integer :: ic, jc, kc, lc
    doubleprecision, intent(out) :: out(6, 6)

    call setGlobals
!    out = 0
    do i=1,6
        do j=1,6
            ic = i_v(i)
            jc = j_v(i)
            kc = i_v(j)
            lc = j_v(j)
            out(i, j) = A(ic, kc) * B(jc, lc) + A(jc, kc)*B(ic, lc)
        end do
    end do

    out = out/2.d0

end subroutine mat_odot


subroutine print_matrix(mat, rows, collumns)
    integer :: rows, collumns
    double precision :: mat(rows, collumns)
    integer :: i, j

    do i =1,rows
        print*,mat(i,:)
    enddo
end
