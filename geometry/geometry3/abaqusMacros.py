# -*- coding: mbcs -*-
# Do not delete the following import lines
from abaqus import *
from abaqusConstants import *
import __main__
from helpful_functions import *
import numpy as np
# import read_odb_utils
import os
import subprocess
import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import optimization
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior
# from model_creating_macros import *
# from post_processing_macros import *
# import scipy

# import matplotlib.pyplot as plt
import time
import os
# from read_odb_utils import *

t_h = 1.5 / 2.
t_w = 1.
c_thick = 0.2
c_trans = 0.02

flat_width = t_w/2.
depth = t_w/2.

flat_length = 1.2*t_h + c_thick*1.2

# Part Names
bone_name = "bone"

a = 2
n_points = 150

# t_h = t_h / 2.
x_path = np.linspace(0, depth*1.05, n_points)
x_path -= (x_path[-1] - depth)/2.
y_path = t_h/2. * np.sin(2.*np.pi * x_path / t_w - np.pi/2 )

x_cut = np.linspace(0, flat_width*1.05, n_points)
x_cut -= (x_cut[-1] - flat_width)/2.
y_cut = t_h/2. * np.sin(2.*np.pi * x_cut / t_w - np.pi/2)

y_cut_func = lambda x: t_h/2. * np.sin(2.*np.pi * x / t_w - np.pi/2)

theta = lambda x: np.pi/2. - np.pi/4. * np.cos(2.*np.pi * x / t_w - np.pi/2)

y_cut_top_func = lambda x: y_cut_func(x) + c_thick/2 * np.sin(theta(x)) + c_trans/2. -  t_h/2.
y_cut_bottom_func = lambda x: y_cut_func(x) - c_thick/2 * np.sin(theta(x)) - c_trans/2. -  t_h/2.

y_cut_top = y_cut + c_thick/2 * np.sin(theta(x_cut)) + c_trans/2.
x_cut_top = x_cut - c_thick/2 * np.sign(np.cos(theta(x_cut))) * np.abs(np.cos(theta(x_cut))) ** a

y_cut_bottom = y_cut - c_thick/2 * np.sin(theta(x_cut)) - c_trans/2.
x_cut_bottom = x_cut + c_thick/2 * np.sign(np.cos(theta(x_cut))) * np.abs(np.cos(theta(x_cut))) ** a

y_cut_top -= t_h/2.
y_cut_bottom -= t_h/2.


def AA_make_rve():
    # Geometric values




    path = tuple(map(tuple, np.array([x_path, y_path]).T))
    cut_top = tuple(map(tuple, np.array([x_cut_top, y_cut_top]).T))
    cut_bottom = tuple(map(tuple, np.array([x_cut_bottom, y_cut_bottom]).T))

    apprx_size = 2 * flat_length
    make_block(bone_name, flat_length, flat_width, depth, apprx_size, b_points=False)
    make_block("coll-block-temp", flat_length, flat_width, depth, apprx_size, b_points=False)

    cut_bone(bone_name, path, cut_top, cut_bottom, flat_width, depth, False)
    cut_collagen("coll-int", "bone-2", bone_name, "coll-block-temp")
    merge_col_bone("merged-shell", "bone-2", "coll-int")
    clean_up_temps()
    remove_redundant(flat_width, depth)


#########################################################################################################
#########################################################################################################
#########################################################################################################

def A_main():
    # Geometric values
    flat_width = 2.8
    depth = 2.5
    # t_h = 3
    # t_h = 2.5
    # t_h = 2.
    t_h = 0.2
    # t_h = 0.
    t_w = 1.
    c_thick = 0.2
    c_trans = 0.2
    total_length = 2.5
    ker_load_area_width = 0.25
    apprx_radius = 300

    parse_geom = np.array([t_h/2., t_w, flat_width])


    coll_shell_thick = 0.03
    ker_shell_thick = 0.05

    # test_data_file_name = "uniaxial.dat"

    # ogden_test_data = ((2.0, 1.0), (2.0, 1.0), (2.0, 1.0))
    alphas = np.array([0.5020, 26.85, 26.39])
    mus = np.array([6.255, 8.711e-2, 2.652e-3])
    k1 = 19.56
    k2 = 3.911
    ks = np.array([k1, k2])

    # np.savetxt("alphas", alphas)
    # np.savetxt("mus", mus)
    # np.savetxt("k1", np.array([k1]))
    # np.savetxt("k2", np.array([k2]))

    # print "hi"
    # # subprocess.Popen('python /home/cerecam/Benjamin_Alheit/Projects/masters-disertation/dissertation_report/write_up/code/bend_test_validation/create_uniaxial_data.py').wait()
    # os.system('python /home/cerecam/Benjamin_Alheit/Projects/masters-disertation/dissertation_report/write_up/code/bend_test_validation/create_uniaxial_data.py')
    # print "hi 2"

    props = np.concatenate((mus, alphas))
    props = np.concatenate((props, ks))
    props = np.concatenate((props, parse_geom))
    # print props
    # # props = np.concatenate((props, np.array([k1, k2])))
    # # props = np.concatenate((props, np.array([k2,])))
    # print "hi"
    # props = tuple(props)
    # print props

    # os.system('python file.py')
    # ogden_test_data = cd.generate_test_data(alphas, mus, k1, k2)
    # ogden_test_data = np.loadtxt(test_data_file_name)
    # ogden_test_data = tuple(map(tuple, ogden_test_data))
    # ogden_test_data = cd.generate_test_data(alphas, mus, k1, k2)

    # print(ogden_test_data)

    # os.remove("alphas")
    # os.remove("mus")
    # os.remove("k1")
    # os.remove("k2")
    # os.remove("uniaxial.dat")


    coll_el_size = 0.18
    coll_deviation_factor = 0.1
    coll_min_size_factor = 0.1

    ker_el_size = 0.2
    ker_deviation_factor = 0.1
    ker_min_size_factor = 0.1


    mer_el_size = 0.2
    mer_deviation_factor = 0.3
    mer_min_size_factor = 0.3

    # Part Names
    bone_name = "bone"
    collagen_block = "coll-block"

    radius_to_flat_length_scale = 50
    total_length_to_flat_length = 3
    flat_length_teeth_length = 1.2
    a = 2
    n_points = 150

    t_h /= 2.
    x_path = np.linspace(0, depth*1.05, n_points)
    x_path -= (x_path[-1] - depth)/2.
    y_path = t_h/2. * np.sin(2.*np.pi * x_path / t_w)

    x_cut = np.linspace(0, flat_width*1.05, n_points)
    x_cut -= (x_cut[-1] - flat_width)/2.
    y_cut = t_h/2. * np.sin(2.*np.pi * x_cut / t_w)

    theta = lambda x: np.pi/2. - np.pi/4. * np.cos(2.*np.pi * x / t_w)

    y_cut_top = y_cut + c_thick/2 * np.sin(theta(x_cut)) + c_trans/2.
    x_cut_top = x_cut - c_thick/2 * np.sign(np.cos(theta(x_cut))) * np.abs(np.cos(theta(x_cut))) ** a

    y_cut_bottom = y_cut - c_thick/2 * np.sin(theta(x_cut)) - c_trans/2.
    x_cut_bottom = x_cut + c_thick/2 * np.sign(np.cos(theta(x_cut))) * np.abs(np.cos(theta(x_cut))) ** a

    t_total_length = np.max(y_path) - np.min(y_path) + np.max(y_cut_top) - np.min(y_cut_bottom)
    flat_length = 1.15*t_total_length*flat_length_teeth_length/2
    # apprx_radius = flat_length * radius_to_flat_length_scale
    # total_length = flat_length * total_length_to_flat_length
    y_circ = -apprx_radius
    inner_radius = np.sqrt(y_circ**2 + flat_length**2)
    outter_radius = np.sqrt((flat_width - y_circ)**2 + flat_length**2)
    theta = np.arcsin(total_length/outter_radius)
    x_inner = inner_radius * np.sin(theta)
    y_inner = y_circ + inner_radius * np.cos(theta)

    x_suture_top = t_h/2. * np.sin(2.*np.pi * flat_width / t_w)
    # print "x_suture_top"
    # print x_suture_top

    apprx_size = total_length*2.5

    # path = np.array([x_path, y_path]).T
    path = tuple(map(tuple, np.array([x_path, y_path]).T))
    cut_top = tuple(map(tuple, np.array([x_cut_top, y_cut_top]).T))
    cut_bottom = tuple(map(tuple, np.array([x_cut_bottom, y_cut_bottom]).T))


    flat_length = flat_width/2.
    make_block(bone_name, flat_length, flat_width, apprx_radius, total_length, depth, apprx_size, theta, b_points=False)
    make_block("coll-block-temp", flat_length, flat_width, apprx_radius, total_length, depth, apprx_size, theta, b_points=False)

    # make_block("coll-temp", flat_length, flat_width+coll_shell_thick, apprx_radius, total_length, depth, apprx_size, theta, b_points=False)
    # make_block("ker-temp", flat_length, flat_width+coll_shell_thick+ker_shell_thick, apprx_radius, total_length, depth, apprx_size, theta, b_points=False)
    # cut_coll_shell()
    # cut_ker_shell()

    cut_bone(bone_name, path, cut_top, cut_bottom, flat_width, depth, False)
    cut_collagen("coll-int", "bone-2", bone_name, "coll-block-temp")
    merge_col_bone("merged-shell", "bone-2", "coll-int")
    clean_up_temps()

    # partition_keratin(ker_load_area_width, flat_width, coll_shell_thick, ker_shell_thick, depth)
    #
    # apply_virtual_topology(flat_length, flat_width, depth,coll_shell_thick,ker_shell_thick,
    #                        "coll-shell", "ker-shell", "merged-shell")
    #
    # make_surfaces("merged-shell", "ker-shell", "coll-shell", coll_shell_thick,ker_shell_thick,
    #               "merged-top", "ker-top", "ker-bottom", "coll-top", "coll-bottom",
    #               flat_length, flat_width, depth, x_suture_top)
    #
    # mesh_base()

    # make_shell_elements(outter_radius, y_circ, flat_length, flat_width)

    # point = (-324.881E-03,2.799863,1.6)

    # print find_top(point, outter_radius, y_circ, flat_length, flat_width)

    #
    # make_sets("merged-shell", "bottom-left-edge", "bottom-right-edge", "back-points",
    #           "load-area", "ker-shell",
    #           x_inner, y_inner, depth, flat_width, ker_shell_thick, coll_shell_thick)
    #
    # make_LE_material("bone", 12000, 0.25)
    # make_LE_material("keratin", 1000, 0.4)
    # make_HGO_material("int-collagen", props)
    # built_in_ogden(ogden_test_data)
    #
    # make_sections(coll_shell_thick, ker_shell_thick)
    #
    # assign_sections("merged-shell", "bone", "bone",
    #                 "collagen-int", "collagen-int",
    #                 "coll-shell", "coll-shell", "collagen-shell",
    #                 "ker-shell", "ker-shell", "keratin-shell",
    #                 flat_length, flat_width, depth, x_suture_top, coll_shell_thick, ker_shell_thick)
    #
    # make_steps("assign-fibre-direction",
    #            "load", 0.05, 1000)
    #
    # interactions("coll-to-merged", "merged-top", "merged-shell-1",
    #              "coll-shell-1", "coll-bottom", "coll-top",
    #              "ker-shell-1", "ker-bottom", "ker-to-coll")
    #
    # load_disps("merged-shell-1", "bottom-left-edge", "bottom-right-edge", "back-points",
    #            "right-slider", "left-pin", "back-lock",
    #            "load", "apply-load", "ker-shell-1", "load-area", -0.015)
    #
    remove_redundant(flat_width, depth)
    #
    # mesh_keratin(ker_el_size, ker_deviation_factor, ker_min_size_factor,
    #              flat_width, coll_shell_thick, ker_shell_thick, depth)
    #
    # mesh_coll(coll_el_size, coll_deviation_factor, coll_min_size_factor,
    #           flat_width, coll_shell_thick, depth)
    #
    # mesh_merged("merged-shell", mer_el_size, mer_deviation_factor,
    #             mer_min_size_factor,
    #             x_suture_top, flat_length, flat_width)


def read_chunk(odbPath, step, frame, field_name):
    # print "hi"
    frame = str(frame)
    # print frame
    comand = "abaqus readChunk " + odbPath + " " + step + " " + frame + " " + field_name
    # print(comand)
    # os.system("abaqus readChunk " + odbPath + " " + step + " " + frame + " " + field_name)
    os.system(comand)

    nBlocks = np.fromfile("./temp/" + step +"/" + frame + "/" + field_name + "/nBlocks.bin", dtype=np.int8)[0]

    data = np.array([])
    for chunk in range(1, nBlocks+1):

        data = np.concatenate((data, np.fromfile("./temp/" + step + "/" + frame + "/" + field_name + "/" + str(chunk) + "data.bin", dtype=np.float32)))

    return data

def load_chunk(base_dir, step, frame, field_name):
    # print(base_dir)
    # print(step)
    # print(frame)
    # print(field_name)
    frame = str(frame)
    nBlocks = np.fromfile(base_dir + "/" + step + "/" + frame + "/" + field_name + "/nBlocks.bin", dtype=np.int8)[0]

    data = np.array([])
    for chunk in range(1, nBlocks+1):

        data = np.concatenate((data, np.fromfile(base_dir + "/" + step + "/" + frame + "/" + field_name + "/" + str(chunk) + "data.bin", dtype=np.float32)))

    return data

def Asave_averaged_values():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    print 1
    path = '/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/simulations/multi-scale/first-test/Job-1.odb'
    o3 = session.openOdb(
        name='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/simulations/multi-scale/first-test/Job-1.odb',
        readOnly=False)
    print 2

    n_points = 0
    for step_key in o3.steps.keys():

        if 'deload' not in step_key:
            n_points += np.alen(o3.steps[step_key].frames)
    print 3

    s_avg = np.zeros([n_points, 6])
    f_avg = np.zeros([n_points, 9])

    point_i = 0
    print 4

    for step_key in o3.steps.keys():

        print step_key, " of ", o3.steps.keys()
        # print 5

        if 'deload' not in step_key:

            step = o3.steps[step_key]
            # frame_
            nFrames = np.alen(step.frames)
            for iFrame in range(nFrames):
                print "Frame ", iFrame + 1, " of ", nFrames

                # os.system("mkdir -p AtestDir")
                # s_weighted = np.zeros([np.alen(frame.fieldOutputs['UVARM1'].values), 6])
                # uvarm1 = read_odb_utils.read_chunk(step_key, step_key, iFrame+1, 'UVARM1')
                # uvarm1 = read_chunk(path, step_key, iFrame, 'UVARM1')
                # print uvarm1

                print "Getting stresses..."
                # s_weighted = np.array([read_chunk(path, step_key, iFrame, 'UVARM'+str(i)) for i in range(1, 7)])
                # print "./Job-1"
                # print step_key
                # print iFrame + 1
                # print 'UVARM'+str(1)
                s_weighted = np.array([load_chunk("./Job-1", step_key, iFrame+1, 'UVARM'+str(i)) for i in range(1, 7)])
                # s_weighted = np.array([[frame.fieldOutputs['UVARM'+str(i)].values[j].data for j in range(np.alen(frame.fieldOutputs['UVARM1'].values))]
                #                        for i in range(1, 7)])

                print "Deformation gradient..."
                f_weighted = np.array([load_chunk("./Job-1", step_key, iFrame+1, 'UVARM'+str(i)) for i in range(7, 16)])

                # f_weighted = np.array([read_chunk(path, step_key, iFrame, 'UVARM'+str(i)) for i in range(7, 16)])
                # f_weighted = np.array([[frame.fieldOutputs['UVARM'+str(i)].values[j].data for j in range(np.alen(frame.fieldOutputs['UVARM1'].values))]
                #                        for i in range(7, 16)])

                print "Getting volume"
                v = np.array([load_chunk("./Job-1", step_key, iFrame+1, 'UVARM16')]).sum()
                # v = np.array([frame.fieldOutputs['UVARM16'].values[j].data for j in range(np.alen(frame.fieldOutputs['UVARM1'].values))]).sum()

                # print 9


                s_avg[point_i, :] = s_weighted.sum(axis=1)/v
                f_avg[point_i, :] = f_weighted.sum(axis=1)/v
                point_i += 1
                # print s_weighted
                # print s_weighted.sum(axis=1)
                # print s_weighted.sum(axis=1)/v

    np.save('stress_avg', s_avg)
    np.save('def_avg', f_avg)


def make_block(part_name, flat_length, flat_width, depth, apprx_size, b_points=False):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior

    s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=apprx_size)
    g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=STANDALONE)
    s.rectangle(point1=(0.0, 0.0), point2=(-flat_length, flat_width))
    s.FixedConstraint(entity=g[5])
    s.FixedConstraint(entity=g[4])
    s.FixedConstraint(entity=g[3])
    s.FixedConstraint(entity=g[2])

    # s.CircleByCenterPerimeter(center=(0.0, -apprx_radius), point1=(-flat_length, 0.0))
    # s.CircleByCenterPerimeter(center=(0.0, -apprx_radius), point1=(-flat_length, flat_width))
    # s.ConstructionLine(point1=(0.0, -apprx_radius), point2=(-np.sin(theta), -apprx_radius+np.cos(theta)))
    # # s.VerticalConstraint(entity=g[8], addUndoState=False)
    # s.FixedConstraint(entity=g[8])
    # s.Line(point1=(-7.75, 3.375), point2=(-6.625, 2.0))
    # break_point(b_points, "1.1")
    # s.CoincidentConstraint(entity1=v[6], entity2=g[6])
    # break_point(b_points, "1.2")
    # s.CoincidentConstraint(entity1=v[5], entity2=g[7])
    # break_point(b_points, "1.3")
    # s.CoincidentConstraint(entity1=v[5], entity2=g[8])
    # break_point(b_points, "1")
    # s.CoincidentConstraint(entity1=v[6], entity2=g[8])
    # # s.PerpendicularConstraint(entity1=g[9], entity2=g[6])
    # # session.viewports['Viewport: 1'].view.setValues(nearPlane=24.1058,
    # #     farPlane=32.4627, width=30.1321, height=15.2269, cameraPosition=(
    # #     1.25103, -3.15898, 28.2843), cameraTarget=(1.25103, -3.15898, 0))
    # # s.PerpendicularConstraint(entity1=g[9], entity2=g[7])
    # break_point(b_points, "1")
    # s.autoTrimCurve(curve1=g[7], point1=(-(total_length+5), flat_width))
    # break_point(b_points, "2")
    # s.autoTrimCurve(curve1=g[6], point1=(-(total_length+5), -4.65462303161621))
    # break_point(b_points, "3")
    # # s.autoTrimCurve(curve1=g[12], point1=(-(total_length*0.99), -flat_width))
    # s.autoTrimCurve(curve1=g[12], point1=(-(total_length*0.999), np.sqrt(apprx_radius**2-(total_length*0.99)**2)-apprx_radius))
    # break_point(b_points, "4")
    # s.autoTrimCurve(curve1=g[13], point1=(-0.0168264961242676, 0.0302357912063599))
    # break_point(b_points, "5")
    # s.autoTrimCurve(curve1=g[15], point1=(0.90483856201172, 0.302357912063599))
    # break_point(b_points, "6")
    # s.autoTrimCurve(curve1=g[11], point1=(0.01, flat_width*1.05))
    # break_point(b_points, "7")
    # s.autoTrimCurve(curve1=g[4], point1=(-flat_length, flat_width/2.))
    # break_point(b_points, "8")


    p = mdb.models['Model-1'].Part(name=part_name, dimensionality=THREE_D,
                                   type=DEFORMABLE_BODY)
    p = mdb.models['Model-1'].parts[part_name]
    p.BaseSolidExtrude(sketch=s, depth=depth)
    s.unsetPrimaryObject()
    p = mdb.models['Model-1'].parts[part_name]
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    del mdb.models['Model-1'].sketches['__profile__']
    p = mdb.models['Model-1'].parts[part_name]
    f = p.faces
    # p.Mirror(mirrorPlane=f[4], keepOriginal=ON)
    p.Mirror(mirrorPlane=f.findAt(coordinates=(0.0, flat_width/2., depth/2.)), keepOriginal=ON)


def cut_coll_shell():
    a1 = mdb.models['Model-1'].rootAssembly
    a1.Instance(name='coll-temp-1',
                part=mdb.models['Model-1'].parts['coll-temp'],
                dependent=ON)
    a1.Instance(name='bone-1',
                part=mdb.models['Model-1'].parts['bone'],
                dependent=ON)
    a1.InstanceFromBooleanCut(name='coll-shell',
                              instanceToBeCut=mdb.models['Model-1'].rootAssembly.instances['coll-temp-1'],
                              cuttingInstances=(a1.instances['bone-1'], ),
                              originalInstances=SUPPRESS)


def cut_ker_shell():
    a1 = mdb.models['Model-1'].rootAssembly
    a1.Instance(name='ker-temp-1',
                part=mdb.models['Model-1'].parts['ker-temp'],
                dependent=ON)
    a1.Instance(name='coll-temp-1',
                part=mdb.models['Model-1'].parts['coll-temp'],
                dependent=ON)
    a1.InstanceFromBooleanCut(name='ker-shell',
        instanceToBeCut=mdb.models['Model-1'].rootAssembly.instances['ker-temp-1'], 
        cuttingInstances=(a1.instances['coll-temp-1'], ), 
        originalInstances=SUPPRESS)


def cut_bone(part_name, path, cut_top, cut_bottom, flat_width, depth, b_points):
    p = mdb.models['Model-1'].parts[part_name]
    f, e = p.faces, p.edges
    # t = p.MakeSketchTransform(sketchPlane=f[6], sketchUpEdge=e[21],
    t = p.MakeSketchTransform(sketchPlane=f.findAt(coordinates=(0.0, flat_width, depth/2.)), sketchUpEdge=e.findAt(coordinates=(0.0, flat_width, depth)),
                              sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, flat_width, 0.0))
    s = mdb.models['Model-1'].ConstrainedSketch(name='__sweep__', sheetSize=depth*3.,
                                                gridSpacing=depth/10., transform=t)
    g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
    s.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=9.34539,
    #     farPlane=14.0878, width=4.72059, height=2.3855, cameraPosition=(
    #     -0.283552, 12, 2.13615), cameraTarget=(-0.283552, 2, 2.13615))
    s.Spline(points=path)
    break_point(b_points, "1")
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=9.21486,
    #     farPlane=14.2183, width=5.89536, height=2.97916, cameraPosition=(
    #     -0.318505, 12, 1.86906), cameraTarget=(-0.318505, 2, 1.86906))
    s.unsetPrimaryObject()
    s.unsetPrimaryObject()
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=33.3508,
    #     farPlane=54.7613, width=21.1243, height=10.1162, cameraPosition=(
    #     -25.6409, 29.4637, -19.2963), cameraUpVector=(0.914255, -0.00632095,
    #     -0.40509), cameraTarget=(-0.664081, -0.017676, 1.44713))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=33.8794,
    #     farPlane=54.1226, width=21.4591, height=10.2766, cameraPosition=(
    #     -24.3944, 34.1074, -12.5509), cameraUpVector=(0.869373, -0.0265511,
    #     -0.493442), cameraTarget=(-0.658399, 0.00349182, 1.47788))
    p = mdb.models['Model-1'].parts[part_name]
    f1, e1 = p.faces, p.edges
    # t = p.MakeSketchTransform(sketchPlane=f1[9], sketchUpEdge=e1[22],
    t = p.MakeSketchTransform(sketchPlane=f1.findAt(coordinates=(0.0, flat_width/2., 0.0)), sketchUpEdge=e1.findAt(coordinates=(0.0, flat_width, 0.0)),
                              sketchPlaneSide=SIDE1, sketchOrientation=RIGHT, origin=(0.0, 0.0, 0.0))
    s1 = mdb.models['Model-1'].ConstrainedSketch(name='__profile__',
                                                 sheetSize=40.58, gridSpacing=1.01, transform=t)
    g1, v1, d1, c1 = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    s1.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts[part_name]
    p.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=39.2367,
    #     farPlane=44.9334, width=9.72343, height=4.91363, cameraPosition=(
    #     -0.583853, 0.115076, -40.5851), cameraTarget=(-0.583853, 0.115076, 0))
    top_start = cut_top[0]
    top_end = cut_top[-1]
    bottom_start = cut_bottom[0]
    bottom_end = cut_bottom[-1]
    #
    break_point(b_points, "2")
    s1.Spline(points=cut_top)
    break_point(b_points, "3")
    s1.Spline(points=cut_bottom)
    break_point(b_points, "4")
    s1.Line(point1=top_start, point2=bottom_start)
    break_point(b_points, "5")
    s1.Line(point1=top_end, point2=bottom_end)
    break_point(b_points, "6")
    # s1.VerticalConstraint(entity=g1[15], addUndoState=False)
    break_point(b_points, "6.1")
    s1.unsetPrimaryObject()
    break_point(b_points, "6.2")
    p = mdb.models['Model-1'].parts[part_name]
    break_point(b_points, "6.3")
    f, e = p.faces, p.edges
    break_point(b_points, "7")

    p.CutSweep(pathPlane=f.findAt(coordinates=(0.0, flat_width, depth/2.)), pathUpEdge=e.findAt(coordinates=(0.0, flat_width, depth)),
               sketchPlane=f.findAt(coordinates=(0.0, flat_width/2., 0.0)),
               sketchUpEdge=e.findAt(coordinates=(0.0, flat_width, 0.0)),
               pathOrientation=RIGHT, path=s,
               sketchOrientation=RIGHT, profile=s1, profileNormal=ON)
    break_point(b_points, "8")
    del mdb.models['Model-1'].sketches['__sweep__']
    break_point(b_points, "9")
    del mdb.models['Model-1'].sketches['__profile__']
    break_point(b_points, "10")


def cut_collagen(part_name, bone_instance_name, bone_name, coll_block_name):
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    a1 = mdb.models['Model-1'].rootAssembly
    p = mdb.models['Model-1'].parts[bone_name]
    a1.Instance(name=bone_instance_name, part=p, dependent=ON)
    p = mdb.models['Model-1'].parts[coll_block_name]
    a1.Instance(name='coll-block-1', part=p, dependent=ON)
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanCut(name='coll-int',
                              instanceToBeCut=mdb.models['Model-1'].rootAssembly.instances['coll-block-1'],
                              cuttingInstances=(a1.instances[bone_instance_name], ),
                              originalInstances=SUPPRESS)



def merge_col_bone(merged_part_name, bone_instance, coll_name):
    coll_instance = coll_name +'-1'
    a = mdb.models['Model-1'].rootAssembly
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    a = mdb.models['Model-1'].rootAssembly
    a.features[bone_instance].resume()
    a1 = mdb.models['Model-1'].rootAssembly
    a1.InstanceFromBooleanMerge(name=merged_part_name, instances=(
        a1.instances[bone_instance], a1.instances[coll_instance], ),
                                keepIntersections=ON, originalInstances=SUPPRESS, domain=GEOMETRY)


def clean_up_temps():
    a = mdb.models['Model-1'].rootAssembly
    a.deleteFeatures(('bone-1', 'ker-temp-1', 'coll-temp-1', 'bone-2',
        'coll-block-1', 'coll-int-1', ))

    del mdb.models['Model-1'].parts['bone']
    del mdb.models['Model-1'].parts['coll-block-temp']
    del mdb.models['Model-1'].parts['coll-int']
    # del mdb.models['Model-1'].parts['coll-temp']
    # del mdb.models['Model-1'].parts['ker-temp']


def partition_keratin(load_area_width, flat_width, coll_shell_thick, ker_shell_thick, depth):
    debug = False
    p = mdb.models['Model-1'].parts['ker-shell']
    f, e, d1 = p.faces, p.edges, p.datums
    s_plane = f.findAt(coordinates=(0, flat_width + coll_shell_thick +ker_shell_thick, depth/2.0))
    up_edge = e.findAt(coordinates=(0.0, flat_width + coll_shell_thick +ker_shell_thick, depth))
    t = p.MakeSketchTransform(sketchPlane=s_plane, sketchUpEdge=up_edge,
        sketchPlaneSide=SIDE1, origin=(0.0, flat_width + coll_shell_thick +ker_shell_thick, depth/2.0))
    s1 = mdb.models['Model-1'].ConstrainedSketch(name='__profile__', 
        sheetSize=7.99, gridSpacing=0.19, transform=t)
    g, v, d, c = s1.geometry, s1.vertices, s1.dimensions, s1.constraints
    s1.setPrimaryObject(option=SUPERIMPOSE)
    p = mdb.models['Model-1'].parts['ker-shell']
    p.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)
    s1.rectangle(point1=(-depth/2.0, -load_area_width/2.), point2=(depth/2.0, load_area_width/2.))
    break_point(debug, "1")
    # s1.CoincidentConstraint(entity1=v.findAt((-1.5, -0.38)), entity2=g.findAt((
    #     -1.5, -0.659935)), addUndoState=False)
    # s1.CoincidentConstraint(entity1=v.findAt((1.5, 0.19)), entity2=g.findAt((1.5,
    #     0.659935)), addUndoState=False)
    # p = mdb.models['Model-1'].parts['ker-shell']
    c = p.cells
    # pickedCells = c.findAt(((0.439957, 3.1, 2.0), ))
    pickedCells = c.findAt(((0.0, flat_width + coll_shell_thick +ker_shell_thick/2.0, depth/2.0), ))
    f1, e1, d2 = p.faces, p.edges, p.datums
    p.PartitionCellBySketch(sketchPlane=s_plane, sketchUpEdge=up_edge,
        cells=pickedCells, sketch=s1)
    s1.unsetPrimaryObject()
    del mdb.models['Model-1'].sketches['__profile__']

    e1, v2, d2 = p.edges, p.vertices, p.datums
    p.PartitionCellByPlanePointNormal(point=v2.findAt(coordinates=(load_area_width/2., flat_width + coll_shell_thick +ker_shell_thick, 0.0)),
                                      normal=e1.findAt(coordinates=(0.9*load_area_width/2., flat_width+coll_shell_thick +ker_shell_thick, 0.0)),cells=pickedCells)
    pickedCells = c.findAt(((0.0, flat_width + coll_shell_thick +ker_shell_thick/2.0, depth/2.0), ))
    p.PartitionCellByPlanePointNormal(point=v2.findAt(coordinates=(-load_area_width/2., flat_width + coll_shell_thick +ker_shell_thick, 0.0)),
                                      normal=e1.findAt(coordinates=(-0.9*load_area_width/2., flat_width+coll_shell_thick +ker_shell_thick, 0.0)),cells=pickedCells)
#     break_point(debug, "2")
# # session.viewports['Viewport: 1'].view.setValues(nearPlane=14.6434,
#     #     farPlane=22.7652, width=9.46804, height=4.71756, cameraPosition=(
#     #     8.16042, 0.708728, 18.1669), cameraUpVector=(-0.558104, 0.829762,
#     #     0.00391689), cameraTarget=(0.393824, 2.95558, 1.63561),
#     #     viewOffsetX=-0.36568, viewOffsetY=0.0117913)
#     # p = mdb.models['Model-1'].parts['ker-shell']
#     # f, e, d1 = p.faces, p.edges, p.datums
#     s_plane = f.findAt(coordinates=(0, flat_width + coll_shell_thick, depth/2.0))
#     up_edge = e.findAt(coordinates=(0.0, flat_width + coll_shell_thick, 0.0))
#     t = p.MakeSketchTransform(sketchPlane=s_plane, sketchUpEdge=up_edge,
#         sketchPlaneSide=SIDE1, origin=(0.0, flat_width + coll_shell_thick, depth/2.0))
#     s = mdb.models['Model-1'].ConstrainedSketch(name='__profile__', sheetSize=7.99,
#         gridSpacing=0.19, transform=t)
#     g, v, d, c = s.geometry, s.vertices, s.dimensions, s.constraints
#     s.setPrimaryObject(option=SUPERIMPOSE)
#     p = mdb.models['Model-1'].parts['ker-shell']
#     p.projectReferencesOntoSketch(sketch=s, filter=COPLANAR_EDGES)
#     s.rectangle(point1=(-depth/2.0, -load_area_width/2.), point2=(depth/2.0, load_area_width/2.))
#     # s.CoincidentConstraint(entity1=v.findAt((-1.5, -0.38)), entity2=g.findAt((-1.5,
#     #     -1.187883)), addUndoState=False)
#     # s.CoincidentConstraint(entity1=v.findAt((1.5, 0.19)), entity2=g.findAt((1.5,
#     #     1.187883)), addUndoState=False)
#     p = mdb.models['Model-1'].parts['ker-shell']
#     c = p.cells
#     # pickedCells = c.findAt(((-0.69329, 3.2, 1.0), ))
#     f1, e1, d2 = p.faces, p.edges, p.datums
#     p.PartitionCellBySketch(sketchPlane=s_plane, sketchUpEdge=up_edge,
#                             cells=pickedCells, sketch=s)
#     s.unsetPrimaryObject()
#     del mdb.models['Model-1'].sketches['__profile__']
    p = mdb.models['Model-1'].parts['ker-shell']
    p.regenerate()




def apply_virtual_topology(flat_length, flat_width, depth,coll_shell_thick,ker_shell_thick,
                           collagen_membrane, keratin_shell, merged_part_name):

    # p = mdb.models['Model-1'].parts[collagen_membrane]
    # session.viewports['Viewport: 1'].setValues(displayedObject=p)
    # p = mdb.models['Model-1'].parts[collagen_membrane]
    # e = p.edges
    # # edges = e.findAt(((flat_length, flat_width, depth/2.), ), ((-flat_length, flat_width, depth/2.), ))
    # # v = p.vertices
    # # verts = v.findAt(((flat_length, flat_width, depth), ), ((flat_length, flat_width, 0.0), ), ((-flat_length, flat_width, depth), ), ((-flat_length, flat_width, 0.0), ))
    # edges = e.findAt(((flat_length, flat_width, depth/2.), ),
    #                  ((flat_length, flat_width+coll_shell_thick, depth/2.), ),
    #                  ((-flat_length, flat_width, depth/2.), ),
    #                  ((-flat_length, flat_width+coll_shell_thick, depth/2.), ))
    # v = p.vertices
    # verts = v.findAt(((flat_length, flat_width, depth), ),
    #                  ((flat_length, flat_width, 0.0), ),
    #                  ((-flat_length, flat_width, depth), ),
    #                  ((-flat_length, flat_width, 0.0), ),
    #                  ((flat_length, flat_width+coll_shell_thick, depth), ),
    #                  ((flat_length, flat_width+coll_shell_thick, 0.0), ),
    #                  ((-flat_length, flat_width+coll_shell_thick, depth), ),
    #                  ((-flat_length, flat_width+coll_shell_thick, 0.0), ))
    #
    # pickedEntities =(verts, edges, )
    # p.ignoreEntity(entities=pickedEntities)
    #
    #
    # p = mdb.models['Model-1'].parts[keratin_shell]
    # session.viewports['Viewport: 1'].setValues(displayedObject=p)
    # p = mdb.models['Model-1'].parts[keratin_shell]
    # e = p.edges
    # edges = e.findAt(((flat_length, flat_width+coll_shell_thick+ker_shell_thick, depth/2.), ),
    #                  ((flat_length, flat_width+coll_shell_thick, depth/2.), ),
    #                  ((-flat_length, flat_width+coll_shell_thick+ker_shell_thick, depth/2.), ),
    #                  ((-flat_length, flat_width+coll_shell_thick, depth/2.), ))
    # v = p.vertices
    # verts = v.findAt(((flat_length, flat_width+coll_shell_thick+ker_shell_thick, depth), ),
    #                  ((flat_length, flat_width+coll_shell_thick+ker_shell_thick, 0.0), ),
    #                  ((-flat_length, flat_width+coll_shell_thick+ker_shell_thick, depth), ),
    #                  ((-flat_length, flat_width+coll_shell_thick+ker_shell_thick, 0.0), ),
    #                  ((flat_length, flat_width+coll_shell_thick, depth), ),
    #                  ((flat_length, flat_width+coll_shell_thick, 0.0), ),
    #                  ((-flat_length, flat_width+coll_shell_thick, depth), ),
    #                  ((-flat_length, flat_width+coll_shell_thick, 0.0), ))
    # # edges = e.findAt(((flat_length, flat_width, depth/2.), ), ((-flat_length, flat_width, depth/2.), ))
    # # v = p.vertices
    # # verts = v.findAt(((flat_length, flat_width, depth), ), ((flat_length, flat_width, 0.0), ), ((-flat_length, flat_width, depth), ), ((-flat_length, flat_width, 0.0), ))
    # pickedEntities =(verts, edges, )
    # p.ignoreEntity(entities=pickedEntities)


    p = mdb.models['Model-1'].parts[merged_part_name]
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    p = mdb.models['Model-1'].parts[merged_part_name]
    e = p.edges

    edges = e.findAt(((flat_length, flat_width, depth/2.), ), ((-flat_length, flat_width, depth/2.), ))
    v = p.vertices
    verts = v.findAt(((flat_length, flat_width, depth), ), ((flat_length, flat_width, 0.0), ), ((-flat_length,
                                                                                                 flat_width, depth), ), ((-flat_length, flat_width, 0.0), ))
    pickedEntities =(verts, edges, )
    p.ignoreEntity(entities=pickedEntities)

    # edges = e.findAt(((-1.31987, 3.0, 0.75), ))
    # v = p.vertices
    # verts = v.findAt(((-1.31987, 3.0, 3.0), ), ((-1.31987, 3.0, 0.0), ))
    # pickedEntities =(verts, edges, )
    # p.ignoreEntity(entities=pickedEntities)
    #
    # p = mdb.models['Model-1'].parts['merged-shell']
    # e = p.edges
    # edges = e.findAt(((1.31987, 3.0, 0.75), ))
    # v = p.vertices
    # verts = v.findAt(((1.31987, 3.0, 0.0), ), ((1.31987, 3.0, 3.0), ))
    # pickedEntities =(verts, edges, )
    # p.ignoreEntity(entities=pickedEntities)

    session.viewports['Viewport: 1'].view.setValues(nearPlane=15.0565,
                                                    farPlane=24.9429, width=11.7737, height=5.6597, cameraPosition=(
            7.09482, -15.0127, 10.4169), cameraUpVector=(0.0615383, 0.778287,
                                                         0.624886), cameraTarget=(0.325857, 1.1457, 1.45536))
    p = mdb.models['Model-1'].parts[merged_part_name]
    e = p.edges

    edges = e.findAt(((flat_length, 0.0, depth/2.), ), ((-flat_length, 0.0, depth/2.), ))
    v = p.vertices
    verts = v.findAt(((flat_length, 0.0, depth), ), ((flat_length, 0.0, 0.0), ), ((-flat_length,
                                                                                   0.0, depth), ), ((-flat_length, 0.0, 0.0), ))
    pickedEntities =(verts, edges, )
    p.ignoreEntity(entities=pickedEntities)

    # edges = e.findAt(((-1.31987, 0.0, 0.75), ))
    # v = p.vertices
    # verts = v.findAt(((-1.31987, 0.0, 3.0), ), ((-1.31987, 0.0, 0.0), ))
    # pickedEntities =(verts, edges, )
    # p.ignoreEntity(entities=pickedEntities)
    #
    # p = mdb.models['Model-1'].parts['merged-shell']
    # e = p.edges
    # edges = e.findAt(((1.31987, 0.0, 0.75), ))
    # v = p.vertices
    # verts = v.findAt(((1.31987, 0.0, 0.0), ), ((1.31987, 0.0, 3.0), ))
    # pickedEntities =(verts, edges, )
    # p.ignoreEntity(entities=pickedEntities)

def make_surfaces(merged_part_name, keratin_shell, collagen_membrane, coll_shell_thick,ker_shell_thick,
                  merged_top, keratin_shell_top, keratin_shell_bottom, coll_membrane_top, coll_membrane_bottom,
                  flat_length, flat_width, depth, x_suture_top):

    p = mdb.models['Model-1'].parts[merged_part_name]
    s = p.faces
    # side1Faces = s.findAt(((0.136125, 0.042807, 3.0), ), ((0.397034, 0.251093,
    #     3.0), ), ((-0.309623, 2.777954, 3.0), ))
    # side1Faces = s.findAt(((0.079719, 3.0, 2.971589), ), ((-3.666878, 2.862395,
    #     1.0), ), ((0.721657, 3.0, 0.044911), ))
    side1Faces = s.findAt(((-flat_length, flat_width, 0.000001), ),
                          ((x_suture_top + 0.000001, flat_width, 0.000001), ),
                          ((flat_length, flat_width, 0.000001), ))
    p.Surface(side1Faces=side1Faces, name=merged_top)
    p.Set('new-el-set', faces=side1Faces)

    # p = mdb.models['Model-1'].parts[keratin_shell]
    # session.viewports['Viewport: 1'].setValues(displayedObject=p)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=14.671,
    #                                                 farPlane=22.1539, width=8.0914, height=3.87491, cameraPosition=(6.0296,
    #                                                                                                                 -0.295887, 18.6031), cameraUpVector=(-0.636407, 0.771247, -0.0127781),
    #                                                 cameraTarget=(0.332166, 2.62969, 1.45619))
    # p = mdb.models['Model-1'].parts[keratin_shell]
    # s = p.faces
    # side1Faces = s.findAt(((-flat_length*0.999, flat_width+coll_shell_thick, depth/2.), ),
    #                       ((0.0, flat_width+coll_shell_thick, depth/2.), ),
    #                       ((flat_length*0.999, flat_width+coll_shell_thick, depth/2.), ))
    # p.Surface(side1Faces=side1Faces, name=keratin_shell_bottom)
    # p = mdb.models['Model-1'].parts[keratin_shell]
    # s = p.faces
    # side1Faces = s.findAt(((-flat_length*0.999, flat_width+coll_shell_thick+ker_shell_thick, depth/2.), ),
    #                       ((0.0, flat_width+coll_shell_thick+ker_shell_thick, depth/2.), ),
    #                       ((flat_length*0.999, flat_width+coll_shell_thick+ker_shell_thick, depth/2.), ))
    # p.Surface(side1Faces=side1Faces, name=keratin_shell_top)
    #
    # p = mdb.models['Model-1'].parts[collagen_membrane]
    # session.viewports['Viewport: 1'].setValues(displayedObject=p)
    # p = mdb.models['Model-1'].parts[collagen_membrane]
    # s = p.faces
    # side1Faces = s.findAt(((0.0, flat_width, depth/2.), ),)
    # p.Surface(side1Faces=side1Faces, name=coll_membrane_bottom)
    # p = mdb.models['Model-1'].parts[collagen_membrane]
    # s = p.faces
    # side1Faces = s.findAt(((0.0, flat_width+coll_shell_thick, depth/2.), ),)
    # p.Surface(side1Faces=side1Faces, name=coll_membrane_top)

    # p = mdb.models['Model-1'].parts[load_applier_name]
    # s = p.faces
    # side1Faces = s.findAt(((0.136125, 0.042807, 3.0), ), ((0.397034, 0.251093,
    #     3.0), ), ((-0.309623, 2.777954, 3.0), ))
    # side1Faces = s.findAt(((0.079719, 3.0, 2.971589), ), ((-3.666878, 2.862395,
    #     1.0), ), ((0.721657, 3.0, 0.044911), ))
    # side1Faces = s.findAt(((0, applier_radius, depth/2.), ))
    # p.Surface(side1Faces=side1Faces, name=applier_surface)

def make_sets(merged_part_name, bottom_left_edge, bottom_right_edge, back_points,
              load_area, ker_part_name,
              x_inner, y_inner, depth, flat_width, ker_shell_thick, coll_shell_thick):

    session.viewports['Viewport: 1'].view.setValues(nearPlane=11.1156,
                                                    farPlane=16.7843, width=10.1392, height=4.85558, cameraPosition=(
            -0.678634, -7.72229, 11.927), cameraUpVector=(0.0224262, 0.938992,
                                                          0.343206), cameraTarget=(-0.375113, 1.46094, 0.798732),
                                                    viewOffsetX=0.579761, viewOffsetY=-0.248695)
    p = mdb.models['Model-1'].parts[merged_part_name]
    e = p.edges
    edges = e.findAt(((-x_inner, y_inner, depth/2.), ))
    p.Set(edges=edges, name=bottom_left_edge)
    p = mdb.models['Model-1'].parts[merged_part_name]
    e = p.edges
    edges = e.findAt(((x_inner, y_inner, depth/2.), ))
    p.Set(edges=edges, name=bottom_right_edge)
    p = mdb.models['Model-1'].parts[merged_part_name]
    v = p.vertices
    # verts = v.findAt(((-3.047445, -0.12578, 0.0), ), ((3.047445, -0.12578, 0.0), ))
    verts = v.findAt(((-x_inner, y_inner, 0.0), ), ((x_inner, y_inner, 0.0), ))
    p.Set(vertices=verts, name=back_points)
    p = mdb.models['Model-1'].parts[ker_part_name]
    f = p.faces
    face = f.findAt(((0.0, flat_width + ker_shell_thick+coll_shell_thick, depth/2.), ))
    p.Set(faces=face, name=load_area)

    # p = mdb.models['Model-1'].parts['load-applier']
    # f = p.faces
    # # faces = f.findAt(((-0.999248, -0.038778, 0.666667), ), ((0.0, 0.98203, 2.0), ),
    # #     ((0.0, 0.98203, 0.0), ))
    # p.Set(faces=f, name=applier_set)

def make_LE_material(bone_mat_name, youngs_mod, poi):

    mdb.models['Model-1'].Material(name=bone_mat_name)
    mdb.models['Model-1'].materials[bone_mat_name].Elastic(table=((youngs_mod, poi), ))

def make_HGO_material(name, props):

    mdb.models['Model-1'].Material(name=name)
    mdb.models['Model-1'].materials[name].Depvar(n=5)
    mdb.models['Model-1'].materials[name].UserMaterial(mechanicalConstants=props)

def built_in_ogden(test_data):
    mdb.models['Model-1'].Material(name='shell-collagen')
    mdb.models['Model-1'].materials['shell-collagen'].Hyperelastic(
        materialType=ISOTROPIC, type=OGDEN, n=3, 
        volumetricResponse=POISSON_RATIO, poissonRatio=0.495, table=())
    mdb.models['Model-1'].materials['shell-collagen'].hyperelastic.UniaxialTestData(table=test_data)


def make_sections(coll_shell_thick, ker_shell_thick):
    mdb.models['Model-1'].HomogeneousSolidSection(name='bone', material='bone', 
        thickness=None)
    mdb.models['Model-1'].HomogeneousSolidSection(name='collagen-int', 
        material='int-collagen', thickness=None)
    mdb.models['Model-1'].HomogeneousShellSection(name='collagen-shell', 
        preIntegrate=OFF, material='shell-collagen', thicknessType=UNIFORM, 
        thickness=coll_shell_thick, thicknessField='', idealization=NO_IDEALIZATION,
        poissonDefinition=DEFAULT, thicknessModulus=None, temperature=GRADIENT, 
        useDensity=OFF, integrationRule=GAUSS, numIntPts=3)
    mdb.models['Model-1'].HomogeneousShellSection(name='keratin-shell', 
        preIntegrate=OFF, material='keratin', thicknessType=UNIFORM, 
        thickness=ker_shell_thick, thicknessField='', idealization=NO_IDEALIZATION,
        poissonDefinition=DEFAULT, thicknessModulus=None, temperature=GRADIENT, 
        useDensity=OFF, integrationRule=GAUSS, numIntPts=3)


def assign_sections(merged_part_name, bone_region_name, bone_section,
                    collagen_region_name, collagen_int_section_name,
                    collagen_membrane, collagen_membrane_region, collagen_membrane_name,
                    keratin_shell, keratin_region_name, keratin_section_name,
                    flat_length, flat_width, depth, x_suture_top, coll_shell_thick, ker_shell_thick):

    p = mdb.models['Model-1'].parts[merged_part_name]
    c = p.cells
    # cells = c.getSequenceFromMask(mask=('[#3 ]', ), )
    cells = c.findAt(((-flat_length, flat_width, 0.000001), ), ((flat_length, flat_width, 0.000001), ))
    region = p.Set(cells=cells, name=bone_region_name)
    p = mdb.models['Model-1'].parts[merged_part_name]
    p.SectionAssignment(region=region, sectionName=bone_section, offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)

    p = mdb.models['Model-1'].parts[merged_part_name]
    c = p.cells
    # cells = c.getSequenceFromMask(mask=('[#4 ]', ), )
    cells = c.findAt(((x_suture_top + 0.000001, flat_width, 0.000001), ))
    region = p.Set(cells=cells, name=collagen_region_name)
    p = mdb.models['Model-1'].parts[merged_part_name]
    p.SectionAssignment(region=region, sectionName=collagen_int_section_name, offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)

    p = mdb.models['Model-1'].parts[collagen_membrane]
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    p = mdb.models['Model-1'].parts[collagen_membrane]
    f = p.faces
    c = p.cells

    # faces = f.getSequenceFromMask(mask=('[#7 ]', ), )
    cells = c.findAt(((0.0, flat_width+ coll_shell_thick/2., depth/2.), ))
    region = p.Set(cells=cells, name=collagen_membrane_region)
    p = mdb.models['Model-1'].parts[collagen_membrane]
    p.SectionAssignment(region=region, sectionName=collagen_membrane_name, offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)

    p = mdb.models['Model-1'].parts[keratin_shell]
    session.viewports['Viewport: 1'].setValues(displayedObject=p)
    p = mdb.models['Model-1'].parts[keratin_shell]
    f = p.faces
    c = p.cells

    # faces = f.getSequenceFromMask(mask=('[#7 ]', ), )
    cells = c.findAt(((0.0, flat_width+coll_shell_thick+ker_shell_thick/2., depth/2.),
                      (flat_length, flat_width+coll_shell_thick+ker_shell_thick/2., depth/2.),
                      (-flat_length, flat_width+coll_shell_thick+ker_shell_thick/2., depth/2.)))
    region = p.Set(cells=cells, name=keratin_region_name)
    p = mdb.models['Model-1'].parts[keratin_shell]
    p.SectionAssignment(region=region, sectionName=keratin_section_name, offset=0.0,
                        offsetType=MIDDLE_SURFACE, offsetField='',
                        thicknessAssignment=FROM_SECTION)


def make_steps(fibre_step_name,
               load_step_name, maxInc, maxNumInc):

    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        adaptiveMeshConstraints=ON)
    mdb.models['Model-1'].StaticStep(name=fibre_step_name,
                                     previous='Initial', nlgeom=ON)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        step=fibre_step_name)
    mdb.models['Model-1'].StaticStep(name=load_step_name,
                                     previous=fibre_step_name, maxNumInc=maxNumInc, initialInc=maxInc,
                                     maxInc=maxInc)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(step=load_step_name)


def interactions(coll_membrane_to_merge, merged_top, merged_instance_name,
                 collagen_membrane_instance, coll_membrane_bottom, coll_membrane_top,
                 keratin_shell_instance, keratin_shell_bottom, ker_shell_to_merge):

    # session.viewports['Viewport: 1'].assemblyDisplay.hideInstances(instances=(
    #     'coll-membrane-1', 'keratin-shell-1', ))
    # session.viewports['Viewport: 1'].assemblyDisplay.hideInstances(instances=(
    #     'merged-shell-1', ))
    # session.viewports['Viewport: 1'].assemblyDisplay.showInstances(instances=(
    #     'coll-membrane-1', ))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=14.6544,
    #     farPlane=25.0773, width=11.5027, height=5.50856, cameraPosition=(
    #     10.6324, -2.61129, 17.7751), cameraUpVector=(-0.222484, 0.974301,
    #     -0.0351907), cameraTarget=(0.328122, 1.17818, 1.4447))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=14.8058,
    #     farPlane=24.4056, width=11.6215, height=5.56545, cameraPosition=(
    #     6.33746, 13.761, 15.4283), cameraUpVector=(-0.573711, 0.490621,
    #     -0.655856), cameraTarget=(0.287498, 1.33304, 1.4225))
    a = mdb.models['Model-1'].rootAssembly
    s1 = a.instances[merged_instance_name].faces
    # side1Faces1 = s1.getSequenceFromMask(mask=('[#84244 ]', ), )
    # region1=a.Surface(side1Faces=side1Faces1, name=merged_top)
    region1=a.instances[merged_instance_name].surfaces[merged_top]
    a = mdb.models['Model-1'].rootAssembly
    s1 = a.instances[collagen_membrane_instance].faces
    # side2Faces1 = s1.getSequenceFromMask(mask=('[#7 ]', ), )
    # region2=a.Surface(side2Faces=side2Faces1, name=coll_membrane_bottom)
    region2=a.instances[collagen_membrane_instance].surfaces[coll_membrane_bottom]
    mdb.models['Model-1'].Tie(name=coll_membrane_to_merge, master=region1,
                              slave=region2, positionToleranceMethod=COMPUTED, adjust=ON,
                              tieRotations=ON, thickness=ON)

    # session.viewports['Viewport: 1'].assemblyDisplay.hideInstances(instances=(
    #     'coll-membrane-1', ))
    # session.viewports['Viewport: 1'].assemblyDisplay.showInstances(instances=(
    #     'keratin-shell-1', ))

    a = mdb.models['Model-1'].rootAssembly
    # region1=a.surfaces[merged_top]
    region2=a.instances[collagen_membrane_instance].surfaces[coll_membrane_top]
    a = mdb.models['Model-1'].rootAssembly
    s1 = a.instances[keratin_shell_instance].faces
    # side2Faces1 = s1.getSequenceFromMask(mask=('[#7 ]', ), )
    # region2=a.Surface(side2Faces=side2Faces1, name=keratin_shell_bottom)
    region1=a.instances[keratin_shell_instance].surfaces[keratin_shell_bottom]
    mdb.models['Model-1'].Tie(name=ker_shell_to_merge, master=region1,
                              slave=region2, positionToleranceMethod=COMPUTED, adjust=ON,
                              tieRotations=ON, thickness=ON)

def load_disps(merged_instance_name, bottom_left_edge, bottom_right_edge, back_points,
               right_slider, left_slider, back_lock,
               load_step_name, apply_load,
               keratin_instance, load_area, dy):


    a = mdb.models['Model-1'].rootAssembly
    region = a.instances[merged_instance_name].sets[bottom_left_edge]
    mdb.models['Model-1'].DisplacementBC(name=left_slider, createStepName="Initial",
                                         region=region, u1=0.0, u2=0.0, u3=UNSET, ur1=UNSET, ur2=UNSET,
                                         ur3=UNSET, amplitude=UNSET, fixed=OFF, distributionType=UNIFORM,
                                         fieldName='', localCsys=None)

    a = mdb.models['Model-1'].rootAssembly
    region = a.instances[merged_instance_name].sets[bottom_right_edge]
    mdb.models['Model-1'].DisplacementBC(name=right_slider,
                                         createStepName="Initial", region=region, u1=UNSET, u2=0.0, u3=UNSET,
                                         ur1=UNSET, ur2=UNSET, ur3=UNSET, amplitude=UNSET, fixed=OFF,
                                         distributionType=UNIFORM, fieldName='', localCsys=None)

    a = mdb.models['Model-1'].rootAssembly
    region = a.instances[merged_instance_name].sets[back_points]
    mdb.models['Model-1'].DisplacementBC(name=back_lock,
                                         createStepName="Initial", region=region, u1=UNSET, u2=UNSET, u3=0.0,
                                         ur1=UNSET, ur2=UNSET, ur3=UNSET, amplitude=UNSET, fixed=OFF,
                                         distributionType=UNIFORM, fieldName='', localCsys=None)

    a = mdb.models['Model-1'].rootAssembly
    region = a.instances[keratin_instance].sets[load_area]
    mdb.models['Model-1'].DisplacementBC(name=apply_load, createStepName=load_step_name,
                                         region=region, u1=UNSET, u2=dy, u3=UNSET, ur1=UNSET, ur2=UNSET,
                                         ur3=UNSET, amplitude=UNSET, fixed=OFF, distributionType=UNIFORM,
                                         fieldName='', localCsys=None)

def mesh_keratin(ker_el_size, ker_deviation_factor, ker_min_size_factor,
                 flat_width, coll_shell_thick, ker_shell_thick, depth):

    p = mdb.models['Model-1'].parts['ker-shell']
    p.seedPart(size=ker_el_size, deviationFactor=ker_deviation_factor, minSizeFactor=ker_min_size_factor)
    p = mdb.models['Model-1'].parts['ker-shell']
    c2, e = p.cells, p.edges
    # p.setSweepPath(region=c2.findAt(coordinates=(1.106623, 3.2, 2.0)),
    #     edge=e.findAt(coordinates=(3.975862, 3.010256, 0.0)), sense=REVERSE)
    elemType1 = mesh.ElemType(elemCode=SC8R, elemLibrary=STANDARD, 
        secondOrderAccuracy=ON, hourglassControl=DEFAULT)
    elemType2 = mesh.ElemType(elemCode=SC6R, elemLibrary=STANDARD, 
        secondOrderAccuracy=ON)
    elemType3 = mesh.ElemType(elemCode=UNKNOWN_TET, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts['ker-shell']
    c = p.cells
    cells = c.findAt(((0.0, flat_width + coll_shell_thick + ker_shell_thick/2., depth/2.0), ))
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2, 
        elemType3))
    p = mdb.models['Model-1'].parts['ker-shell']
    p.generateMesh()
    # f = p.faces
    # p.assignStackDirection(referenceRegion=f[3], cells=cells)


def mesh_coll(coll_el_size, coll_deviation_factor, coll_min_size_factor,
              flat_width, coll_shell_thick, depth):

    p = mdb.models['Model-1'].parts['coll-shell']
    p.deleteMesh()
    p = mdb.models['Model-1'].parts['coll-shell']
    p.seedPart(size=coll_el_size, deviationFactor=coll_deviation_factor, minSizeFactor=coll_min_size_factor)
    p = mdb.models['Model-1'].parts['coll-shell']
    c = p.cells
    pickedRegions = c.findAt(((0.0, flat_width + coll_shell_thick/2., depth/2.0), ))
    p.setMeshControls(regions=pickedRegions, elemShape=HEX, 
        algorithm=ADVANCING_FRONT)
    p = mdb.models['Model-1'].parts['coll-shell']
    c2, e = p.cells, p.edges
    # p.setSweepPath(region=c2.findAt(coordinates=(0.0, flat_width + coll_shell_thick/2., depth/2.0)),
    #     edge=e.findAt(coordinates=(-3.961932, 2.860976, 3.0)), sense=REVERSE)
    elemType1 = mesh.ElemType(elemCode=SC8R, elemLibrary=STANDARD, 
        secondOrderAccuracy=ON, hourglassControl=DEFAULT)
    elemType2 = mesh.ElemType(elemCode=SC6R, elemLibrary=STANDARD, 
        secondOrderAccuracy=ON)
    elemType3 = mesh.ElemType(elemCode=UNKNOWN_TET, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts['coll-shell']
    c = p.cells
    cells = c.findAt(((0.0, flat_width + coll_shell_thick/2., depth/2.0), ))
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2, 
        elemType3))
    p = mdb.models['Model-1'].parts['coll-shell']
    p.generateMesh()
    # f1 = p.faces.findAt(((0.0, flat_width + coll_shell_thick, depth/2.0), ))
    f1 = p.faces
    p.assignStackDirection(referenceRegion=f1[4], cells=cells)



def mesh_merged(merged_part_name, mer_el_size, mer_deviation_factor,
                mer_min_size_factor,
                x_suture_top, flat_length, flat_width):
    p = mdb.models['Model-1'].parts[merged_part_name]
    c = p.cells
    # pickedRegions = c.getSequenceFromMask(mask=('[#7 ]', ), )
    pickedRegions = c
    p.setMeshControls(regions=pickedRegions, elemShape=TET, technique=FREE)
    elemType1 = mesh.ElemType(elemCode=C3D20R)
    elemType2 = mesh.ElemType(elemCode=C3D15)
    elemType3 = mesh.ElemType(elemCode=C3D10)
    p = mdb.models['Model-1'].parts[merged_part_name]
    c = p.cells
    # cells = c.getSequenceFromMask(mask=('[#7 ]', ), )
    cells = c
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
                                                       elemType3))

    elemType1 = mesh.ElemType(elemCode=C3D20R, elemLibrary=STANDARD)
    elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
    elemType3 = mesh.ElemType(elemCode=C3D10, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts[merged_part_name]
    c = p.cells
    # cells = c.getSequenceFromMask(mask=('[#6 ]', ), )
    cells = c.findAt(((-flat_length, flat_width, 0.000001), ), ((flat_length, flat_width, 0.000001), ))
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
                                                       elemType3))

    elemType1 = mesh.ElemType(elemCode=C3D20R, elemLibrary=STANDARD)
    elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
    elemType3 = mesh.ElemType(elemCode=C3D10H, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts[merged_part_name]
    c = p.cells
    # cells = c.getSequenceFromMask(mask=('[#1 ]', ), )
    cells = c.findAt(((x_suture_top + 0.000001, flat_width, 0.000001), ))
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2,
                                                       elemType3))
    p = mdb.models['Model-1'].parts[merged_part_name]
    p.seedPart(size=mer_el_size, deviationFactor=mer_deviation_factor, minSizeFactor=mer_min_size_factor)
    p = mdb.models['Model-1'].parts[merged_part_name]
    # p.generateMesh()

def delete_all():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    a = mdb.models['Model-1'].rootAssembly
    a.regenerate()
    session.viewports['Viewport: 1'].setValues(displayedObject=a)
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(step='load')
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(mesh=OFF, loads=ON, 
        bcs=ON, predefinedFields=ON, connectors=ON)
    session.viewports['Viewport: 1'].assemblyDisplay.meshOptions.setValues(
        meshTechnique=OFF)
    mdb.models['Model-1'].boundaryConditions.delete(('apply-load', 'back-lock', 
        'left-pin', 'right-slider', ))
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=OFF, bcs=OFF, 
        predefinedFields=OFF, interactions=ON, constraints=ON, 
        engineeringFeatures=ON)
    mdb.models['Model-1'].constraints.delete(('coll-to-merged', 'ker-to-coll', ))
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        step='assign-fibre-direction')
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(interactions=OFF, 
        constraints=OFF, connectors=OFF, engineeringFeatures=OFF, 
        adaptiveMeshConstraints=ON)
    del mdb.models['Model-1'].steps['load']
    del mdb.models['Model-1'].steps['assign-fibre-direction']
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(step='Initial')
    session.viewports['Viewport: 1'].assemblyDisplay.setValues(
        adaptiveMeshConstraints=OFF)
    a = mdb.models['Model-1'].rootAssembly
    a.deleteFeatures(('coll-shell-1', 'ker-shell-1', 'merged-shell-1', ))
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=ON, 
        engineeringFeatures=ON, mesh=OFF)
    session.viewports['Viewport: 1'].partDisplay.meshOptions.setValues(
        meshTechnique=OFF)
    p1 = mdb.models['Model-1'].parts['merged-shell']
    session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    del mdb.models['Model-1'].sections['bone']
    del mdb.models['Model-1'].sections['collagen-int']
    del mdb.models['Model-1'].sections['collagen-shell']
    del mdb.models['Model-1'].sections['keratin-shell']
    del mdb.models['Model-1'].materials['bone']
    del mdb.models['Model-1'].materials['int-collagen']
    del mdb.models['Model-1'].materials['keratin']
    del mdb.models['Model-1'].materials['shell-collagen']
    session.viewports['Viewport: 1'].partDisplay.setValues(sectionAssignments=OFF, 
        engineeringFeatures=OFF)
    session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
        referenceRepresentation=ON)
    p1 = mdb.models['Model-1'].parts['coll-shell']
    session.viewports['Viewport: 1'].setValues(displayedObject=p1)
    del mdb.models['Model-1'].parts['coll-shell']
    del mdb.models['Model-1'].parts['ker-shell']
    del mdb.models['Model-1'].parts['merged-shell']


def assign_stack():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    p = mdb.models['Model-1'].parts['coll-shell']
    c = p.cells
    pickedCells = c.getSequenceFromMask(mask=('[#1 ]', ), )
    f1 = p.faces
    p.assignStackDirection(referenceRegion=f1[4], cells=pickedCells)


def assign_ker_stack():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    session.viewports['Viewport: 1'].view.setValues(nearPlane=13.3781, 
        farPlane=20.7456, width=9.31856, height=4.65658, cameraPosition=(
        6.19816, 12.1685, 14.5333), cameraUpVector=(-0.565751, 0.588963, 
        -0.577103), cameraTarget=(0.00501439, 3.09695, 1.5033))
    p = mdb.models['Model-1'].parts['ker-shell']
    c = p.cells
    pickedCells = c.getSequenceFromMask(mask=('[#1 ]', ), )
    f = p.faces
    p.assignStackDirection(referenceRegion=f[3], cells=pickedCells)


def plane_partition():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    session.viewports['Viewport: 1'].view.setValues(nearPlane=18.9485, 
        farPlane=22.8877, width=1.11729, height=0.556702, 
        viewOffsetX=-0.045584, viewOffsetY=0.224285)
    p = mdb.models['Model-1'].parts['ker-shell']
    c = p.cells
    pickedCells = c.getSequenceFromMask(mask=('[#1 ]', ), )
    e, v1, d1 = p.edges, p.vertices, p.datums
    p.PartitionCellByPlanePointNormal(point=v1[4], normal=e[16], cells=pickedCells)


def plane_partition2():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    session.viewports['Viewport: 1'].view.setValues(nearPlane=15.4579, 
        farPlane=23.4079, width=0.741089, height=0.369256, 
        viewOffsetX=-0.230716, viewOffsetY=0.46641)
    p = mdb.models['Model-1'].parts['ker-shell']
    c = p.cells
    pickedCells = c.findAt(((0.166667, 3.51, 2.333333), ))
    e1, v2, d2 = p.edges, p.vertices, p.datums
    p.PartitionCellByPlanePointNormal(point=v2.findAt(coordinates=(0.5, 3.52, 
        3.5)), normal=e1.findAt(coordinates=(0.375, 3.52, 3.5)), 
        cells=pickedCells)


def plae_partition3():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    session.viewports['Viewport: 1'].view.setValues(nearPlane=15.7546, 
        farPlane=22.5536, width=1.15316, height=0.574576, 
        viewOffsetX=-0.0812899, viewOffsetY=0.333909)
    session.viewports['Viewport: 1'].view.setValues(width=1.22861, height=0.612169, 
        cameraPosition=(19.1164, 3.21608, 1.82917), cameraUpVector=(0, 1, 0), 
        cameraTarget=(-0.0377498, 3.21608, 1.82917), viewOffsetX=0, 
        viewOffsetY=0)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=14.1177, 
        farPlane=24.1151, width=2.33627, height=1.16407, viewOffsetX=-0.37823, 
        viewOffsetY=0.105029)
    session.viewports['Viewport: 1'].view.setValues(width=2.34088, height=1.16637, 
        cameraPosition=(0, 3.3583, 21.4577), cameraTarget=(0, 3.3583, 2.34132), 
        viewOffsetX=0, viewOffsetY=0)


def remove_redundant(flat_width, depth):
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    session.viewports['Viewport: 1'].view.setValues(nearPlane=26.3811, 
        farPlane=33.191, width=13.1709, height=6.56253, cameraPosition=(
        2.06433, -16.1075, 25.0359), cameraUpVector=(-0.0529636, 0.956564, 
        0.286671), cameraTarget=(0.126557, 1.32854, 1.05097))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=26.039, 
        farPlane=33.5417, width=13.0001, height=6.47744, cameraPosition=(
        2.97123, -25.9106, 12.6085), cameraUpVector=(-0.0176719, 0.675817, 
        0.736858), cameraTarget=(0.128686, 1.30552, 1.02179))
    p = mdb.models['Model-1'].parts['merged-shell']
    v1 = p.vertices
    p.RemoveRedundantEntities(vertexList=(v1.findAt(coordinates=(0.0, 0.0, 0.0)), 
        v1.findAt(coordinates=(0.0, flat_width, 0.0)), v1.findAt(coordinates=(0.0, flat_width, depth)), v1.findAt(coordinates=(0.0, 0.0, depth))))
    mdb.models['Model-1'].parts['merged-shell'].checkGeometry()

def save_eig_vals():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    # session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=OFF, bcs=OFF,
    #     predefinedFields=OFF, connectors=OFF)
    # mdb.jobs['Job-1'].writeInput(consistencyChecking=OFF)
    # mdb.jobs['Job-1'].submit(consistencyChecking=OFF)
    # session.mdbData.summary()
    o3 = session.openOdb(
        # name='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/simulations/fibre_effect_2/Job-1.odb', readOnly=False)
        name='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/simulations/fibre_effect_2/setup-new-constants.odb', readOnly=False)

    # lastFrame = o3.steps['load'].frames[-1]
    load_step = o3.steps['load']
    # results =


    # for frame in load_step.frames:

    frame = load_step.frames[-1]

    max1=frame.fieldOutputs['UVARM1'].values
    max2=frame.fieldOutputs['UVARM2'].values
    max3=frame.fieldOutputs['UVARM3'].values

    min1=frame.fieldOutputs['UVARM4'].values
    min2=frame.fieldOutputs['UVARM5'].values
    min3=frame.fieldOutputs['UVARM6'].values

    noels=frame.fieldOutputs['UVARM7'].values
    npts=frame.fieldOutputs['UVARM8'].values


    n_points = np.alen(max1)
    # n_points = 16

    max_vecs = np.zeros([n_points, 3])
    min_vecs = np.zeros([n_points, 3])

    print max1[0]

    collagen_instance = max1[0].instance
    elementLabels = []
    positions = []

    for i in range(n_points):

        # print i+1, '/', n_points

        max = np.array([max1[i].data, max2[i].data, max3[i].data])
        min = np.array([min1[i].data, min2[i].data, min3[i].data])


        max /= np.linalg.norm(max)
        min /= np.linalg.norm(min)

        max_vecs[i, :] = max
        min_vecs[i, :] = min

        noel = noels[i].data
        npt = npts[i].data

        el_labe = max1[i].elementLabel
        int_point = max1[i].integrationPoint
        noel = int(noel)
        print i+1, '/', n_points, "\t\tnoel ", noel, " ", el_labe, "\t\t npt ", int(npt), " ", int_point
        # positions.append((sv1[i].data, sv2[i].data, sv3[i].data))
        if len(elementLabels) == 0:
            # elementLabels.append(el_labe)
            elementLabels.append(noel)
        else:
            if elementLabels[-1] != noel:
                elementLabels.append(noel)

    # elementLabels = tuple(elementLabels)
    # positions = tuple(positions)

    elementLabels = np.array(elementLabels)

    # print("maxs")
    # print max_vecs
    #
    # print("mins")
    # print min_vecs
    #
    # print "Element labels"
    # print elementLabels

    np.save('max_vals_check_els', max_vecs)
    np.save('min_vals_check_els', min_vecs)
    np.save('element_labels_check_els', elementLabels)


    o3.save()
    o3.close()


def create_fd_field_data():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    # session.viewports['Viewport: 1'].assemblyDisplay.setValues(loads=OFF, bcs=OFF,
    #     predefinedFields=OFF, connectors=OFF)
    # mdb.jobs['Job-1'].writeInput(consistencyChecking=OFF)
    # mdb.jobs['Job-1'].submit(consistencyChecking=OFF)
    # session.mdbData.summary()
    o3 = session.openOdb(
        # name='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/simulations/bend_test/Job-1.odb', readOnly=False)
        name='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/simulations/fibre_effect_2/setup.odb', readOnly=False)

    # lastFrame = o3.steps['load'].frames[-1]
    load_step = o3.steps['load']


    for frame in load_step.frames:
        # n_pts = np.alen(frame.fieldOutputs['UVARM1'].values)
        sv1=frame.fieldOutputs['UVARM1'].values
        sv2=frame.fieldOutputs['UVARM2'].values
        sv3=frame.fieldOutputs['UVARM3'].values
        n_points = np.alen(sv1)
        collagen_instance = sv1[0].instance
        # n_elements = frame.fieldOutputs['UVARM1'].values[-1].elementLabel
        elementLabels = []
        positions = []
        # n_points = 15
        for i in range(n_points):
            comp1 = sv1[i].data
            comp2 = sv2[i].data
            comp3 = sv3[i].data
            mag = (comp1**2 + comp2**2 + comp3**2) ** 0.5
            if mag > 0:
                comp1 /= mag
                comp2 /= mag
                comp3 /= mag
            # positions.append((sv1[i].data, sv2[i].data, sv3[i].data))
            positions.append((comp1, comp2, comp3))
            if len(elementLabels) == 0:
                elementLabels.append(sv1[i].elementLabel)
            else:
                if elementLabels[-1] != sv1[i].elementLabel:
                    elementLabels.append(sv1[i].elementLabel)

        elementLabels = tuple(elementLabels)
        positions = tuple(positions)


        FD = frame.FieldOutput(name='Fibre1', description='Fibre Direction', type=VECTOR, validInvariants=(MAGNITUDE,),
                               componentLabels=('x comp', 'y comp', 'z comp'))
        # print(1)
        FD.addData(position=INTEGRATION_POINT, instance=collagen_instance, labels=elementLabels, data=positions)

    o3.save()
    o3.close()

    # lastFrame = o3.steps['assign-fibre-direction'].frames[-1]
    # collagen_instance = lastFrame.fieldOutputs['SDV1'].values[0].instance
    #
    # # print(collagen_instance)
    # n_pts = np.alen(lastFrame.fieldOutputs['SDV1'].values)
    # # print()
    # # print(lastFrame.fieldOutputs['SDV1'].values[0])
    # # print("Data")
    # # print(lastFrame.fieldOutputs['SDV1'].values[0].data)
    # # print(lastFrame.fieldOutputs['SDV1'].values[1])
    # # print("Data")
    # # print(lastFrame.fieldOutputs['SDV1'].values[1].data)
    # # print("last element")
    # # print(lastFrame.fieldOutputs['SDV1'].values[-1].elementLabel)
    # # FB = np.zeros([, 3])
    #
    # # print(collagen_instance)
    # sv1=lastFrame.fieldOutputs['SDV1'].values
    # sv2=lastFrame.fieldOutputs['SDV2'].values
    # sv3=lastFrame.fieldOutputs['SDV3'].values
    # n_points = np.alen(sv1)
    # n_elements = lastFrame.fieldOutputs['SDV1'].values[-1].elementLabel
    # elementLabels = []
    # positions = []
    # # n_points = 15
    # for i in range(n_points):
    #     positions.append((sv1[i].data, sv2[i].data, sv3[i].data))
    #     if len(elementLabels) == 0:
    #         elementLabels.append(sv1[i].elementLabel)
    #     else:
    #         if elementLabels[-1] != sv1[i].elementLabel:
    #             elementLabels.append(sv1[i].elementLabel)
    #
    # elementLabels = tuple(elementLabels)
    # positions = tuple(positions)
    # # print("*************************")
    # # print("Element labels")
    # # print(elementLabels)
    # # print("positions")
    # # print(positions)
    #
    # # positions = np.array([(sv1[i].data, sv2[i].data, sv3[i].data) for i in range(n_points)])
    # # # print("Positions")
    # # # print(positions)
    # # positions = tuple(map(tuple, positions))
    # # # print("tuple positions")
    # # # print(positions)
    # #
    # # elementLabels = tuple(map(tuple, np.array[lastFrame.fieldOutputs['SDV1'].values[i].elementLabel for i in range()]))
    # # # print("Element labels")
    # # # print(elementLabels)
    # #
    # FD = lastFrame.FieldOutput(name='FD', description='Fibre Direction', type=VECTOR, validInvariants=(MAGNITUDE,),
    #                            componentLabels=('x comp', 'y comp', 'z comp'))
    # print(1)
    # FD.addData(position=INTEGRATION_POINT, instance=collagen_instance, labels=elementLabels, data=positions)
    # print(2)
    # o3.save()
    # o3.close()

def AA_save_bite_data():
    names = [
        "bite-1",
        "bite-2",
        "bite-3",
        "bite-4",
        "bite-5",
        "bite-6",
        "bite-7",
        "bite-8",
        "bite-9"
        # "bone-for-coll"
    ]
    for name in names:
        print name
        o3 = session.openOdb(
            # name='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/simulations/bend_test/Job-1.odb', readOnly=True)
            name='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/simulations/fibre_effect_2/'+name+'.odb', readOnly=True)
            # name='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/simulations/fibre_effect_2/bite-9.odb', readOnly=True)
            # name='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/simulations/fibre_effect_2/bone-for-coll.odb', readOnly=True)
        load_step = o3.steps['load']

        # print o3.rootAssembly.instances['MERGED-SHELL-1']
        #
        # print load_step.frames[0]
        # print o3.steps['load'].historyRegions['Assembly ASSEMBLY'].historyOutputs['ALLSE'].data

        ###########################
        total_energy = np.array(o3.steps['load'].historyRegions['Assembly ASSEMBLY'].historyOutputs['ALLSE'].data)[:, 1]
        left_bone_energy = np.array(o3.steps['load'].historyRegions['ElementSet MERGED-SHELL-1.LEFT-BONE'].historyOutputs['ALLSE'].data)[:, 1]
        right_bone_energy = np.array(o3.steps['load'].historyRegions['ElementSet MERGED-SHELL-1.RIGHT-BONE'].historyOutputs['ALLSE'].data)[:, 1]
        collagen_energy = np.array(o3.steps['load'].historyRegions['ElementSet MERGED-SHELL-1.COLLAGEN'].historyOutputs['ALLSE'].data)[:, 1]
        # print total_energy
        # print left_bone_energy
        ###########################

        node_set =  o3.rootAssembly.instances['MERGED-SHELL-1'].nodeSets['LOAD-AREA']
        bone_set =  o3.rootAssembly.instances['MERGED-SHELL-1'].elementSets['LEFT-BONE']

        # n_nodes = np.alen(load_step.frames[0].fieldOutputs['RF'].getSubset(region=node_set).values)

        # print node_set.nodes[0]

        frames = load_step.frames
        n_frames = np.alen(frames)
        max_stress = np.zeros(n_frames)
        disp = np.zeros(n_frames)
        rf = np.zeros(n_frames)

        for i in range(n_frames):
            print i+1, '/', n_frames

            field=frames[i].fieldOutputs['S'].getSubset(region=bone_set).getScalarField(invariant=MISES)
            # max_val = field.values[0]
            # for val in field.values:
            #     if val > max_val:
            #         max_val = val
            #
            # print 'max_val ', max_val
            # print 'max_val.data ', max_val.data

            # print field.values[0].data
            # max_stress[i] = max_val.data

            max_stress[i] = max([ g.data for g in field.values ])
            print 'max_stress[i] ', max_stress[i]
            # current_rf = 0
            node_rfs = frames[i].fieldOutputs['RF'].getSubset(region=node_set).values
            rf[i] = -np.sum([g.data[1] for g in node_rfs])
            # print()
            node_us = frames[i].fieldOutputs['U'].getSubset(region=node_set).values
            disp[i] = -node_us[0].data[1]

        np.save("./simulation_results/COLLAGEN_EN/"+name, collagen_energy)
        np.save("./simulation_results/LEFT_BONE_EN/"+name, left_bone_energy)
        np.save("./simulation_results/RIGHT_BONE_EN/"+name, right_bone_energy)
        np.save("./simulation_results/RF2/"+name, rf)
        np.save("./simulation_results/STRESS/"+name, max_stress)
        np.save("./simulation_results/TOT_EN/"+name, total_energy)
        np.save("./simulation_results/U2/"+name, disp)
        o3.close()



    # print load_step.frames[1].fieldOutputs['S'].values[0]

    # print o3.steps['load'].historyRegions['ElementSet MERGED-SHELL-1.LEFT-BONE'].historyOutputs['ALLSE'].data
    # print o3.steps['load'].historyRegions
    # node_set=o3.rootAssembly.nodeSets['LOAD-AREA-NODE']
    # # node_set=o3.rootAssembly.instances['MERGED-SHELL-1'].nodeSets['LOAD-AREA-NODE']
    # n_frames = np.alen(load_step.frames)
    # force = np.zeros(n_frames)
    # disp = np.zeros(n_frames)
    # print load_step
    # for i in range(n_frames):
    #     disp[i] = load_step.frames[i].fieldOutputs['U'].getSubset(region=node_set).values[0].data[1]
    #
    #     r_force = 0
    #     reactionForce = load_step.frames[i].fieldOutputs['RT']
    #     subset = reactionForce.getSubset(region=node_set)
    #     for j in range(n_nodes):
    #         r_force += subset.values[j].data[1]
    #     force[i] = -r_force
    #
    # print 'disp'
    # print disp
    # print 'force'
    # print force
    # print 'diff'
    # np.savetxt(name+'_disp', disp)
    # np.savetxt(name+'_force', force)
    # print force[1:] - force[:-1]

def get_bend_test_data():
    name = "small-area-3-stiff"
    o3 = session.openOdb(
        # name='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/simulations/bend_test/Job-1.odb', readOnly=True)
        name='/home/cerecam/Benjamin_Alheit/Projects/masters-disertation/simulations/bend_test/'+name+'.odb', readOnly=True)
    load_step = o3.steps['load']

    print o3.rootAssembly.instances['MERGED-SHELL-1']

    node_set=o3.rootAssembly.nodeSets['LOAD-AREA-NODE']
    # node_set=o3.rootAssembly.instances['MERGED-SHELL-1'].nodeSets['LOAD-AREA-NODE']
    n_frames = np.alen(load_step.frames)
    force = np.zeros(n_frames)
    disp = np.zeros(n_frames)
    n_nodes = np.alen(load_step.frames[0].fieldOutputs['RT'].getSubset(region=node_set).values)
    print load_step
    for i in range(n_frames):
        disp[i] = load_step.frames[i].fieldOutputs['U'].getSubset(region=node_set).values[0].data[1]

        r_force = 0
        reactionForce = load_step.frames[i].fieldOutputs['RT']
        subset = reactionForce.getSubset(region=node_set)
        for j in range(n_nodes):
            r_force += subset.values[j].data[1]
        force[i] = -r_force

    print 'disp'
    print disp
    print 'force'
    print force
    print 'diff'
    np.savetxt(name+'_disp', disp)
    np.savetxt(name+'_force', force)
    print force[1:] - force[:-1]

    # plt.plot(disp, force)
    # plt.show()

def mesh_base():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    session.viewports['Viewport: 1'].view.setValues(nearPlane=16.0485, 
        farPlane=23.5794, width=21.8272, height=10.9072, viewOffsetX=1.95786, 
        viewOffsetY=-0.0860152)
    p = mdb.models['Model-1'].parts['merged-shell']
    p.seedPart(size=0.2, deviationFactor=0.1, minSizeFactor=0.1)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=16.4838, 
        farPlane=23.144, width=17.5928, height=8.7913, viewOffsetX=0.548579, 
        viewOffsetY=0.468278)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    pickedRegions = c.getSequenceFromMask(mask=('[#7 ]', ), )
    p.setMeshControls(regions=pickedRegions, elemShape=TET, technique=FREE)
    elemType1 = mesh.ElemType(elemCode=C3D20R)
    elemType2 = mesh.ElemType(elemCode=C3D15)
    elemType3 = mesh.ElemType(elemCode=C3D10)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.getSequenceFromMask(mask=('[#7 ]', ), )
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2, 
        elemType3))
    elemType1 = mesh.ElemType(elemCode=C3D20R, elemLibrary=STANDARD)
    elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
    elemType3 = mesh.ElemType(elemCode=C3D10, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.getSequenceFromMask(mask=('[#6 ]', ), )
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2, 
        elemType3))
    elemType1 = mesh.ElemType(elemCode=C3D20R, elemLibrary=STANDARD)
    elemType2 = mesh.ElemType(elemCode=C3D15, elemLibrary=STANDARD)
    elemType3 = mesh.ElemType(elemCode=C3D10H, elemLibrary=STANDARD)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.getSequenceFromMask(mask=('[#1 ]', ), )
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2, 
        elemType3))
    p = mdb.models['Model-1'].parts['merged-shell']
    p.generateMesh(boundaryPreview=ON)
    p = mdb.models['Model-1'].parts['merged-shell']
    p.generateMesh()


def make_coll_and_ker_mesh():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    session.viewports['Viewport: 1'].view.setValues(nearPlane=16.2761, 
        farPlane=23.3517, width=19.6097, height=9.79916, viewOffsetX=1.02973, 
        viewOffsetY=0.775827)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=16.0266, 
        farPlane=25.494, width=19.3091, height=9.64897, cameraPosition=(
        3.24315, 19.3879, 10.8178), cameraUpVector=(-0.384501, 0.140542, 
        -0.912363), cameraTarget=(0.543697, 1.96322, 2.0819), 
        viewOffsetX=1.01395, viewOffsetY=0.763935)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=16.3579, 
        farPlane=25.1627, width=16.1032, height=8.04691, viewOffsetX=0.848889, 
        viewOffsetY=1.02754)
    mdb.meshEditOptions.setValues(enableUndo=True, maxUndoCacheElements=0.5)
    p = mdb.models['Model-1'].parts['merged-shell']
    f = p.elements
    face1Elements = f.getSequenceFromMask(mask=(
        '[#0:174 #4 #0:9 #800000 #0:9 #800000 #0:141', 
        ' #8000 #0:5 #1 #0:2 #10004000 #0 #40', 
        ' #8000 #0:5 #20 #0:4 #4000 #20000000 #0', 
        ' #10000000 #0:3 #8 #0:7 #20000000 #0 #80000', 
        ' #0 #100 #8000000 #0:3 #20000000 #0:3 #800', 
        ' #0:11 #800 #0:123 #100000 #0:47 #80 #4', 
        ' #0:58 #80 #0:12 #8 #0:49 #400 #0:203', 
        ' #2000000 #0:2 #800000 #0:360 #400 #10000000 #100400', 
        ' #80 #100000 #80 #0:2 #440000 #100 #0', 
        ' #80 #0:31 #10 #0:8 #10 #0:4 #480', 
        ' #80000000 #100000 #0:57 #8000000 #2100 #0:5 #800', 
        ' #0:68 #400000 #0:135 #800000 #0:36 #20000 #0:91', ' #1000 ]', ), )
    face2Elements = f.getSequenceFromMask(mask=(
        '[#0:31 #1 #0:143 #2000 #0:24 #40000000 #100000', 
        ' #20 #0 #800000 #0:5 #8000 #0:11 #80200', 
        ' #0:98 #10000000 #0:12 #10400000 #1000 #0:3 #40', 
        ' #0 #10 #0 #100000 #0:2 #4 #0', 
        ' #1000000 #0 #20000 #0 #4000 #800000 #0:2', 
        ' #4 #200100 #0:3 #10400 #0:2 #4000 #0:2', 
        ' #10000000 #0 #800000 #0 #1000 #0 #10', 
        ' #10000000 #0 #800000 #0:2 #2000000 #0 #20400000', 
        ' #0:3 #2010000 #0:3 #3 #40000000 #0 #400', 
        ' #0:796 #400000 #0 #800 #0:70 #300000 #0:6', 
        ' #20000 #0:6 #80 #0:8 #8000000 #0:2 #40', 
        ' #802 #0:7 #2 #0:14 #10 #0:2 #40200', 
        ' #8000 #0 #204000 #0 #100 #40000 #0:60', 
        ' #10 #0:22 #100000 #0:39 #10000 #0:3 #100000', ' #0:126 #100 ]', ), )
    face3Elements = f.getSequenceFromMask(mask=(
        '[#0:3 #800000 #10000 #0:2 #8 #200 #0:9', 
        ' #100000 #0:2 #1000 #0 #200 #0:20 #200', 
        ' #0:7 #2 #80080 #20002 #2002011 #108040 #e020042', 
        ' #880 #0 #8000 #92800000 #24840102 #0:28 #2000', 
        ' #0:11 #400 #0:9 #400000 #40 #0:19 #10000', 
        ' #0 #8 #0 #800 #0:2 #8000000 #40', 
        ' #40000000 #0:2 #482 #2100 #1 #10400 #0', 
        ' #40080000 #2000 #2060 #0:4 #400000 #0:3 #1000000', 
        ' #0:2 #80400 #0:2 #40 #0:4 #2000000 #1', 
        ' #400000 #2000 #80000000 #0 #c00 #10000400 #1000', 
        ' #0 #1000000 #400 #0:3 #10004400 #0:2 #600041', 
        ' #100000 #0 #30090000 #0:2 #40000080 #0 #20000', 
        ' #80011000 #85000020 #0 #400 #1000000 #0 #80000', 
        ' #0 #100 #0 #10000000 #0 #202008 #0', 
        ' #2000 #0:4 #5000 #210000 #1000000 #200000 #1082000', 
        ' #0 #400 #50 #0:6 #2010 #0 #1', 
        ' #100000 #0 #1100000 #0 #400000 #0:2 #200', 
        ' #1000 #0:10 #80000 #0:57 #1c0100 #0:2 #40000', 
        ' #0 #a00000 #80002 #0:3 #4021000 #1 #20000000', 
        ' #4 #80000000 #0 #40000000 #0:2 #800000 #0', 
        ' #c000000 #40000000 #400010 #8a50802 #32040100 #80010000 #c0202001', 
        ' #80440000 #30201510 #44800000 #40480041 #a282804 #4021080b #2105800', 
        ' #88a90180 #2c021 #84002204 #8400204 #22041801 #8006404 #2820b11', 
        ' #200889a0 #95a00301 #90001 #6304901 #101a0246 #45029089 #43080084', 
        ' #906652 #11000008 #10c00000 #32110800 #18008400 #1022208 #4900401', 
        ' #c014479 #c402102 #4208040 #35002000 #22010101 #58044f1 #a4582100', 
        ' #c0014022 #4100 #82080400 #40521021 #a802d804 #8048080 #11020810', 
        ' #48c1114 #288440 #81000681 #89014087 #c408012 #73200302 #40451080', 
        ' #218c1090 #86030000 #2080000 #640844 #46800000 #8201000 #1008001', 
        ' #10 #400 #0:4 #400 #240000 #0:12 #1000000', 
        ' #0:4 #19500000 #6aa98820 #122000 #0 #40000030 #100000', 
        ' #0:2 #b0260 #60000000 #1000 #0 #8 #0', 
        ' #40000000 #0 #10000000 #0:5 #40440000 #0:4 #8', 
        ' #0:2 #400000 #0:2 #20000100 #8 #1000000 #0:3', 
        ' #10588000 #0:2 #20000000 #0:2 #50000 #0:3 #2100', 
        ' #140 #2000 #800000 #0:4 #8000 #0:5 #2000000', 
        ' #0:5 #200000 #100000 #0 #200 #0:5 #4', 
        ' #0 #400600 #0 #200000 #800000 #4000 #2000000', 
        ' #0:2 #1000 #0 #4001 #0:2 #4000 #0', 
        ' #20000000 #4 #0:2 #200 #0:2 #19400000 #0:5', 
        ' #4400 #0 #2 #0:3 #940 #0:5 #40000000', 
        ' #13 #0:2 #10000 #0:8 #20 #0:6 #8000000', 
        ' #0:3 #1 #0:37 #2000 #0:13 #2400 #0:28', 
        ' #10000 #0:30 #28 #0:44 #100000 #0:43 #5', 
        ' #0:42 #1 #0:23 #3000000 #0:27 #400 #0:35', 
        ' #20000000 #1000000 #0:229 #400000 #0:5 #100000 #0:20', 
        ' #800 #0:9 #40000 #0:19 #860 #100040 #400a0000', 
        ' #4688 #9 #a0802000 #10202 #20000 #10200 #801000', 
        ' #80280000 #2000000 #0:8 #800 #0 #1000 #0:51', 
        ' #110000 #80003c0 #2000 #10048 #2600201 #4220222 #82401084', 
        ' #141c0002 #180202 #c0044006 #1000 #82302000 #c022000 #c0048', 
        ' #44080100 #3d0e0308 #8004442a #8280cf81 #20d72 #28280081 #48020120', 
        ' #400d4980 #105049c #15158000 #1240050 #4400100 #400011 #448c420', 
        ' #1290204 #2102088 #80008080 #10008300 #4800409 #50401220 #4287a0', 
        ' #22808 #104a60 #482850 #892c4832 #c2300 #12018000 #42048080', 
        ' #12104540 #1102ca4 #9d02160 #1040080 #85a28222 #16908047 #c618055', 
        ' #1c80100b #2030000a #10204000 #84242a #c80208c2 #38840004 #30a0080', 
        ' #4400c00 #40100404 #60800042 #820000 #201000 #20000440 #1000820', 
        ' #6000 #22411404 #420323 #2000000 #1006000 #0 #b0080000', 
        ' #a00 #800 #0 #2000 #10000000 #20040 #c000', 
        ' #0:9 #400 #0:9 #4000 #0:6 #401330 #0:4', 
        ' #80000000 #2052c1 #0 #4000000 #2000000 #80000000 #4', 
        ' #0 #900001 #40080602 #20000000 #15440800 #20c4006 #4480000', 
        ' #118001a4 #0 #400 #0 #11000 #10082000 #0:8', 
        ' #c000400 #1000c2 #1000000 #0:4 #800100 #400000 #80000', 
        ' #0 #2080000 #20000 #80000104 #4 #0 #1', 
        ' #0 #1802000 #20000 #200 #42 #40000000 #1', 
        ' #20000000 #30000 #0:3 #4000000 #0 #10001000 #100200', 
        ' #4 #0:5 #20000 #0:3 #400 #0:4 #4000000', 
        ' #0:3 #1000 #80040000 #211 #200 #2d083 #28900022', 
        ' #0 #4020800 #4 #0:3 #4000000 #200804 #100000', 
        ' #c0 #20040000 #4 #0:2 #80008000 #50000 #0:6', 
        ' #280000 #400 #0:3 #20 #800 #0 #60000000', 
        ' #200 #0 #200 #0:3 #1000208 #0:3 #200', 
        ' #80000000 #480000 #8084200 #20 #0:3 #4000000 #1', 
        ' #0 #8000000 #0 #804000 #0:6 #8 #10', 
        ' #0:5 #8000000 #0:3 #8000000 #0:2 #10 #0:9', 
        ' #80 #0:5 #8000000 #0:3 #800 #40000000 #0', 
        ' #8000010 #400000 #0 #1000 #0 #2008 #0', 
        ' #40000 #800000 #0 #80000000 #1 #0:2 #28000', 
        ' #0:2 #20000000:2 #0:70 #8000 #0:17 #20000000 #0:20', 
        ' #20 #0:13 #40000 #0:109 #200 #0:22 #2000000', 
        ' #0:2 #800000 #0:27 #200000 ]', ), )
    face4Elements = f.getSequenceFromMask(mask=(
        '[#0:147 #800000 #0:27 #100 #40000000 #0:4 #200', 
        ' #0:10 #1000 #0:50 #20000 #0:75 #400 #0:18', 
        ' #10000 #0:20 #800 #0:7 #400000 #80 #0:8', 
        ' #8000 #0:46 #8000000 #0:19 #80000 #0:4 #10000', 
        ' #0:87 #4000000 #0:39 #10 #0:2 #20000 #0:9', 
        ' #20000000 #0:26 #10000 #0:54 #10 #0:23 #8', 
        ' #0:134 #10 #0:14 #8000 #0:16 #20010000 #0:55', 
        ' #100 #0:17 #20 #0:85 #8000000 #0:164 #400', 
        ' #800 #0:73 #10 #0:4 #800000 #0 #2000000', 
        ' #0:9 #200000 #0:4 #80000000 #0:7 #20000400 #0:27', 
        ' #2000 #0:53 #80000000 #0:52 #20000 #0:25 #1', 
        ' #0:28 #20 #0:105 #2000 #0:86 #10000 #0:419', ' #40 ]', ), )
    p.generateMeshByOffset(region=regionToolset.Region(face1Elements=face1Elements, 
        face2Elements=face2Elements, face3Elements=face3Elements, 
        face4Elements=face4Elements), meshType=SHELL, 
        distanceBetweenLayers=0.0, numLayers=2, initialOffset=0.0, 
        shareNodes=True)
    p = mdb.models['Model-1'].parts['merged-shell']
    e = p.elements
    elements = e.getSequenceFromMask(mask=(
        '[#0:2176 #fc000000 #ffffffff:43 #3ffff ]', ), )
    p.Set(elements=elements, name='OffsetElements-Layer-1')
    p = mdb.models['Model-1'].parts['merged-shell']
    e = p.elements
    elements = e.getSequenceFromMask(mask=(
        '[#0:2220 #fffc0000 #ffffffff:43 #3ff ]', ), )
    p.Set(elements=elements, name='OffsetElements-Layer-2')
    mdb.models['Model-1'].parts['merged-shell'].sets.changeKey(
        fromName='OffsetElements-Layer-1', toName='collagen-elements')
    mdb.models['Model-1'].parts['merged-shell'].sets.changeKey(
        fromName='OffsetElements-Layer-2', toName='keratin-elements')


def coll_and_ker_mesh_2():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    p = mdb.models['Model-1'].parts['merged-shell']
    f = p.elements
    face1Elements = f[5570:5571]+f[5911:5912]+f[6231:6232]+f[10767:10768]+\
        f[10944:10945]+f[11054:11055]+f[11068:11069]+f[11110:11111]+\
        f[11151:11152]+f[11333:11334]+f[11502:11503]+f[11549:11550]+\
        f[11612:11613]+f[11715:11716]+f[11997:11998]+f[12051:12052]+\
        f[12104:12105]+f[12155:12156]+f[12285:12286]+f[12395:12396]+\
        f[12779:12780]+f[16756:16757]+f[18279:18280]+f[18306:18307]+\
        f[20199:20200]+f[20611:20612]+f[22218:22219]+f[28761:28762]+\
        f[28855:28856]+f[40394:40395]+f[40444:40445]+f[40458:40459]+\
        f[40468:40469]+f[40487:40488]+f[40532:40533]+f[40551:40552]+\
        f[40658:40659]+f[40662:40663]+f[40680:40681]+f[40743:40744]+\
        f[41764:41765]+f[42052:42053]+f[42215:42216]+f[42218:42219]+\
        f[42271:42272]+f[42292:42293]+f[44155:44156]+f[44168:44169]+\
        f[44173:44174]+f[44363:44364]+f[46582:46583]+f[50935:50936]+\
        f[52113:52114]+f[55052:55053]
    face2Elements = f[992:993]+f[5613:5614]+f[6430:6431]+f[6452:6453]+f[6469:6470]+\
        f[6551:6552]+f[6735:6736]+f[7113:7114]+f[7123:7124]+f[10300:10301]+\
        f[10710:10711]+f[10716:10717]+f[10732:10733]+f[10854:10855]+\
        f[10916:10917]+f[10996:10997]+f[11074:11075]+f[11160:11161]+\
        f[11217:11218]+f[11278:11279]+f[11319:11320]+f[11394:11395]+\
        f[11432:11433]+f[11445:11446]+f[11562:11563]+f[11568:11569]+\
        f[11662:11663]+f[11772:11773]+f[11831:11832]+f[11884:11885]+\
        f[11940:11941]+f[11996:11997]+f[12055:12056]+f[12153:12154]+\
        f[12214:12215]+f[12221:12222]+f[12336:12337]+f[12345:12346]+\
        f[12448:12450]+f[12510:12511]+f[12554:12555]+f[38070:38071]+\
        f[38123:38124]+f[40404:40406]+f[40625:40626]+f[40839:40840]+\
        f[41147:41148]+f[41222:41223]+f[41249:41250]+f[41259:41260]+\
        f[41505:41506]+f[41988:41989]+f[42089:42090]+f[42098:42099]+\
        f[42127:42128]+f[42190:42191]+f[42197:42198]+f[42248:42249]+\
        f[42290:42291]+f[44228:44229]+f[44980:44981]+f[46256:46257]+\
        f[46388:46389]+f[50440:50441]
    face3Elements = f[119:120]+f[144:145]+f[227:228]+f[265:266]+f[596:597]+\
        f[684:685]+f[745:746]+f[1417:1418]+f[1665:1666]+f[1703:1704]+\
        f[1715:1716]+f[1729:1730]+f[1745:1746]+f[1760:1761]+f[1764:1765]+\
        f[1773:1774]+f[1785:1786]+f[1798:1799]+f[1807:1808]+f[1812:1813]+\
        f[1825:1826]+f[1830:1831]+f[1841:1842]+f[1849:1852]+f[1863:1864]+\
        f[1867:1868]+f[1935:1936]+f[1975:1976]+f[1977:1978]+f[1980:1981]+\
        f[1983:1984]+f[1985:1986]+f[1992:1993]+f[2002:2003]+f[2007:2008]+\
        f[2010:2011]+f[2013:2014]+f[2925:2926]+f[3306:3307]+f[3638:3639]+\
        f[3654:3655]+f[4304:4305]+f[4355:4356]+f[4427:4428]+f[4539:4540]+\
        f[4550:4551]+f[4606:4607]+f[4673:4674]+f[4679:4680]+f[4682:4683]+\
        f[4712:4713]+f[4717:4718]+f[4736:4737]+f[4778:4779]+f[4784:4785]+\
        f[4851:4852]+f[4862:4863]+f[4877:4878]+f[4901:4903]+f[4909:4910]+\
        f[5078:5079]+f[5208:5209]+f[5290:5291]+f[5299:5300]+f[5382:5383]+\
        f[5561:5562]+f[5568:5569]+f[5622:5623]+f[5645:5646]+f[5695:5696]+\
        f[5738:5740]+f[5770:5771]+f[5788:5789]+f[5804:5805]+f[5880:5881]+\
        f[5898:5899]+f[6026:6027]+f[6030:6031]+f[6044:6045]+f[6112:6113]+\
        f[6118:6119]+f[6133:6135]+f[6164:6165]+f[6224:6225]+f[6227:6228]+\
        f[6236:6238]+f[6311:6312]+f[6334:6335]+f[6385:6386]+f[6412:6413]+\
        f[6416:6417]+f[6431:6432]+f[6437:6438]+f[6456:6457]+f[6458:6459]+\
        f[6463:6464]+f[6506:6507]+f[6552:6553]+f[6611:6612]+f[6664:6665]+\
        f[6748:6749]+f[6787:6788]+f[6797:6798]+f[6805:6806]+f[6861:6862]+\
        f[7020:7021]+f[7022:7023]+f[7056:7057]+f[7061:7062]+f[7096:7097]+\
        f[7125:7126]+f[7149:7150]+f[7155:7156]+f[7160:7161]+f[7210:7211]+\
        f[7236:7237]+f[7238:7239]+f[7460:7461]+f[7469:7470]+f[7520:7521]+\
        f[7572:7573]+f[7636:7637]+f[7640:7641]+f[7702:7703]+f[7785:7786]+\
        f[7820:7821]+f[8179:8180]+f[10024:10025]+f[10034:10037]+f[10130:10131]+\
        f[10197:10198]+f[10199:10200]+f[10209:10210]+f[10227:10228]+\
        f[10348:10349]+f[10353:10354]+f[10362:10363]+f[10368:10369]+\
        f[10429:10430]+f[10434:10435]+f[10495:10496]+f[10558:10559]+\
        f[10647:10648]+f[10714:10716]+f[10750:10751]+f[10756:10757]+\
        f[10774:10775]+f[10785:10786]+f[10795:10796]+f[10800:10801]+\
        f[10802:10803]+f[10805:10806]+f[10807:10808]+f[10811:10812]+\
        f[10824:10825]+f[10834:10835]+f[10841:10842]+f[10844:10846]+\
        f[10864:10865]+f[10879:10881]+f[10893:10894]+f[10901:10902]+\
        f[10910:10912]+f[10930:10931]+f[10934:10935]+f[10943:10944]+\
        f[10948:10949]+f[10952:10953]+f[10954:10955]+f[10956:10957]+\
        f[10965:10966]+f[10972:10974]+f[10999:11000]+f[11002:11003]+\
        f[11006:11007]+f[11008:11009]+f[11014:11015]+f[11027:11028]+\
        f[11030:11031]+f[11038:11039]+f[11042:11043]+f[11051:11052]+\
        f[11053:11054]+f[11059:11060]+f[11061:11062]+f[11065:11066]+\
        f[11067:11068]+f[11072:11074]+f[11075:11076]+f[11083:11084]+\
        f[11088:11089]+f[11093:11094]+f[11102:11103]+f[11115:11117]+\
        f[11118:11119]+f[11124:11125]+f[11129:11130]+f[11143:11145]+\
        f[11152:11153]+f[11155:11156]+f[11157:11158]+f[11159:11160]+\
        f[11163:11164]+f[11167:11169]+f[11173:11174]+f[11182:11184]+\
        f[11185:11186]+f[11202:11203]+f[11209:11210]+f[11213:11214]+\
        f[11226:11227]+f[11231:11232]+f[11234:11235]+f[11241:11242]+\
        f[11254:11255]+f[11259:11260]+f[11264:11265]+f[11275:11277]+\
        f[11282:11283]+f[11289:11290]+f[11293:11294]+f[11298:11299]+\
        f[11306:11307]+f[11309:11311]+f[11323:11324]+f[11328:11329]+\
        f[11332:11333]+f[11336:11338]+f[11339:11340]+f[11345:11346]+\
        f[11351:11352]+f[11353:11354]+f[11365:11366]+f[11367:11369]+\
        f[11371:11372]+f[11375:11376]+f[11379:11380]+f[11389:11390]+\
        f[11392:11393]+f[11400:11402]+f[11413:11414]+f[11415:11417]+\
        f[11418:11419]+f[11420:11421]+f[11423:11425]+f[11440:11441]+\
        f[11443:11444]+f[11456:11457]+f[11464:11465]+f[11467:11468]+\
        f[11470:11471]+f[11476:11478]+f[11481:11483]+f[11489:11491]+\
        f[11494:11495]+f[11497:11498]+f[11505:11506]+f[11507:11509]+\
        f[11516:11517]+f[11520:11521]+f[11523:11524]+f[11527:11528]+\
        f[11532:11533]+f[11535:11536]+f[11537:11538]+f[11544:11545]+\
        f[11546:11547]+f[11550:11551]+f[11554:11555]+f[11559:11560]+\
        f[11571:11572]+f[11576:11578]+f[11582:11583]+f[11585:11586]+\
        f[11588:11589]+f[11590:11591]+f[11593:11595]+f[11597:11599]+\
        f[11604:11605]+f[11607:11608]+f[11619:11620]+f[11640:11641]+\
        f[11644:11645]+f[11670:11672]+f[11676:11677]+f[11691:11692]+\
        f[11696:11697]+f[11700:11701]+f[11705:11706]+f[11708:11710]+\
        f[11722:11723]+f[11727:11728]+f[11739:11741]+f[11747:11748]+\
        f[11753:11754]+f[11757:11758]+f[11761:11762]+f[11768:11769]+\
        f[11776:11777]+f[11786:11787]+f[11796:11797]+f[11799:11800]+\
        f[11802:11803]+f[11808:11809]+f[11811:11815]+f[11818:11819]+\
        f[11822:11823]+f[11824:11825]+f[11834:11836]+f[11841:11842]+\
        f[11848:11849]+f[11853:11854]+f[11862:11863]+f[11866:11868]+\
        f[11878:11879]+f[11887:11888]+f[11893:11894]+f[11898:11899]+\
        f[11917:11918]+f[11928:11929]+f[11930:11931]+f[11932:11934]+\
        f[11936:11937]+f[11944:11945]+f[11952:11953]+f[11961:11962]+\
        f[11965:11966]+f[11968:11969]+f[11972:11976]+f[11978:11979]+\
        f[11982:11983]+f[11991:11993]+f[11994:11995]+f[12008:12009]+\
        f[12013:12014]+f[12019:12021]+f[12022:12023]+f[12026:12027]+\
        f[12029:12030]+f[12031:12032]+f[12033:12034]+f[12037:12038]+\
        f[12046:12047]+f[12048:12049]+f[12062:12064]+f[12072:12073]+\
        f[12078:12079]+f[12106:12107]+f[12115:12116]+f[12121:12122]+\
        f[12127:12129]+f[12133:12134]+f[12140:12141]+f[12145:12146]+\
        f[12148:12149]+f[12150:12151]+f[12158:12159]+f[12162:12163]+\
        f[12171:12173]+f[12174:12176]+f[12177:12178]+f[12187:12188]+\
        f[12189:12190]+f[12191:12192]+f[12199:12200]+f[12207:12208]+\
        f[12210:12211]+f[12219:12220]+f[12228:12229]+f[12235:12236]+\
        f[12241:12242]+f[12248:12249]+f[12252:12253]+f[12258:12259]+\
        f[12260:12261]+f[12264:12265]+f[12268:12269]+f[12274:12276]+\
        f[12279:12280]+f[12282:12283]+f[12294:12295]+f[12298:12299]+\
        f[12303:12304]+f[12307:12308]+f[12309:12310]+f[12320:12321]+\
        f[12327:12328]+f[12329:12331]+f[12344:12345]+f[12351:12355]+\
        f[12359:12360]+f[12366:12367]+f[12368:12369]+f[12376:12377]+\
        f[12379:12380]+f[12383:12384]+f[12385:12386]+f[12388:12389]+\
        f[12399:12400]+f[12406:12407]+f[12410:12412]+f[12417:12418]+\
        f[12424:12426]+f[12437:12438]+f[12440:12442]+f[12444:12447]+\
        f[12455:12456]+f[12460:12461]+f[12464:12465]+f[12466:12467]+\
        f[12470:12471]+f[12478:12479]+f[12484:12485]+f[12487:12488]+\
        f[12492:12493]+f[12498:12500]+f[12503:12505]+f[12509:12510]+\
        f[12528:12530]+f[12537:12539]+f[12543:12544]+f[12563:12564]+\
        f[12569:12570]+f[12578:12579]+f[12582:12583]+f[12587:12588]+\
        f[12594:12595]+f[12597:12599]+f[12631:12632]+f[12633:12635]+\
        f[12638:12639]+f[12652:12653]+f[12661:12662]+f[12667:12668]+\
        f[12672:12673]+f[12687:12688]+f[12696:12697]+f[12708:12709]+\
        f[12746:12747]+f[12906:12907]+f[12946:12947]+f[12949:12950]+\
        f[13368:13369]+f[13524:13525]+f[13526:13527]+f[13528:13529]+\
        f[13531:13533]+f[13541:13542]+f[13547:13548]+f[13551:13553]+\
        f[13555:13556]+f[13557:13558]+f[13559:13560]+f[13561:13562]+\
        f[13563:13564]+f[13565:13567]+f[13581:13582]+f[13585:13586]+\
        f[13588:13589]+f[13636:13638]+f[13662:13663]+f[13684:13685]+\
        f[13765:13767]+f[13769:13770]+f[13776:13778]+f[13779:13780]+\
        f[13821:13823]+f[13836:13837]+f[13891:13892]+f[13982:13983]+\
        f[14044:14045]+f[14226:14227]+f[14230:14231]+f[14238:14239]+\
        f[14371:14372]+f[14486:14487]+f[14568:14569]+f[14589:14590]+\
        f[14595:14596]+f[14648:14649]+f[14767:14768]+f[14771:14773]+\
        f[14774:14775]+f[14780:14781]+f[14877:14878]+f[14960:14961]+\
        f[14962:14963]+f[15080:15081]+f[15085:15086]+f[15110:15111]+\
        f[15112:15113]+f[15149:15150]+f[15191:15192]+f[15343:15344]+\
        f[15545:15546]+f[15733:15734]+f[15764:15765]+f[15817:15818]+\
        f[16002:16003]+f[16073:16075]+f[16086:16087]+f[16149:16150]+\
        f[16183:16184]+f[16206:16207]+f[16249:16250]+f[16332:16333]+\
        f[16384:16385]+f[16398:16399]+f[16494:16495]+f[16573:16574]+\
        f[16578:16579]+f[16681:16682]+f[16790:16791]+f[16792:16793]+\
        f[16795:16797]+f[16970:16971]+f[16974:16975]+f[17025:17026]+\
        f[17158:17159]+f[17160:17161]+f[17163:17164]+f[17374:17375]+\
        f[17376:17378]+f[17380:17381]+f[17488:17489]+f[17765:17766]+\
        f[18011:18012]+f[18112:18113]+f[19341:19342]+f[19786:19787]+\
        f[19789:19790]+f[20720:20721]+f[21699:21700]+f[21701:21702]+\
        f[23156:23157]+f[24544:24545]+f[24546:24547]+f[25920:25921]+\
        f[26712:26714]+f[27594:27595]+f[28765:28766]+f[28792:28793]+\
        f[36150:36151]+f[36340:36341]+f[37003:37004]+f[37330:37331]+\
        f[37957:37959]+f[37963:37964]+f[37990:37991]+f[38004:38005]+\
        f[38033:38034]+f[38035:38036]+f[38046:38047]+f[38051:38052]+\
        f[38055:38056]+f[38057:38059]+f[38062:38063]+f[38080:38081]+\
        f[38083:38084]+f[38125:38126]+f[38135:38136]+f[38141:38142]+\
        f[38143:38144]+f[38145:38146]+f[38153:38154]+f[38160:38161]+\
        f[38193:38194]+f[38217:38218]+f[38224:38225]+f[38252:38253]+\
        f[38263:38264]+f[38291:38292]+f[38293:38294]+f[38303:38304]+\
        f[38329:38330]+f[38603:38604]+f[38668:38669]+f[40336:40337]+\
        f[40340:40341]+f[40358:40362]+f[40379:40380]+f[40397:40398]+\
        f[40419:40420]+f[40422:40423]+f[40432:40433]+f[40448:40449]+\
        f[40457:40458]+f[40469:40471]+f[40473:40474]+f[40481:40482]+\
        f[40485:40486]+f[40489:40490]+f[40497:40498]+f[40501:40502]+\
        f[40506:40507]+f[40514:40515]+f[40519:40520]+f[40524:40525]+\
        f[40534:40535]+f[40537:40538]+f[40543:40544]+f[40545:40546]+\
        f[40562:40565]+f[40570:40571]+f[40572:40573]+f[40577:40578]+\
        f[40585:40586]+f[40595:40597]+f[40609:40611]+f[40622:40623]+\
        f[40626:40627]+f[40638:40640]+f[40652:40653]+f[40685:40686]+\
        f[40692:40694]+f[40697:40698]+f[40703:40704]+f[40717:40718]+\
        f[40721:40722]+f[40730:40732]+f[40739:40740]+f[40742:40743]+\
        f[40754:40756]+f[40776:40777]+f[40787:40788]+f[40794:40795]+\
        f[40798:40799]+f[40803:40804]+f[40808:40810]+f[40817:40820]+\
        f[40824:40825]+f[40826:40830]+f[40833:40834]+f[40835:40836]+\
        f[40837:40838]+f[40842:40843]+f[40846:40847]+f[40850:40851]+\
        f[40863:40865]+f[40871:40876]+f[40878:40880]+f[40887:40888]+\
        f[40889:40890]+f[40895:40896]+f[40897:40898]+f[40900:40903]+\
        f[40904:40905]+f[40906:40908]+f[40913:40914]+f[40928:40929]+\
        f[40935:40936]+f[40947:40948]+f[40949:40950]+f[40955:40956]+\
        f[40957:40958]+f[40965:40966]+f[40968:40969]+f[40977:40978]+\
        f[40987:40988]+f[40990:40991]+f[40999:41001]+f[41003:41004]+\
        f[41006:41007]+f[41008:41009]+f[41010:41012]+f[41022:41023]+\
        f[41026:41029]+f[41031:41032]+f[41034:41035]+f[41040:41041]+\
        f[41042:41043]+f[41048:41049]+f[41071:41073]+f[41074:41075]+\
        f[41076:41077]+f[41080:41081]+f[41082:41083]+f[41084:41085]+\
        f[41092:41093]+f[41094:41095]+f[41106:41107]+f[41109:41110]+\
        f[41112:41113]+f[41128:41129]+f[41142:41143]+f[41146:41147]+\
        f[41152:41153]+f[41156:41157]+f[41174:41175]+f[41189:41190]+\
        f[41194:41195]+f[41198:41200]+f[41203:41204]+f[41206:41207]+\
        f[41210:41211]+f[41218:41219]+f[41225:41226]+f[41232:41233]+\
        f[41235:41236]+f[41237:41238]+f[41240:41241]+f[41251:41252]+\
        f[41255:41256]+f[41261:41262]+f[41268:41269]+f[41273:41274]+\
        f[41287:41288]+f[41295:41296]+f[41311:41312]+f[41320:41322]+\
        f[41327:41328]+f[41340:41341]+f[41344:41345]+f[41347:41348]+\
        f[41354:41355]+f[41367:41368]+f[41370:41371]+f[41381:41382]+\
        f[41385:41386]+f[41388:41389]+f[41398:41399]+f[41404:41405]+\
        f[41406:41407]+f[41413:41414]+f[41415:41419]+f[41423:41424]+\
        f[41425:41426]+f[41430:41431]+f[41443:41444]+f[41451:41452]+\
        f[41453:41454]+f[41457:41458]+f[41477:41479]+f[41481:41482]+\
        f[41483:41484]+f[41486:41487]+f[41492:41493]+f[41508:41509]+\
        f[41510:41511]+f[41515:41516]+f[41517:41518]+f[41523:41524]+\
        f[41526:41527]+f[41537:41538]+f[41540:41542]+f[41547:41548]+\
        f[41550:41551]+f[41554:41556]+f[41557:41558]+f[41560:41561]+\
        f[41563:41564]+f[41567:41568]+f[41576:41578]+f[41581:41582]+\
        f[41586:41588]+f[41615:41617]+f[41625:41626]+f[41628:41629]+\
        f[41639:41640]+f[41647:41648]+f[41650:41651]+f[41657:41658]+\
        f[41662:41663]+f[41670:41671]+f[41672:41673]+f[41674:41675]+\
        f[41678:41679]+f[41684:41685]+f[41689:41690]+f[41692:41693]+\
        f[41698:41699]+f[41701:41702]+f[41703:41704]+f[41706:41708]+\
        f[41709:41710]+f[41716:41717]+f[41720:41721]+f[41733:41735]+\
        f[41736:41737]+f[41741:41742]+f[41748:41749]+f[41750:41753]+\
        f[41755:41756]+f[41767:41768]+f[41778:41779]+f[41784:41785]+\
        f[41793:41794]+f[41797:41798]+f[41801:41802]+f[41807:41808]+\
        f[41809:41810]+f[41813:41814]+f[41815:41817]+f[41818:41819]+\
        f[41823:41827]+f[41830:41831]+f[41839:41840]+f[41844:41845]+\
        f[41847:41848]+f[41849:41851]+f[41852:41853]+f[41856:41857]+\
        f[41858:41859]+f[41860:41861]+f[41862:41863]+f[41871:41873]+\
        f[41877:41879]+f[41882:41884]+f[41888:41890]+f[41891:41892]+\
        f[41900:41901]+f[41911:41912]+f[41914:41917]+f[41921:41922]+\
        f[41923:41924]+f[41940:41942]+f[41949:41950]+f[41966:41967]+\
        f[41973:41974]+f[41980:41981]+f[41985:41986]+f[41987:41988]+\
        f[41989:41990]+f[41994:41995]+f[41997:41998]+f[42002:42003]+\
        f[42007:42008]+f[42017:42018]+f[42022:42024]+f[42027:42028]+\
        f[42033:42034]+f[42043:42044]+f[42046:42048]+f[42050:42051]+\
        f[42066:42067]+f[42071:42072]+f[42075:42078]+f[42087:42088]+\
        f[42097:42098]+f[42099:42100]+f[42104:42106]+f[42122:42124]+\
        f[42134:42135]+f[42138:42139]+f[42146:42147]+f[42154:42155]+\
        f[42164:42165]+f[42174:42175]+f[42177:42178]+f[42182:42183]+\
        f[42199:42200]+f[42205:42207]+f[42225:42226]+f[42231:42232]+\
        f[42252:42253]+f[42261:42262]+f[42278:42279]+f[42282:42283]+\
        f[42301:42302]+f[42309:42310]+f[42315:42316]+f[42328:42329]+\
        f[42349:42351]+f[42370:42371]+f[42378:42379]+f[42380:42381]+\
        f[42384:42385]+f[42390:42391]+f[42393:42394]+f[42397:42398]+\
        f[42400:42402]+f[42405:42406]+f[42408:42410]+f[42417:42418]+\
        f[42422:42423]+f[42457:42458]+f[42477:42479]+f[42488:42489]+\
        f[42547:42548]+f[42556:42558]+f[42559:42560]+f[42569:42570]+\
        f[42571:42572]+f[42603:42604]+f[42669:42670]+f[42716:42717]+\
        f[42726:42727]+f[42737:42738]+f[42766:42768]+f[43082:43083]+\
        f[43406:43407]+f[43620:43622]+f[43624:43626]+f[43628:43629]+\
        f[43638:43639]+f[43807:43809]+f[43814:43816]+f[43817:43818]+\
        f[43820:43821]+f[43822:43823]+f[43829:43830]+f[43898:43899]+\
        f[43929:43930]+f[43967:43968]+f[43970:43971]+f[44032:44033]+\
        f[44052:44053]+f[44055:44056]+f[44065:44066]+f[44073:44075]+\
        f[44083:44084]+f[44094:44095]+f[44125:44126]+f[44139:44140]+\
        f[44146:44147]+f[44150:44151]+f[44152:44153]+f[44154:44155]+\
        f[44156:44157]+f[44161:44163]+f[44174:44175]+f[44178:44180]+\
        f[44185:44186]+f[44211:44212]+f[44214:44215]+f[44218:44219]+\
        f[44226:44227]+f[44229:44230]+f[44231:44233]+f[44247:44249]+\
        f[44252:44253]+f[44298:44299]+f[44364:44365]+f[44368:44369]+\
        f[44397:44398]+f[44403:44404]+f[44412:44413]+f[44682:44683]+\
        f[44698:44700]+f[44705:44706]+f[44710:44712]+f[44724:44725]+\
        f[44760:44761]+f[44904:44905]+f[44919:44920]+f[44950:44951]+\
        f[44979:44980]+f[45043:45044]+f[45049:45050]+f[45073:45074]+\
        f[45090:45091]+f[45096:45097]+f[45119:45120]+f[45122:45123]+\
        f[45184:45185]+f[45261:45262]+f[45271:45273]+f[45297:45298]+\
        f[45321:45322]+f[45345:45346]+f[45350:45351]+f[45406:45407]+\
        f[45408:45409]+f[45469:45470]+f[45488:45490]+f[45626:45627]+\
        f[45676:45677]+f[45692:45693]+f[45705:45706]+f[45716:45717]+\
        f[45730:45731]+f[45937:45938]+f[46058:46059]+f[46234:46235]+\
        f[46348:46349]+f[46386:46387]+f[46399:46401]+f[46404:46405]+\
        f[46409:46410]+f[46441:46442]+f[46464:46466]+f[46471:46472]+\
        f[46476:46477]+f[46478:46480]+f[46481:46482]+f[46497:46498]+\
        f[46501:46502]+f[46516:46517]+f[46519:46520]+f[46523:46524]+\
        f[46525:46526]+f[46571:46572]+f[46577:46578]+f[46586:46587]+\
        f[46594:46595]+f[46746:46747]+f[46754:46755]+f[46763:46764]+\
        f[46773:46774]+f[46804:46805]+f[46822:46824]+f[46866:46867]+\
        f[46877:46878]+f[46882:46883]+f[46991:46992]+f[47007:47008]+\
        f[47024:47025]+f[47026:47027]+f[47251:47252]+f[47253:47254]+\
        f[47274:47275]+f[47397:47398]+f[47435:47436]+f[47517:47519]+\
        f[47529:47530]+f[47593:47594]+f[47715:47716]+f[47721:47722]+\
        f[47736:47737]+f[47849:47850]+f[47903:47904]+f[47923:47924]+\
        f[47926:47927]+f[47945:47946]+f[47950:47951]+f[47955:47956]+\
        f[47963:47964]+f[47973:47974]+f[48122:48123]+f[48128:48129]+\
        f[48219:48220]+f[48270:48271]+f[48279:48280]+f[48483:48484]+\
        f[48516:48517]+f[48731:48732]+f[48859:48860]+f[48932:48933]+\
        f[49255:49256]+f[49467:49468]+f[49579:49580]+f[49630:49631]+\
        f[49668:49669]+f[49691:49692]+f[49718:49719]+f[49772:49773]+\
        f[49827:49828]+f[49837:49838]+f[49906:49907]+f[49943:49944]+\
        f[50015:50017]+f[50127:50128]+f[50129:50130]+f[50237:50238]+\
        f[50269:50270]+f[52527:52528]+f[53117:53118]+f[53765:53766]+\
        f[54226:54227]+f[57737:57738]+f[58489:58490]+f[58583:58584]+\
        f[59477:59478]
    face4Elements = f[4727:4728]+f[5608:5609]+f[5662:5663]+f[5801:5802]+\
        f[6156:6157]+f[7793:7794]+f[10218:10219]+f[10832:10833]+f[11499:11500]+\
        f[11766:11767]+f[11783:11784]+f[12079:12080]+f[13595:13596]+\
        f[14227:14228]+f[14384:14385]+f[17210:17211]+f[18468:18469]+\
        f[18577:18578]+f[18909:18910]+f[19760:19761]+f[21508:21509]+\
        f[22275:22276]+f[26596:26597]+f[27087:27088]+f[27632:27633]+\
        f[27645:27646]+f[29416:29417]+f[29989:29990]+f[32763:32764]+\
        f[38026:38027]+f[38059:38060]+f[40420:40421]+f[40599:40600]+\
        f[40665:40666]+f[40981:40982]+f[41151:41152]+f[41386:41387]+\
        f[41405:41406]+f[42285:42286]+f[44031:44032]+f[45713:45714]+\
        f[46528:46529]+f[47461:47462]+f[50861:50862]+f[53648:53649]+\
        f[67078:67079]
    print face1Elements
    p.generateMeshByOffset(region=regionToolset.Region(face1Elements=face1Elements, 
        face2Elements=face2Elements, face3Elements=face3Elements, 
        face4Elements=face4Elements), meshType=SHELL, 
        distanceBetweenLayers=0.0, numLayers=2, initialOffset=0.0, 
        shareNodes=True)
    p = mdb.models['Model-1'].parts['merged-shell']
    e = p.elements
    elements = e[69658:72458]
    p.Set(elements=elements, name='OffsetElements')


def surf_test():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    p = mdb.models['Model-1'].parts['merged-shell']
    f = p.elements
    n = p.nodes

    face3Elements = f[40358:40359]+f[40361:40362]+f[46497:46498]
    print(f[10])
    print(f[10].getNodes())
    print(n[1].getElements())
    print(n[1])
    print(n[1].coordinates)
    # print(n['coordinates'])
    # face3Elements = f.findAt(coordinates=(0, 2.8, 1.0))
    # p.Surface(face3Elements=face3Elements, name='Surf-3')




def make_shell_elements(radius, y_cent, flat_length, flat_width):
    p = mdb.models['Model-1'].parts['merged-shell']
    els = p.elements
    nodes = p.nodes
    n_els = np.alen(els)

    face1Elements = els[0:0]
    face2Elements = els[0:0]
    face3Elements = els[0:0]
    face4Elements = els[0:0]

    nodes_per_element = np.alen(els[0].getNodes())

    pct = 0

    if nodes_per_element == 4:
        face1 = np.array([True, True, True, False])
        face2 = np.array([True, True, False, True])
        face3 = np.array([False, True, True, False])
        face4 = np.array([True, False, True, False])

        for el_index in range(np.alen(els)):
            el = els[el_index]
            nodes_on_surface = np.array([None] * 4)
            nodes = el.getNodes()
            for node_index in range(np.alen(nodes)):
                nodes_on_surface[node_index] = find_top(nodes[node_index].coordinates, radius, y_cent, flat_length, flat_width)

            if array_all_equal(nodes_on_surface, np.array([False]*4)):
                pass
            elif array_all_equal(nodes_on_surface, face1):
                # face1Elements.append(el)
                # face1Elements = face1Elements + [el]
                face1Elements = face1Elements + els[el_index:el_index+1]

            elif array_all_equal(nodes_on_surface, face2):
                # face2Elements.append(el)
                # face2Elements = face2Elements + [el]
                face2Elements = face2Elements + els[el_index:el_index+1]

            elif array_all_equal(nodes_on_surface, face3):
                # face3Elements.append(el)
                # face3Elements = face3Elements + [el]
                face3Elements = face3Elements + els[el_index:el_index+1]

            elif array_all_equal(nodes_on_surface, face4):
                # face4Elements.append(el)
                # face4Elements = face4Elements + [el]
                face4Elements = face4Elements + els[el_index:el_index+1]


    elif nodes_per_element == 10:
        face1 = np.array([True, True, True, False, True, True, True, False, False, False])
        face2 = np.array([True, True, False, True, True, False, False, True, True, False])
        face3 = np.array([False, True, True, True, False, True, False, False, True, True])
        face4 = np.array([True, False, True, True, False, False, True, True, False, True])


        print('Quadratic tets')
        for el_index in range(np.alen(els)):
            el = els[el_index]
            nodes_on_surface = np.array([None] * 10)
            nodes = el.getNodes()
            for node_index in range(np.alen(nodes)):
                nodes_on_surface[node_index] = find_top(nodes[node_index].coordinates, radius, y_cent, flat_length, flat_width)


            if (100 * el_index/n_els) - pct > 10:
                pct = (100 * el_index/n_els)
                print str(pct) + " %"

            # if nodes_on_surface[0]:
            #     print(nodes_on_surface)

            # if nodes_on_surface[0]:
            #     print nodes_on_surface

            if array_all_equal(nodes_on_surface, np.array([False]*10)):
                pass
            elif array_all_equal(nodes_on_surface, face1):
                # face1Elements.append(el)
                face1Elements = face1Elements + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_surface, face2):
                # face2Elements.append(el)
                face2Elements = face2Elements + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_surface, face3):
                # face3Elements.append(el)
                face3Elements = face3Elements + els[el_index:el_index+1]
            elif array_all_equal(nodes_on_surface, face4):
                # face4Elements.append(el)
                face4Elements = face4Elements + els[el_index:el_index+1]
    else:
        raise Exception("This is no bueno")

    # print("face1Elements")
    # print(face1Elements)
    # print("face2Elements")
    # print(face2Elements)
    # print("face3Elements")
    # print(face3Elements)
    # print("face4Elements")
    # print(face4Elements)

    p.generateMeshByOffset(region=regionToolset.Region(face1Elements=face1Elements,
                                                       face2Elements=face2Elements, face3Elements=face3Elements,
                                                       face4Elements=face4Elements), meshType=SHELL,
                           distanceBetweenLayers=0.0, numLayers=2, initialOffset=0.0,
                           shareNodes=True)
    p = mdb.models['Model-1'].parts['merged-shell']
    e = p.elements
    current_n_elements = np.alen(e)
    n_new_per_sheet = (current_n_elements - n_els)/2

    coll_elements = e[n_els:n_els+n_new_per_sheet]
    p.Set(elements=coll_elements, name='collagen-elements')
    keratin_elements = e[n_els+n_new_per_sheet:]
    p.Set(elements=keratin_elements, name='keratin-elements')



def float_equals(a, b):
    return np.abs((a-b)) < 1.0e-5



def find_top(point, radius, y_cent, flat_length, flat_width):
    # print y_cent + (radius**2 - point[0]**2)**(1./2.)
    # print point[0]
    # print point[1]
    if -flat_length <= point[0] <= flat_length:
        return float_equals(point[1], flat_width)
    else:
        return float_equals(point[1], y_cent + (radius**2 - point[0]**2)**(1./2.))

def array_all_equal(a, b):
    if a.size != b.size:
        return False
    else:
        a = a.astype(np.int)
        b = b.astype(np.int)
        return np.sum(np.abs(a-b)) == 0


def one_mopre():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    p = mdb.models['Model-1'].parts['merged-shell']
    f = p.elements
    face1Elements = f[5570:5571]+f[5911:5912]+f[6231:6232]+f[10767:10768]+\
        f[10944:10945]+f[11054:11055]+f[11068:11069]+f[11110:11111]+\
        f[11151:11152]+f[11333:11334]+f[11502:11503]+f[11549:11550]+\
        f[11612:11613]+f[11715:11716]+f[11997:11998]+f[12051:12052]+\
        f[12104:12105]+f[12155:12156]+f[12285:12286]+f[12395:12396]+\
        f[12779:12780]+f[16756:16757]+f[18279:18280]+f[18306:18307]+\
        f[20199:20200]+f[20611:20612]+f[22218:22219]+f[28761:28762]+\
        f[28855:28856]+f[40394:40395]+f[40444:40445]+f[40458:40459]+\
        f[40468:40469]+f[40487:40488]+f[40532:40533]+f[40551:40552]+\
        f[40658:40659]+f[40662:40663]+f[40680:40681]+f[40743:40744]+\
        f[41764:41765]+f[42052:42053]+f[42215:42216]+f[42218:42219]+\
        f[42271:42272]+f[42292:42293]+f[44155:44156]+f[44168:44169]+\
        f[44173:44174]+f[44363:44364]+f[46582:46583]+f[50935:50936]+\
        f[52113:52114]+f[55052:55053]
    face2Elements = f[992:993]+f[5613:5614]+f[6430:6431]+f[6452:6453]+f[6469:6470]+\
        f[6551:6552]+f[6735:6736]+f[7113:7114]+f[7123:7124]+f[10300:10301]+\
        f[10710:10711]+f[10716:10717]+f[10732:10733]+f[10854:10855]+\
        f[10916:10917]+f[10996:10997]+f[11074:11075]+f[11160:11161]+\
        f[11217:11218]+f[11278:11279]+f[11319:11320]+f[11394:11395]+\
        f[11432:11433]+f[11445:11446]+f[11562:11563]+f[11568:11569]+\
        f[11662:11663]+f[11772:11773]+f[11831:11832]+f[11884:11885]+\
        f[11940:11941]+f[11996:11997]+f[12055:12056]+f[12153:12154]+\
        f[12214:12215]+f[12221:12222]+f[12336:12337]+f[12345:12346]+\
        f[12448:12450]+f[12510:12511]+f[12554:12555]+f[38070:38071]+\
        f[38123:38124]+f[40404:40406]+f[40625:40626]+f[40839:40840]+\
        f[41147:41148]+f[41222:41223]+f[41249:41250]+f[41259:41260]+\
        f[41505:41506]+f[41988:41989]+f[42089:42090]+f[42098:42099]+\
        f[42127:42128]+f[42190:42191]+f[42197:42198]+f[42248:42249]+\
        f[42290:42291]+f[44228:44229]+f[44980:44981]+f[46256:46257]+\
        f[46388:46389]+f[50440:50441]
    face3Elements = f[119:120]+f[144:145]+f[227:228]+f[265:266]+f[596:597]+\
        f[684:685]+f[745:746]+f[1417:1418]+f[1665:1666]+f[1703:1704]+\
        f[1715:1716]+f[1729:1730]+f[1745:1746]+f[1760:1761]+f[1764:1765]+\
        f[1773:1774]+f[1785:1786]+f[1798:1799]+f[1807:1808]+f[1812:1813]+\
        f[1825:1826]+f[1830:1831]+f[1841:1842]+f[1849:1852]+f[1863:1864]+\
        f[1867:1868]+f[1935:1936]+f[1975:1976]+f[1977:1978]+f[1980:1981]+\
        f[1983:1984]+f[1985:1986]+f[1992:1993]+f[2002:2003]+f[2007:2008]+\
        f[2010:2011]+f[2013:2014]+f[2925:2926]+f[3306:3307]+f[3638:3639]+\
        f[3654:3655]+f[4304:4305]+f[4355:4356]+f[4427:4428]+f[4539:4540]+\
        f[4550:4551]+f[4606:4607]+f[4673:4674]+f[4679:4680]+f[4682:4683]+\
        f[4712:4713]+f[4717:4718]+f[4736:4737]+f[4778:4779]+f[4784:4785]+\
        f[4851:4852]+f[4862:4863]+f[4877:4878]+f[4901:4903]+f[4909:4910]+\
        f[5078:5079]+f[5208:5209]+f[5290:5291]+f[5299:5300]+f[5382:5383]+\
        f[5561:5562]+f[5568:5569]+f[5622:5623]+f[5645:5646]+f[5695:5696]+\
        f[5738:5740]+f[5770:5771]+f[5788:5789]+f[5804:5805]+f[5880:5881]+\
        f[5898:5899]+f[6026:6027]+f[6030:6031]+f[6044:6045]+f[6112:6113]+\
        f[6118:6119]+f[6133:6135]+f[6164:6165]+f[6224:6225]+f[6227:6228]+\
        f[6236:6238]+f[6311:6312]+f[6334:6335]+f[6385:6386]+f[6412:6413]+\
        f[6416:6417]+f[6431:6432]+f[6437:6438]+f[6456:6457]+f[6458:6459]+\
        f[6463:6464]+f[6506:6507]+f[6552:6553]+f[6611:6612]+f[6664:6665]+\
        f[6748:6749]+f[6787:6788]+f[6797:6798]+f[6805:6806]+f[6861:6862]+\
        f[7020:7021]+f[7022:7023]+f[7056:7057]+f[7061:7062]+f[7096:7097]+\
        f[7125:7126]+f[7149:7150]+f[7155:7156]+f[7160:7161]+f[7210:7211]+\
        f[7236:7237]+f[7238:7239]+f[7460:7461]+f[7469:7470]+f[7520:7521]+\
        f[7572:7573]+f[7636:7637]+f[7640:7641]+f[7702:7703]+f[7785:7786]+\
        f[7820:7821]+f[8179:8180]+f[10024:10025]+f[10034:10037]+f[10130:10131]+\
        f[10197:10198]+f[10199:10200]+f[10209:10210]+f[10227:10228]+\
        f[10348:10349]+f[10353:10354]+f[10362:10363]+f[10368:10369]+\
        f[10429:10430]+f[10434:10435]+f[10495:10496]+f[10558:10559]+\
        f[10647:10648]+f[10714:10716]+f[10750:10751]+f[10756:10757]+\
        f[10774:10775]+f[10785:10786]+f[10795:10796]+f[10800:10801]+\
        f[10802:10803]+f[10805:10806]+f[10807:10808]+f[10811:10812]+\
        f[10824:10825]+f[10834:10835]+f[10841:10842]+f[10844:10846]+\
        f[10864:10865]+f[10879:10881]+f[10893:10894]+f[10901:10902]+\
        f[10910:10912]+f[10930:10931]+f[10934:10935]+f[10943:10944]+\
        f[10948:10949]+f[10952:10953]+f[10954:10955]+f[10956:10957]+\
        f[10965:10966]+f[10972:10974]+f[10999:11000]+f[11002:11003]+\
        f[11006:11007]+f[11008:11009]+f[11014:11015]+f[11027:11028]+\
        f[11030:11031]+f[11038:11039]+f[11042:11043]+f[11051:11052]+\
        f[11053:11054]+f[11059:11060]+f[11061:11062]+f[11065:11066]+\
        f[11067:11068]+f[11072:11074]+f[11075:11076]+f[11083:11084]+\
        f[11088:11089]+f[11093:11094]+f[11102:11103]+f[11115:11117]+\
        f[11118:11119]+f[11124:11125]+f[11129:11130]+f[11143:11145]+\
        f[11152:11153]+f[11155:11156]+f[11157:11158]+f[11159:11160]+\
        f[11163:11164]+f[11167:11169]+f[11173:11174]+f[11182:11184]+\
        f[11185:11186]+f[11202:11203]+f[11209:11210]+f[11213:11214]+\
        f[11226:11227]+f[11231:11232]+f[11234:11235]+f[11241:11242]+\
        f[11254:11255]+f[11259:11260]+f[11264:11265]+f[11275:11277]+\
        f[11282:11283]+f[11289:11290]+f[11293:11294]+f[11298:11299]+\
        f[11306:11307]+f[11309:11311]+f[11323:11324]+f[11328:11329]+\
        f[11332:11333]+f[11336:11338]+f[11339:11340]+f[11345:11346]+\
        f[11351:11352]+f[11353:11354]+f[11365:11366]+f[11367:11369]+\
        f[11371:11372]+f[11375:11376]+f[11379:11380]+f[11389:11390]+\
        f[11392:11393]+f[11400:11402]+f[11413:11414]+f[11415:11417]+\
        f[11418:11419]+f[11420:11421]+f[11423:11425]+f[11440:11441]+\
        f[11443:11444]+f[11456:11457]+f[11464:11465]+f[11467:11468]+\
        f[11470:11471]+f[11476:11478]+f[11481:11483]+f[11489:11491]+\
        f[11494:11495]+f[11497:11498]+f[11505:11506]+f[11507:11509]+\
        f[11516:11517]+f[11520:11521]+f[11523:11524]+f[11527:11528]+\
        f[11532:11533]+f[11535:11536]+f[11537:11538]+f[11544:11545]+\
        f[11546:11547]+f[11550:11551]+f[11554:11555]+f[11559:11560]+\
        f[11571:11572]+f[11576:11578]+f[11582:11583]+f[11585:11586]+\
        f[11588:11589]+f[11590:11591]+f[11593:11595]+f[11597:11599]+\
        f[11604:11605]+f[11607:11608]+f[11619:11620]+f[11640:11641]+\
        f[11644:11645]+f[11670:11672]+f[11676:11677]+f[11691:11692]+\
        f[11696:11697]+f[11700:11701]+f[11705:11706]+f[11708:11710]+\
        f[11722:11723]+f[11727:11728]+f[11739:11741]+f[11747:11748]+\
        f[11753:11754]+f[11757:11758]+f[11761:11762]+f[11768:11769]+\
        f[11776:11777]+f[11786:11787]+f[11796:11797]+f[11799:11800]+\
        f[11802:11803]+f[11808:11809]+f[11811:11815]+f[11818:11819]+\
        f[11822:11823]+f[11824:11825]+f[11834:11836]+f[11841:11842]+\
        f[11848:11849]+f[11853:11854]+f[11862:11863]+f[11866:11868]+\
        f[11878:11879]+f[11887:11888]+f[11893:11894]+f[11898:11899]+\
        f[11917:11918]+f[11928:11929]+f[11930:11931]+f[11932:11934]+\
        f[11936:11937]+f[11944:11945]+f[11952:11953]+f[11961:11962]+\
        f[11965:11966]+f[11968:11969]+f[11972:11976]+f[11978:11979]+\
        f[11982:11983]+f[11991:11993]+f[11994:11995]+f[12008:12009]+\
        f[12013:12014]+f[12019:12021]+f[12022:12023]+f[12026:12027]+\
        f[12029:12030]+f[12031:12032]+f[12033:12034]+f[12037:12038]+\
        f[12046:12047]+f[12048:12049]+f[12062:12064]+f[12072:12073]+\
        f[12078:12079]+f[12106:12107]+f[12115:12116]+f[12121:12122]+\
        f[12127:12129]+f[12133:12134]+f[12140:12141]+f[12145:12146]+\
        f[12148:12149]+f[12150:12151]+f[12158:12159]+f[12162:12163]+\
        f[12171:12173]+f[12174:12176]+f[12177:12178]+f[12187:12188]+\
        f[12189:12190]+f[12191:12192]+f[12199:12200]+f[12207:12208]+\
        f[12210:12211]+f[12219:12220]+f[12228:12229]+f[12235:12236]+\
        f[12241:12242]+f[12248:12249]+f[12252:12253]+f[12258:12259]+\
        f[12260:12261]+f[12264:12265]+f[12268:12269]+f[12274:12276]+\
        f[12279:12280]+f[12282:12283]+f[12294:12295]+f[12298:12299]+\
        f[12303:12304]+f[12307:12308]+f[12309:12310]+f[12320:12321]+\
        f[12327:12328]+f[12329:12331]+f[12344:12345]+f[12351:12355]+\
        f[12359:12360]+f[12366:12367]+f[12368:12369]+f[12376:12377]+\
        f[12379:12380]+f[12383:12384]+f[12385:12386]+f[12388:12389]+\
        f[12399:12400]+f[12406:12407]+f[12410:12412]+f[12417:12418]+\
        f[12424:12426]+f[12437:12438]+f[12440:12442]+f[12444:12447]+\
        f[12455:12456]+f[12460:12461]+f[12464:12465]+f[12466:12467]+\
        f[12470:12471]+f[12478:12479]+f[12484:12485]+f[12487:12488]+\
        f[12492:12493]+f[12498:12500]+f[12503:12505]+f[12509:12510]+\
        f[12528:12530]+f[12537:12539]+f[12543:12544]+f[12563:12564]+\
        f[12569:12570]+f[12578:12579]+f[12582:12583]+f[12587:12588]+\
        f[12594:12595]+f[12597:12599]+f[12631:12632]+f[12633:12635]+\
        f[12638:12639]+f[12652:12653]+f[12661:12662]+f[12667:12668]+\
        f[12672:12673]+f[12687:12688]+f[12696:12697]+f[12708:12709]+\
        f[12746:12747]+f[12906:12907]+f[12946:12947]+f[12949:12950]+\
        f[13368:13369]+f[13524:13525]+f[13526:13527]+f[13528:13529]+\
        f[13531:13533]+f[13541:13542]+f[13547:13548]+f[13551:13553]+\
        f[13555:13556]+f[13557:13558]+f[13559:13560]+f[13561:13562]+\
        f[13563:13564]+f[13565:13567]+f[13581:13582]+f[13585:13586]+\
        f[13588:13589]+f[13636:13638]+f[13662:13663]+f[13684:13685]+\
        f[13765:13767]+f[13769:13770]+f[13776:13778]+f[13779:13780]+\
        f[13821:13823]+f[13836:13837]+f[13891:13892]+f[13982:13983]+\
        f[14044:14045]+f[14226:14227]+f[14230:14231]+f[14238:14239]+\
        f[14371:14372]+f[14486:14487]+f[14568:14569]+f[14589:14590]+\
        f[14595:14596]+f[14648:14649]+f[14767:14768]+f[14771:14773]+\
        f[14774:14775]+f[14780:14781]+f[14877:14878]+f[14960:14961]+\
        f[14962:14963]+f[15080:15081]+f[15085:15086]+f[15110:15111]+\
        f[15112:15113]+f[15149:15150]+f[15191:15192]+f[15343:15344]+\
        f[15545:15546]+f[15733:15734]+f[15764:15765]+f[15817:15818]+\
        f[16002:16003]+f[16073:16075]+f[16086:16087]+f[16149:16150]+\
        f[16183:16184]+f[16206:16207]+f[16249:16250]+f[16332:16333]+\
        f[16384:16385]+f[16398:16399]+f[16494:16495]+f[16573:16574]+\
        f[16578:16579]+f[16681:16682]+f[16790:16791]+f[16792:16793]+\
        f[16795:16797]+f[16970:16971]+f[16974:16975]+f[17025:17026]+\
        f[17158:17159]+f[17160:17161]+f[17163:17164]+f[17374:17375]+\
        f[17376:17378]+f[17380:17381]+f[17488:17489]+f[17765:17766]+\
        f[18011:18012]+f[18112:18113]+f[19341:19342]+f[19786:19787]+\
        f[19789:19790]+f[20720:20721]+f[21699:21700]+f[21701:21702]+\
        f[23156:23157]+f[24544:24545]+f[24546:24547]+f[25920:25921]+\
        f[26712:26714]+f[27594:27595]+f[28765:28766]+f[28792:28793]+\
        f[36150:36151]+f[36340:36341]+f[37003:37004]+f[37330:37331]+\
        f[37957:37959]+f[37963:37964]+f[37990:37991]+f[38004:38005]+\
        f[38033:38034]+f[38035:38036]+f[38046:38047]+f[38051:38052]+\
        f[38055:38056]+f[38057:38059]+f[38062:38063]+f[38080:38081]+\
        f[38083:38084]+f[38125:38126]+f[38135:38136]+f[38141:38142]+\
        f[38143:38144]+f[38145:38146]+f[38153:38154]+f[38160:38161]+\
        f[38193:38194]+f[38217:38218]+f[38224:38225]+f[38252:38253]+\
        f[38263:38264]+f[38291:38292]+f[38293:38294]+f[38303:38304]+\
        f[38329:38330]+f[38603:38604]+f[38668:38669]+f[40336:40337]+\
        f[40340:40341]+f[40358:40362]+f[40379:40380]+f[40397:40398]+\
        f[40419:40420]+f[40422:40423]+f[40432:40433]+f[40448:40449]+\
        f[40457:40458]+f[40469:40471]+f[40473:40474]+f[40481:40482]+\
        f[40485:40486]+f[40489:40490]+f[40497:40498]+f[40501:40502]+\
        f[40506:40507]+f[40514:40515]+f[40519:40520]+f[40524:40525]+\
        f[40534:40535]+f[40537:40538]+f[40543:40544]+f[40545:40546]+\
        f[40562:40565]+f[40570:40571]+f[40572:40573]+f[40577:40578]+\
        f[40585:40586]+f[40595:40597]+f[40609:40611]+f[40622:40623]+\
        f[40626:40627]+f[40638:40640]+f[40652:40653]+f[40685:40686]+\
        f[40692:40694]+f[40697:40698]+f[40703:40704]+f[40717:40718]+\
        f[40721:40722]+f[40730:40732]+f[40739:40740]+f[40742:40743]+\
        f[40754:40756]+f[40776:40777]+f[40787:40788]+f[40794:40795]+\
        f[40798:40799]+f[40803:40804]+f[40808:40810]+f[40817:40820]+\
        f[40824:40825]+f[40826:40830]+f[40833:40834]+f[40835:40836]+\
        f[40837:40838]+f[40842:40843]+f[40846:40847]+f[40850:40851]+\
        f[40863:40865]+f[40871:40876]+f[40878:40880]+f[40887:40888]+\
        f[40889:40890]+f[40895:40896]+f[40897:40898]+f[40900:40903]+\
        f[40904:40905]+f[40906:40908]+f[40913:40914]+f[40928:40929]+\
        f[40935:40936]+f[40947:40948]+f[40949:40950]+f[40955:40956]+\
        f[40957:40958]+f[40965:40966]+f[40968:40969]+f[40977:40978]+\
        f[40987:40988]+f[40990:40991]+f[40999:41001]+f[41003:41004]+\
        f[41006:41007]+f[41008:41009]+f[41010:41012]+f[41022:41023]+\
        f[41026:41029]+f[41031:41032]+f[41034:41035]+f[41040:41041]+\
        f[41042:41043]+f[41048:41049]+f[41071:41073]+f[41074:41075]+\
        f[41076:41077]+f[41080:41081]+f[41082:41083]+f[41084:41085]+\
        f[41092:41093]+f[41094:41095]+f[41106:41107]+f[41109:41110]+\
        f[41112:41113]+f[41128:41129]+f[41142:41143]+f[41146:41147]+\
        f[41152:41153]+f[41156:41157]+f[41174:41175]+f[41189:41190]+\
        f[41194:41195]+f[41198:41200]+f[41203:41204]+f[41206:41207]+\
        f[41210:41211]+f[41218:41219]+f[41225:41226]+f[41232:41233]+\
        f[41235:41236]+f[41237:41238]+f[41240:41241]+f[41251:41252]+\
        f[41255:41256]+f[41261:41262]+f[41268:41269]+f[41273:41274]+\
        f[41287:41288]+f[41295:41296]+f[41311:41312]+f[41320:41322]+\
        f[41327:41328]+f[41340:41341]+f[41344:41345]+f[41347:41348]+\
        f[41354:41355]+f[41367:41368]+f[41370:41371]+f[41381:41382]+\
        f[41385:41386]+f[41388:41389]+f[41398:41399]+f[41404:41405]+\
        f[41406:41407]+f[41413:41414]+f[41415:41419]+f[41423:41424]+\
        f[41425:41426]+f[41430:41431]+f[41443:41444]+f[41451:41452]+\
        f[41453:41454]+f[41457:41458]+f[41477:41479]+f[41481:41482]+\
        f[41483:41484]+f[41486:41487]+f[41492:41493]+f[41508:41509]+\
        f[41510:41511]+f[41515:41516]+f[41517:41518]+f[41523:41524]+\
        f[41526:41527]+f[41537:41538]+f[41540:41542]+f[41547:41548]+\
        f[41550:41551]+f[41554:41556]+f[41557:41558]+f[41560:41561]+\
        f[41563:41564]+f[41567:41568]+f[41576:41578]+f[41581:41582]+\
        f[41586:41588]+f[41615:41617]+f[41625:41626]+f[41628:41629]+\
        f[41639:41640]+f[41647:41648]+f[41650:41651]+f[41657:41658]+\
        f[41662:41663]+f[41670:41671]+f[41672:41673]+f[41674:41675]+\
        f[41678:41679]+f[41684:41685]+f[41689:41690]+f[41692:41693]+\
        f[41698:41699]+f[41701:41702]+f[41703:41704]+f[41706:41708]+\
        f[41709:41710]+f[41716:41717]+f[41720:41721]+f[41733:41735]+\
        f[41736:41737]+f[41741:41742]+f[41748:41749]+f[41750:41753]+\
        f[41755:41756]+f[41767:41768]+f[41778:41779]+f[41784:41785]+\
        f[41793:41794]+f[41797:41798]+f[41801:41802]+f[41807:41808]+\
        f[41809:41810]+f[41813:41814]+f[41815:41817]+f[41818:41819]+\
        f[41823:41827]+f[41830:41831]+f[41839:41840]+f[41844:41845]+\
        f[41847:41848]+f[41849:41851]+f[41852:41853]+f[41856:41857]+\
        f[41858:41859]+f[41860:41861]+f[41862:41863]+f[41871:41873]+\
        f[41877:41879]+f[41882:41884]+f[41888:41890]+f[41891:41892]+\
        f[41900:41901]+f[41911:41912]+f[41914:41917]+f[41921:41922]+\
        f[41923:41924]+f[41940:41942]+f[41949:41950]+f[41966:41967]+\
        f[41973:41974]+f[41980:41981]+f[41985:41986]+f[41987:41988]+\
        f[41989:41990]+f[41994:41995]+f[41997:41998]+f[42002:42003]+\
        f[42007:42008]+f[42017:42018]+f[42022:42024]+f[42027:42028]+\
        f[42033:42034]+f[42043:42044]+f[42046:42048]+f[42050:42051]+\
        f[42066:42067]+f[42071:42072]+f[42075:42078]+f[42087:42088]+\
        f[42097:42098]+f[42099:42100]+f[42104:42106]+f[42122:42124]+\
        f[42134:42135]+f[42138:42139]+f[42146:42147]+f[42154:42155]+\
        f[42164:42165]+f[42174:42175]+f[42177:42178]+f[42182:42183]+\
        f[42199:42200]+f[42205:42207]+f[42225:42226]+f[42231:42232]+\
        f[42252:42253]+f[42261:42262]+f[42278:42279]+f[42282:42283]+\
        f[42301:42302]+f[42309:42310]+f[42315:42316]+f[42328:42329]+\
        f[42349:42351]+f[42370:42371]+f[42378:42379]+f[42380:42381]+\
        f[42384:42385]+f[42390:42391]+f[42393:42394]+f[42397:42398]+\
        f[42400:42402]+f[42405:42406]+f[42408:42410]+f[42417:42418]+\
        f[42422:42423]+f[42457:42458]+f[42477:42479]+f[42488:42489]+\
        f[42547:42548]+f[42556:42558]+f[42559:42560]+f[42569:42570]+\
        f[42571:42572]+f[42603:42604]+f[42669:42670]+f[42716:42717]+\
        f[42726:42727]+f[42737:42738]+f[42766:42768]+f[43082:43083]+\
        f[43406:43407]+f[43620:43622]+f[43624:43626]+f[43628:43629]+\
        f[43638:43639]+f[43807:43809]+f[43814:43816]+f[43817:43818]+\
        f[43820:43821]+f[43822:43823]+f[43829:43830]+f[43898:43899]+\
        f[43929:43930]+f[43967:43968]+f[43970:43971]+f[44032:44033]+\
        f[44052:44053]+f[44055:44056]+f[44065:44066]+f[44073:44075]+\
        f[44083:44084]+f[44094:44095]+f[44125:44126]+f[44139:44140]+\
        f[44146:44147]+f[44150:44151]+f[44152:44153]+f[44154:44155]+\
        f[44156:44157]+f[44161:44163]+f[44174:44175]+f[44178:44180]+\
        f[44185:44186]+f[44211:44212]+f[44214:44215]+f[44218:44219]+\
        f[44226:44227]+f[44229:44230]+f[44231:44233]+f[44247:44249]+\
        f[44252:44253]+f[44298:44299]+f[44364:44365]+f[44368:44369]+\
        f[44397:44398]+f[44403:44404]+f[44412:44413]+f[44682:44683]+\
        f[44698:44700]+f[44705:44706]+f[44710:44712]+f[44724:44725]+\
        f[44760:44761]+f[44904:44905]+f[44919:44920]+f[44950:44951]+\
        f[44979:44980]+f[45043:45044]+f[45049:45050]+f[45073:45074]+\
        f[45090:45091]+f[45096:45097]+f[45119:45120]+f[45122:45123]+\
        f[45184:45185]+f[45261:45262]+f[45271:45273]+f[45297:45298]+\
        f[45321:45322]+f[45345:45346]+f[45350:45351]+f[45406:45407]+\
        f[45408:45409]+f[45469:45470]+f[45488:45490]+f[45626:45627]+\
        f[45676:45677]+f[45692:45693]+f[45705:45706]+f[45716:45717]+\
        f[45730:45731]+f[45937:45938]+f[46058:46059]+f[46234:46235]+\
        f[46348:46349]+f[46386:46387]+f[46399:46401]+f[46404:46405]+\
        f[46409:46410]+f[46441:46442]+f[46464:46466]+f[46471:46472]+\
        f[46476:46477]+f[46478:46480]+f[46481:46482]+f[46497:46498]+\
        f[46501:46502]+f[46516:46517]+f[46519:46520]+f[46523:46524]+\
        f[46525:46526]+f[46571:46572]+f[46577:46578]+f[46586:46587]+\
        f[46594:46595]+f[46746:46747]+f[46754:46755]+f[46763:46764]+\
        f[46773:46774]+f[46804:46805]+f[46822:46824]+f[46866:46867]+\
        f[46877:46878]+f[46882:46883]+f[46991:46992]+f[47007:47008]+\
        f[47024:47025]+f[47026:47027]+f[47251:47252]+f[47253:47254]+\
        f[47274:47275]+f[47397:47398]+f[47435:47436]+f[47517:47519]+\
        f[47529:47530]+f[47593:47594]+f[47715:47716]+f[47721:47722]+\
        f[47736:47737]+f[47849:47850]+f[47903:47904]+f[47923:47924]+\
        f[47926:47927]+f[47945:47946]+f[47950:47951]+f[47955:47956]+\
        f[47963:47964]+f[47973:47974]+f[48122:48123]+f[48128:48129]+\
        f[48219:48220]+f[48270:48271]+f[48279:48280]+f[48483:48484]+\
        f[48516:48517]+f[48731:48732]+f[48859:48860]+f[48932:48933]+\
        f[49255:49256]+f[49467:49468]+f[49579:49580]+f[49630:49631]+\
        f[49668:49669]+f[49691:49692]+f[49718:49719]+f[49772:49773]+\
        f[49827:49828]+f[49837:49838]+f[49906:49907]+f[49943:49944]+\
        f[50015:50017]+f[50127:50128]+f[50129:50130]+f[50237:50238]+\
        f[50269:50270]+f[52527:52528]+f[53117:53118]+f[53765:53766]+\
        f[54226:54227]+f[57737:57738]+f[58489:58490]+f[58583:58584]+\
        f[59477:59478]
    face4Elements = f[4727:4728]+f[5608:5609]+f[5662:5663]+f[5801:5802]+\
        f[6156:6157]+f[7793:7794]+f[10218:10219]+f[10832:10833]+f[11499:11500]+\
        f[11766:11767]+f[11783:11784]+f[12079:12080]+f[13595:13596]+\
        f[14227:14228]+f[14384:14385]+f[17210:17211]+f[18468:18469]+\
        f[18577:18578]+f[18909:18910]+f[19760:19761]+f[21508:21509]+\
        f[22275:22276]+f[26596:26597]+f[27087:27088]+f[27632:27633]+\
        f[27645:27646]+f[29416:29417]+f[29989:29990]+f[32763:32764]+\
        f[38026:38027]+f[38059:38060]+f[40420:40421]+f[40599:40600]+\
        f[40665:40666]+f[40981:40982]+f[41151:41152]+f[41386:41387]+\
        f[41405:41406]+f[42285:42286]+f[44031:44032]+f[45713:45714]+\
        f[46528:46529]+f[47461:47462]+f[50861:50862]+f[53648:53649]+\
        f[67078:67079]
    p.generateMeshByOffset(region=regionToolset.Region(face1Elements=face1Elements, 
        face2Elements=face2Elements, face3Elements=face3Elements, 
        face4Elements=face4Elements), meshType=SHELL, 
        distanceBetweenLayers=0.0, numLayers=2, initialOffset=0.0, 
        shareNodes=True)
    p = mdb.models['Model-1'].parts['merged-shell']
    e = p.elements
    elements = e[69658:71058]
    p.Set(elements=elements, name='OffsetElementsfgd-Layer-1')
    p = mdb.models['Model-1'].parts['merged-shell']
    e = p.elements
    elements = e[71058:72458]
    p.Set(elements=elements, name='OffsetElementsfgd-Layer-2')


def get_min():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    pass

def make_spring_BCs():
    # model = 'setup'
    model = 'Model-1'
    p = mdb.models[model].parts['merged-shell']
    print p.allSets
    x_spring_nodes = p.allSets['x-spring-nodes'].nodes
    z_spring_nodes = p.allSets['z-spring-nodes'].nodes
    E = 2000
    L = 5

    # apply_spring_bc_to_set2(x_spring_nodes, E, L, 1, p)
    apply_spring_bc_to_set2(x_spring_nodes, E, L, 1, p, model)
    apply_spring_bc_to_set2(z_spring_nodes, 12000, L, 3, p, model)

    # rename_spring_bcs(x_spring_nodes, 1)
    # rename_spring_bcs(z_spring_nodes, 3)

def apply_spring_bc_to_set(node_set, E, L, n):

    map_orientation = {1: 'x', 2: 'y', 3: 'z'}
    shear = [1, 2, 3]
    shear.remove(n)
    n_nodes = np.alen(node_set)
    for i in range(n_nodes):
        node = node_set[i]
        print i+1, "/", n_nodes

        neighbour_els = node.getElements()
        neighbour_nodes = []
        for el in neighbour_els:
            el_nodes = el.getNodes()
            for el_node in el_nodes:
                if el_node not in neighbour_nodes and el_node != node:
                    neighbour_nodes.append(el_node)

        n_neighbours = np.alen(neighbour_nodes)
        neighbour_coords = np.zeros([n_neighbours, 3])
        for j in range(n_neighbours):
            neighbour_coords[j, :] = neighbour_nodes[j].coordinates

        node_coords = np.array(node.coordinates)
        avg_distance = np.mean(np.linalg.norm(neighbour_coords - node_coords))
        r = avg_distance * 3./4.

        A = np.pi * r ** 2
        I = np.pi * r ** 4 / 4.

        k_direct = A * E / L
        k_shear = 12. * E * I / L ** 3

        nodes1 = node_set[i:i+1]

        region=regionToolset.Region(nodes=nodes1)
        mdb.models['Model-1-Copy'].rootAssembly.engineeringFeatures.SpringDashpotToGround(
            name=str(i) + map_orientation[n] + "-dir", region=region, orientation=None, dof=n,
            springBehavior=ON, springStiffness=k_direct, dashpotBehavior=OFF,
            dashpotCoefficient=0.0)
        for shear_i in shear:
            mdb.models['Model-1-Copy'].rootAssembly.engineeringFeatures.SpringDashpotToGround(
                name=str(i) + map_orientation[n] + "-shear-" + map_orientation[shear_i], region=region, orientation=None, dof=shear_i,
                springBehavior=ON, springStiffness=k_shear, dashpotBehavior=OFF,
                dashpotCoefficient=0.0)

def rename_spring_bcs(node_set, n):
    map_orientation = {1: 'x', 2: 'y', 3: 'z'}
    shear = [1, 2, 3]
    shear.remove(n)
    n_nodes = np.alen(node_set)
    for i in range(1, n_nodes):
        print i+1, "/", n_nodes

        mdb.models['Model-1-Copy'].rootAssembly.engineeringFeatures.springDashpots.changeKey(
            fromName=str(i) + map_orientation[n] + "-dir", toName='zz' + str(i) + map_orientation[n] + "-dir")
        for shear_i in shear:
            mdb.models['Model-1-Copy'].rootAssembly.engineeringFeatures.springDashpots.changeKey(
                fromName=str(i) + map_orientation[n] + "-shear-" + map_orientation[shear_i], toName='zz' + str(i) + map_orientation[n] + "-shear-" + map_orientation[shear_i])


def apply_spring_bc_to_set2(node_set, E, L, n, p, model):
    """

    :param node_set:
    :param E:
    :param L:
    :param n:
    :param p:
    :param model:
    :return:
    """

    map_orientation = {1: 'x', 2: 'y', 3: 'z'}
    shear = [1, 2, 3]
    shear.remove(n)
    n_nodes = np.alen(node_set)
    t_first = 0
    t_second = 0
    t_third = 0
    for i in range(n_nodes):
    # for i in range(5):
        t1 = time.time()
        # for i in range(35):
        node = node_set[i]
        nodes1 = node_set[i:i+1]
        # p = mdb.models['Model-1-Copy'].parts['merged-shell']
        # n = p.nodes
        # nodes = n[10864:10865]
        p.Set(nodes=nodes1, name='zz' + str(i) + map_orientation[n])
        # p.Set(nodes=nodes1, name=str(i) + map_orientation[n])
        print i+1, "/", n_nodes
        t_first += time.time() - t1
        t1 = time.time()
        # neighbour_els = node.getElements()

        neighbour_nodes = node.getElemEdges()[0].getNodes()
        if node == neighbour_nodes[1]:
            neighbour = neighbour_nodes[0]
        else:
            neighbour = neighbour_nodes[1]

        # node_coords = np.array(node.coordinates)
        # neighbour_coords = np.array(neighbour.coordinates)
        # avg_distance = np.linalg.norm(neighbour_coords - node_coords)
        avg_distance = ( (node.coordinates[0] - neighbour.coordinates[0])**2 +
                         (node.coordinates[1] - neighbour.coordinates[1])**2 +
                         (node.coordinates[2] - neighbour.coordinates[2])**2 ) **0.5

        # min = 0.
        # node_coords = np.array(node.coordinates)
        # for j in range(np.alen(neighbour_nodes)):
        #     neighbour = neighbour_nodes[j]
        #     neighbour_coords = np.array(neighbour.coordinates)
        #     avg_distance = np.mean(np.linalg.norm(neighbour_coords - node_coords))
        #     # print 'j = ', j
        #     # print 'avg distance = ', avg_distance
        #     # print 'min = ', min
        #     if avg_distance > 1.e-20 and (min < 1.e-20 or avg_distance < min):
        #         min = avg_distance
        # if neighbour_nodes[0] == node:
        #     neighbour = neighbour_nodes[0]
        # else:
        #     neighbour = neighbour_nodes[1]


        # avg_distance = np.mean(np.linalg.norm(neighbour_coords - node_coords))
        # r = avg_distance * 3./4.
        # print 'min final = ', min

        r = avg_distance * 3./4.

        A = np.pi * r ** 2
        I = np.pi * r ** 4 / 4.

        # if A == 0.0:
        #     neighbour = neighbour_nodes[1]
        #     node_coords = np.array(node.coordinates)
        #     neighbour_coords = np.array(neighbour.coordinates)
        #
        #     avg_distance = np.mean(np.linalg.norm(neighbour_coords - node_coords))
        #     r = avg_distance * 3./4.
        #
        #     A = np.pi * r ** 2
        #     I = np.pi * r ** 4 / 4.

        # print 'A'
        # print A
        # print 'I'
        # print I

        k_direct = A * E / L
        k_shear = 12. * E * I / L ** 3
        t_second += time.time() - t1
        t1 = time.time()

        # nodes1 = node_set[i:i+1]

        # region=regionToolset.Region(nodes=nodes1)
        a = mdb.models[model].rootAssembly
        region=a.instances['merged-shell-1'].sets['zz' + str(i) + map_orientation[n]]
        # region=a.instances['merged-shell-1'].sets[ str(i) + map_orientation[n]]
        mdb.models[model].rootAssembly.engineeringFeatures.SpringDashpotToGround(
            name='zz' + str(i) + map_orientation[n] + "-dir", region=region, orientation=None, dof=n,
            springBehavior=ON, springStiffness=k_direct, dashpotBehavior=OFF,
            dashpotCoefficient=0.0)
        for shear_i in shear:
            mdb.models[model].rootAssembly.engineeringFeatures.SpringDashpotToGround(
                name='zz' + str(i) + map_orientation[n] + "-shear-" + map_orientation[shear_i], region=region, orientation=None, dof=shear_i,
                springBehavior=ON, springStiffness=k_shear, dashpotBehavior=OFF,
                dashpotCoefficient=0.0)
        t_third +=  time.time() - t1
        t1 = time.time()
    print 'time first ', t_first
    print 'time second ', t_second
    print 'time third ', t_third

def impl_supress_springs():
    p = mdb.models['Model-1-Copy'].parts['merged-shell']
    x_spring_nodes = p.allSets['x-srping-nodes'].nodes
    z_spring_nodes = p.allSets['z-spring-nodes'].nodes
    E = 2000
    L = 20

    supress_springs(x_spring_nodes, E, L, 1)
    supress_springs(z_spring_nodes, E, L, 3)

def supress_springs(node_set, E, L, n):
    map_orientation = {1: 'x', 2: 'y', 3: 'z'}
    shear = [1, 2, 3]
    shear.remove(n)
    n_nodes = np.alen(node_set)
    for i in range(n_nodes):
        node = node_set[i]
        print i+1, "/", n_nodes

        mdb.models['Model-1-Copy'].rootAssembly.engineeringFeatures.springDashpots[str(i) + map_orientation[n] + "-dir"].suppress()
        for shear_i in shear:
            mdb.models['Model-1-Copy'].rootAssembly.engineeringFeatures.springDashpots[str(i) + map_orientation[n] + "-shear-" + map_orientation[shear_i]].suppress()




def A_test():
    p = mdb.models['Model-1-Copy'].parts['merged-shell']
    print p
    print "*************"
    # print p.allSets
    # print "*************"
    # print p.allSets['x-srping-nodes']
    x_nodes = p.allSets['x-srping-nodes']
    # print "*************"
    # print p.allSets['x-srping-nodes'].nodes
    # print "*************"
    # print p.allSets['x-srping-nodes'].nodes[0].getElements()
    # print p.allSets['x-srping-nodes'].nodes[0].coordinates
    print x_nodes.nodes[0]
    print '***********'
    # print p.allSets['x-srping-nodes'].nodes[0].getElemEdges()
    # print "*************"
    a = x_nodes.nodes[1].getElemEdges()
    print a[0].getNodes()
    print "*************"
    print a[0].getNodes()[0].getElemEdges()
    print a[0].getNodes()[1].getElemEdges()
    print a[0].getNodes()[2].getElemEdges()
    node1 = x_nodes.nodes[0]
    node2 = x_nodes.nodes[1]
    print node1 == x_nodes.nodes[0]
    print node1 == x_nodes.nodes[1]
    # print "*************"
    # print p.allSets['collagen-membrane'].elements[0]
    # print '***********************'
    # print p.elements[0]

def set_spring_bc():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    session.viewports['Viewport: 1'].view.setValues(nearPlane=15.3702, 
        farPlane=24.8365, width=22.7671, height=11.344, viewOffsetX=4.03568, 
        viewOffsetY=-0.741327)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=7.75393, 
        farPlane=21.5495, width=11.4855, height=5.72279, cameraPosition=(
        -13.7724, 7.72266, 0.664803), cameraUpVector=(0.602739, 0.717788, 
        -0.348547), cameraTarget=(4.58539, -0.0734148, -3.04555), 
        viewOffsetX=2.03592, viewOffsetY=-0.373984)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=8.58927, 
        farPlane=20.7141, width=14.9546, height=7.45133, viewOffsetX=2.21159, 
        viewOffsetY=-0.313108)
    a = mdb.models['Model-1-Copy'].rootAssembly
    region=a.instances['merged-shell-1'].sets['z-spring-nodes']
    mdb.models['Model-1-Copy'].rootAssembly.engineeringFeatures.SpringDashpotToGround(
        name='spring-bc-z-shear2', region=region, orientation=None, dof=1, 
        springBehavior=ON, springStiffness=0.1, dashpotBehavior=OFF, 
        dashpotCoefficient=0.0)


def make_one_spring():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior

    a = mdb.models['Model-1-Copy'].rootAssembly
    n1 = a.instances['merged-shell-1'].nodes
    nodes1 = n1[7251:7252]
    # nodes1 = n1[7251]
    print nodes1
    print type(nodes1)
    print type(n1)
    # print nodes1.pop()
    # print nodes1
    # print type(nodes1)
    nodes1 = [nodes1[0]]
    print nodes1
    print type(nodes1)
    # region=regionToolset.Region(nodes=nodes1)
    # mdb.models['Model-1-Copy'].rootAssembly.engineeringFeatures.SpringDashpotToGround(
    #     name='Springs/Dashpots-7', region=region, orientation=None, dof=1,
    #     springBehavior=ON, springStiffness=0.1, dashpotBehavior=OFF,
    #     dashpotCoefficient=0.0)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=9.30723,
    #     farPlane=19.7562, width=14.0781, height=6.46319, viewOffsetX=5.25083,
    #     viewOffsetY=-0.546096)
    # mdb.models['Model-1-Copy'].rootAssembly.engineeringFeatures.springDashpots['spring-bc-z-shear2'].suppress(
    #     )


def make_node_set():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    session.viewports['Viewport: 1'].view.setValues(nearPlane=18.4883, 
        farPlane=27.4001, width=3.77582, height=1.73851, cameraPosition=(
        13.5032, 7.52068, -15.8373), cameraTarget=(3.12973, -0.469795, 
        1.02604))
    p = mdb.models['Model-1-Copy'].parts['merged-shell']
    n = p.nodes
    nodes = n[10864:10865]
    p.Set(nodes=nodes, name='z')


def rename():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    mdb.models['Model-1-Copy'].rootAssembly.engineeringFeatures.springDashpots.changeKey(
        fromName='0x-dir', toName='zz0x-dir')


# make_spring_BCs()def delete_sets():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    mdb.models['Model-1-Copy'].parts['merged-shell'].deleteSets(setNames=('zz0x', 
        'zz0z', 'zz1x', 'zz1z', 'zz2x', 'zz2z', 'zz3x', 'zz3z', 'zz4x', 'zz4z', 
        ))


def make_wire():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    
    a = mdb.models['orphan'].rootAssembly
    n1 = a.instances['merged-shell-1'].nodes
    a.WirePolyLine(points=((n1[6366], n1[6363]), (n1[5875], n1[6362])), 
        mergeType=IMPRINT, meshable=OFF)
    a = mdb.models['orphan'].rootAssembly
    e1 = a.edges
    edges1 = e1.getSequenceFromMask(mask=('[#3 ]', ), )
    a.Set(edges=edges1, name='Wire-1-Set-4')
    a = mdb.models['orphan'].rootAssembly
    region=a.sets['Wire-1-Set-4']
    csa = a.SectionAssignment(sectionName='connection', region=region)


def make_wire_2():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=3.86647,
    #     farPlane=6.74716, width=2.13786, height=1.13689, cameraPosition=(
    #     -3.11536, 1.9219, -3.5761), cameraUpVector=(-0.0340058, 0.71549,
    #     0.697795), cameraTarget=(0.61585, 0.641541, 0.696428),
    #     viewOffsetX=1.02529, viewOffsetY=0.0786637)
    a = mdb.models['orphan'].rootAssembly
    n11 = a.instances['merged-shell-1'].nodes
    print ((n11[5700], n11[5575]), (n11[132], n11[150]), (
        n11[5220], n11[5582]))
    print ((-1.14, 0.3, 0.25), ), ((-1.14, 0.2, 0.25), ), ((-1.14,
        0.1, 0.25), )
    # a.WirePolyLine(points=((n11[5700], n11[5575]), (n11[132], n11[150]), (
    #     n11[5220], n11[5582])), mergeType=IMPRINT, meshable=OFF)
    # a = mdb.models['orphan'].rootAssembly
    # e1 = a.edges
    # edges1 = e1.findAt(((-1.14, 0.3, 0.25), ), ((-1.14, 0.2, 0.25), ), ((-1.14,
    #     0.1, 0.25), ))
    # a.Set(edges=edges1, name='Wire-2-Set-3')

def Automate_make_wire():
    a = mdb.models['orphan'].rootAssembly
    sets = a.instances['merged-shell-1'].sets
    # ps = [sets['p-1']]
    ps = [sets['p-1'],
          sets['p-2'],
          sets['p-3']]

    # ns = [sets['n-1']]
    ns = [sets['n-1'],
          sets['n-2'],
          sets['n-3']]

    delta = [2.28, 1., 1.]

    # test_size = 10

    for iSet in range(3):
    # for iSet in range(1):
        p = list(ps[iSet].nodes)
        n = list(ns[iSet].nodes)
        n_nodes = np.alen(p)

        poly_wire_points = np.empty((n_nodes, 2), dtype=type(p[0]))
        edges_positions = [None] * n_nodes
        iNode = 0

        while len(p) > 0:
        # while iNode < test_size:
            print iSet+1, "/", 3, "\t\t", iNode + 1, "/", n_nodes

            found = False
            n_node = n[0]
            n_node_coords_project = np.array(n_node.coordinates)
            n_node_coords_project[iSet] += delta[iSet]*0.2
            edges_positions[iNode] = ((n_node_coords_project[0],
                           n_node_coords_project[1],
                           n_node_coords_project[2]), )
            n_node_coords_project[iSet] += delta[iSet]*0.8

            p_node = None

            iPNode = 0
            while not found:
                # print 1
                # print iPNode
                p_node_coords = np.array(p[iPNode].coordinates)
                print n_node
                # print 2
                if np.linalg.norm(p_node_coords - n_node_coords_project) < 1.e-6:
                    # print 3
                    found = True
                    # print 4
                    p_node = p[iPNode]
                    # print 5
                    p.pop(iPNode)
                    # print 6
                    break
                    # print 7
                else:
                    # print 8
                    iPNode += 1
                    # print 9
            print "herhe"

            poly_wire_points[iNode, 0] = n_node
            poly_wire_points[iNode, 1] = p_node

            n.pop(0)

            iNode += 1
        # print a.features
        # print a.features['Wire-1']

        poly_wire_points = tuple(map(tuple, poly_wire_points))
        # a.WirePolyLine(tuple(map(tuple, poly_wire_points)), name="wire-"+str(iSet+1))
        a.WirePolyLine(tuple(map(tuple, poly_wire_points)), mergeType=IMPRINT, meshable=OFF)
        e1 = a.edges
        edges1 = e1.findAt(*edges_positions)
        # print edges1[0]
        # for edge in edges1:
        #     if edge.featureName!='Wire-1':
        #         print edge.featureName
        a.Set(edges=edges1, name='wire-'+str(iSet+1))
        region=a.sets['wire-'+str(iSet+1)]
        # a.SectionAssignment(sectionName='connection', region=region)


def Automate_make_wire2():
    a = mdb.models['orphan'].rootAssembly
    sets = a.instances['merged-shell-1'].sets
    # ps = [sets['p-1']]
    ps = [sets['p-1'],
          sets['p-2'],
          sets['p-3']]

    # ns = [sets['n-1']]
    ns = [sets['n-1'],
          sets['n-2'],
          sets['n-3']]

    delta = [2.28, 1., 1.]

    # test_size = 10

    for iSet in range(3):
        # for iSet in range(1):
        p = list(ps[iSet].nodes)
        n = list(ns[iSet].nodes)
        n_nodes = np.alen(p)

        poly_wire_points = np.empty((n_nodes, 2), dtype=type(p[0]))
        edges_positions = [None] * n_nodes
        # iNode = 0

        p_coords = np.array([np.array(p[iPNode].coordinates) for iPNode in range(n_nodes)])

        for iPair in range(n_nodes):
            # while iNode < test_size:
            print iSet+1, "/", 3, "\t\t", iPair + 1, "/", n_nodes

            n_node = n[iPair]
            n_node_coords = np.array(n_node.coordinates)
            n_node_coords_project = np.array(n_node.coordinates)
            n_node_coords_project[iSet] += delta[iSet]

            arg_min_dist = np.argmin(np.sum((p_coords - n_node_coords_project)**2, axis=1))
            mid_point = (p_coords[arg_min_dist, :] + n_node_coords)*0.5


            edges_positions[iPair] = ((mid_point[0],
                                       mid_point[1],
                                       mid_point[2]), )

            p_node = p[arg_min_dist]
            p.pop(arg_min_dist)
            p_coords = np.delete(p_coords, arg_min_dist, axis=0)


            poly_wire_points[iPair, 0] = n_node
            poly_wire_points[iPair, 1] = p_node


        poly_wire_points = tuple(map(tuple, poly_wire_points))
        a.WirePolyLine(tuple(map(tuple, poly_wire_points)), mergeType=IMPRINT, meshable=OFF)
        e1 = a.edges
        edges1 = e1.findAt(*edges_positions)
        a.Set(edges=edges1, name='wire-'+str(iSet+1))
        region=a.sets['wire-'+str(iSet+1)]
        a.SectionAssignment(sectionName='connection', region=region)



def AAA_scratch():
    a = mdb.models['orphan'].rootAssembly
    sets = a.instances['merged-shell-1'].sets
    ps = [sets['p-1'],
          sets['p-2'],
          sets['p-3']]

    ns = [sets['n-1'],
          sets['n-2'],
          sets['n-3']]

    p = ps[0]
    print p
    print np.array(p.nodes[0].coordinates)


def find_node(coords, node_list):
    pass

def AAA_copy_mesh_faces():
    p = mdb.models['Model-1'].parts['merged-shell']
    f = p.faces

    # Bottom left

    faces = f.findAt(((-flat_length/2., 0.0, depth/2.), ))
    pickedGeomSourceSide=regionToolset.Region(faces=faces)

    faces = f.findAt(((-flat_length/2., flat_width, depth/2.), ))
    pickedTargetFace = faces[0]

    ps = np.array([(-flat_length, 0., 0.),
                   (-flat_length, 0., depth),
                   (y_cut_bottom_func(0), 0., depth),
                   (y_cut_bottom_func(0), 0., 0.)])

    nodes = [None]*4

    for i in range(4):
        nodes[i] = AAA_findNodeAt(ps[i, :], p.nodes)[0]

    ps[:, 1] += flat_width
    nodes = tuple(nodes)

    coords = totuple(ps)

    p.copyMeshPattern(faces=pickedGeomSourceSide, targetFace=pickedTargetFace, nodes=nodes, coordinates=coords)

    # Bottom mid

    faces = f.findAt((((y_cut_bottom_func(0) + y_cut_top_func(0))/2., 0.0, 5.e-5), ))
    pickedGeomSourceSide=regionToolset.Region(faces=faces)

    faces = f.findAt((((y_cut_bottom_func(0) + y_cut_top_func(0))/2., flat_width, 5.e-5), ))
    pickedTargetFace = faces[0]

    ps = np.array([(y_cut_bottom_func(0), 0., depth),
                   (y_cut_top_func(0), 0., depth),
                   (y_cut_top_func(0), 0., 0.),
                   (y_cut_bottom_func(0), 0., 0.)])

    nodes = [None]*4

    for i in range(4):
        nodes[i] = AAA_findNodeAt(ps[i, :], p.nodes)[0]

    ps[:, 1] += flat_width
    nodes = tuple(nodes)

    coords = totuple(ps)

    p.copyMeshPattern(faces=pickedGeomSourceSide, targetFace=pickedTargetFace, nodes=nodes, coordinates=coords)

    # Bottom right

    faces = f.findAt(((flat_length*0.99, 0.0, depth/2.), ))
    pickedGeomSourceSide=regionToolset.Region(faces=faces)

    faces = f.findAt(((flat_length*0.99, flat_width, depth/2.), ))
    pickedTargetFace = faces[0]

    ps = np.array([(flat_length, 0., 0.),
                   (flat_length, 0., depth),
                   (y_cut_top_func(0), 0., depth),
                   (y_cut_top_func(0), 0., 0.)])

    nodes = [None]*4

    for i in range(4):
        nodes[i] = AAA_findNodeAt(ps[i, :], p.nodes)[0]

    ps[:, 1] += flat_width
    nodes = tuple(nodes)

    coords = totuple(ps)

    p.copyMeshPattern(faces=pickedGeomSourceSide, targetFace=pickedTargetFace, nodes=nodes, coordinates=coords)

    # back left

    faces = f.findAt(((-flat_length/2., flat_width/2., 0.0), ))
    pickedGeomSourceSide=regionToolset.Region(faces=faces)

    faces = f.findAt(((-flat_length/2., flat_width/2., depth), ))
    pickedTargetFace = faces[0]

    ps = np.array([(-flat_length, 0., 0.),
                   (-flat_length, flat_width, 0.),
                   (y_cut_bottom_func(0), flat_width, 0.),
                   (y_cut_bottom_func(0), 0., 0.)])

    nodes = [None]*4

    for i in range(4):
        nodes[i] = AAA_findNodeAt(ps[i, :], p.nodes)[0]

    ps[:, 2] += depth
    nodes = tuple(nodes)

    coords = totuple(ps)

    p.copyMeshPattern(faces=pickedGeomSourceSide, targetFace=pickedTargetFace, nodes=nodes, coordinates=coords)


    # Back mid

    faces = f.findAt((((y_cut_bottom_func(0) + y_cut_top_func(0))/2.,  5.e-5, 0.), ))
    pickedGeomSourceSide=regionToolset.Region(faces=faces)

    faces = f.findAt((((y_cut_bottom_func(0) + y_cut_top_func(0))/2., 5.e-5, depth), ))
    pickedTargetFace = faces[0]

    ps = np.array([(y_cut_bottom_func(0), 0., 0.),
                   (y_cut_top_func(0), 0., 0.),
                   (y_cut_top_func(0), flat_width, 0.),
                   (y_cut_bottom_func(0), flat_width, 0.)])

    nodes = [None]*4

    for i in range(4):
        nodes[i] = AAA_findNodeAt(ps[i, :], p.nodes)[0]

    ps[:, 2] += depth
    nodes = tuple(nodes)

    coords = totuple(ps)

    p.copyMeshPattern(faces=pickedGeomSourceSide, targetFace=pickedTargetFace, nodes=nodes, coordinates=coords)


    # Back right

    faces = f.findAt(((flat_length*0.99,  flat_width/2., 0.), ))
    pickedGeomSourceSide=regionToolset.Region(faces=faces)

    faces = f.findAt(((flat_length*0.99,  flat_width/2., depth), ))
    pickedTargetFace = faces[0]

    ps = np.array([(y_cut_top_func(0), 0., 0.),
                   (y_cut_top_func(0), flat_width, 0.),
                   (flat_length, flat_width, 0.),
                   (flat_length, 0., 0.)])

    nodes = [None]*4

    for i in range(4):
        nodes[i] = AAA_findNodeAt(ps[i, :], p.nodes)[0]

    ps[:, 2] += depth
    nodes = tuple(nodes)

    coords = totuple(ps)

    p.copyMeshPattern(faces=pickedGeomSourceSide, targetFace=pickedTargetFace, nodes=nodes, coordinates=coords)

    # Left

    faces = f.findAt(((-flat_length,  flat_width/2., depth/2.), ))
    pickedGeomSourceSide=regionToolset.Region(faces=faces)

    faces = f.findAt(((flat_length,  flat_width/2., depth/2.), ))
    pickedTargetFace = faces[0]

    ps = np.array([(-flat_length, flat_width, 0.),
                   (-flat_length, flat_width, depth),
                   (-flat_length, 0., depth),
                   (-flat_length, 0., 0.)])

    nodes = [None]*4

    for i in range(4):
        nodes[i] = AAA_findNodeAt(ps[i, :], p.nodes)[0]

    ps[:, 0] += 2*flat_length
    nodes = tuple(nodes)

    coords = totuple(ps)

    p.copyMeshPattern(faces=pickedGeomSourceSide, targetFace=pickedTargetFace, nodes=nodes, coordinates=coords)


def mesh_protrusion():
    import section
    import regionToolset
    import displayGroupMdbToolset as dgm
    import part
    import material
    import assembly
    import step
    import interaction
    import load
    import mesh
    import optimization
    import job
    import sketch
    import visualization
    import xyPlot
    import displayGroupOdbToolset as dgo
    import connectorBehavior
    p = mdb.models['Model-1'].parts['merged-shell']
    p.seedPart(size=25.0, deviationFactor=0.1, minSizeFactor=0.1)
    p = mdb.models['Model-1'].parts['merged-shell']
    p.seedPart(size=0.25, deviationFactor=0.1, minSizeFactor=0.1)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    pickedRegions = c.findAt(((-0.641988, 1.0, 0.019022), ), ((-0.948645, 1.0, 
        0.991519), ), ((-0.714579, 0.0, 0.004692), ))
    p.setMeshControls(regions=pickedRegions, elemShape=TET, technique=FREE)
    elemType1 = mesh.ElemType(elemCode=C3D20R)
    elemType2 = mesh.ElemType(elemCode=C3D15)
    elemType3 = mesh.ElemType(elemCode=C3D10)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((-0.641988, 1.0, 0.019022), ), ((-0.948645, 1.0, 0.991519), 
        ), ((-0.714579, 0.0, 0.004692), ))
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2, 
        elemType3))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.75326,
    #     farPlane=7.13519, width=3.76622, height=1.85372, cameraPosition=(
    #     0.483782, 5.32405, 3.93931), cameraUpVector=(-0.0536518, 0.281015,
    #     -0.958202), cameraTarget=(0.0261402, 0.613459, 0.560513))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.90409,
    #     farPlane=6.99226, width=3.88573, height=1.91254, cameraPosition=(
    #     -0.0436354, 6.18023, 2.26475), cameraUpVector=(-0.0219328, -0.0361553,
    #     -0.999105), cameraTarget=(0.0146831, 0.632058, 0.524137))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.8923,
    #     farPlane=7.00404, width=3.87639, height=1.90795, cameraPosition=(
    #     -0.0436354, 6.18023, 2.26475), cameraUpVector=(0.00426355, -0.0358033,
    #     -0.99935), cameraTarget=(0.0146831, 0.632058, 0.524137))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.92889,
    #     farPlane=6.98369, width=3.90539, height=1.92222, cameraPosition=(
    #     -0.0594418, 6.31579, -0.784874), cameraUpVector=(0.206087, -0.519649,
    #     -0.829152), cameraTarget=(0.0143295, 0.635091, 0.45591))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.62474,
    #     farPlane=7.27633, width=3.6644, height=1.8036, cameraPosition=(1.2636,
    #     5.56909, 3.34903), cameraUpVector=(0.187535, 0.113188, -0.975715),
    #     cameraTarget=(0.0456925, 0.61739, 0.553905))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.65072,
    #     farPlane=7.25036, width=3.68498, height=1.81373, cameraPosition=(
    #     1.2636, 5.56909, 3.34903), cameraUpVector=(-0.0280958, 0.171357,
    #     -0.984808), cameraTarget=(0.0456925, 0.61739, 0.553905))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.80402,
    #     farPlane=7.08767, width=3.80644, height=1.87351, cameraPosition=(
    #     -0.318353, 5.67242, 3.41516), cameraUpVector=(0.0772106, 0.17674,
    #     -0.981225), cameraTarget=(0.00968497, 0.619742, 0.55541))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.79092,
    #     farPlane=7.10076, width=3.79606, height=1.8684, cameraPosition=(
    #     -0.318353, 5.67242, 3.41516), cameraUpVector=(0.0173273, 0.17469,
    #     -0.984471), cameraTarget=(0.00968495, 0.619742, 0.55541))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.72618,
    #     farPlane=7.17742, width=3.74477, height=1.84316, cameraPosition=(
    #     -0.939355, 6.01735, -1.52502), cameraUpVector=(0.228941, -0.600241,
    #     -0.766353), cameraTarget=(-0.00397044, 0.627327, 0.446778))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.50068,
    #     farPlane=7.37594, width=3.5661, height=1.75522, cameraPosition=(
    #     -2.49063, 1.86032, 5.71633), cameraUpVector=(0.288931, 0.839774,
    #     -0.459672), cameraTarget=(-0.0396014, 0.531845, 0.613103))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.30696,
    #     farPlane=7.59649, width=3.41261, height=1.67968, cameraPosition=(
    #     5.60242, 0.715039, 2.49744), cameraUpVector=(-0.433713, 0.891803,
    #     0.128767), cameraTarget=(0.128336, 0.508079, 0.546308))
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.25643,
    #     farPlane=7.64308, width=3.37257, height=1.65997, cameraPosition=(
    #     4.85443, -2.47035, -1.23524), cameraUpVector=(0.259312, 0.92118,
    #     0.290145), cameraTarget=(0.111165, 0.434955, 0.46062))
    p = mdb.models['Model-1'].parts['merged-shell']
    e = p.edges
    pickedEdges = e.findAt(((-0.695001, 1.0, 0.0), ), ((-0.804999, 1.0, 1.0), ), ((
        -0.804999, 0.0, 0.0), ), ((-0.695001, 0.0, 1.0), ), ((-1.07, 1.0, 1.0), 
        ), ((-1.14, 0.25, 1.0), ), ((-0.929999, 0.0, 1.0), ), ((-1.14, 0.0, 
        0.25), ), ((-1.07, 0.0, 0.0), ), ((-1.14, 1.0, 0.25), ), ((-1.14, 0.75, 
        0.0), ), ((-0.929999, 1.0, 0.0), ), ((0.695, 1.0, 0.0), ), ((1.14, 
        0.75, 0.0), ), ((-0.195001, 0.0, 0.0), ), ((-0.195001, 1.0, 1.0), ), ((
        1.14, 1.0, 0.25), ), ((-0.195001, 0.0, 1.0), ), ((1.14, 0.25, 1.0), ), 
        ((1.14, 0.0, 0.25), ))
    p.seedEdgeBySize(edges=pickedEdges, size=0.25, deviationFactor=0.1, 
        minSizeFactor=0.1, constraint=FIXED)
    session.viewports['Viewport: 1'].view.setValues(nearPlane=4.23176, 
        farPlane=7.66774, width=3.70943, height=1.82577, viewOffsetX=0.121912, 
        viewOffsetY=0.00947845)
    elemType1 = mesh.ElemType(elemCode=C3D8R, elemLibrary=STANDARD)
    elemType2 = mesh.ElemType(elemCode=C3D6, elemLibrary=STANDARD)
    elemType3 = mesh.ElemType(elemCode=C3D4, elemLibrary=STANDARD, 
        secondOrderAccuracy=OFF, distortionControl=DEFAULT)
    p = mdb.models['Model-1'].parts['merged-shell']
    c = p.cells
    cells = c.findAt(((-0.641988, 1.0, 0.019022), ), ((-0.948645, 1.0, 0.991519), 
        ), ((-0.714579, 0.0, 0.004692), ))
    pickedRegions =(cells, )
    p.setElementType(regions=pickedRegions, elemTypes=(elemType1, elemType2, 
        elemType3))
    p = mdb.models['Model-1'].parts['merged-shell']
    p.generateMesh(boundaryPreview=ON)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.43463,
    #     farPlane=7.15677, width=3.88726, height=1.91329, cameraPosition=(
    #     -1.61995, -3.39598, 4.4804), cameraUpVector=(0.0274501, 0.914157,
    #     0.404429), cameraTarget=(-0.21153, 0.491205, 0.391204),
    #     viewOffsetX=0.127756, viewOffsetY=0.00993285)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.61574,
    #     farPlane=6.89385, width=4.04601, height=1.99143, cameraPosition=(
    #     0.221048, -1.46816, 5.90827), cameraUpVector=(-0.20964, 0.977778,
    #     0.00108495), cameraTarget=(-0.217418, 0.42634, 0.427941),
    #     viewOffsetX=0.132974, viewOffsetY=0.0103385)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.14267,
    #     farPlane=7.25978, width=3.63133, height=1.78733, cameraPosition=(
    #     2.40822, 2.9509, 5.05414), cameraUpVector=(-0.233128, 0.696848,
    #     -0.678273), cameraTarget=(-0.226581, 0.437852, 0.520067),
    #     viewOffsetX=0.119345, viewOffsetY=0.00927889)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.03099,
    #     farPlane=7.35464, width=3.53344, height=1.73915, cameraPosition=(
    #     3.79477, 2.28067, 4.35747), cameraUpVector=(-0.332788, 0.777485,
    #     -0.533637), cameraTarget=(-0.225046, 0.455421, 0.572632),
    #     viewOffsetX=0.116128, viewOffsetY=0.00902875)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.01021,
    #     farPlane=7.36207, width=3.51523, height=1.73019, cameraPosition=(
    #     3.77957, 3.16335, 3.81623), cameraUpVector=(-0.528032, 0.671782,
    #     -0.519511), cameraTarget=(-0.211934, 0.421549, 0.596624),
    #     viewOffsetX=0.115529, viewOffsetY=0.00898221)
    mdb.meshEditOptions.setValues(enableUndo=True, maxUndoCacheElements=0.5)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.22927,
    #     farPlane=7.19195, width=3.70726, height=1.8247, cameraPosition=(
    #     2.21105, -3.9237, 3.36267), cameraUpVector=(0.178687, 0.845612,
    #     0.503003), cameraTarget=(-0.216635, 0.552207, 0.554105),
    #     viewOffsetX=0.12184, viewOffsetY=0.00947287)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.16072,
    #     farPlane=7.27064, width=3.64717, height=1.79512, cameraPosition=(
    #     2.50641, 3.91275, 4.34478), cameraUpVector=(-0.447151, 0.555384,
    #     -0.701145), cameraTarget=(-0.211023, 0.43499, 0.558462),
    #     viewOffsetX=0.119865, viewOffsetY=0.00931932)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.65301,
    #     farPlane=6.8225, width=4.07869, height=2.00752, cameraPosition=(
    #     -0.122454, -2.18302, 5.5742), cameraUpVector=(0.113972, 0.982949,
    #     0.144294), cameraTarget=(-0.198375, 0.551091, 0.442499),
    #     viewOffsetX=0.134047, viewOffsetY=0.010422)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=4.55533,
    #     farPlane=6.91217, width=3.99307, height=1.96538, cameraPosition=(
    #     -0.0306865, -3.77719, 4.32355), cameraUpVector=(0.0345357, 0.876646,
    #     0.479894), cameraTarget=(-0.199672, 0.55939, 0.453023),
    #     viewOffsetX=0.131233, viewOffsetY=0.0102032)
    p = mdb.models['Model-1'].parts['merged-shell']
    f = p.faces
    faces = f.findAt(((-0.954839, 0.0, 0.009538), ))
    pickedGeomSourceSide=regionToolset.Region(faces=faces)
    p = mdb.models['Model-1'].parts['merged-shell']
    f = p.faces
    faces = f.findAt(((-0.948645, 1.0, 0.991519), ))
    pickedTargetFace = faces[0]
    p = mdb.models['Model-1'].parts['merged-shell']
    n1 = p.nodes
    nodes1 = n1[14:15]
    n2 = p.nodes
    nodes2 = n2[15:16]
    n3 = p.nodes
    nodes3 = n3[12:13]
    n4 = p.nodes
    nodes4 = n4[13:14]
    p.copyMeshPattern(faces=pickedGeomSourceSide, targetFace=pickedTargetFace, 
        nodes=(nodes1[0], nodes2[0], nodes3[0], nodes4[0], ), coordinates=((
        -1.14, 1.0, 1.0), (-0.859998294013634, 1.0, 1.0), (-0.859998289976156, 
        1.0, 0.0), (-1.14, 1.0, 0.0)))
    session.viewports['Viewport: 1'].view.setValues(nearPlane=4.36292, 
        farPlane=7.0936, width=3.82442, height=1.88237, cameraPosition=(
        1.03073, -3.5278, 4.44544), cameraUpVector=(0.0712509, 0.905018, 
        0.419364), cameraTarget=(-0.210516, 0.546359, 0.486152), 
        viewOffsetX=0.12569, viewOffsetY=0.00977224)


def AAA_findNodeAt(coord, nodes):

    coord = np.array(coord)
    for i in range(np.alen(nodes)):
        if np.linalg.norm(coord - np.array(nodes[i].coordinates)) < 1.e-5:
            return nodes[i: i+1]

    # p = mdb.models['Model-1'].parts['merged-shell']
    # n1 = p.nodes
    # print n1[0].coordinates

def totuple(a):
    try:
        return tuple(totuple(i) for i in a)
    except TypeError:
        return a