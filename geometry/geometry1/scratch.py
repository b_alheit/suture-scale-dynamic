import numpy as np
import matplotlib.pyplot as plt

flat_width = 2.8
depth = 1.5
t_h = 2.
t_w = 1.5
c_thick = 0.2
c_trans = 0.2
total_length = 2.5
ker_load_area_width = 0.25
apprx_radius = 300

coll_shell_thick = 0.03
ker_shell_thick = 0.05


coll_el_size = 0.18
coll_deviation_factor = 0.1
coll_min_size_factor = 0.1

ker_el_size = 0.2
ker_deviation_factor = 0.1
ker_min_size_factor = 0.1


mer_el_size = 0.2
mer_deviation_factor = 0.3
mer_min_size_factor = 0.3

# Part Names
bone_name = "bone"
collagen_block = "coll-block"

radius_to_flat_length_scale = 50
total_length_to_flat_length = 3
flat_length_teeth_length = 1.2
a = 2
n_points = 10000
n_path_pro = int(depth/t_w + 1)
n_points_per_protrusion = 31

x_path = np.linspace(-depth*0.025, 0, 5).tolist()[:-1] + np.linspace(0, t_w/4, int(n_points_per_protrusion/4)).tolist()
for i in range(n_path_pro):
    last_point = x_path[-1]
    x_path += np.linspace(last_point, last_point+t_w, n_points_per_protrusion).tolist()[1:]

x_path = np.array(x_path)
t_h /= 2.
# x_path = np.linspace(0, depth*1.05, n_points)
# x_path -= (x_path[-1] - depth)/2.
y_path = t_h/2. * np.sin(2.*np.pi * x_path / t_w)
print(x_path[np.argmax(y_path)])
print(x_path[np.argmin(y_path)])
plt.plot(x_path, y_path)
plt.show()