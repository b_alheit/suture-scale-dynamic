# -*- coding: mbcs -*-
# Do not delete the following import lines
from abaqus import *
from abaqusConstants import *
import __main__
import numpy as np
import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import optimization
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior

t_h = 1.5 / 2.
t_w = 1.
c_thick = 0.2
c_trans = 0.02

flat_width = t_w/2.
depth = t_w/2.

flat_length = 1.2*t_h + c_thick*1.2

# Part Names
bone_name = "bone"

a = 2
n_points = 150

# t_h = t_h / 2.
x_path = np.linspace(0, depth*1.05, n_points)
x_path -= (x_path[-1] - depth)/2.
y_path = t_h/2. * np.sin(2.*np.pi * x_path / t_w - np.pi/2 )

x_cut = np.linspace(0, flat_width*1.05, n_points)
x_cut -= (x_cut[-1] - flat_width)/2.
y_cut = t_h/2. * np.sin(2.*np.pi * x_cut / t_w - np.pi/2)

y_cut_func = lambda x: t_h/2. * np.sin(2.*np.pi * x / t_w - np.pi/2)

theta = lambda x: np.pi/2. - np.pi/4. * np.cos(2.*np.pi * x / t_w - np.pi/2)

y_cut_top_func = lambda x: y_cut_func(x) + c_thick/2 * np.sin(theta(x)) + c_trans/2. -  t_h/2.
y_cut_bottom_func = lambda x: y_cut_func(x) - c_thick/2 * np.sin(theta(x)) - c_trans/2. -  t_h/2.

y_cut_top = y_cut + c_thick/2 * np.sin(theta(x_cut)) + c_trans/2.
x_cut_top = x_cut - c_thick/2 * np.sign(np.cos(theta(x_cut))) * np.abs(np.cos(theta(x_cut))) ** a

y_cut_bottom = y_cut - c_thick/2 * np.sin(theta(x_cut)) - c_trans/2.
x_cut_bottom = x_cut + c_thick/2 * np.sign(np.cos(theta(x_cut))) * np.abs(np.cos(theta(x_cut))) ** a

y_cut_top -= t_h/2.
y_cut_bottom -= t_h/2.

slope = 15

def make_csys():

    # a = mdb.models['Model-1'].rootAssembly
    # a.DatumPointByCoordinate(coords=(1.14, 0.0, 1.5))
    # a = mdb.models['Model-1'].rootAssembly
    # a.DatumPointByCoordinate(coords=(1.2, -0.2, 1.5))
    # a = mdb.models['Model-1'].rootAssembly
    # a.DatumPointByCoordinate(coords=(1.2, 0.0, 1.5))
    # a = mdb.models['Model-1'].rootAssembly
    # a.features['Datum pt-2'].setValues(yValue=-0.02)
    # a = mdb.models['Model-1'].rootAssembly
    # a.regenerate()
    # a = mdb.models['Model-1'].rootAssembly
    # a.regenerate()
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=7.48017,
    #     farPlane=12.73, width=1.92441, height=0.944361, viewOffsetX=0.533636,
    #     viewOffsetY=-0.833281)
    # a = mdb.models['Model-1'].rootAssembly
    # d1 = a.datums
    # a.DatumCsysByThreePoints(origin=d1[32], point1=d1[33], point2=d1[34],
    #     name='xp', coordSysType=CARTESIAN)
    # session.viewports['Viewport: 1'].view.setValues(nearPlane=7.26539,
    #     farPlane=12.9448, width=4.03783, height=1.98147, viewOffsetX=-0.111249,
    #     viewOffsetY=-0.656281)
    a = mdb.models['Model-1'].rootAssembly
    a.DatumCsysByThreePoints(name='xp', coordSysType=CARTESIAN, origin=(flat_length, 0.0, 3.*t_w/2.),
                             point1=(flat_length + np.cos(np.deg2rad(slope)), -np.sin(np.deg2rad(slope)), 3.*t_w/2.),
                             point2=(flat_length + 1, 0.0, 3*t_w/2.))
    a.DatumCsysByThreePoints(name='xn', coordSysType=CARTESIAN, origin=(-flat_length, 0.0, 3.*t_w/2.),
                             point1=(-flat_length - np.cos(np.deg2rad(slope)), -np.sin(np.deg2rad(slope)), 3.*t_w/2.),
                             point2=(-flat_length - 1, 0.0, 3*t_w/2.))
    a.DatumCsysByThreePoints(name='zp', coordSysType=CARTESIAN, origin=(0., 0.0, 3.*t_w),
                             point1=(0., -np.sin(np.deg2rad(slope)), 3.*t_w + np.cos(np.deg2rad(slope))),
                             point2=(0., 0.0, 3*t_w+1))
    a.DatumCsysByThreePoints(name='zn', coordSysType=CARTESIAN, origin=(0., 0.0, 0.),
                             point1=(0., -np.sin(np.deg2rad(slope)), - np.cos(np.deg2rad(slope))),
                             point2=(0., 0.0, -1))



